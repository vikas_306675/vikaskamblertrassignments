/**************************************************************************************************************************
Program::Programmable Pipeline: Procedural Texture: CheckerBoard!		
***************************************************************************************************************************/
#include<windows.h>
#include<stdio.h> //for file i/o

#include<gl\glew.h> // for GLSL extensions.
					//This file should be included before below statement: #include<gl/gl.h>
#include<gl\GL.h>
#include "vmath.h"

//#include<gl/glu.h> //In Programmable pipeline, none of the functions under GLU work!

#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"glew32.lib") //The GLEW library.
#pragma comment(lib,"opengl32.lib")
//#pragma comment(lib,"glu32.lib")

#define WIN_WIDTH 1024
#define WIN_HEIGHT 768

#define checkerImageWidth 64
#define checkerImageHeight 64

using namespace vmath;
enum
{
	VDG_ATTRIBUTE_VERTEX = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

bool gbActiveWindow=false;
bool gbEscapeKeyIsPressed=false;
bool gbFullscreen=false;


DWORD dwStyle;
WINDOWPLACEMENT wpPrev= {sizeof(WINDOWPLACEMENT)};
HWND ghwnd;
HDC ghdc;
HGLRC ghrc; 
FILE *gpFile=NULL;


//Prototype of WndProc()
LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

//Shader and Shader Program Objects
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_Normal_Square;   //New Vao for "Normal Square" shape
GLuint gVbo_Normal_Square; //For Normal Square
GLuint gVbo_Normal_Square_Texture;

GLuint gVao_Tilted_Square;  //New Vao for "Tilted Square Geometry".
GLuint gVbo_Tilted_Square; //For Tilted Square
GLuint gVbo_Tilted_Square_Texture;

GLuint gMVPUniform;
GLuint gTexture_Sampler_Uniform;

GLubyte checkerImage[checkerImageHeight][checkerImageWidth][4]; //This array will actually hold the image data(Checkerboard image)
GLuint texName; //It will just hold the handle of the texture memory "handle"

mat4 gPerspectiveProjectionMatrix;

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR nCmdLine,int nCmdShow)
{
	//function prototypes
	void initialize(void);
	void display(void);
	void uninitialize(void);

	//Variables
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	bool bDone=false;
	TCHAR szAppName[]=TEXT("PP Checkerboard!!");

	if(fopen_s(&gpFile,"PP_Checkerboard_Log.txt","w")!=0)
	{
		MessageBox(NULL,TEXT("Log file could not be created..exiting!!"),TEXT("Error"),MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile,"Log File Created Successfully!!\n");
	}

	//code
	wndClass.cbSize=sizeof(WNDCLASSEX);
	wndClass.style=CS_HREDRAW | CS_VREDRAW | CS_OWNDC; //CS_OWNDC-->Non-purgable DC
	wndClass.cbClsExtra=0;
	wndClass.cbWndExtra=0;

	wndClass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hInstance=hInstance;
	wndClass.hIcon=LoadIcon(NULL,IDI_APPLICATION); //load the default icon
	wndClass.hIconSm=LoadIcon(NULL,IDI_APPLICATION); //Icon for "System menu"
	wndClass.hCursor=LoadCursor(NULL,IDC_ARROW);	 //Load default cursor, you can specific the cursor type while creating a game application (Confirm?)

	wndClass.lpfnWndProc=WndProc;
	wndClass.lpszClassName=szAppName;
	wndClass.lpszMenuName=NULL;

	//Register the created class
	RegisterClassEx(&wndClass);

	//create the window
	hwnd=CreateWindowEx(
						 WS_EX_APPWINDOW, //exclusive full-screen window
						 szAppName,
						 TEXT("PP::Checkerboard !!"),
						 WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS|WS_VISIBLE,
						 0,
						 0,
						 WIN_WIDTH,
						 WIN_HEIGHT,
						 NULL,		
						 NULL,
						 hInstance,
						 NULL
						);

	ghwnd=hwnd;	//For further use of the global window handle

	ShowWindow(hwnd,nCmdShow);//Show the created window
	SetForegroundWindow(hwnd);//Bring that window in foreground
	SetFocus(hwnd);//Set the focus on the window

	initialize(); //Call to initialize(Do the preparation of the "Picture element" (Pixel)

	//Game loop
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}//Events further generating the messages
		else
		{
			if(gbActiveWindow==true)
			{
				if(gbEscapeKeyIsPressed==true)
				{
					bDone=true;
				}
				display();
			}
			
		}//game will run here

	}//end of while
	uninitialize();
	return msg.wParam; 
}//WinMain

/*Initialize:
  1. PIXELFORMATDESCRIPTOR inialize
  2. ChoosePixelFormat
  3. SetPixelFormat
  4. Get the device context from OS
  5. Create OpenGL Context:wglCreateContext()
  6. HandOver control from OS DC to Rendering DC: wglMakeCurrent()
  7. Clear the window with desired color
  8. Give a warmup call to resize()

*/
void initialize(void)
{
	//function prototypes
	void resize(int, int);
	void LoadGLTextures();
	void uninitialize(void);

	//Variables
	PIXELFORMATDESCRIPTOR pfd;	//a structure which we need to fill will possible set of values from our side and rest can be achieved from OS
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));//Make the 26-membered structure to 0

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; //dword flags depicting pixel characteristics
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);//Get the regular device context from OS (this is non-purgeable)

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);//After getting the DC, "Choose the pixel format" which is the closest match to the values which we have provides

	if (iPixelFormatIndex == 0)//if the pixelformatindex is empty means function did not succeed!!
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false) //Set the chosen matching closest pixel format
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);//Now handover the device context to OpenGL's "Rendering context" through WGL

	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (wglMakeCurrent(ghdc, ghrc) == NULL)//Once the handover is successful-->Make "Rendering context" as the current context through WGL
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	/////////////////// The Place for glew //////////////////////////
	// (IMPORTANT: This piece should be here means, after creating the OpenGL context but before using any OpenGL Function).

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	fprintf(gpFile, "%s \n", glGetString(GL_VERSION)); //Get the OpenGL Version supported!

	fprintf(gpFile, "%s \n", glGetString(GL_SHADING_LANGUAGE_VERSION)); //Get the GLSL Version supported on this configuration!

	////////////////// VERTEX SHADER /////////////////////////////
	//Create Shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER); //glCreateShader()-->Specialist who writes the shader.

	//To Provide source code to shader, write it![Here you can specify the GLSL version supported by your graphics hardware and OS (Ex: "#version 440 core")]
	const GLchar *vertexShaderSourceCode = "#version 440 core"
		"\n"\
		"in vec4 vPosition;"\
		"in vec2 vTexture0_Coord;"\
		"out vec2 out_vTexture0_Coord;"\
		"uniform mat4 u_mvp_matrix;"\
		"void main(void)"\
		"{"\
		"gl_Position=u_mvp_matrix * vPosition;"\
		"out_vTexture0_Coord=vTexture0_Coord;"	//As it will go to "fragment shader"!!												
		"}";
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	//Compile the shader
	glCompileShader(gVertexShaderObject);

	GLint iShaderCompiledStatus = 0;
	GLint iInfoLogLength = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			} //szInfoLog!=NULL

		}//iInfoLogLength>0

	}//iShaderCompiledStatus==GL_FALSE

	////////////////// FRAGMENT SHADER///////////////////////
	//Create shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Provide source code to Shader[Here you can specify the GLSL version supported by your graphics hardware and OS (Ex: "#version 440 core")]
	const GLchar *fragmentShaderSourceCode = " #version 440 core"
		"\n"\
		"in vec2 out_vTexture0_Coord;"\
		"out vec4 FragColor;"\
		"uniform sampler2D u_texture0_sampler;"\
		"void main(void)"\
		"{"\
		"FragColor=texture(u_texture0_sampler,out_vTexture0_Coord);"\
		"}";
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	//Compile the shader
	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			} //szInfoLog!=NULL

		}//iInfoLogLength>0

	}//iShaderCompiledStatus==GL_FALSE

	//////////////////SHADER PROGRAM/////////////////////////
	//Create the Shader Program Object
	gShaderProgramObject = glCreateProgram();

	//Attach Vertex Shader to Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	//Attach fragment shader to Shader Program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	///////////////////////////////////////////////////////////////////////////////////////
	//Pre-link binding of "shader program object" with vertex shader "position attribute".
	///////////////////////////////////////////////////////////////////////////////////////
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition"); //Tikadam-2

	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");
	////////////////////////////
	//Link shader
	////////////////////////////
	glLinkProgram(gShaderProgramObject);

	GLint iShaderProgramLinkStatus = 0;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);

	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{

				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			} //szInfoLog!=NULL

		}//iInfoLogLength>0

	}//iShaderProgramLinkStatus==GL_FALSE

	 ////////////////////////////////////////////////////////////////////////////////////////////// 	
	 //get MVP uniform location
	 // gMVPUniform: ha aplya application madhla "uniform" type cha variable aahe jyala aapan "bharun"
	 //                pudhe shader la pathwnar aahot!
	 // gTexture0_Sampler_Uniform: Ha suddha aplya program madhala uniform cha variable aahe jyala aapan
	 //                             value deun shader kade pathwnar aahot!
	//  Khalchya statements madhe aapan tyanche ek mekanshi binding kele!
	 //////////////////////////////////////////////////////////////////////////////////////////////

	gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	gTexture_Sampler_Uniform = glGetUniformLocation(gShaderProgramObject, "u_texture0_sampler");

	////////////////////// Vertices, colors, shader attribs, vbo, vao initializations//////////////

	//************Square************************************************************************
	const GLfloat NormalSquareVertices[] =
	{
		0.0f,1.0f,0.0f, //Right top
	   -2.0f,1.0f,0.0f, //Left top
	   -2.0f,-1.0f,0.0f, //Left bottom
	   0.0f,-1.0f,0.0f   //Right bottom
	};

	const GLfloat TiltedSquareVertices[] =
	{
		2.41421f,1.0f,-1.4142f, //Right Top
		1.0f,1.0f,0.0f,			//Left Top
		1.0f,-1.0f,0.0f,		//Left Bottom
		2.41421f,-1.0f,-1.4142f //Right bottom
	};

	const GLfloat squareTexCoords[]=
	{
		1.0f,1.0f,	//Right Top
		0.0f,1.0f, //Left top
		0.0f,0.0f, //Left bottom
		1.0f,0.0f  //Right bottom
	};

	//*******************************************************************************************
	/////// vao for :Normal 2D Square //////////////////////////////////////////
	//*******************************************************************************************
	glGenVertexArrays(1,&gVao_Normal_Square);
	glBindVertexArray(gVao_Normal_Square);		//Binding of Vao for Square

	//***********vbo for Normal Square**************

	glGenBuffers(1, &gVbo_Normal_Square);
	glBindBuffer(GL_ARRAY_BUFFER,gVbo_Normal_Square);
	glBufferData(GL_ARRAY_BUFFER,sizeof(NormalSquareVertices),NormalSquareVertices,GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER,0);

	//********* vbo for Normal Square Texture********
	glGenBuffers(1,&gVbo_Normal_Square_Texture);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Normal_Square_Texture);
	glBufferData(GL_ARRAY_BUFFER, sizeof(squareTexCoords), squareTexCoords, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);				//Un-binding of Vao for Normal Square
	//*******************************************************************************************
	//////////////////////////////////////////Start: vao for Tilted Square/////////////////////////////
	//*******************************************************************************************
	glGenVertexArrays(1,&gVao_Tilted_Square);
	glBindVertexArray(gVao_Tilted_Square);
	
	//***********vbo for Tilted Square**************
	glGenBuffers(1,&gVbo_Tilted_Square);
	glBindBuffer(GL_ARRAY_BUFFER,gVbo_Tilted_Square);
	glBufferData(GL_ARRAY_BUFFER,sizeof(TiltedSquareVertices),TiltedSquareVertices,GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER,0);

	///**********vbo for Tilted Square Texture************
	glGenBuffers(1, &gVbo_Tilted_Square_Texture);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Tilted_Square_Texture);
	glBufferData(GL_ARRAY_BUFFER, sizeof(squareTexCoords), squareTexCoords, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);				//Un-binding of Vao for Tilted Square
	//*******************************************************************************************
	/////////////////Define the depth initialization/////////////////
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	//////////////////////////////// Call LoadGLTextures(): Your own function to load the Image Texture////
	
	LoadGLTextures();
	glEnable(GL_TEXTURE_2D); //Without enabling the texture, it will not display the texture!!!!

	/////////////////////////// Paint the window with initial color/////////////////////////////////
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //Now clear the window with desired color
	//////////////////////////////Set perspectiveProjectionMatrix to Identity Matrix./////////////////
	gPerspectiveProjectionMatrix = mat4::identity();

	///////////////////////////// Warmup call to resize()/////////////////////////////////////////////
	resize(WIN_WIDTH,WIN_HEIGHT); //a warmup call to resize function--In the case of "Projection" this call is of crucial importance as this call will help keep the viewing volume ready before drawing happens.
								  //Otherwise, in this case, application is launching in windowed mode with some weird output(as viewing volume will not be ready if this call is commentedout) and after pressing "F"(for fullscreen) desired output comes.	
								  // This warm up call to resize() goig to be crucial in case of DirectX
}//end of initialize()


void LoadGLTextures(void)
{
	//variable declarations
	void MakeCheckerImage(void);
	//code
	//Main Crux of this program is: Programmatically generating an image and texturing it on the geometry.
	//We are not taking "ready made available" image anywhere here in program.
	//so this call of the function is doing the generation of the image.

	MakeCheckerImage();

	//step:1) Generate the texture (GPU has created a special location inside VRAM and returned a number representing that location)
	glGenTextures(1,&texName); //Ek image mhanun '1' dilay aani second parameter is the base address of an array(which is currently single pointer variable)
	
	glBindTexture(GL_TEXTURE_2D, texName); //Bind texture

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); //S chya side ne repeat kar aani sampale ki "WRAP" kar. 

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); //T chya side ne repeat kar, sampale ki "WRAP" kar.

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);

	glTexImage2D(
					GL_TEXTURE_2D,  //Target image type
					0,				//Level of Depth
					GL_RGBA,			//Internal format of the image
					checkerImageWidth,	//Width of the image	
					checkerImageHeight,	//Height of the image
					0,				//border width
					GL_RGBA,			//Pixel format
					GL_UNSIGNED_BYTE,	//Image chya data cha type
					checkerImage	//Actual Image data
				);
	//Create MIPMAPs for this texture for better image quality
	glGenerateMipmap(GL_TEXTURE_2D);

	// Tuza mode set kar, kashawar karu? -->GL_TEXTURE_ENV_MODE
	// Additive "replace kar"-->GL_REPLACE
	// he je function aahe te waprale nahi tar black war pan black replace karel
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

}//LoadGLTextures()

void MakeCheckerImage(void)
{
	//code
	int i = 0, j = 0, c = 0;

	for (i = 0; i<checkerImageHeight; i++)
	{
		for (j = 0; j<checkerImageWidth; j++)
		{
			//c is getting created as constant 
			c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;

			checkerImage[i][j][0] = (GLubyte)c;// R= 0/255 [C will be either 0 or 255]
			checkerImage[i][j][1] = (GLubyte)c;// G= 0/255
			checkerImage[i][j][2] = (GLubyte)c;// B= 0/255
			checkerImage[i][j][3] = (GLubyte)255;// A=255

		}

	}

}//MakeCheckerImage()

//Resize function:
// Projection functions are conventionally called inside the function which deals with the change in the size of the window
void resize(int width,int height)
{
	if(height==0)
		height=1;
	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	//gluPerspective(fovy,Aspect Ration,near,far);
	// Deliberate changes in the "Fov angle" and "far" parameter.
	gPerspectiveProjectionMatrix = vmath::perspective(60.0f, (GLfloat)width / (GLfloat)height, 1.0f, 30.0f);
	
}//end of resize

//CALLBACK Function
LRESULT CALLBACK WndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam)
{
	//function declarations
	void resize(int,int);
	void uninitialize(void);
	void ToggleFullscreen(void);

	//Code
	switch(message)
	{

	case WM_CREATE:
					break;

	case WM_ACTIVATE:
					 if(HIWORD(wParam)==0)
						 gbActiveWindow=true;
					 else
						 gbActiveWindow=false;
					break;

	case WM_KEYDOWN:
					switch(wParam)
					{
					case VK_ESCAPE:
									gbEscapeKeyIsPressed=true;
									break;

						case 0x46: //for fullscreen
									if(gbFullscreen==false)
									{
										ToggleFullscreen();
										gbFullscreen=true;
									}
									else
									{
										ToggleFullscreen();
										gbFullscreen=false;
									}
									break;

						case 0x51: //Q
									uninitialize();
									PostQuitMessage(0);
									break;
						
					}
					break;

	case WM_SIZE:
					resize(LOWORD(lParam),HIWORD(lParam));
					break;

	case WM_DESTROY:
					PostQuitMessage(0);
					break;

	}
	return(DefWindowProc(hwnd,message,wParam,lParam));

}//End of call back function

/*ToggleFullscreen function
	1. Get the window style using GetWindowLong()
	2. If the "Current window" type contains the "WS_OVERLAPPEDWINDOW" -->Remove it.
	3. Store the current window position
	4. Get the monitor info(Primary display)
	5. Set the window position
*/
void ToggleFullscreen(void)
{
	//variable declaration
	MONITORINFO mi={sizeof(MONITORINFO)};
	bool IsWindowPlacement;
	bool IsMonitorInfo;
	HMONITOR hMonitor;

	//code
	if(gbFullscreen==false) //windowed to full-screen
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE); //Get the window style/entire information of the window
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			//a) Get the window Placement and Monitor information then and then toggle to fullscreen
			IsWindowPlacement=GetWindowPlacement(ghwnd,&wpPrev);

			//b) Now get the monitor first 
			hMonitor=MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY); //the primary monitor in case of extended view

			//c) Get and store the monitor information from the monitor which is been just received
			IsMonitorInfo=GetMonitorInfo(hMonitor,&mi);

			//
			if(IsWindowPlacement && IsMonitorInfo)//both information have been received, now set the window to the top of the screen
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~ WS_OVERLAPPEDWINDOW);

				//Set the window position
				
				SetWindowPos(
							  ghwnd,
							  HWND_TOP,
							  mi.rcMonitor.left,
							  mi.rcMonitor.top,
							  mi.rcMonitor.right-mi.rcMonitor.left, //width
							  mi.rcMonitor.bottom-mi.rcMonitor.top, //height
							  SWP_NOZORDER | SWP_FRAMECHANGED       //ignore any of the Z order and trigger frame change event
							);

			}
			ShowCursor(FALSE);
		}
	}
	else //full-screen to windowed mode
	{
		// Add the removed style to the window style
		// Set the window placement/position to back to the saved position
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd,&wpPrev);

		SetWindowPos(
						ghwnd,
						HWND_TOP,
						0,0,0,0,
						SWP_NOMOVE|
						SWP_NOSIZE|
						SWP_NOOWNERZORDER|
						SWP_NOZORDER|
						SWP_FRAMECHANGED
					);
		ShowCursor(TRUE);
	}
	
}//end of ToggleFullscreen()

//Display function
void display(void)
{
	//code
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	
	//Start using OpenGL Program Object
	glUseProgram(gShaderProgramObject);

	//OpenGL Drawing
	//set modelview & modelviewprojection matrices to identity

	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();

	//************* DRAW Normal and Tilted SQUARE **************************************************/
	modelViewMatrix = vmath::translate(0.0f,0.0f,-3.6f);////////////////////////// ModelViewProjection matrix cha uniform aadhi vertex shader kade gela////
														//Now Translate Square inside the screen to be visible(due to perspective projection set) side:
	
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix; //Multiply the modelViewMatrix by PerspectiveProjectionMatrix to generate modelViewProjectionMatrix.
	
	glUniformMatrix4fv(gMVPUniform,1,GL_FALSE,modelViewProjectionMatrix); //Pass above modelViewProjectionMatrix to "VERTEX SHADER" in "u_mvp_matrix" shader variable whose position 
																		// value we already calculated in "initWithFrame()" by using "glUniformLocation()".
	//*******************************************************************************************
	///////////////// Aactul draw with Normal Square texture //////////////////////////////
	//*******************************************************************************************
	glBindVertexArray(gVao_Normal_Square);    //Binding to Vao_square

	//3 Step binding of the texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texName); //glBindTexture(Target type, actual texture bind kelele)
	glUniform1i(gTexture_Sampler_Uniform,0);

	glDrawArrays(GL_TRIANGLE_FAN,0,4); //This is the way to draw QUAD in PP.

	glBindVertexArray(0);			   // Un-binding to Vao_Normal_Square	
	//***************************************************************************/
	///////////////// Aactul draw with Tilted Square texture //////////////////////////////
	//*******************************************************************************************
	glBindVertexArray(gVao_Tilted_Square);    //Binding to Vao_Tilted_Square
											  //3 Step binding of the texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texName); //glBindTexture(Target type, actual texture bind kelele)
	glUniform1i(gTexture_Sampler_Uniform, 0);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4); //This is the way to draw QUAD in PP.

	glBindVertexArray(0);			   // Un-binding to Vao_Normal_Square	
	//***************************************************************************/

	//Stop using OpenGL Program Object
	glUseProgram(0);
	
	SwapBuffers(ghdc);
	
}//end of display function

 //Uninitialize function
void uninitialize(void)
{	
	//if window is in fullscreen mode-->Switch back to windowed mode and then exit
	if(gbFullscreen==true)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);

		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd,&wpPrev);

		SetWindowPos(
					  ghwnd,
					  HWND_TOP,
					  0,0,0,0,
					  SWP_NOMOVE|
					  SWP_NOSIZE|
					  SWP_NOOWNERZORDER|
					  SWP_NOZORDER|
					  SWP_FRAMECHANGED
					);
		ShowCursor(TRUE);
	}

	if (gVao_Normal_Square)
	{
		glDeleteVertexArrays(1,&gVao_Normal_Square);
		gVao_Normal_Square = 0;
	}
	//destroy Vbo
	if (gVbo_Normal_Square)
	{
		glDeleteBuffers(1, &gVbo_Normal_Square);
		gVbo_Normal_Square = 0;

	}
	if (gVbo_Normal_Square_Texture)
	{
		glDeleteBuffers(1, &gVbo_Normal_Square_Texture);
		gVbo_Normal_Square_Texture = 0;
	}
	//Destroy Tilted Square Vao
	if (gVao_Tilted_Square)
	{
		glDeleteVertexArrays(1, &gVao_Tilted_Square);
		gVao_Tilted_Square = 0;
	}
	//destroy Vbo
	if (gVbo_Tilted_Square)
	{
		glDeleteBuffers(1, &gVbo_Tilted_Square);
		gVbo_Tilted_Square = 0;
	}
	if (gVbo_Tilted_Square_Texture)
	{
		glDeleteBuffers(1, &gVbo_Tilted_Square_Texture);
		gVbo_Tilted_Square_Texture = 0;
	}
	//detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject,gVertexShaderObject);
	
	//detach Fragment shader object from shader program object
	glDetachShader(gShaderProgramObject,gFragmentShaderObject);	
	
	//Delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject=0;
	
	//Delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject=0;
	
	//Now delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject=0;
	
	//unlink shader program
	glUseProgram(0);

	//Deselect the rendering context
	wglMakeCurrent(NULL,NULL);

	//Delete the rendering context
	wglDeleteContext(ghrc);
	ghrc=NULL;

	//Delete the device context
	ReleaseDC(ghwnd,ghdc);
	ghdc=NULL;

	if(gpFile)
	{
		fprintf(gpFile,"Log File Closed Successfully!!");
		fclose(gpFile);
		gpFile=NULL;
	}
	DestroyWindow(ghwnd);
}//end of uninitialize()