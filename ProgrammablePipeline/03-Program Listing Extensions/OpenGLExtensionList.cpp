/**************************************************************************************************************************

Program To List Extensions Supported by Graphics Card and OS!
			
***************************************************************************************************************************/
#include<windows.h>
#include<stdio.h> //for file i/o
#include<gl/glew.h> //This file should be included before below statement: #include<gl/gl.h>
#include<gl/GL.h>

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"glew32.lib") //The GLEW library.

DWORD dwStyle;
WINDOWPLACEMENT wpPrev= {sizeof(WINDOWPLACEMENT)};

HWND ghwnd;
HDC ghdc;
HGLRC ghrc; 

FILE *gpFile=NULL;


int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR nCmdLine,int nCmdShow)
{
	//function prototypes
	void initialize(void);
	void uninitialize(void);

	//Variables

	if(fopen_s(&gpFile,"OpenGLExtensionListLog.txt","w")!=0)
	{
		MessageBox(NULL,TEXT("Log file could not be created..exiting!!"),TEXT("Error"),MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile,"Log File Created Successfully!!\n");
	}

	initialize(); //Call to initialize(Do the preparation of the "Picture element" (Pixel)

	uninitialize();

}//WinMain

void initialize(void)
{

	//Variables
	PIXELFORMATDESCRIPTOR pfd;	//a structure which we need to fill will possible set of values from our side and rest can be achieved from OS
	int iPixelFormatIndex;
	//code
	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));//Make the 26-membered structure to 0

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags= PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; //dword flags depicting pixel characteristics
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cGreenBits=8;
	pfd.cBlueBits=8;
	pfd.cAlphaBits=8;
	pfd.cDepthBits = 32;

	ghdc=GetDC(ghwnd);//Get the regular device context from OS (this is non-purgeable)

	iPixelFormatIndex=ChoosePixelFormat(ghdc,&pfd);//After getting the DC, "Choose the pixel format" which is the closest match to the values which we have provides

	if(iPixelFormatIndex==0)//if the pixelformatindex is empty means function did not succeed!!
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd)==false) //Set the chosen matching closest pixel format
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	ghrc=wglCreateContext(ghdc);//Now handover the device context to OpenGL's "Rendering context" through WGL

	if(ghrc==NULL)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	if(wglMakeCurrent(ghdc,ghrc)==NULL)//Once the handover is successful-->Make "Rendering context" as the current context through WGL
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	/////////////////// The Place for glew //////////////////////////
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	////////////////// Code to Get the Extensions into File /////////

	GLint numberOfExtensions;
	glGetIntegerv(GL_NUM_EXTENSIONS, &numberOfExtensions); //Count of extensions supported by your graphics card + OS who has implemented those
														   // extensions.		
	fprintf(gpFile, "Number Of Extensions:%d\n", numberOfExtensions);

	for (int i = 0; i < numberOfExtensions; i++)
	{

		fprintf(gpFile,"%s \n",(const char*)glGetStringi(GL_EXTENSIONS, i));
		//printf("%s \n",(const char *)glGetStringi(GL_EXTENSIONS,i));// This will work when you will create a console application not the Win32 application

	}

}//end of initialize()
void uninitialize(void)
{	
	wglMakeCurrent(NULL,NULL);

	wglDeleteContext(ghrc);
	ghrc=NULL;

	ReleaseDC(ghwnd,ghdc);
	ghdc=NULL;

	if(gpFile)
	{
		fprintf(gpFile,"Log File Closed Successfully!!");
		fclose(gpFile);
		gpFile=NULL;
	}
}//end of uninitialize()