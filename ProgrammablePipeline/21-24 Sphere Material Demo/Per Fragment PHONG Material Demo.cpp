/**************************************************************************************************************************

Program::Programmable Pipeline: Lights:24 Sphere Material Demo illuminated using "Per Fragment PHONG Lighting Model"!!!

24 Sphere's with different material properties will be illuminated by:
								Red Light-Along X Axis Rotation with "X" key Press.
								Green Light-Along Y Axis Rotation with "Y" key Press.
								Blue Light-along Z Axis Rotation with "Z" key Press.

By default with "Lighting Enabled-->"Per Fragment PHONG Model" should be enabled.

***************************************************************************************************************************/
#include<windows.h>
#include<stdio.h> //for file i/o
#include<gl\glew.h> // for GLSL extensions.
					//This file should be included before below statement: #include<gl/gl.h>
#include<gl\GL.h>

#include "vmath.h"
#include "Sphere.h"

#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"glew32.lib") //The GLEW library.
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"Sphere.lib") //For Hardcoded Sphere function use using the DLL provided by sir. 

#define WIN_WIDTH 1024
#define WIN_HEIGHT 768

using namespace vmath;

enum
{
	VDG_ATTRIBUTE_VERTEX = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

bool gbActiveWindow=false;
bool gbEscapeKeyIsPressed=false;
bool gbFullscreen=false;
bool gbLight;
bool gbSpin;
bool gbIsLKeyPressed = false;
bool gbxKeyIsPressed = false;
bool gbyKeyIsPressed = false;
bool gbzKeyIsPressed = false;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev= {sizeof(WINDOWPLACEMENT)};
HWND ghwnd;
HDC ghdc;
HGLRC ghrc; 
FILE *gpFile=NULL;


//Prototype of WndProc()
LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

//Shader Program and Shader objects for "Per Vertex PHONG Shading Model"
GLuint gPerVertex_Fragment_PHONG_VertexShaderObject;
GLuint gPerVertex_Fragment_PHONG_FragmentShaderObject;
GLuint gShaderProgramObject;


GLuint gNumElements;
GLuint gNumVertices;
float sphere_vertices[1146]; //This is a hardcoded way we are drawing a sphere using DLL given by sir.
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gVao_sphere;			//Vao and Vbo for Sphere(Geometry)
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform; //Uniforms for Matrices and Key press to be passed to Vertex Shader.
GLuint L_KeyPressed_uniform;
GLuint F_KeyPressed_uniform; //To enabled "Per Fragment Phong lighting"

//Uniforms for "Light's Ambient,diffuse,specular" terms/values:RED
GLuint La_uniform;		
GLuint Ld_uniform;
GLuint Ls_uniform;
GLuint light_position_uniform;

//Uniforms for "Material's Ambient,diffuse,specular" values + shinniness.
GLuint Ka_uniform;		
GLuint Kd_uniform;
GLuint Ks_uniform;
GLuint material_shinniness_uniform;

mat4 gPerspectiveProjectionMatrix;

//Singular Light: White in Nature!
GLfloat light_ambient[] = {1.0f,1.0f,1.0f,0.0f}; //La
GLfloat light_diffuse[] = {1.0f,1.0f,1.0f,0.0f}; //Ld
GLfloat light_specular[] = {1.0f,1.0f,1.0f,0.0f}; //Ls
GLfloat light_position[] = {0.0f,0.0f,0.0f,0.0f};
GLfloat angleOfRotation = 0.0f;

/////////////////////////////////////////////////// GLOBAL ARRAY DECLARATION FOR MATERIALS OF 24 SPHERES////////////////////////

///////////////Emerald::Material For Sphere[1][1]////////////////////////////////////
GLfloat material_sphere_1_1_ambient[] = { 0.0215f,0.1745f,0.0215f,1.0f };
GLfloat material_sphere_1_1_diffuse[] = { 0.07568f,0.61424f,0.07568f,1.0f };
GLfloat material_sphere_1_1_specular[] = { 0.633f,0.727811f,0.633f,1.0f };
GLfloat material_sphere_1_1_shininess = 0.6*50.0f;

///////////////Jade:: Material For Sphere[2][1]////////////////////////////////////
GLfloat material_sphere_2_1_ambient[] = { 0.135f,0.2225f,0.1575f,1.0f };
GLfloat material_sphere_2_1_diffuse[] = { 0.54f,0.89f,0.63f,1.0f };
GLfloat material_sphere_2_1_specular[] = { 0.316228f,0.316228f,0.316228f,1.0f };
GLfloat material_sphere_2_1_shininess = 0.1*128.0f;

///////////////Obsidian::Material For Sphere[3][1]////////////////////////////////////
GLfloat material_sphere_3_1_ambient[] = { 0.05375f,0.05f,0.06625f,1.0f };
GLfloat material_sphere_3_1_diffuse[] = { 0.18275f,0.17f,0.22525f,1.0f };
GLfloat material_sphere_3_1_specular[] = { 0.332741f,0.328634f,0.346435f,1.0f };
GLfloat material_sphere_3_1_shininess = 0.3*128.0f;

///////////////Pearl::Material For Sphere[4][1]////////////////////////////////////
GLfloat material_sphere_4_1_ambient[] = { 0.25f,0.20725f,0.20725f,1.0f };
GLfloat material_sphere_4_1_diffuse[] = { 1.0f,0.829f,0.829f,1.0f };
GLfloat material_sphere_4_1_specular[] = { 0.296648f,0.296648f,0.296648f,1.0f };
GLfloat material_sphere_4_1_shininess = 0.088*128.0f;

///////////////Ruby::Material For Sphere[5][1]////////////////////////////////////
GLfloat material_sphere_5_1_ambient[] = { 0.1745f,0.01175f,0.01175f,1.0f };
GLfloat material_sphere_5_1_diffuse[] = { 0.61424f,0.04136f,0.04136f,1.0f };
GLfloat material_sphere_5_1_specular[] = { 0.0727811f,0.626959f,0.626959f,1.0f };
GLfloat material_sphere_5_1_shininess = 0.6*128.0f;

///////////////Turquoise::Material For Sphere[6][1]////////////////////////////////////
GLfloat material_sphere_6_1_ambient[] = { 0.1f,0.18725f,0.1745f,1.0f };
GLfloat material_sphere_6_1_diffuse[] = { 0.396f,0.74151f,0.69102f,1.0f };
GLfloat material_sphere_6_1_specular[] = { 0.297254f,0.30829f,0.306678f,1.0f };
GLfloat material_sphere_6_1_shininess = 0.1*128.0f;

///////////////Brass::Material For Sphere[1][2]////////////////////////////////////
GLfloat material_sphere_1_2_ambient[] = { 0.329412f,0.223529f,0.027451f,1.0f };
GLfloat material_sphere_1_2_diffuse[] = { 0.780392f,0.568627f,0.113725f,1.0f };
GLfloat material_sphere_1_2_specular[] = { 0.992157f,0.941176f,0.807843f,1.0f };
GLfloat material_sphere_1_2_shininess = 0.21794872f*128.0f;

///////////////Bronze::Material For Sphere[2][2]////////////////////////////////////
GLfloat material_sphere_2_2_ambient[] = { 0.2125f,0.1275f,0.054f,1.0f };
GLfloat material_sphere_2_2_diffuse[] = { 0.714f,0.4284f,0.18144f,1.0f };
GLfloat material_sphere_2_2_specular[] = { 0.393548f,0.271906f,0.166721f,1.0f };
GLfloat material_sphere_2_2_shininess = 0.2f*128.0f;

///////////////Chrome::Material For Sphere[3][2]////////////////////////////////////
GLfloat material_sphere_3_2_ambient[] = { 0.25f,0.25f,0.25f,1.0f };
GLfloat material_sphere_3_2_diffuse[] = { 0.4f,0.4f,0.4f,1.0f };
GLfloat material_sphere_3_2_specular[] = { 0.774597f,0.774597f,0.744597f,1.0f };
GLfloat material_sphere_3_2_shininess = 0.6f*128.0f;

///////////////Copper::Material For Sphere[4][2]////////////////////////////////////
GLfloat material_sphere_4_2_ambient[] = { 0.19125f,0.0735f,0.0225f,1.0f };
GLfloat material_sphere_4_2_diffuse[] = { 0.7038f,0.27048f,0.0828f,1.0f };
GLfloat material_sphere_4_2_specular[] = { 0.256777f,0.137622f,0.086014f,1.0f };
GLfloat material_sphere_4_2_shininess = 0.1f*128.0f;

///////////////Gold::Material For Sphere[5][2]////////////////////////////////////
GLfloat material_sphere_5_2_ambient[] = { 0.24725f,0.1995f,0.0745f,1.0f };
GLfloat material_sphere_5_2_diffuse[] = { 0.75164f,0.60048f,0.22648f,1.0f };
GLfloat material_sphere_5_2_specular[] = { 0.628281f,0.555802f,0.366065f,1.0f };
GLfloat material_sphere_5_2_shininess = 0.4f*128.0f;

///////////////Silver::Material For Sphere[6][2]////////////////////////////////////
GLfloat material_sphere_6_2_ambient[] = { 0.19225f,0.19225f,0.19225f,1.0f };
GLfloat material_sphere_6_2_diffuse[] = { 0.50754f,0.50754f,0.50754f,1.0f };
GLfloat material_sphere_6_2_specular[] = { 0.508273f,0.508273f,0.508273f,1.0f };
GLfloat material_sphere_6_2_shininess = 0.4f*128.0f;

///////////////Black::Material For Sphere[1][3]////////////////////////////////////
GLfloat material_sphere_1_3_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_sphere_1_3_diffuse[] = { 0.01f,0.01f,0.01f,1.0f };
GLfloat material_sphere_1_3_specular[] = { 0.50f,0.50f,0.50f,1.0f };
GLfloat material_sphere_1_3_shininess = 0.25f*128.0f;

///////////////Cyan::Material For Sphere[2][3]////////////////////////////////////
GLfloat material_sphere_2_3_ambient[] = { 0.0f,0.1f,0.06f,1.0f };
GLfloat material_sphere_2_3_diffuse[] = { 0.0f,0.50980392f,0.50980392f,1.0f };
GLfloat material_sphere_2_3_specular[] = { 0.50196078f,0.50106078f,0.50196078f,1.0f };
GLfloat material_sphere_2_3_shininess = 0.25f*128.0f;

///////////////Green::Material For Sphere[3][3]////////////////////////////////////
GLfloat material_sphere_3_3_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_sphere_3_3_diffuse[] = { 0.1f,0.35f,0.1f,1.0f };
GLfloat material_sphere_3_3_specular[] = { 0.45f,0.55f,0.45f,1.0f };
GLfloat material_sphere_3_3_shininess = 0.25f*128.0f;

///////////////Red::Material For Sphere[4][3]////////////////////////////////////
GLfloat material_sphere_4_3_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_sphere_4_3_diffuse[] = { 0.5f,0.0f,0.0f,1.0f };
GLfloat material_sphere_4_3_specular[] = { 0.7f,0.6f,0.6f,1.0f };
GLfloat material_sphere_4_3_shininess = 0.25f*128.0f;

///////////////White::Material For Sphere[5][3]////////////////////////////////////
GLfloat material_sphere_5_3_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_sphere_5_3_diffuse[] = { 0.55f,0.55f,0.55f,1.0f };
GLfloat material_sphere_5_3_specular[] = { 0.70f,0.70f,0.70f,1.0f };
GLfloat material_sphere_5_3_shininess = 0.25f*128.0f;

///////////////Yellow Plastic::Material For Sphere[6][3]////////////////////////////////////
GLfloat material_sphere_6_3_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_sphere_6_3_diffuse[] = { 0.5f,0.5f,0.0f,1.0f };
GLfloat material_sphere_6_3_specular[] = { 0.60f,0.60f,0.50f,1.0f };
GLfloat material_sphere_6_3_shininess = 0.25f*128.0f;

///////////////Black::Material For Sphere[1][4]////////////////////////////////////
GLfloat material_sphere_1_4_ambient[] = { 0.02f,0.02f,0.02f,1.0f };
GLfloat material_sphere_1_4_diffuse[] = { 0.01f,0.01f,0.01f,1.0f };
GLfloat material_sphere_1_4_specular[] = { 0.4f,0.4f,0.4f,1.0f };
GLfloat material_sphere_1_4_shininess = 0.078125f*128.0f;

///////////////Cyan::Material For Sphere[2][4]////////////////////////////////////
GLfloat material_sphere_2_4_ambient[] = { 0.0f,0.05f,0.05f,1.0f };
GLfloat material_sphere_2_4_diffuse[] = { 0.4f,0.5f,0.5f,1.0f };
GLfloat material_sphere_2_4_specular[] = { 0.04f,0.7f,0.7f,1.0f };
GLfloat material_sphere_2_4_shininess = 0.078125f*128.0f;

///////////////Green::Material For Sphere[3][4]////////////////////////////////////
GLfloat material_sphere_3_4_ambient[] = { 0.0f,0.05f,0.0f,1.0f };
GLfloat material_sphere_3_4_diffuse[] = { 0.4f,0.5f,0.4f,1.0f };
GLfloat material_sphere_3_4_specular[] = { 0.04f,0.7f,0.04f,1.0f };
GLfloat material_sphere_3_4_shininess = 0.078125f*128.0f;

///////////////Red::Material For Sphere[4][4]////////////////////////////////////
GLfloat material_sphere_4_4_ambient[] = { 0.05f,0.0f,0.0f,1.0f };
GLfloat material_sphere_4_4_diffuse[] = { 0.5f,0.4f,0.4f,1.0f };
GLfloat material_sphere_4_4_specular[] = { 0.7f,0.04f,0.04f,1.0f };
GLfloat material_sphere_4_4_shininess = 0.078125f*128.0f;

///////////////White::Material For Sphere[5][4]////////////////////////////////////
GLfloat material_sphere_5_4_ambient[] = { 0.05f,0.05f,0.05f,1.0f };
GLfloat material_sphere_5_4_diffuse[] = { 0.5f,0.5f,0.5f,1.0f };
GLfloat material_sphere_5_4_specular[] = { 0.7f,0.7f,0.7f,1.0f };
GLfloat material_sphere_5_4_shininess = 0.078125f*128.0f;

///////////////Yellow Rubber::Material For Sphere[6][4]////////////////////////////////////
GLfloat material_sphere_6_4_ambient[] = { 0.05f,0.05f,0.0f,1.0f };
GLfloat material_sphere_6_4_diffuse[] = { 0.5f,0.5f,0.4f,1.0f };
GLfloat material_sphere_6_4_specular[] = { 0.7f,0.7f,0.04f,1.0f };
GLfloat material_sphere_6_4_shininess = 0.078125f*128.0f;


int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR nCmdLine,int nCmdShow)
{
	//function prototypes
	void initialize(void);
	void spin(void);
	void display(void);
	void uninitialize(void);

	//Variables
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	bool bDone=false;
	TCHAR szAppName[]=TEXT("PP:Material Demo!");

	if(fopen_s(&gpFile,"PP_Material_Demo_Log.txt","w")!=0)
	{
		MessageBox(NULL,TEXT("Log file could not be created..exiting!!"),TEXT("Error"),MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile,"Log File Created Successfully!!\n");
	}

	//code
	wndClass.cbSize=sizeof(WNDCLASSEX);
	wndClass.style=CS_HREDRAW | CS_VREDRAW | CS_OWNDC; //CS_OWNDC-->Non-purgable DC
	wndClass.cbClsExtra=0;
	wndClass.cbWndExtra=0;

	wndClass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hInstance=hInstance;
	wndClass.hIcon=LoadIcon(NULL,IDI_APPLICATION); //load the default icon
	wndClass.hIconSm=LoadIcon(NULL,IDI_APPLICATION); //Icon for "System menu"
	wndClass.hCursor=LoadCursor(NULL,IDC_ARROW);	 //Load default cursor, you can specific the cursor type while creating a game application (Confirm?)

	wndClass.lpfnWndProc=WndProc;
	wndClass.lpszClassName=szAppName;
	wndClass.lpszMenuName=NULL;

	//Register the created class
	RegisterClassEx(&wndClass);

	//create the window
	hwnd=CreateWindowEx(
						 WS_EX_APPWINDOW, //exclusive full-screen window
						 szAppName,
						 TEXT("PP::MaterialDemo!!"),
						 WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS|WS_VISIBLE,
						 0,
						 0,
						 WIN_WIDTH,
						 WIN_HEIGHT,
						 NULL,		
						 NULL,
						 hInstance,
						 NULL
						);

	ghwnd=hwnd;	//For further use of the global window handle

	ShowWindow(hwnd,nCmdShow);//Show the created window
	SetForegroundWindow(hwnd);//Bring that window in foreground
	SetFocus(hwnd);//Set the focus on the window

	initialize(); //Call to initialize(Do the preparation of the "Picture element" (Pixel)

	//Game loop
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}//Events further generating the messages
		else
		{
			if(gbActiveWindow==true)
			{
				if(gbEscapeKeyIsPressed==true)
				{
					bDone=true;
				}
				if (gbSpin == true)
				{
					spin();
				}
				display();
			}
			
		}//game will run here

	}//end of while
	uninitialize();
	return msg.wParam; 
}//WinMain

/*Initialize:
  1. PIXELFORMATDESCRIPTOR inialize
  2. ChoosePixelFormat
  3. SetPixelFormat
  4. Get the device context from OS
  5. Create OpenGL Context:wglCreateContext()
  6. HandOver control from OS DC to Rendering DC: wglMakeCurrent()
  7. Clear the window with desired color
  8. Give a warmup call to resize()

*/
void initialize(void)
{
	//function prototypes
	void resize(int,int);
	void uninitialize(void);

	//Variables
	PIXELFORMATDESCRIPTOR pfd;	//a structure which we need to fill will possible set of values from our side and rest can be achieved from OS
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));//Make the 26-membered structure to 0

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags= PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; //dword flags depicting pixel characteristics
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cGreenBits=8;
	pfd.cBlueBits=8;
	pfd.cAlphaBits=8;
	pfd.cDepthBits = 32;

	ghdc=GetDC(ghwnd);//Get the regular device context from OS (this is non-purgeable)

	iPixelFormatIndex=ChoosePixelFormat(ghdc,&pfd);//After getting the DC, "Choose the pixel format" which is the closest match to the values which we have provides

	if(iPixelFormatIndex==0)//if the pixelformatindex is empty means function did not succeed!!
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd)==false) //Set the chosen matching closest pixel format
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	ghrc=wglCreateContext(ghdc);//Now handover the device context to OpenGL's "Rendering context" through WGL

	if(ghrc==NULL)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	if(wglMakeCurrent(ghdc,ghrc)==NULL)//Once the handover is successful-->Make "Rendering context" as the current context through WGL
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	/////////////////// The Place for glew //////////////////////////
	// (IMPORTANT: This piece should be here means, after creating the OpenGL context but before using any OpenGL Function).
	
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	fprintf(gpFile, "%s \n", glGetString(GL_VERSION)); //Get the OpenGL Version supported!

	fprintf(gpFile, "%s \n", glGetString(GL_SHADING_LANGUAGE_VERSION)); //Get the GLSL Version supported on this configuration!

	//******************Per Vertex PHONG Lighting Model******************************************/
	////////////////// PER VERTEX PHONG: VERTEX SHADER /////////////////////////////
	//Create Shader
	gPerVertex_Fragment_PHONG_VertexShaderObject=glCreateShader(GL_VERTEX_SHADER); //glCreateShader()-->Specialist who writes the shader.

	//To Provide source code to shader, write it![Here you can specify the GLSL version supported by your graphics hardware and OS (Ex: "#version 440 core")]
	//Lookout the notebook for calculating "tnorm internally"
	// normalize mhanje "1" and  "-1" madhe aanane-->Karan "perspective" projection"
	// tnorm: transformed normal

	//PHONG Light Model Equation: I=Ia + Id + Is
	// Ia= La * Ka;
	// Id= Ld * Kd *(s.n);
	// Is= Ls * Ks *(r.v)^f;
	// Where r=-L + 2.(L.N).N

	const GLchar *vertexShaderSourceCode = "#version 440 core"\
		"\n"\
		"in vec4 vPosition;"\
		"in vec3 vNormal;"\
		"uniform mat4 u_model_matrix;"\
		"uniform mat4 u_view_matrix;"\
		"uniform mat4 u_projection_matrix;"\
		"uniform int u_lighting_enabled;"\
		"uniform vec4 u_light_position;"\
		"out vec3 transformed_normals;"\
		"out vec3 light_direction;"\
		"out vec3 viewer_vector;"\
		"void main(void)"\
		"{"\
		"if(u_lighting_enabled==1)"\
		"{"\
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"\

		"transformed_normals=mat3(u_view_matrix * u_model_matrix)*vNormal;"\

		"light_direction=vec3(u_light_position)-eye_coordinates.xyz;"\

		"viewer_vector=-eye_coordinates.xyz;"\
		"}"\

		"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"\

		"}";

	glShaderSource(gPerVertex_Fragment_PHONG_VertexShaderObject,1, (const GLchar **)&vertexShaderSourceCode,NULL);
	
	//Compile the shader
	glCompileShader(gPerVertex_Fragment_PHONG_VertexShaderObject);

	GLint iShaderCompiledStatus = 0;
	GLint iInfoLogLength = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gPerVertex_Fragment_PHONG_VertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
	if (iShaderCompiledStatus==GL_FALSE)
	{
		glGetShaderiv(gPerVertex_Fragment_PHONG_VertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gPerVertex_Fragment_PHONG_VertexShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Vertex Shader Compilation Log: %s\n",szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			} //szInfoLog!=NULL

		}//iInfoLogLength>0

	}//iShaderCompiledStatus==GL_FALSE
	
	////////////////// PER VERTEX PHONG FRAGMENT SHADER///////////////////////
	//Create shader
	gPerVertex_Fragment_PHONG_FragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
	
	//Provide source code to Shader[Here you can specify the GLSL version supported by your graphics hardware and OS (Ex: "#version 440 core")]
	//name out_color should be same across shader to get pass the color from vertex to fragment shader
	
	//FRAGMENT SHADER: Will only accept the output of Vertex Shader(which is "phong_ads_color") and it will just display/render it.
	// Such model is named as "Gouraud Shading model/Per Vertex Lighting!!!".

	const GLchar *fragmentShaderSourceCode = " #version 440 core"\
		"\n"\
		"in vec3 transformed_normals;"\
		"in vec3 light_direction;"\
		"in vec3 viewer_vector;"\
		"out vec4 FragColor;"\
		"uniform vec3 u_La;"\
		"uniform vec3 u_Ld;"\
		"uniform vec3 u_Ls;"\
		"uniform vec3 u_Ka;"\
		"uniform vec3 u_Kd;"\
		"uniform vec3 u_Ks;"\
		"uniform float u_material_shininess;"\
		"uniform int u_lighting_enabled;"\
		"void main(void)"\
		"{"\
		"vec3 phong_ads_color;"\
		"if(u_lighting_enabled==1)"\
		"{"\

		"vec3 normalized_transformed_normals=normalize(transformed_normals);"\
		"vec3 normalized_light_direction=normalize(light_direction);"\
		"vec3 normalized_viewer_vector=normalize(viewer_vector);"\

		"vec3 ambient=u_La * u_Ka;"\
		
		"float tn_dot_ld=max(dot(normalized_transformed_normals,normalized_light_direction),0.0);"\
		
		"vec3 diffuse=u_Ld * u_Kd * tn_dot_ld;"\
		
		"vec3 reflection_vector=reflect(-normalized_light_direction,normalized_transformed_normals);"\
		
		"vec3 specular=u_Ls * u_Ks * pow(max(dot(reflection_vector,normalized_viewer_vector),0.0),u_material_shininess);"\
		
		"phong_ads_color=ambient + diffuse + specular;"\
		"}"\
		"else"\
		"{"\
		"phong_ads_color=vec3(1.0,1.0,1.0);"\
		"}"\

		"FragColor=vec4(phong_ads_color,1.0);"\
		
		"}";
											
	glShaderSource(gPerVertex_Fragment_PHONG_FragmentShaderObject,1,(const GLchar **)&fragmentShaderSourceCode,NULL);
																			   
	//Compile the shader
	glCompileShader(gPerVertex_Fragment_PHONG_FragmentShaderObject);																			   
	glGetShaderiv(gPerVertex_Fragment_PHONG_FragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gPerVertex_Fragment_PHONG_FragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog =(char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gPerVertex_Fragment_PHONG_FragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Fragment Shader Log: %s\n",szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			} //szInfoLog!=NULL

		}//iInfoLogLength>0

	}//iShaderCompiledStatus==GL_FALSE

	/**********************SHADER PROGRAM: PRE Link Binding!!**********************************/
	//Create the Shader Program Object
	gShaderProgramObject=glCreateProgram();
	
	//Attach Vertex Shader to Shader Program
	glAttachShader(gShaderProgramObject,gPerVertex_Fragment_PHONG_VertexShaderObject);
	
	//Attach fragment shader to Shader Program
	glAttachShader(gShaderProgramObject,gPerVertex_Fragment_PHONG_FragmentShaderObject);
	
	/**********************Pre-link binding of "shader program object" with vertex shader position attribute****************/

	glBindAttribLocation(gShaderProgramObject,VDG_ATTRIBUTE_VERTEX, "vPosition"); //Tikadam-2

	glBindAttribLocation(gShaderProgramObject,VDG_ATTRIBUTE_NORMAL,"vNormal");
																				  
	/************Link shader*************/
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;

	glGetProgramiv(gShaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);

	if (iShaderProgramLinkStatus==GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{

				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			} //szInfoLog!=NULL

		}//iInfoLogLength>0

	}//iShaderProgramLinkStatus==GL_FALSE

	/******************* Provide the Uniforms/Get the Uniform locations***************************************************/
	model_matrix_uniform = glGetUniformLocation(gShaderProgramObject,"u_model_matrix");

	view_matrix_uniform = glGetUniformLocation(gShaderProgramObject,"u_view_matrix");

	projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject,"u_projection_matrix");

	// 'L' Key is pressed on not?
	L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");

	/////////// Light:RED 
	//ambient color intensity of light
	La_uniform = glGetUniformLocation(gShaderProgramObject, "u_La");
	//diffuse color intensity of light
	Ld_uniform= glGetUniformLocation(gShaderProgramObject, "u_Ld");
	//Specular color intensity of light
	Ls_uniform= glGetUniformLocation(gShaderProgramObject, "u_Ls");	
	//position of light
	light_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");

	///////// Material
	//ambient reflective color intensity of material
	Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");

	//diffuse reflective color intensity of material
	Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");

	//specular reflective color internsity of material
	Ks_uniform = glGetUniformLocation(gShaderProgramObject,"u_Ks");

	//Shininess of material(This value is conventionally between 1 to 200)
	material_shinniness_uniform = glGetUniformLocation(gShaderProgramObject,"u_material_shininess");

	/**********************************Vertices, colors, shader attribs, vbo, vao initializations******************************/
	getSphereVertexData(sphere_vertices,sphere_normals,sphere_textures,sphere_elements);

	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	/********* Binding of Vao for Sphere *******/
	glGenVertexArrays(1,&gVao_sphere);
	glBindVertexArray(gVao_sphere);		

	/////////Vbo Sphere Position///////////
	glGenBuffers(1,&gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER,gVbo_sphere_position); //binding of square_vbo_position
	glBufferData(GL_ARRAY_BUFFER,sizeof(sphere_vertices),sphere_vertices,GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER,0);            //un-binding of square_vbo_position
	////////////////////////////////
	
	////// For Normal Attribute for Sphere//////
	// ----------------------------------------------------------------------
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER,sizeof(sphere_normals),sphere_normals,GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL,3,GL_FLOAT,GL_FALSE,0,NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL); //No need to Give(If this line is given...Square does not appear!!).

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	/////////////////////////////////////////////

	/////// For Element Vbo for Sphere///////////
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ARRAY_BUFFER,gVbo_sphere_element);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	/////////////////////////////////////////////
	glBindVertexArray(0);				//Un-binding of Vao for Square
	//*******************************************************************************************
	/////////////////Define the depth initialization/////////////////
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST); //This need not be specified hence forth in PP for windows(Reason is: Android does not support it, so "jun te son")
	//We will always cull back faces for better performance
	glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //Now clear the window with desired color

	//Set perspectiveProjectionMatrix to Identity Matrix.
	gPerspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH,WIN_HEIGHT); //a warmup call to resize function--In the case of "Projection" this call is of crucial importance as this call will help keep the viewing volume ready before drawing happens.
								  //Otherwise, in this case, application is launching in windowed mode with some weird output(as viewing volume will not be ready if this call is commentedout) and after pressing "F"(for fullscreen) desired output comes.	
								  // This warm up call to resize() goig to be crucial in case of DirectX
}//end of initialize()

//Resize function:
// Projection functions are conventionally called inside the function which deals with the change in the size of the window
void resize(int width,int height)
{
	if(height==0)
		height=1;
	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	//gluPerspective(fovy,Aspect Ration,near,far);
	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
	
}//end of resize

//CALLBACK Function
LRESULT CALLBACK WndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam)
{
	//function declarations
	void resize(int,int);
	void uninitialize(void);
	void ToggleFullscreen(void);

	//Code
	switch(message)
	{

	case WM_CREATE:
					break;

	case WM_ACTIVATE:
					 if(HIWORD(wParam)==0)
						 gbActiveWindow=true;
					 else
						 gbActiveWindow=false;
					break;

	case WM_KEYDOWN:
					switch(wParam)
					{
					case VK_ESCAPE:
									gbEscapeKeyIsPressed=true;
									break;

						case 0x46: //'f' or 'F' for fullscreen
									if(gbFullscreen==false)
									{
										ToggleFullscreen();
										gbFullscreen=true;
									}
									else
									{
										ToggleFullscreen();
										gbFullscreen=false;
									}
									break;
						
						case 0x4C:	//for 'L' or 'l'
									if (gbIsLKeyPressed == false)
									{
										gbLight = true;
										gbIsLKeyPressed = true;
									}
									else
									{
										gbLight = false;
										gbSpin = false;
										gbIsLKeyPressed = false;	
									}
									break;

						case 0x58: // 'X' Key press: X axis rotation of light
									angleOfRotation = 0.0f;
									
									gbxKeyIsPressed = true;
									gbyKeyIsPressed = false;
									gbzKeyIsPressed = false;

									gbSpin = true; //Start spinning the light only if 'x' key pressed.
									break;

						case 0x59: //'Y' Key press: Y-axis rotation of light				
									angleOfRotation = 0.0f;
									
									gbxKeyIsPressed = false;
									gbyKeyIsPressed = true;
									gbzKeyIsPressed = false;
									
									gbSpin = true; //Start spinning of light only if 'y' key pressed.

									break;

						case 0x5A: // 'Z' Key Press: Z-axis rotation of light
									angleOfRotation = 0.0f;
									
									gbxKeyIsPressed = false;
									gbyKeyIsPressed = false;
									gbzKeyIsPressed = true;

									gbSpin = true; //Start spinning of light only if 'z' key pressed.

									break;

						case 0x51: //Q
									uninitialize();
									PostQuitMessage(0);
									break;
					}
					break;

	case WM_SIZE:
					resize(LOWORD(lParam),HIWORD(lParam));
					break;

	case WM_DESTROY:
					PostQuitMessage(0);
					break;

	}
	return(DefWindowProc(hwnd,message,wParam,lParam));

}//End of call back function

/*ToggleFullscreen function
	1. Get the window style using GetWindowLong()
	2. If the "Current window" type contains the "WS_OVERLAPPEDWINDOW" -->Remove it.
	3. Store the current window position
	4. Get the monitor info(Primary display)
	5. Set the window position
*/
void ToggleFullscreen(void)
{
	//variable declaration
	MONITORINFO mi={sizeof(MONITORINFO)};
	bool IsWindowPlacement;
	bool IsMonitorInfo;
	HMONITOR hMonitor;

	//code
	if(gbFullscreen==false) //windowed to full-screen
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE); //Get the window style/entire information of the window
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			//a) Get the window Placement and Monitor information then and then toggle to fullscreen
			IsWindowPlacement=GetWindowPlacement(ghwnd,&wpPrev);

			//b) Now get the monitor first 
			hMonitor=MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY); //the primary monitor in case of extended view

			//c) Get and store the monitor information from the monitor which is been just received
			IsMonitorInfo=GetMonitorInfo(hMonitor,&mi);

			//
			if(IsWindowPlacement && IsMonitorInfo)//both information have been received, now set the window to the top of the screen
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~ WS_OVERLAPPEDWINDOW);

				//Set the window position
				
				SetWindowPos(
							  ghwnd,
							  HWND_TOP,
							  mi.rcMonitor.left,
							  mi.rcMonitor.top,
							  mi.rcMonitor.right-mi.rcMonitor.left, //width
							  mi.rcMonitor.bottom-mi.rcMonitor.top, //height
							  SWP_NOZORDER | SWP_FRAMECHANGED       //ignore any of the Z order and trigger frame change event
							);

			}
			ShowCursor(FALSE);
		}
	}
	else //full-screen to windowed mode
	{
		// Add the removed style to the window style
		// Set the window placement/position to back to the saved position
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd,&wpPrev);

		SetWindowPos(
						ghwnd,
						HWND_TOP,
						0,0,0,0,
						SWP_NOMOVE|
						SWP_NOSIZE|
						SWP_NOOWNERZORDER|
						SWP_NOZORDER|
						SWP_FRAMECHANGED
					);
		ShowCursor(TRUE);
	}
	
}//end of ToggleFullscreen()

void spin(void)
{
	angleOfRotation = angleOfRotation + 0.05f;

	if (angleOfRotation > 360.0f)
		angleOfRotation = 0.0f;

}//spin()


 //Display function
void display(void)
{
	//code
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	
	//Start using OpenGL Program Object
	glUseProgram(gShaderProgramObject);

	if (gbLight == true)
	{
	
		fprintf(gpFile, "Display()::Lighting Enabled!\n");

		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform,1);

		// Setting light's properties
		glUniform3fv(La_uniform, 1, light_ambient);
		glUniform3fv(Ld_uniform, 1, light_diffuse);
		glUniform3fv(Ls_uniform, 1, light_specular);
		
		if (gbxKeyIsPressed == true)
		{
			fprintf(gpFile, "Display()::X Key Rotation Of Light!\n");

			light_position[0] = 0.0f;
			light_position[1] = 10 * sin(angleOfRotation);
			light_position[2] = 10 * cos(angleOfRotation);
			light_position[3] = 0.0f;

			fprintf(gpFile, "angleOfRotation=%f\n y=%f \t z=%f ", angleOfRotation, light_position[1], light_position[2]);
		}
		else if (gbyKeyIsPressed == true)
		{
			fprintf(gpFile, "Display()::Y Key Rotation of Light!\n");

			light_position[0] = 10 * sin(angleOfRotation);
			light_position[1] = 0.0f;
			light_position[2] = 10 * cos(angleOfRotation);
			light_position[3] = 0.0f;

			fprintf(gpFile, "angleOfRotation=%f\n x=%f \t z=%f ", angleOfRotation, light_position[0], light_position[2]);
		}
		else if (gbzKeyIsPressed == true)
		{
			fprintf(gpFile, "Display()::Z Key Rotation of light!\n");


			light_position[0] = 10 * sin(angleOfRotation);
			light_position[1] = 10 * cos(angleOfRotation);
			light_position[2] = 0.0f;
			light_position[3] = 0.0f;

			fprintf(gpFile, "angleOfRotation=%f\n x=%f \t z=%f ", angleOfRotation, light_position[0], light_position[1]);
		}
		else
		{
			fprintf(gpFile, "Display()::Lighting With Default Position Values!\n");

			light_position[0] = 100.0f;
			light_position[1] = 100.0f;
			light_position[2] = 100.0f;
			light_position[3] = 1.0f;
		}
	
		glUniform4fv(light_position_uniform, 1, light_position);
		
		// Setting Materials properties
		glUniform3fv(Ka_uniform, 1, material_sphere_1_1_ambient);
		glUniform3fv(Kd_uniform, 1, material_sphere_1_1_diffuse);
		glUniform3fv(Ks_uniform, 1, material_sphere_1_1_specular);
		glUniform1f(material_shinniness_uniform, material_sphere_1_1_shininess);
	}
	else
	{
		fprintf(gpFile, "Display()::Lighting Not Enabled!\n");

		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform,0);
	}

	//*****************************************************************************/
	//OpenGL Drawing-Sphere-1-->[1][1]
	//******************************************************************************/

	//set model and view matrices to identity
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();

	// Apply 'z' axis translation to go deep into the screen by -2.0,
	// so that sphere will same fullscreen coordinates, but due to translation
	// it will look small.
	modelMatrix = translate(-4.0f, 2.7f, -8.0f);

	// Pass this "modelMatrix" to "Vertex Shader" in "u_model_matrix" shader variable!
    // whose position/location we already calculated in initialize() by using glGetUniformLocation()
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	//Pass "viewMatrix" to "Vertex Shader" in "u_view_matrix" uniform!
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);

	// Pass projection matrix also to the "vertex shader" in "u_projection_matrix" shader variable
	// whose position/location we have already calculated in initialize() by using glGetUniformLocation()
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere**************************************/
	glBindVertexArray(gVao_sphere);    

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES,gNumElements,GL_UNSIGNED_SHORT,0);

	glBindVertexArray(0);// Un-binding to Vao_Square
	
	//***************************************************************************
	//OpenGL Drawing-Sphere-2-->[2][1]
	//***************************************************************************	
	//set model and view matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	modelMatrix = translate(-4.0f, 1.6f, -8.0f);

	// Setting Materials properties
	glUniform3fv(Ka_uniform, 1, material_sphere_2_1_ambient);
	glUniform3fv(Kd_uniform, 1, material_sphere_2_1_diffuse);
	glUniform3fv(Ks_uniform, 1, material_sphere_2_1_specular);
	glUniform1f(material_shinniness_uniform, material_sphere_2_1_shininess);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere**************************************/
	glBindVertexArray(gVao_sphere);

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);// Un-binding to Vao_Square	

   //***************************************************************************		
   //OpenGL Drawing-Sphere-3-->[3][1]
   //***************************************************************************	
   //set model and view matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	modelMatrix = translate(-4.0f, 0.5f, -8.0f);

	// Setting Materials properties
	glUniform3fv(Ka_uniform, 1, material_sphere_3_1_ambient);
	glUniform3fv(Kd_uniform, 1, material_sphere_3_1_diffuse);
	glUniform3fv(Ks_uniform, 1, material_sphere_3_1_specular);
	glUniform1f(material_shinniness_uniform, material_sphere_3_1_shininess);


	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere**************************************/
	glBindVertexArray(gVao_sphere); //Binding to Vao_sphere**

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);// Un-binding to Vao_Square	
   //***************************************************************************			
   //OpenGL Drawing-Sphere-4-->[4][1]
   //***************************************************************************
   //set model and view matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	modelMatrix = translate(-4.0f, -0.6f, -8.0f);

	// Setting Materials properties
	glUniform3fv(Ka_uniform, 1, material_sphere_4_1_ambient);
	glUniform3fv(Kd_uniform, 1, material_sphere_4_1_diffuse);
	glUniform3fv(Ks_uniform, 1, material_sphere_4_1_specular);
	glUniform1f(material_shinniness_uniform, material_sphere_4_1_shininess);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere**************************************/
	glBindVertexArray(gVao_sphere);

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);// Un-binding to Vao_Square	
	//***************************************************************************			
	//OpenGL Drawing-Sphere-5-->[5][1]
	//***************************************************************************	
	//set model and view matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	modelMatrix = translate(-4.0f, -1.7f, -8.0f);

	// Setting Materials properties
	glUniform3fv(Ka_uniform, 1, material_sphere_5_1_ambient);
	glUniform3fv(Kd_uniform, 1, material_sphere_5_1_diffuse);
	glUniform3fv(Ks_uniform, 1, material_sphere_5_1_specular);
	glUniform1f(material_shinniness_uniform, material_sphere_5_1_shininess);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere**************************************/
	glBindVertexArray(gVao_sphere);

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);// Un-binding to Vao_Square	
	//***************************************************************************	
	//OpenGL Drawing-Sphere-6-->[6][1]
	//***************************************************************************	
	//set model and view matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	modelMatrix = translate(-4.0f, -2.8f, -8.0f);

	// Setting Materials properties
	glUniform3fv(Ka_uniform, 1, material_sphere_6_1_ambient);
	glUniform3fv(Kd_uniform, 1, material_sphere_6_1_diffuse);
	glUniform3fv(Ks_uniform, 1, material_sphere_6_1_specular);
	glUniform1f(material_shinniness_uniform, material_sphere_6_1_shininess);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere**************************************/
	glBindVertexArray(gVao_sphere);

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);// Un-binding to Vao_Square	
   //***************************************************************************	
   //OpenGL Drawing-Sphere-7-->[1][2]
   //***************************************************************************	
   //set model and view matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	modelMatrix = translate(-1.5f, 2.7f, -8.0f);

	// Setting Materials properties
	glUniform3fv(Ka_uniform, 1, material_sphere_2_1_ambient);
	glUniform3fv(Kd_uniform, 1, material_sphere_2_1_diffuse);
	glUniform3fv(Ks_uniform, 1, material_sphere_2_1_specular);
	glUniform1f(material_shinniness_uniform, material_sphere_2_1_shininess);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere**************************************/
	glBindVertexArray(gVao_sphere);

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);// Un-binding to Vao_Square	
	//***************************************************************************	
	//OpenGL Drawing-Sphere-8-->[2][2]
	//***************************************************************************	
	//set model and view matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	modelMatrix = translate(-1.5f, 1.6f, -8.0f);

	// Setting Materials properties
	glUniform3fv(Ka_uniform, 1, material_sphere_2_2_ambient);
	glUniform3fv(Kd_uniform, 1, material_sphere_2_2_diffuse);
	glUniform3fv(Ks_uniform, 1, material_sphere_2_2_specular);
	glUniform1f(material_shinniness_uniform, material_sphere_2_2_shininess);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere**************************************/
	glBindVertexArray(gVao_sphere);

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);// Un-binding to Vao_Square	
   //***************************************************************************	
   //OpenGL Drawing-Sphere-9-->[3][2]
   //***************************************************************************	
   //set model and view matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	modelMatrix = translate(-1.5f, 0.5f, -8.0f);

	// Setting Materials properties
	glUniform3fv(Ka_uniform, 1, material_sphere_3_2_ambient);
	glUniform3fv(Kd_uniform, 1, material_sphere_3_2_diffuse);
	glUniform3fv(Ks_uniform, 1, material_sphere_3_2_specular);
	glUniform1f(material_shinniness_uniform, material_sphere_3_2_shininess);


	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere**************************************/
	glBindVertexArray(gVao_sphere);

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);// Un-binding to Vao_Square	
	//***************************************************************************	
	//OpenGL Drawing-Sphere-10-->[4][2]
	//***************************************************************************	
	//set model and view matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	modelMatrix = translate(-1.5f, -0.6f, -8.0f);

	// Setting Materials properties
	glUniform3fv(Ka_uniform, 1, material_sphere_4_2_ambient);
	glUniform3fv(Kd_uniform, 1, material_sphere_4_2_diffuse);
	glUniform3fv(Ks_uniform, 1, material_sphere_4_2_specular);
	glUniform1f(material_shinniness_uniform, material_sphere_4_2_shininess);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere**************************************/
	glBindVertexArray(gVao_sphere);

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);// Un-binding to Vao_Square	
	//***************************************************************************	
	//OpenGL Drawing-Sphere-11-->[5][2]
	//***************************************************************************	
	//set model and view matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	modelMatrix = translate(-1.5f, -1.7f, -8.0f);

	// Setting Materials properties
	glUniform3fv(Ka_uniform, 1, material_sphere_5_2_ambient);
	glUniform3fv(Kd_uniform, 1, material_sphere_5_2_diffuse);
	glUniform3fv(Ks_uniform, 1, material_sphere_5_2_specular);
	glUniform1f(material_shinniness_uniform, material_sphere_5_2_shininess);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere**************************************/
	glBindVertexArray(gVao_sphere);

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);// Un-binding to Vao_Square	
	//***************************************************************************	
	//OpenGL Drawing-Sphere-12-->[6][2]
	//***************************************************************************	
	//set model and view matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	modelMatrix = translate(-1.5f, -2.8f, -8.0f);

	// Setting Materials properties
	glUniform3fv(Ka_uniform, 1, material_sphere_6_2_ambient);
	glUniform3fv(Kd_uniform, 1, material_sphere_6_2_diffuse);
	glUniform3fv(Ks_uniform, 1, material_sphere_6_2_specular);
	glUniform1f(material_shinniness_uniform, material_sphere_6_2_shininess);


	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere**************************************/
	glBindVertexArray(gVao_sphere);

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);// Un-binding to Vao_Square	
	//***************************************************************************	
	//OpenGL Drawing-Sphere-13-->[1][3]
	//***************************************************************************	
	//set model and view matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	modelMatrix = translate(1.0f, 2.7f, -8.0f);

	// Setting Materials properties
	glUniform3fv(Ka_uniform, 1, material_sphere_1_3_ambient);
	glUniform3fv(Kd_uniform, 1, material_sphere_1_3_diffuse);
	glUniform3fv(Ks_uniform, 1, material_sphere_1_3_specular);
	glUniform1f(material_shinniness_uniform, material_sphere_1_3_shininess);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere**************************************/
	glBindVertexArray(gVao_sphere);

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);// Un-binding to Vao_Square	
   //***************************************************************************	
   //OpenGL Drawing-Sphere-14-->[2][3]
   //***************************************************************************	
   //set model and view matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	modelMatrix = translate(1.0f, 1.6f, -8.0f);

	// Setting Materials properties
	glUniform3fv(Ka_uniform, 1, material_sphere_2_3_ambient);
	glUniform3fv(Kd_uniform, 1, material_sphere_2_3_diffuse);
	glUniform3fv(Ks_uniform, 1, material_sphere_2_3_specular);
	glUniform1f(material_shinniness_uniform, material_sphere_2_3_shininess);


	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere*************************************/
	glBindVertexArray(gVao_sphere);

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);// Un-binding to Vao_Square	
   //***************************************************************************	
   //OpenGL Drawing-Sphere-15-->[3][3]
   //***************************************************************************	
   //set model and view matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	modelMatrix = translate(1.0f, 0.5f, -8.0f);

	// Setting Materials properties
	glUniform3fv(Ka_uniform, 1, material_sphere_3_3_ambient);
	glUniform3fv(Kd_uniform, 1, material_sphere_3_3_diffuse);
	glUniform3fv(Ks_uniform, 1, material_sphere_3_3_specular);
	glUniform1f(material_shinniness_uniform, material_sphere_3_3_shininess);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere**************************************/
	glBindVertexArray(gVao_sphere);

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);// Un-binding to Vao_Square	
	//***************************************************************************	
	//OpenGL Drawing-Sphere-16-->[4][3]
	//***************************************************************************	
	//set model and view matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	modelMatrix = translate(1.0f, -0.6f, -8.0f);

	// Setting Materials properties
	glUniform3fv(Ka_uniform, 1, material_sphere_4_3_ambient);
	glUniform3fv(Kd_uniform, 1, material_sphere_4_3_diffuse);
	glUniform3fv(Ks_uniform, 1, material_sphere_4_3_specular);
	glUniform1f(material_shinniness_uniform, material_sphere_4_3_shininess);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere**************************************/
	glBindVertexArray(gVao_sphere);

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);// Un-binding to Vao_Square	
	//***************************************************************************/	
	//OpenGL Drawing-Sphere-17-->[5][3]
	//***************************************************************************	
	//set model and view matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	modelMatrix = translate(1.0f, -1.7f, -8.0f);

	// Setting Materials properties
	glUniform3fv(Ka_uniform, 1, material_sphere_5_3_ambient);
	glUniform3fv(Kd_uniform, 1, material_sphere_5_3_diffuse);
	glUniform3fv(Ks_uniform, 1, material_sphere_5_3_specular);
	glUniform1f(material_shinniness_uniform, material_sphere_5_3_shininess);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere**************************************/
	glBindVertexArray(gVao_sphere);

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);// Un-binding to Vao_Square	
	//***************************************************************************	
	//OpenGL Drawing-Sphere-18-->[6][3]
	//***************************************************************************	
	//set model and view matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	modelMatrix = translate(1.0f, -2.8f, -8.0f);

	// Setting Materials properties
	glUniform3fv(Ka_uniform, 1, material_sphere_6_3_ambient);
	glUniform3fv(Kd_uniform, 1, material_sphere_6_3_diffuse);
	glUniform3fv(Ks_uniform, 1, material_sphere_6_3_specular);
	glUniform1f(material_shinniness_uniform, material_sphere_6_3_shininess);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere**************************************/
	glBindVertexArray(gVao_sphere);

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);// Un-binding to Vao_Square	
	//***************************************************************************/	
	//OpenGL Drawing-Sphere-19-->[1][4]
	//***************************************************************************	
	//set model and view matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	modelMatrix = translate(3.5f, 2.7f, -8.0f);

	// Setting Materials properties
	glUniform3fv(Ka_uniform, 1, material_sphere_1_4_ambient);
	glUniform3fv(Kd_uniform, 1, material_sphere_1_4_diffuse);
	glUniform3fv(Ks_uniform, 1, material_sphere_1_4_specular);
	glUniform1f(material_shinniness_uniform, material_sphere_1_4_shininess);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere**************************************/
	glBindVertexArray(gVao_sphere);

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);// Un-binding to Vao_Square	
	//***************************************************************************	
	//OpenGL Drawing-Sphere-20-->[2][4]
	//***************************************************************************	
	//set model and view matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	modelMatrix = translate(3.5f, 1.6f, -8.0f);

	// Setting Materials properties
	glUniform3fv(Ka_uniform, 1, material_sphere_2_4_ambient);
	glUniform3fv(Kd_uniform, 1, material_sphere_2_4_diffuse);
	glUniform3fv(Ks_uniform, 1, material_sphere_2_4_specular);
	glUniform1f(material_shinniness_uniform, material_sphere_2_4_shininess);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere**************************************/
	glBindVertexArray(gVao_sphere);

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);// Un-binding to Vao_Square	
	//***************************************************************************	
	//OpenGL Drawing-Sphere-21-->[3][4]
	//***************************************************************************	
	//set model and view matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	modelMatrix = translate(3.5f, 0.5f, -8.0f);

	// Setting Materials properties
	glUniform3fv(Ka_uniform, 1, material_sphere_3_4_ambient);
	glUniform3fv(Kd_uniform, 1, material_sphere_3_4_diffuse);
	glUniform3fv(Ks_uniform, 1, material_sphere_3_4_specular);
	glUniform1f(material_shinniness_uniform, material_sphere_3_4_shininess);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere**************************************/
	glBindVertexArray(gVao_sphere);

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);// Un-binding to Vao_Square	
	//***************************************************************************	
	//OpenGL Drawing-Sphere-22-->[4][4]
	//***************************************************************************	
	//set model and view matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	modelMatrix = translate(3.5f, -0.6f, -8.0f);

	// Setting Materials properties
	glUniform3fv(Ka_uniform, 1, material_sphere_4_4_ambient);
	glUniform3fv(Kd_uniform, 1, material_sphere_4_4_diffuse);
	glUniform3fv(Ks_uniform, 1, material_sphere_4_4_specular);
	glUniform1f(material_shinniness_uniform, material_sphere_4_4_shininess);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere**************************************/
	glBindVertexArray(gVao_sphere);

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);// Un-binding to Vao_Square	
	//***************************************************************************	
	//OpenGL Drawing-Sphere-23-->[5][4]
	//***************************************************************************	
	//set model and view matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	modelMatrix = translate(3.5f, -1.7f, -8.0f);

	// Setting Materials properties
	glUniform3fv(Ka_uniform, 1, material_sphere_5_4_ambient);
	glUniform3fv(Kd_uniform, 1, material_sphere_5_4_diffuse);
	glUniform3fv(Ks_uniform, 1, material_sphere_5_4_specular);
	glUniform1f(material_shinniness_uniform, material_sphere_5_4_shininess);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere**************************************/
	glBindVertexArray(gVao_sphere);

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);// Un-binding to Vao_Square	
	//***************************************************************************	
	//OpenGL Drawing-Sphere-24-->[6][4]
	//***************************************************************************	
	//set model and view matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	modelMatrix = translate(3.5f, -2.8f, -8.0f);

	// Setting Materials properties
	glUniform3fv(Ka_uniform, 1, material_sphere_6_4_ambient);
	glUniform3fv(Kd_uniform, 1, material_sphere_6_4_diffuse);
	glUniform3fv(Ks_uniform, 1, material_sphere_6_4_specular);
	glUniform1f(material_shinniness_uniform, material_sphere_6_4_shininess);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere**************************************/
	glBindVertexArray(gVao_sphere);

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);// Un-binding to Vao_Square	
	//***************************************************************************/	

	//Stop using OpenGL Program Object
	glUseProgram(0);
	
	SwapBuffers(ghdc);
	
}//end of display function

 //Uninitialize function
void uninitialize(void)
{	
	//if window is in fullscreen mode-->Switch back to windowed mode and then exit
	if(gbFullscreen==true)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);

		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd,&wpPrev);

		SetWindowPos(
					  ghwnd,
					  HWND_TOP,
					  0,0,0,0,
					  SWP_NOMOVE|
					  SWP_NOSIZE|
					  SWP_NOOWNERZORDER|
					  SWP_NOZORDER|
					  SWP_FRAMECHANGED
					);
		ShowCursor(TRUE);
	}
	//destroy vao
	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

	//destroy position Vbo
	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1,&gVbo_sphere_position);
		gVbo_sphere_position = 0;

	}
	//destroy normal vbo
	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;

	}
	//destroy element vbo
	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}

	//detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject,gPerVertex_Fragment_PHONG_VertexShaderObject);
	
	//detach Fragment shader object from shader program object
	glDetachShader(gShaderProgramObject,gPerVertex_Fragment_PHONG_FragmentShaderObject);	
	
	//Delete vertex shader object
	glDeleteShader(gPerVertex_Fragment_PHONG_VertexShaderObject);
	gPerVertex_Fragment_PHONG_VertexShaderObject=0;
	
	//Delete fragment shader object
	glDeleteShader(gPerVertex_Fragment_PHONG_FragmentShaderObject);
	gPerVertex_Fragment_PHONG_FragmentShaderObject=0;
	
	//Now delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject=0;
	
	//unlink shader program
	glUseProgram(0);

	//Deselect the rendering context
	wglMakeCurrent(NULL,NULL);

	//Delete the rendering context
	wglDeleteContext(ghrc);
	ghrc=NULL;

	//Delete the device context
	ReleaseDC(ghwnd,ghdc);
	ghdc=NULL;

	if(gpFile)
	{
		fprintf(gpFile,"Log File Closed Successfully!!");
		fclose(gpFile);
		gpFile=NULL;
	}
	DestroyWindow(ghwnd);
}//end of uninitialize()