/**************************************************************************************************************************

Program::Programmable Pipeline: Lights:Per Vertex and Fragment PHONG Lighting Model!!!

A Sphere will be illuminated by:
								Red Light-Along X Axis Rotation
								Green Light-Along Y Axis Rotation
								Blue Light-along Z Axis Rotation.

By default with "Lighting Enabled-->"Vertex PHONG Model" should be enabled.

***************************************************************************************************************************/
#include<windows.h>
#include<stdio.h> //for file i/o
#include<gl\glew.h> // for GLSL extensions.
					//This file should be included before below statement: #include<gl/gl.h>
#include<gl\GL.h>

#include "vmath.h"
#include "Sphere.h"

#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"glew32.lib") //The GLEW library.
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"Sphere.lib") //For Hardcoded Sphere function use using the DLL provided by sir. 

#define WIN_WIDTH 1024
#define WIN_HEIGHT 768

using namespace vmath;

enum
{
	VDG_ATTRIBUTE_VERTEX = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

bool gbActiveWindow=false;
bool gbEscapeKeyIsPressed=false;
bool gbFullscreen=false;
bool gbLight;
bool gbSpin;
bool gbIsFKeyPressed = false;
bool gbPerFragmentPhongEnabled;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev= {sizeof(WINDOWPLACEMENT)};
HWND ghwnd;
HDC ghdc;
HGLRC ghrc; 
FILE *gpFile=NULL;


//Prototype of WndProc()
LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

//Shader Program and Shader objects for "Per Vertex PHONG Shading Model"
GLuint gPerVertex_Fragment_PHONG_VertexShaderObject;
GLuint gPerVertex_Fragment_PHONG_FragmentShaderObject;
GLuint gShaderProgramObject;


GLuint gNumElements;
GLuint gNumVertices;
float sphere_vertices[1146]; //This is a hardcoded way we are drawing a sphere using DLL given by sir.
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gVao_sphere;			//Vao and Vbo for Sphere(Geometry)
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform; //Uniforms for Matrices and Key press to be passed to Vertex Shader.
GLuint L_KeyPressed_uniform;
GLuint F_KeyPressed_uniform; //To enabled "Per Fragment Phong lighting"

//Uniforms for "Light's Ambient,diffuse,specular" terms/values:RED
GLuint red_La_uniform;		
GLuint red_Ld_uniform;
GLuint red_Ls_uniform;
GLuint red_light_position_uniform;

//Uniforms for "Light's Ambient,diffuse,specular" terms/values:GREEN
GLuint green_La_uniform;		
GLuint green_Ld_uniform;
GLuint green_Ls_uniform;
GLuint green_light_position_uniform;

//Uniforms for "Light's Ambient,diffuse,specular" terms/values:BLUE
GLuint blue_La_uniform;		
GLuint blue_Ld_uniform;
GLuint blue_Ls_uniform;
GLuint blue_light_position_uniform;

//Uniforms for "Material's Ambient,diffuse,specular" values + shinniness.
GLuint Ka_uniform;		
GLuint Kd_uniform;
GLuint Ks_uniform;
GLuint material_shinniness_uniform;

mat4 gPerspectiveProjectionMatrix;

//Red Light
GLfloat RedLightAmbient[] = {0.0f,0.0f,0.0f,0.0f}; //La
GLfloat RedLightDiffuse[] = {1.0f,0.0f,0.0f,0.0f}; //Ld
GLfloat RedLightSpecular[] = {1.0f,0.0f,0.0f,0.0f}; //Ls
GLfloat RedLightPosition[] = {0.0f,0.0f,0.0f,0.0f};
GLfloat angleRed = 0.0f;

//Green Light
GLfloat GreenLightAmbient[] = { 0.0f,0.0f,0.0f,0.0f }; //La
GLfloat GreenLightDiffuse[] = { 0.0f,1.0f,0.0f,0.0f }; //Ld
GLfloat GreenLightSpecular[] = { 0.0f,1.0f,0.0f,0.0f }; //Ls
GLfloat GreenLightPosition[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat angleGreen = 0.0f;

//Blue Light
GLfloat BlueLightAmbient[] = { 0.0f,0.0f,0.0f,0.0f }; //La
GLfloat BlueLightDiffuse[] = { 0.0f,0.0f,1.0f,0.0f }; //Ld
GLfloat BlueLightSpecular[] = { 0.0f,0.0f,1.0f,0.0f }; //Ls
GLfloat BlueLightPosition[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat angleBlue = 0.0f;

GLfloat material_ambient[] = {0.0f,0.0f,0.0f,1.0f}; //Ka
GLfloat material_diffuse[] = {1.0f,1.0f,1.0f,1.0f};//Kd
GLfloat material_specular[] = {1.0f,1.0f,1.0f,1.0f}; //Ks
GLfloat material_shinniness = 50.0f;


int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR nCmdLine,int nCmdShow)
{
	//function prototypes
	void initialize(void);
	void spin(void);
	void display(void);
	void uninitialize(void);

	//Variables
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	bool bDone=false;
	TCHAR szAppName[]=TEXT("PP:Per_Vertex_Fragment_PHONG_Moving_Light!!");

	if(fopen_s(&gpFile,"PP_Per_Vertex_Fragment_PHONG_Moving_Light_Log.txt","w")!=0)
	{
		MessageBox(NULL,TEXT("Log file could not be created..exiting!!"),TEXT("Error"),MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile,"Log File Created Successfully!!\n");
	}

	//code
	wndClass.cbSize=sizeof(WNDCLASSEX);
	wndClass.style=CS_HREDRAW | CS_VREDRAW | CS_OWNDC; //CS_OWNDC-->Non-purgable DC
	wndClass.cbClsExtra=0;
	wndClass.cbWndExtra=0;

	wndClass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hInstance=hInstance;
	wndClass.hIcon=LoadIcon(NULL,IDI_APPLICATION); //load the default icon
	wndClass.hIconSm=LoadIcon(NULL,IDI_APPLICATION); //Icon for "System menu"
	wndClass.hCursor=LoadCursor(NULL,IDC_ARROW);	 //Load default cursor, you can specific the cursor type while creating a game application (Confirm?)

	wndClass.lpfnWndProc=WndProc;
	wndClass.lpszClassName=szAppName;
	wndClass.lpszMenuName=NULL;

	//Register the created class
	RegisterClassEx(&wndClass);

	//create the window
	hwnd=CreateWindowEx(
						 WS_EX_APPWINDOW, //exclusive full-screen window
						 szAppName,
						 TEXT("PP::Per Vertex Fragment PHONG Moving LIGHT !!"),
						 WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS|WS_VISIBLE,
						 0,
						 0,
						 WIN_WIDTH,
						 WIN_HEIGHT,
						 NULL,		
						 NULL,
						 hInstance,
						 NULL
						);

	ghwnd=hwnd;	//For further use of the global window handle

	ShowWindow(hwnd,nCmdShow);//Show the created window
	SetForegroundWindow(hwnd);//Bring that window in foreground
	SetFocus(hwnd);//Set the focus on the window

	initialize(); //Call to initialize(Do the preparation of the "Picture element" (Pixel)

	//Game loop
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}//Events further generating the messages
		else
		{
			if(gbActiveWindow==true)
			{
				if(gbEscapeKeyIsPressed==true)
				{
					bDone=true;
				}
				if (gbSpin == true)
				{
					spin();
				}
				display();
			}
			
		}//game will run here

	}//end of while
	uninitialize();
	return msg.wParam; 
}//WinMain

/*Initialize:
  1. PIXELFORMATDESCRIPTOR inialize
  2. ChoosePixelFormat
  3. SetPixelFormat
  4. Get the device context from OS
  5. Create OpenGL Context:wglCreateContext()
  6. HandOver control from OS DC to Rendering DC: wglMakeCurrent()
  7. Clear the window with desired color
  8. Give a warmup call to resize()

*/
void initialize(void)
{
	//function prototypes
	void resize(int,int);
	void uninitialize(void);

	//Variables
	PIXELFORMATDESCRIPTOR pfd;	//a structure which we need to fill will possible set of values from our side and rest can be achieved from OS
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));//Make the 26-membered structure to 0

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags= PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; //dword flags depicting pixel characteristics
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cGreenBits=8;
	pfd.cBlueBits=8;
	pfd.cAlphaBits=8;
	pfd.cDepthBits = 32;

	ghdc=GetDC(ghwnd);//Get the regular device context from OS (this is non-purgeable)

	iPixelFormatIndex=ChoosePixelFormat(ghdc,&pfd);//After getting the DC, "Choose the pixel format" which is the closest match to the values which we have provides

	if(iPixelFormatIndex==0)//if the pixelformatindex is empty means function did not succeed!!
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd)==false) //Set the chosen matching closest pixel format
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	ghrc=wglCreateContext(ghdc);//Now handover the device context to OpenGL's "Rendering context" through WGL

	if(ghrc==NULL)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	if(wglMakeCurrent(ghdc,ghrc)==NULL)//Once the handover is successful-->Make "Rendering context" as the current context through WGL
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	/////////////////// The Place for glew //////////////////////////
	// (IMPORTANT: This piece should be here means, after creating the OpenGL context but before using any OpenGL Function).
	
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	fprintf(gpFile, "%s \n", glGetString(GL_VERSION)); //Get the OpenGL Version supported!

	fprintf(gpFile, "%s \n", glGetString(GL_SHADING_LANGUAGE_VERSION)); //Get the GLSL Version supported on this configuration!

	//******************Per Vertex PHONG Lighting Model******************************************/
	////////////////// PER VERTEX PHONG: VERTEX SHADER /////////////////////////////
	//Create Shader
	gPerVertex_Fragment_PHONG_VertexShaderObject=glCreateShader(GL_VERTEX_SHADER); //glCreateShader()-->Specialist who writes the shader.

	//To Provide source code to shader, write it![Here you can specify the GLSL version supported by your graphics hardware and OS (Ex: "#version 440 core")]
	//Lookout the notebook for calculating "tnorm internally"
	// normalize mhanje "1" and  "-1" madhe aanane-->Karan "perspective" projection"
	// tnorm: transformed normal

	//PHONG Light Model Equation: I=Ia + Id + Is
	// Ia= La * Ka;
	// Id= Ld * Kd *(s.n);
	// Is= Ls * Ks *(r.v)^f;
	// Where r=-L + 2.(L.N).N

	const GLchar *vertexShaderSourceCode = "#version 440 core"\
		"\n"\
		"in vec4 vPosition;"\
		"in vec3 vNormal;"\
		"uniform mat4 u_model_matrix;"\
		"uniform mat4 u_view_matrix;"\
		"uniform mat4 u_projection_matrix;"\
		"uniform int u_lighting_enabled;"\
		"uniform int u_fragment_phong_enabled;"\
		"uniform vec3 u_La_red;"\
		"uniform vec3 u_Ld_red;"\
		"uniform vec3 u_Ls_red;"\
		"uniform vec4 u_light_position_red;"\
		"uniform vec3 u_La_green;"\
		"uniform vec3 u_Ld_green;"\
		"uniform vec3 u_Ls_green;"\
		"uniform vec4 u_light_position_green;"\
		"uniform vec3 u_La_blue;"\
		"uniform vec3 u_Ld_blue;"\
		"uniform vec3 u_Ls_blue;"\
		"uniform vec4 u_light_position_blue;"\
		"uniform vec3 u_Ka;"\
		"uniform vec3 u_Kd;"\
		"uniform vec3 u_Ks;"\
		"uniform float u_material_shininess;"\
		"out vec3 vertex_phong_ads_color;"\
		"out vec3 transformed_normals;"\
		"out vec3 light_direction_red;"\
		"out vec3 light_direction_green;"\
		"out vec3 light_direction_blue;"\
		"out vec3 viewer_vector;"\
		"void main(void)"\
		"{"\
		"vec3 vertex_phong_ads_color_red;"\
		"vec3 vertex_phong_ads_color_green;"\
		"vec3 vertex_phong_ads_color_blue;"\

		"if(u_lighting_enabled==1)"\
		"{"\
		"if(u_fragment_phong_enabled==1)"\
		"{"\

		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"\

		"transformed_normals=mat3(u_view_matrix * u_model_matrix)*vNormal;"\

		"light_direction_red=vec3(u_light_position_red)-eye_coordinates.xyz;"\
		"light_direction_green=vec3(u_light_position_green)-eye_coordinates.xyz;"\
		"light_direction_blue=vec3(u_light_position_blue)-eye_coordinates.xyz;"\

		"viewer_vector=-eye_coordinates.xyz;"\

		"}"\
		"else"\
		"{"\
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"\

		"transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix)*vNormal);"\

		"light_direction_red=normalize(vec3(u_light_position_red)-eye_coordinates.xyz);"\
		"light_direction_green=normalize(vec3(u_light_position_green)-eye_coordinates.xyz);"\
		"light_direction_blue=normalize(vec3(u_light_position_blue)-eye_coordinates.xyz);"\

		"float tn_dot_ld_red= max(dot(transformed_normals,light_direction_red),0.0);"\
		"float tn_dot_ld_green=max(dot(transformed_normals,light_direction_green),0.0);"\
		"float tn_dot_ld_blue=max(dot(transformed_normals,light_direction_blue),0.0);"\

		"vec3 ambient_red=u_La_red * u_Ka;"\
		"vec3 ambient_green=u_La_green * u_Ka;"\
		"vec3 ambient_blue=u_La_blue * u_Ka;"\

		"vec3 diffuse_red=u_Ld_red * u_Kd * tn_dot_ld_red;"\
		"vec3 diffuse_green=u_Ld_green * u_Kd * tn_dot_ld_green;"\
		"vec3 diffuse_blue=u_Ld_blue * u_Kd * tn_dot_ld_blue;"\
		
		"vec3 reflection_vector_red=reflect(-light_direction_red,transformed_normals);"\
		"vec3 reflection_vector_green=reflect(-light_direction_green,transformed_normals);"\
		"vec3 reflection_vector_blue=reflect(-light_direction_blue,transformed_normals);"\

		"viewer_vector=normalize(-eye_coordinates.xyz);"\

		"vec3 specular_red= u_Ls_red * u_Ks * pow(max(dot(reflection_vector_red,viewer_vector),0.0),u_material_shininess);"\
		"vec3 specular_green=u_Ls_green * u_Ks * pow(max(dot(reflection_vector_green,viewer_vector),0.0),u_material_shininess);"\
		"vec3 specular_blue=u_Ls_blue * u_Ks * pow(max(dot(reflection_vector_blue,viewer_vector),0.0),u_material_shininess);"\

		"vertex_phong_ads_color_red= ambient_red + diffuse_red + specular_red;"\
		"vertex_phong_ads_color_green=ambient_green + diffuse_green + specular_green;"\
		"vertex_phong_ads_color_blue=ambient_blue + diffuse_blue + specular_blue;"\
		"vertex_phong_ads_color=vertex_phong_ads_color_red + vertex_phong_ads_color_green + vertex_phong_ads_color_blue;"\

		"}"\
		"}"\
		"else"\
		"{"\
		"vertex_phong_ads_color=vec3(1.0,1.0,1.0);"\
		"}"\

		"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"\

		"}";

	glShaderSource(gPerVertex_Fragment_PHONG_VertexShaderObject,1, (const GLchar **)&vertexShaderSourceCode,NULL);
	
	//Compile the shader
	glCompileShader(gPerVertex_Fragment_PHONG_VertexShaderObject);

	GLint iShaderCompiledStatus = 0;
	GLint iInfoLogLength = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gPerVertex_Fragment_PHONG_VertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
	if (iShaderCompiledStatus==GL_FALSE)
	{
		glGetShaderiv(gPerVertex_Fragment_PHONG_VertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gPerVertex_Fragment_PHONG_VertexShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Vertex Shader Compilation Log: %s\n",szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			} //szInfoLog!=NULL

		}//iInfoLogLength>0

	}//iShaderCompiledStatus==GL_FALSE
	
	////////////////// PER VERTEX PHONG FRAGMENT SHADER///////////////////////
	//Create shader
	gPerVertex_Fragment_PHONG_FragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
	
	//Provide source code to Shader[Here you can specify the GLSL version supported by your graphics hardware and OS (Ex: "#version 440 core")]
	//name out_color should be same across shader to get pass the color from vertex to fragment shader
	
	//FRAGMENT SHADER: Will only accept the output of Vertex Shader(which is "phong_ads_color") and it will just display/render it.
	// Such model is named as "Gouraud Shading model/Per Vertex Lighting!!!".

	const GLchar *fragmentShaderSourceCode = " #version 440 core"\
		"\n"\
		"in vec3 vertex_phong_ads_color;"\
		"in vec3 transformed_normals;"\
		"in vec3 light_direction_red;"\
		"in vec3 light_direction_green;"\
		"in vec3 light_direction_blue;"\
		"in vec3 viewer_vector;"\
		"out vec4 FragColor;"\
		"uniform vec3 u_La_red;"\
		"uniform vec3 u_Ld_red;"\
		"uniform vec3 u_Ls_red;"\
		"uniform vec3 u_La_green;"\
		"uniform vec3 u_Ld_green;"\
		"uniform vec3 u_Ls_green;"\
		"uniform vec3 u_La_blue;"\
		"uniform vec3 u_Ld_blue;"\
		"uniform vec3 u_Ls_blue;"\
		"uniform vec3 u_Ka;"\
		"uniform vec3 u_Kd;"\
		"uniform vec3 u_Ks;"\
		"uniform float u_material_shininess;"\
		"uniform int u_lighting_enabled;"\
		"uniform int u_fragment_phong_enabled;"\
		"void main(void)"\
		"{"\
		"vec3 phong_ads_color;"\
		"if(u_lighting_enabled==1)"\
		"{"\

		"if(u_fragment_phong_enabled==1)"\
		"{"\

		"vec3 normalized_transformed_normals=normalize(transformed_normals);"\

		"vec3 normalized_light_direction_red=normalize(light_direction_red);"\
		"vec3 normalized_light_direction_green=normalize(light_direction_green);"\
		"vec3 normalized_light_direction_blue=normalize(light_direction_blue);"\

		"vec3 normalized_viewer_vector=normalize(viewer_vector);"\

		"vec3 ambient_red=u_La_red * u_Ka;"\
		"vec3 ambient_green=u_La_green * u_Ka;"\
		"vec3 ambient_blue=u_La_blue * u_Ka;"\

		"float tn_dot_ld_red=max(dot(normalized_transformed_normals,normalized_light_direction_red),0.0);"\
		"float tn_dot_ld_green=max(dot(normalized_transformed_normals,normalized_light_direction_green),0.0);"
		"float tn_dot_ld_blue=max(dot(normalized_transformed_normals,normalized_light_direction_blue),0.0);"\

		"vec3 diffuse_red=u_Ld_red * u_Kd * tn_dot_ld_red;"\
		"vec3 diffuse_green=u_Ld_green * u_Kd * tn_dot_ld_green;"\
		"vec3 diffuse_blue=u_Ld_blue * u_Kd * tn_dot_ld_blue;"\

		"vec3 reflection_vector_red=reflect(-normalized_light_direction_red,normalized_transformed_normals);"\
		"vec3 reflection_vector_green=reflect(-normalized_light_direction_green,normalized_transformed_normals);"
		"vec3 reflection_vector_blue=reflect(-normalized_light_direction_blue,normalized_transformed_normals);"


		"vec3 specular_red=u_Ls_red * u_Ks * pow(max(dot(reflection_vector_red,normalized_viewer_vector),0.0),u_material_shininess);"\
		"vec3 specular_green=u_Ls_green * u_Ks * pow(max(dot(reflection_vector_green,normalized_viewer_vector),0.0),u_material_shininess);"
		"vec3 specular_blue=u_Ls_blue * u_Ks * pow(max(dot(reflection_vector_blue,normalized_viewer_vector),0.0),u_material_shininess);"\

		"vec3 phong_ads_color_red=ambient_red + diffuse_red + specular_red;"\
		"vec3 phong_ads_color_green=ambient_green + diffuse_green + specular_green;"\
		"vec3 phong_ads_color_blue=ambient_blue + diffuse_blue + specular_blue;"\

		"phong_ads_color=phong_ads_color_red + phong_ads_color_green + phong_ads_color_blue;"\

		"}"\
		"else"\
		"{"\
		"phong_ads_color=vertex_phong_ads_color;"\
		"}"\
		
		"}"\
		"else"\
		"{"\
		"phong_ads_color=vec3(1.0,1.0,1.0);"\
		"}"\

		"FragColor=vec4(phong_ads_color,1.0);"\
		
		"}";
											
	glShaderSource(gPerVertex_Fragment_PHONG_FragmentShaderObject,1,(const GLchar **)&fragmentShaderSourceCode,NULL);
																			   
	//Compile the shader
	glCompileShader(gPerVertex_Fragment_PHONG_FragmentShaderObject);																			   
	glGetShaderiv(gPerVertex_Fragment_PHONG_FragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gPerVertex_Fragment_PHONG_FragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog =(char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gPerVertex_Fragment_PHONG_FragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Fragment Shader Log: %s\n",szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			} //szInfoLog!=NULL

		}//iInfoLogLength>0

	}//iShaderCompiledStatus==GL_FALSE

	/**********************SHADER PROGRAM: PRE Link Binding!!**********************************/
	//Create the Shader Program Object
	gShaderProgramObject=glCreateProgram();
	
	//Attach Vertex Shader to Shader Program
	glAttachShader(gShaderProgramObject,gPerVertex_Fragment_PHONG_VertexShaderObject);
	
	//Attach fragment shader to Shader Program
	glAttachShader(gShaderProgramObject,gPerVertex_Fragment_PHONG_FragmentShaderObject);
	
	/**********************Pre-link binding of "shader program object" with vertex shader position attribute****************/

	glBindAttribLocation(gShaderProgramObject,VDG_ATTRIBUTE_VERTEX, "vPosition"); //Tikadam-2

	glBindAttribLocation(gShaderProgramObject,VDG_ATTRIBUTE_NORMAL,"vNormal");
																				  
	/************Link shader*************/
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;

	glGetProgramiv(gShaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);

	if (iShaderProgramLinkStatus==GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{

				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			} //szInfoLog!=NULL

		}//iInfoLogLength>0

	}//iShaderProgramLinkStatus==GL_FALSE

	/******************* Provide the Uniforms/Get the Uniform locations***************************************************/
	model_matrix_uniform = glGetUniformLocation(gShaderProgramObject,"u_model_matrix");

	view_matrix_uniform = glGetUniformLocation(gShaderProgramObject,"u_view_matrix");

	projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject,"u_projection_matrix");

	// 'L' Key is pressed on not?
	L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");

	// 'F/f' Key is pressed or not?
	F_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_fragment_phong_enabled");

	/////////// Light:RED 
	//ambient color intensity of light
	red_La_uniform = glGetUniformLocation(gShaderProgramObject, "u_La_red");
	//diffuse color intensity of light
	red_Ld_uniform= glGetUniformLocation(gShaderProgramObject, "u_Ld_red");
	//Specular color intensity of light
	red_Ls_uniform= glGetUniformLocation(gShaderProgramObject, "u_Ls_red");	
	//position of light
	red_light_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_position_red");

	/////////// Light:GREEN
	//ambient color intensity of light
	green_La_uniform = glGetUniformLocation(gShaderProgramObject, "u_La_green");
	//diffuse color intensity of light
	green_Ld_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld_green");
	//Specular color intensity of light
	green_Ls_uniform= glGetUniformLocation(gShaderProgramObject, "u_Ls_green");
	//position of light
	green_light_position_uniform= glGetUniformLocation(gShaderProgramObject, "u_light_position_green");

	/////////// Light:BLUE
	//ambient color intensity of light
	blue_La_uniform = glGetUniformLocation(gShaderProgramObject, "u_La_blue");
	//diffuse color intensity of light
	blue_Ld_uniform= glGetUniformLocation(gShaderProgramObject, "u_Ld_blue");
	//Specular color intensity of light
	blue_Ls_uniform= glGetUniformLocation(gShaderProgramObject, "u_Ls_blue");
	//position of light
	blue_light_position_uniform= glGetUniformLocation(gShaderProgramObject, "u_light_position_blue");
	
	///////// Material
	//ambient reflective color intensity of material
	Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");

	//diffuse reflective color intensity of material
	Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");

	//specular reflective color internsity of material
	Ks_uniform = glGetUniformLocation(gShaderProgramObject,"u_Ks");

	//Shininess of material(This value is conventionally between 1 to 200)
	material_shinniness_uniform = glGetUniformLocation(gShaderProgramObject,"u_material_shininess");

	/**********************************Vertices, colors, shader attribs, vbo, vao initializations******************************/
	getSphereVertexData(sphere_vertices,sphere_normals,sphere_textures,sphere_elements);

	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	/********* Binding of Vao for Sphere *******/
	glGenVertexArrays(1,&gVao_sphere);
	glBindVertexArray(gVao_sphere);		

	/////////Vbo Sphere Position///////////
	glGenBuffers(1,&gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER,gVbo_sphere_position); //binding of square_vbo_position
	glBufferData(GL_ARRAY_BUFFER,sizeof(sphere_vertices),sphere_vertices,GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER,0);            //un-binding of square_vbo_position
	////////////////////////////////
	
	////// For Normal Attribute for Sphere//////
	// ----------------------------------------------------------------------
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER,sizeof(sphere_normals),sphere_normals,GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL,3,GL_FLOAT,GL_FALSE,0,NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL); //No need to Give(If this line is given...Square does not appear!!).

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	/////////////////////////////////////////////

	/////// For Element Vbo for Sphere///////////
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ARRAY_BUFFER,gVbo_sphere_element);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	/////////////////////////////////////////////
	glBindVertexArray(0);				//Un-binding of Vao for Square
	//*******************************************************************************************
	/////////////////Define the depth initialization/////////////////
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST); //This need not be specified hence forth in PP for windows(Reason is: Android does not support it, so "jun te son")
	//We will always cull back faces for better performance
	glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //Now clear the window with desired color

	//Set perspectiveProjectionMatrix to Identity Matrix.
	gPerspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH,WIN_HEIGHT); //a warmup call to resize function--In the case of "Projection" this call is of crucial importance as this call will help keep the viewing volume ready before drawing happens.
								  //Otherwise, in this case, application is launching in windowed mode with some weird output(as viewing volume will not be ready if this call is commentedout) and after pressing "F"(for fullscreen) desired output comes.	
								  // This warm up call to resize() goig to be crucial in case of DirectX
}//end of initialize()

//Resize function:
// Projection functions are conventionally called inside the function which deals with the change in the size of the window
void resize(int width,int height)
{
	if(height==0)
		height=1;
	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	//gluPerspective(fovy,Aspect Ration,near,far);
	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
	
}//end of resize

//CALLBACK Function
LRESULT CALLBACK WndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam)
{
	//function declarations
	void resize(int,int);
	void uninitialize(void);
	void ToggleFullscreen(void);

	//variable declarations
	static bool bIsAKeyPressed = false;
	static bool bIsLKeyPressed = false;

	//Code
	switch(message)
	{

	case WM_CREATE:
					break;

	case WM_ACTIVATE:
					 if(HIWORD(wParam)==0)
						 gbActiveWindow=true;
					 else
						 gbActiveWindow=false;
					break;

	case WM_KEYDOWN:
					switch(wParam)
					{
					case VK_ESCAPE:
									gbEscapeKeyIsPressed=true;
									break;

						case 0x45: //'e' or 'E' for fullscreen
									if(gbFullscreen==false)
									{
										ToggleFullscreen();
										gbFullscreen=true;
									}
									else
									{
										ToggleFullscreen();
										gbFullscreen=false;
									}
									break;
						case 0x46: //'f' or 'F' for: Per Fragment Phong Lighting enable/disable.

									if(gbIsFKeyPressed == false)
									{
										gbPerFragmentPhongEnabled = true;
										gbIsFKeyPressed = true;
									}
									else
									{
										gbPerFragmentPhongEnabled = false;
										gbIsFKeyPressed = false;
									}
									break;

						case 0x4C:	//for 'L' or 'l'
									if (bIsLKeyPressed == false)
									{
										gbLight = true;
										gbSpin = true;
										bIsLKeyPressed = true;
									}
									else
									{
										gbLight = false;
										gbSpin = false;
										bIsLKeyPressed = false;
										gbIsFKeyPressed = false;
									}
									break;

						case 0x51: //Q
									uninitialize();
									PostQuitMessage(0);
									break;
					}
					break;

	case WM_SIZE:
					resize(LOWORD(lParam),HIWORD(lParam));
					break;

	case WM_DESTROY:
					PostQuitMessage(0);
					break;

	}
	return(DefWindowProc(hwnd,message,wParam,lParam));

}//End of call back function

/*ToggleFullscreen function
	1. Get the window style using GetWindowLong()
	2. If the "Current window" type contains the "WS_OVERLAPPEDWINDOW" -->Remove it.
	3. Store the current window position
	4. Get the monitor info(Primary display)
	5. Set the window position
*/
void ToggleFullscreen(void)
{
	//variable declaration
	MONITORINFO mi={sizeof(MONITORINFO)};
	bool IsWindowPlacement;
	bool IsMonitorInfo;
	HMONITOR hMonitor;

	//code
	if(gbFullscreen==false) //windowed to full-screen
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE); //Get the window style/entire information of the window
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			//a) Get the window Placement and Monitor information then and then toggle to fullscreen
			IsWindowPlacement=GetWindowPlacement(ghwnd,&wpPrev);

			//b) Now get the monitor first 
			hMonitor=MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY); //the primary monitor in case of extended view

			//c) Get and store the monitor information from the monitor which is been just received
			IsMonitorInfo=GetMonitorInfo(hMonitor,&mi);

			//
			if(IsWindowPlacement && IsMonitorInfo)//both information have been received, now set the window to the top of the screen
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~ WS_OVERLAPPEDWINDOW);

				//Set the window position
				
				SetWindowPos(
							  ghwnd,
							  HWND_TOP,
							  mi.rcMonitor.left,
							  mi.rcMonitor.top,
							  mi.rcMonitor.right-mi.rcMonitor.left, //width
							  mi.rcMonitor.bottom-mi.rcMonitor.top, //height
							  SWP_NOZORDER | SWP_FRAMECHANGED       //ignore any of the Z order and trigger frame change event
							);

			}
			ShowCursor(FALSE);
		}
	}
	else //full-screen to windowed mode
	{
		// Add the removed style to the window style
		// Set the window placement/position to back to the saved position
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd,&wpPrev);

		SetWindowPos(
						ghwnd,
						HWND_TOP,
						0,0,0,0,
						SWP_NOMOVE|
						SWP_NOSIZE|
						SWP_NOOWNERZORDER|
						SWP_NOZORDER|
						SWP_FRAMECHANGED
					);
		ShowCursor(TRUE);
	}
	
}//end of ToggleFullscreen()

void spin(void)
{
	angleRed = angleRed + 0.05f;

	if (angleRed > 360.0f)
		angleRed = 0.0f;

	angleGreen = angleGreen + 0.05f;
	if (angleGreen > 360.0f)
		angleGreen = 0.0f;

	angleBlue = angleBlue + 0.05f;
	if (angleBlue > 360.0f)
		angleBlue = 0.0f;

}//spin()


 //Display function
void display(void)
{
	//code
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	
	//Start using OpenGL Program Object
	glUseProgram(gShaderProgramObject);

	if (gbLight == true)
	{
		if(gbPerFragmentPhongEnabled == true)
		{
			glUniform1i(F_KeyPressed_uniform, 1);
		}
		else
		{
			glUniform1i(F_KeyPressed_uniform,0);
		}
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform,1);

		// Setting Red light's properties
		glUniform3fv(red_La_uniform,1,RedLightAmbient);
		glUniform3fv(red_Ld_uniform, 1, RedLightDiffuse);
		glUniform3fv(red_Ls_uniform, 1, RedLightSpecular);
		RedLightPosition[1] = 10 * sin(angleRed);
		RedLightPosition[2] = 10 * cos(angleRed);

		fprintf(gpFile, "angleRed=%f\n y=%f \t z=%f ",angleRed,RedLightPosition[1],RedLightPosition[2]);

		glUniform4fv(red_light_position_uniform, 1, RedLightPosition);

		// Setting Green light's properties
		glUniform3fv(green_La_uniform, 1, GreenLightAmbient);
		glUniform3fv(green_Ld_uniform, 1, GreenLightDiffuse);
		glUniform3fv(green_Ls_uniform, 1, GreenLightSpecular);
		GreenLightPosition[0] = 10 * sin(angleGreen);
		GreenLightPosition[2] = 10 * cos(angleGreen);

		fprintf(gpFile, "angleGreen=%f\n x=%f \t z=%f ", angleGreen, GreenLightPosition[0], GreenLightPosition[2]);

		glUniform4fv(green_light_position_uniform, 1, GreenLightPosition);

		//Setting Blue light's properties 
		glUniform3fv(blue_La_uniform,1,BlueLightAmbient);
		glUniform3fv(blue_Ld_uniform,1,BlueLightDiffuse);
		glUniform3fv(blue_Ls_uniform,1,BlueLightSpecular);

		BlueLightPosition[0] = 10 * sin(angleBlue);
		BlueLightPosition[1] = 10 * cos(angleBlue);

		glUniform4fv(blue_light_position_uniform,1,BlueLightPosition);

		// Setting Materials properties
		glUniform3fv(Ka_uniform, 1, material_ambient);
		glUniform3fv(Kd_uniform, 1, material_diffuse);
		glUniform3fv(Ks_uniform, 1, material_specular);
		glUniform1f(material_shinniness_uniform, material_shinniness);
	}
	else
	{
		//set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform,0);
		gbIsFKeyPressed = false;
		gbPerFragmentPhongEnabled = false;
		//Per Fragment Phong lighting should also get disabled if it was in enabled state before turning Light OFF.
	}

	//************* DRAW SPHERE **************************************************/
	//OpenGL Drawing
	//set model and view matrices to identity
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();

	// Apply 'z' axis translation to go deep into the screen by -2.0,
	// so that sphere will same fullscreen coordinates, but due to translation
	// it will look small.
	modelMatrix = translate(0.0f, 0.0f, -2.0f);

	// Pass this "modelMatrix" to "Vertex Shader" in "u_model_matrix" shader variable!
    // whose position/location we already calculated in initialize() by using glGetUniformLocation()
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	//Pass "viewMatrix" to "Vertex Shader" in "u_view_matrix" uniform!
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);

	// Pass projection matrix also to the "vertex shader" in "u_projection_matrix" shader variable
	// whose position/location we have already calculated in initialize() by using glGetUniformLocation()
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/******************Binding to Vao_sphere**************************************/
	glBindVertexArray(gVao_sphere);    

	//Draw either by glDrawArrays() or glDrawElements() or glDrawTriangles()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES,gNumElements,GL_UNSIGNED_SHORT,0);

	glBindVertexArray(0);// Un-binding to Vao_Square	
	//***************************************************************************/	
	//Stop using OpenGL Program Object
	glUseProgram(0);
	
	SwapBuffers(ghdc);
	
}//end of display function

 //Uninitialize function
void uninitialize(void)
{	
	//if window is in fullscreen mode-->Switch back to windowed mode and then exit
	if(gbFullscreen==true)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);

		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd,&wpPrev);

		SetWindowPos(
					  ghwnd,
					  HWND_TOP,
					  0,0,0,0,
					  SWP_NOMOVE|
					  SWP_NOSIZE|
					  SWP_NOOWNERZORDER|
					  SWP_NOZORDER|
					  SWP_FRAMECHANGED
					);
		ShowCursor(TRUE);
	}
	//destroy vao
	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

	//destroy position Vbo
	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1,&gVbo_sphere_position);
		gVbo_sphere_position = 0;

	}
	//destroy normal vbo
	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;

	}
	//destroy element vbo
	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}

	//detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject,gPerVertex_Fragment_PHONG_VertexShaderObject);
	
	//detach Fragment shader object from shader program object
	glDetachShader(gShaderProgramObject,gPerVertex_Fragment_PHONG_FragmentShaderObject);	
	
	//Delete vertex shader object
	glDeleteShader(gPerVertex_Fragment_PHONG_VertexShaderObject);
	gPerVertex_Fragment_PHONG_VertexShaderObject=0;
	
	//Delete fragment shader object
	glDeleteShader(gPerVertex_Fragment_PHONG_FragmentShaderObject);
	gPerVertex_Fragment_PHONG_FragmentShaderObject=0;
	
	//Now delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject=0;
	
	//unlink shader program
	glUseProgram(0);

	//Deselect the rendering context
	wglMakeCurrent(NULL,NULL);

	//Delete the rendering context
	wglDeleteContext(ghrc);
	ghrc=NULL;

	//Delete the device context
	ReleaseDC(ghwnd,ghdc);
	ghdc=NULL;

	if(gpFile)
	{
		fprintf(gpFile,"Log File Closed Successfully!!");
		fclose(gpFile);
		gpFile=NULL;
	}
	DestroyWindow(ghwnd);
}//end of uninitialize()