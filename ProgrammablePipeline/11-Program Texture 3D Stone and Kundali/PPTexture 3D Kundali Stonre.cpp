/**************************************************************************************************************************

Texture Program:1:Programmable Pipeline: 3D Stone and Kundali !!	
***************************************************************************************************************************/
#include<windows.h>
#include<stdio.h> //for file i/o

#include<gl\glew.h> // for GLSL extensions.
					//This file should be included before below statement: #include<gl/gl.h>
#include<gl\GL.h>

#include "vmath.h" //For Math related operations and matrices etc.

#include "PPTextureKundaliStone.h"



//#include<gl/glu.h> //In Programmable pipeline, none of the functions from GLU work!
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"glew32.lib") //The GLEW library.
#pragma comment(lib,"opengl32.lib")
//#pragma comment(lib,"glu32.lib")


#define WIN_WIDTH 1024
#define WIN_HEIGHT 768

using namespace vmath;

enum
{
	VDG_ATTRIBUTE_VERTEX = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

bool gbActiveWindow=false;
bool gbEscapeKeyIsPressed=false;
bool gbFullscreen=false;

float anglePyramid = 0.0f;
float angleCube = 0.0f;


DWORD dwStyle;
WINDOWPLACEMENT wpPrev= {sizeof(WINDOWPLACEMENT)};
HWND ghwnd;
HDC ghdc;
HGLRC ghrc; 
FILE *gpFile=NULL;

//Prototype of WndProc()
LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

//Shader and Shader Program Objects
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_Pyramid; //Existing Vao renamed to depict the shape
GLuint gVbo_pyramid_position; //For Position
GLuint gVbo_pyramid_texture;   //For Texture

GLuint gVao_Cube;   //New Vao and Vbo for "Cube" shape
GLuint gVbo_cube_position; 
GLuint gVbo_cube_texture; //One of the attribute of Vertex.

GLuint gMVPUniform; //[global variable for Model View Projection Uniform]

mat4 gPerspectiveProjectionMatrix;

GLuint gTexture_sampler_uniform;//[global variable for sampler uniform]
GLuint gTexture_Kundali;
GLuint gTexture_Stone;


int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR nCmdLine,int nCmdShow)
{
	//function prototypes
	void initialize(void);
	void update(void);
	void display(void);
	void uninitialize(void);

	//Variables
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	bool bDone=false;
	TCHAR szAppName[]=TEXT("PP 3D Texture: Stone and Kundali!!");

	if(fopen_s(&gpFile,"PP_3D_Texture_Stone_Kundali_Log.txt","w")!=0)
	{
		MessageBox(NULL,TEXT("Log file could not be created..exiting!!"),TEXT("Error"),MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile,"Log File Created Successfully!!\n");
	}

	//code
	wndClass.cbSize=sizeof(WNDCLASSEX);
	wndClass.style=CS_HREDRAW | CS_VREDRAW | CS_OWNDC; //CS_OWNDC-->Non-purgable DC
	wndClass.cbClsExtra=0;
	wndClass.cbWndExtra=0;

	wndClass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hInstance=hInstance;
	wndClass.hIcon=LoadIcon(NULL,IDI_APPLICATION); //load the default icon
	wndClass.hIconSm=LoadIcon(NULL,IDI_APPLICATION); //Icon for "System menu"
	wndClass.hCursor=LoadCursor(NULL,IDC_ARROW);	 //Load default cursor, you can specific the cursor type while creating a game application (Confirm?)

	wndClass.lpfnWndProc=WndProc;
	wndClass.lpszClassName=szAppName;
	wndClass.lpszMenuName=NULL;

	//Register the created class
	RegisterClassEx(&wndClass);

	//create the window
	hwnd=CreateWindowEx(
						 WS_EX_APPWINDOW, //exclusive full-screen window
						 szAppName,
						 TEXT("PP::Texture::3D Stone & Kundali Rotation !!"),
						 WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS|WS_VISIBLE,
						 0,
						 0,
						 WIN_WIDTH,
						 WIN_HEIGHT,
						 NULL,		
						 NULL,
						 hInstance,
						 NULL
						);

	ghwnd=hwnd;	//For further use of the global window handle

	ShowWindow(hwnd,nCmdShow);//Show the created window
	SetForegroundWindow(hwnd);//Bring that window in foreground
	SetFocus(hwnd);//Set the focus on the window

	initialize(); //Call to initialize(Do the preparation of the "Picture element" (Pixel)

	//Game loop
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}//Events further generating the messages
		else
		{
			if(gbActiveWindow==true)
			{
				if(gbEscapeKeyIsPressed==true)
				{
					bDone=true;
				}
				update();
				display();
			}
			
		}//game will run here

	}//end of while
	uninitialize();
	return msg.wParam; 
}//WinMain

/*Initialize:
  1. PIXELFORMATDESCRIPTOR inialize
  2. ChoosePixelFormat
  3. SetPixelFormat
  4. Get the device context from OS
  5. Create OpenGL Context:wglCreateContext()
  6. HandOver control from OS DC to Rendering DC: wglMakeCurrent()
  7. Clear the window with desired color
  8. Give a warmup call to resize()

*/
void initialize(void)
{
	//function prototypes
	void resize(int,int);
	int LoadGLTextures(GLuint *, TCHAR[]);
	void uninitialize(void);

	//Variables
	PIXELFORMATDESCRIPTOR pfd;	//a structure which we need to fill with all possible set of values from our side and rest are fill by OS
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));//Make the 26-membered structure to 0

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags= PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; //dword flags depicting pixel characteristics
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cGreenBits=8;
	pfd.cBlueBits=8;
	pfd.cAlphaBits=8;
	pfd.cDepthBits = 32;

	ghdc=GetDC(ghwnd);//Get the regular device context from OS (this is non-purgeable)

	iPixelFormatIndex=ChoosePixelFormat(ghdc,&pfd);//After getting the DC, "Choose the pixel format" which is the closest match to the values which we have provided!

	if(iPixelFormatIndex==0)//if the pixelformatindex is empty means function did not succeed!!
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd)==false) //Set the chosen matching closest pixel format
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	ghrc=wglCreateContext(ghdc);//Now handover the device context to OpenGL's "Rendering context" through WGL

	if(ghrc==NULL)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	if(wglMakeCurrent(ghdc,ghrc)==NULL)//Once the handover is successful-->Make "Rendering context" as the current context through WGL
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	/////////////////// The Place for glew //////////////////////////
	// (IMPORTANT: This piece should be here means, after creating the OpenGL context but before using any OpenGL Function).
	
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	fprintf(gpFile, "%s \n", glGetString(GL_VERSION)); //Get the OpenGL Version supported!

	fprintf(gpFile, "%s \n", glGetString(GL_SHADING_LANGUAGE_VERSION)); //Get the GLSL Version supported on this configuration!

	////////////////// VERTEX SHADER /////////////////////////////
	// It runs for every vertex of your geometry.
	// That is the reason "these are smaller in size". It is not recommended to have "Program Loop/Conditional branching" within shader code.

	//Create Shader
	gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER); //glCreateShader()-->Specialist who writes the shader.
	
	// To Provide source code to shader, write it![Here you can specify the GLSL version supported by your graphics hardware and OS (Ex: "#version 440 core")]
	// "#version 130" was the primitive OpenGL version line.
	// Texture: Which is an attribute for a vertex. Texture Coords are made up of 2 values (s,t) that is why data type is vec2
	// Actual use of Texture(Ultimately color) is there in "fragment shader" hence texture attribute will come as 
	// an "in" variable and will go out to "fragmet shader" .
	
	//gl_Position: It is a hard coded/pre-defined variable KNOWN to OpenGL pipeline, from where next stage in pipeline picks up the position 
	//			   of the Vertex. So basically it is a Keyword.              
	const GLchar *vertexShaderSourceCode =	"#version 440 core" //Driver version: 390.65 supporting GLSL version: 4.6.0 
											"\n"\
											"in vec4 vPosition;"\
											"in vec2 vTexture0_Coord;" //New Attribute for a "vertex" which will come first to vertex shader!
											"out vec2 out_vTexture0_Coord;"\
											"uniform mat4 u_mvp_matrix;"\
											"void main(void)"\
											"{"\
											"gl_Position=u_mvp_matrix * vPosition;"\
											"out_vTexture0_Coord=vTexture0_Coord;" //It will go to Fragment shader! 
											"}";
	glShaderSource(gVertexShaderObject,1,(const GLchar **)&vertexShaderSourceCode,NULL);
	
	//Compile the shader
	glCompileShader(gVertexShaderObject);

	GLint iShaderCompiledStatus = 0;
	GLint iInfoLogLength = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
	if (iShaderCompiledStatus==GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Vertex Shader Compilation Log: %s\n",szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			} //szInfoLog!=NULL

		}//iInfoLogLength>0

	}//iShaderCompiledStatus==GL_FALSE
	
	////////////////// FRAGMENT SHADER///////////////////////
	// It runs per fragment which is made up of vertices.
	//Create shader
	gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
	
	//Provide source code to Shader[Here you can specify the GLSL version supported by your graphics hardware and OS (Ex: "#version 440 core")]
	// name "out_color" should be same across shader to get pass the color from vertex to fragment shader!
	// FragColor==> glFragColor chi aathwan (jo aadhi hardcoded/pre-defined OpenGL variable hota!) aata to variable waparat nahi!! :) 
	//
	// texture() function: sampler the "painter" is ready with "brush to paint" and where to paint!
	// The only information pending is the "texture image".
	
	const GLchar *fragmentShaderSourceCode=	" #version 440 core"\
											"\n"\
											"in vec2 out_vTexture0_Coord;"\
											"out vec4 FragColor;"\
											"uniform sampler2D u_texture0_sampler;"\
											"void main(void)"\
											"{"\
											 "FragColor=texture(u_texture0_sampler,out_vTexture0_Coord);"\
										    "}";
	glShaderSource(gFragmentShaderObject,1,(const GLchar **)&fragmentShaderSourceCode,NULL);
																			   
	//Compile the shader
	glCompileShader(gFragmentShaderObject);																			   
	glGetShaderiv(gFragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog =(char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Fragment Shader Log: %s\n",szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			} //szInfoLog!=NULL

		}//iInfoLogLength>0

	}//iShaderCompiledStatus==GL_FALSE

	//////////////////SHADER PROGRAM/////////////////////////
	//Create the Shader Program Object
	gShaderProgramObject=glCreateProgram();
	
	//Attach Vertex Shader to Shader Program
	glAttachShader(gShaderProgramObject,gVertexShaderObject);
	
	//Attach fragment shader to Shader Program
	glAttachShader(gShaderProgramObject,gFragmentShaderObject);
	
	////////////////// Binding the attributes of the "Vertex"(position and Texture coord)"///// 

	//Pre-link binding of "shader program object" with "vertex's position" attribute!
	glBindAttribLocation(gShaderProgramObject,VDG_ATTRIBUTE_VERTEX, "vPosition"); //Tikadam-2

	//Similarly binding of "Shader Program Object" with "Another attribute of Vertex:==>Texture"!!
	glBindAttribLocation(gShaderProgramObject,VDG_ATTRIBUTE_TEXTURE0,"vTexture0_Coord");

	///////////////////Link shader//////////////////////////////////////////////////////////////
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;

	glGetProgramiv(gShaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);

	if (iShaderProgramLinkStatus==GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{

				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			} //szInfoLog!=NULL

		}//iInfoLogLength>0

	}//iShaderProgramLinkStatus==GL_FALSE

	///////////////////////////////get MVP uniform location///////////////////////////////////
	//"Uniform" he ajun ek prakarcha marg aahe Shader la Data denyacha, apart from "in" type che variables. 
	gMVPUniform = glGetUniformLocation(gShaderProgramObject,"u_mvp_matrix");

	gTexture_sampler_uniform = glGetUniformLocation(gShaderProgramObject,"u_texture0_sampler");

	////////////////////// Vertices, colors, shader attribs, vbo, vao initializations//////////////
	//************ Pyramid Vao*******************************************************************
	const GLfloat pyramidVertices[]=
	{
		//Front Face
		0.0f,1.0f,0.0f,  //apex
		-1.0f,-1.0f,1.0f,//left-bottom
		1.0f,-1.0f,1.0f, //right-bottom
		
		//Left Face
		 0.0f,1.0f,0.0f,  
		-1.0f,-1.0f,-1.0f,
		-1.0f,-1.0f,1.0f,
	   
		//Back Face
	   0.0f,1.0f,0.0f, 
	   1.0f,-1.0f,-1.0f,
	  -1.0f,-1.0f,-1.0f,
	   
	  //Right Face
	   0.0f,1.0f,0.0f, 
	   1.0f,-1.0f,1.0f,
	   1.0f,-1.0f,-1.0f

	};

	const GLfloat pyramidTexcoords[]=
	{
		//Front Face	
		0.5,1.0,    //Front Top
		0.0,0.0,    //Front Left
		1.0,0.0,	//Front Right

		//Left Face
		0.5,1.0,	//Left-Top
		0.0,0.0,	//Left-Left
		1.0,0.0,		//Left-Right

		//Back Face
		0.5,1.0,	//Back-Top
		1.0,0.0,	//Back-left
		0.0,0.0,	//Back-Right
		
		//Right Face
		0.5,1.0,	//Right-Top
		1.0,0.0,    //Right-left
		0.0,0.0	//Right-right
	};
	////////// Vbo_pyramid_position////////////
	
	glGenVertexArrays(1,&gVao_Pyramid); //Pyramid Sathicha Vao
	glBindVertexArray(gVao_Pyramid);

	glGenBuffers(1,&gVbo_pyramid_position);
	glBindBuffer(GL_ARRAY_BUFFER,gVbo_pyramid_position); //binding to vbo_position
	glBufferData(GL_ARRAY_BUFFER,sizeof(pyramidVertices),pyramidVertices,GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL); //Tikadam-1

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX); //Tikadam-3

	glBindBuffer(GL_ARRAY_BUFFER,0); //unbinding of vbo_position

	///////// Vbo_pyramid_texture///////////////

	glGenBuffers(1,&gVbo_pyramid_texture);
	glBindBuffer(GL_ARRAY_BUFFER,gVbo_pyramid_texture); //binding to vbo_color
	glBufferData(GL_ARRAY_BUFFER,sizeof(pyramidTexcoords),pyramidTexcoords,GL_STATIC_DRAW);

	// (s,t)-->(2 components which define the "Texture coordinate" attribute for a vertex!)
	glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0,2,GL_FLOAT,GL_FALSE,0,NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);

	glBindBuffer(GL_ARRAY_BUFFER,0); //un-binding of vbo_color
	//////////////////////////////////
	glBindVertexArray(0);				//Un-binding of Pyramid Vao!!
	
	//************Cube Vao*********************************************************************
	const GLfloat cubeVertices[]=
	{
		//Front Face
		1.0f,1.0f,1.0f,   //Front-top-Right
		-1.0f,1.0f,1.0f,  //Front-top-Left
		-1.0f,-1.0f,1.0f, //Front-bottom-left
		1.0f,-1.0f,1.0f,  //Front-bottom-right

		//Left Face
		-1.0f,1.0f,1.0f,   //Left-Top-Right
		-1.0f,1.0f,-1.0f, //Left-Top-Left
		-1.0f,-1.0f,-1.0f, //Left-bottom-left
		-1.0f,-1.0f,1.0f,  //Left-bottom-right

		//Back Face
		-1.0f,1.0f,-1.0f,  //Back-Top-Right
		1.0f,1.0f,-1.0f,   //Back-Top-Left	
	    1.0f,-1.0f,-1.0f,  //Back-Bottom-left
	   -1.0f,-1.0f,-1.0f,  //Back-Bottom-right

	   //Right Face
	   1.0f,1.0f,-1.0f,	  //top-Right
	   1.0f,1.0f,1.0f,	  //top-left
	   1.0f,-1.0f,1.0f,   //bottom-left
	   1.0f,-1.0f,-1.0f,  //bottom-right

	   //Top Face
	    1.0f,1.0f,-1.0f, //Right-top
	   -1.0f,1.0f,-1.0f,  //left-top
	   -1.0f,1.0f,1.0f,   //bottom-left
		1.0f,1.0f,1.0f,   //bottom-right

		//Bottom Face
		 1.0f,-1.0f,1.0f, //bottom-top-right
		-1.0f,-1.0f,1.0f, //bottom-top-left
		 -1.0f,-1.0f,-1.0f,  //bottom-bottom-left
		 1.0f,-1.0f,-1.0f  //bottom-bottom-right
	};

	const GLfloat cubeTexcoords[]=
	{
		//Front Face-
		0.0f,0.0f,
		1.0f,0.0f,
		1.0f,1.0f,
		0.0f,1.0f,

		//Left Face
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		
		//Back-Face-
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		
		//Right Face
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,

		//Top-Face-
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,

		//Bottom Face-
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f
	};

	/////////////////// CUBE VAO ////////////////////////////////////////

	glGenVertexArrays(1,&gVao_Cube);
	glBindVertexArray(gVao_Cube);		//Binding of Vao for Square

	/////////Vbo Cube "Position"///////////
	glGenBuffers(1,&gVbo_cube_position);
	glBindBuffer(GL_ARRAY_BUFFER,gVbo_cube_position); //binding of square_vbo_position
	glBufferData(GL_ARRAY_BUFFER,sizeof(cubeVertices),cubeVertices,GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER,0);            //un-binding of square_vbo_position
	////////////////////////////////
	
	////// Vbo Cube "Texture Coords"/////////////////////////
	// ----------------------------------------------------------------------
	glGenBuffers(1, &gVbo_cube_texture);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube_texture);
	glBufferData(GL_ARRAY_BUFFER,sizeof(cubeTexcoords),cubeTexcoords,GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0,2,GL_FLOAT,GL_FALSE,0,NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0); //No need to Give(If this line is given...Square does not appear!!).

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//////////////////////////////////
	glBindVertexArray(0);				//Un-binding of Vao for Cube
	//*******************************************************************************************
	/////////////////Define the depth initialization/////////////////
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
	glEnable(GL_CULL_FACE);

	LoadGLTextures(&gTexture_Kundali,MAKEINTRESOURCE(IDBITMAP_KUNDALI));

	LoadGLTextures(&gTexture_Stone,MAKEINTRESOURCE(IDBITMAP_STONE));

	glEnable(GL_TEXTURE_2D); //enable texture mapping

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //Now clear the window with desired color

	//Set perspectiveProjectionMatrix to Identity Matrix.
	gPerspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH,WIN_HEIGHT); //a warmup call to resize function--In the case of "Projection" this call is of crucial importance as this call will help keep the viewing volume ready before drawing happens.
								  //Otherwise, in this case, application is launching in windowed mode with some weird output(as viewing volume will not be ready if this call is commentedout) and after pressing "F"(for fullscreen) desired output comes.	
								  // This warm up call to resize() goig to be crucial in case of DirectX
}//end of initialize()

int LoadGLTextures(GLuint *texture, TCHAR imageResourceId[])
{
	//variable declarations
	HBITMAP hBitmap;
	BITMAP bmp;
	int iStatus = FALSE;

	//code
	glGenTextures(1,texture); //1 image
	hBitmap =(HBITMAP)LoadImage(GetModuleHandle(NULL),imageResourceId,IMAGE_BITMAP,0,0,LR_CREATEDIBSECTION);

	if (hBitmap)
	{
		iStatus = TRUE;

		GetObject(hBitmap,sizeof(bmp),&bmp);
		glPixelStorei(GL_UNPACK_ALIGNMENT,1); //set 1 rather than default 4 (Was set in case of FFP program, for better performance.
		glBindTexture(GL_TEXTURE_2D,*texture); //bind texture

		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
		glTexImage2D(GL_TEXTURE_2D,
					 0,                //MIPMAP LOD (Level of Details)
					 GL_RGB,		   //Texture image cha internal format
					 bmp.bmWidth,
					 bmp.bmHeight,
					 0,				   // border width	
					 GL_BGR,		   // Pixel format (External jo aapan deto)	
					 GL_UNSIGNED_BYTE, // Image chya data cha type
					 bmp.bmBits		   // Actual image data	
					);
		//Create mipmaps for this texture for better image quality
		glGenerateMipmap(GL_TEXTURE_2D); //glTexImage2D() and glGenerateMipmap() function calls in combine form: gluBuild2DMipMaps()
		
		DeleteObject(hBitmap);//Delete unwanted bitmap handle
	
	}
	return(iStatus);
}//LoadGLTextures()



//Resize function:
// Projection functions are conventionally called inside the function which deals with the change in the size of the window
void resize(int width,int height)
{
	if(height==0)
		height=1;
	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	//gluPerspective(fovy,Aspect Ration,near,far);
	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
	
}//end of resize

//CALLBACK Function
LRESULT CALLBACK WndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam)
{
	//function declarations
	void resize(int,int);
	void uninitialize(void);
	void ToggleFullscreen(void);

	//Code
	switch(message)
	{

	case WM_CREATE:
					break;

	case WM_ACTIVATE:
					 if(HIWORD(wParam)==0)
						 gbActiveWindow=true;
					 else
						 gbActiveWindow=false;
					break;

	case WM_KEYDOWN:
					switch(wParam)
					{
					case VK_ESCAPE:
									gbEscapeKeyIsPressed=true;
									break;

						case 0x46: //for fullscreen
									if(gbFullscreen==false)
									{
										ToggleFullscreen();
										gbFullscreen=true;
									}
									else
									{
										ToggleFullscreen();
										gbFullscreen=false;
									}
									break;
						case 0x51: //Q
									uninitialize();
									PostQuitMessage(0);
									break;
						
					}
					break;

	case WM_SIZE:
					resize(LOWORD(lParam),HIWORD(lParam));
					break;

	case WM_DESTROY:
					PostQuitMessage(0);
					break;

	}
	return(DefWindowProc(hwnd,message,wParam,lParam));

}//End of call back function

/*ToggleFullscreen function
	1. Get the window style using GetWindowLong()
	2. If the "Current window" type contains the "WS_OVERLAPPEDWINDOW" -->Remove it.
	3. Store the current window position
	4. Get the monitor info(Primary display)
	5. Set the window position
*/
void ToggleFullscreen(void)
{
	//variable declaration
	MONITORINFO mi={sizeof(MONITORINFO)};
	bool IsWindowPlacement;
	bool IsMonitorInfo;
	HMONITOR hMonitor;

	//code
	if(gbFullscreen==false) //windowed to full-screen
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE); //Get the window style/entire information of the window
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			//a) Get the window Placement and Monitor information then and then toggle to fullscreen
			IsWindowPlacement=GetWindowPlacement(ghwnd,&wpPrev);

			//b) Now get the monitor first 
			hMonitor=MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY); //the primary monitor in case of extended view

			//c) Get and store the monitor information from the monitor which is been just received
			IsMonitorInfo=GetMonitorInfo(hMonitor,&mi);

			//
			if(IsWindowPlacement && IsMonitorInfo)//both information have been received, now set the window to the top of the screen
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~ WS_OVERLAPPEDWINDOW);

				//Set the window position
				
				SetWindowPos(
							  ghwnd,
							  HWND_TOP,
							  mi.rcMonitor.left,
							  mi.rcMonitor.top,
							  mi.rcMonitor.right-mi.rcMonitor.left, //width
							  mi.rcMonitor.bottom-mi.rcMonitor.top, //height
							  SWP_NOZORDER | SWP_FRAMECHANGED       //ignore any of the Z order and trigger frame change event
							);

			}
			ShowCursor(FALSE);
		}
	}
	else //full-screen to windowed mode
	{
		// Add the removed style to the window style
		// Set the window placement/position to back to the saved position
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd,&wpPrev);

		SetWindowPos(
						ghwnd,
						HWND_TOP,
						0,0,0,0,
						SWP_NOMOVE|
						SWP_NOSIZE|
						SWP_NOOWNERZORDER|
						SWP_NOZORDER|
						SWP_FRAMECHANGED
					);
		ShowCursor(TRUE);
	}
	
}//end of ToggleFullscreen()

void update(void)
{
	anglePyramid = anglePyramid + 1.0f;
	if (anglePyramid >= 360.0f)
		anglePyramid = 0.0f;
	
	angleCube = angleCube + 1.0f;
	if (angleCube >= 360.0f)
		angleCube = 0.0f;

}//update()

//Display function
void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	
	//Start using OpenGL Program Object
	glUseProgram(gShaderProgramObject);

	//OpenGL Drawing
	//set modelview & modelviewprojection matrices to identity

	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();
	mat4 rotationMatrix = mat4::identity();
	mat4 scaleMatrix = mat4::identity();

	//************** DRAW PYRAMID *********************************************/
	
	modelViewMatrix = vmath::translate(-1.5f, 0.0f, -6.0f); //Need to translate the Shape in order to see it..
	rotationMatrix = vmath::rotate(anglePyramid, 0.0f, 1.0f, 0.0f); //Define the rotation matrix and accordingly modelViewMatrix!
	modelViewMatrix = modelViewMatrix * rotationMatrix;

	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix; //ORDER IS IMPORTANT (Rigthmost to Leftmost asa sequence aahe) //Multiply the modelview and perspective projection matrix to get modelviewprojection matrix!

	//Pass above modelViewProjectionMatrix to the "Vertex Shader" in "u_mvp_matrix" shader variable whose position value we already calculated
	// in "initWithFrame()" by using "glGetUniformLocation()"

	glUniformMatrix4fv(gMVPUniform,1,GL_FALSE,modelViewProjectionMatrix); //glUniformMatrix4fv(initialize madhe tayar karun thewlela,count,transpose karu ki nako, modelview projection matrix)

	// ****** Bind Vao for Pyramid ***********//
	glBindVertexArray(gVao_Pyramid);

	////////////////// EPIC 3 STEPS FOR TEXTURE BINDING/////////////////
	glActiveTexture(GL_TEXTURE0); //0th Texture(corresponds to VGD_ATTRIBUTE_TEXTURE0)
	glBindTexture(GL_TEXTURE_2D,gTexture_Stone);
	glUniform1i(gTexture_sampler_uniform,0); //0th sampler enable(as we have only 1 texture sampler in fragment shader)

	//******** Draw, either by glDrawTriangles() OR glDrawArrays() OR glDrawElements()***********//
	glDrawArrays(GL_TRIANGLES,0,12); // 3 (each with its x,y,z) vertices in triangleVertices array

	//******* Unbind vao **********//
	glBindVertexArray(0);
	
	//************* DRAW CUBE **************************************************/
	//Before Drawing->Make the matrices Identity:
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	//Now Translate Square to right side:
	modelViewMatrix = vmath::translate(1.5f,0.0f,-6.0f);

	//Define the scale matrix
	scaleMatrix = vmath::scale(0.75f, 0.75f, 0.75f);

	modelViewMatrix = modelViewMatrix * scaleMatrix;

	//Define the rotation for Square and accordingly modelViewMatrix!
	rotationMatrix = vmath::rotate(angleCube,angleCube,angleCube);

	modelViewMatrix = modelViewMatrix * rotationMatrix;

	//Multiply the modelViewMatrix by PerspectiveProjectionMatrix to generate modelViewProjectionMatrix.
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;

	//Pass above modelViewProjectionMatrix to "VERTEX SHADER" in "u_mvp_matrix" shader variable whose position 
	// value we already calculated in "initWithFrame()" by using "glUniformLocation()".
	glUniformMatrix4fv(gMVPUniform,1,GL_FALSE,modelViewProjectionMatrix);

	// ****** Bind Vao for Cube ***********//
	glBindVertexArray(gVao_Cube);    //Binding to Vao_square

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,gTexture_Kundali);
	glUniform1i(gTexture_sampler_uniform, 0);

	glDrawArrays(GL_TRIANGLE_FAN,0,4); //This is the way to draw QUAD in PP.
	glDrawArrays(GL_TRIANGLE_FAN,4,4);
	glDrawArrays(GL_TRIANGLE_FAN,8,4);
	glDrawArrays(GL_TRIANGLE_FAN,12,4);
	glDrawArrays(GL_TRIANGLE_FAN,16,4);
	glDrawArrays(GL_TRIANGLE_FAN,20,4);

	glBindVertexArray(0);			   // Un-binding to Vao_Square	
	//***************************************************************************/
	//Stop using OpenGL Program Object

	glUseProgram(0);
	
	SwapBuffers(ghdc);
	
}//end of display function

 //Uninitialize function
void uninitialize(void)
{	
	//if window is in fullscreen mode-->Switch back to windowed mode and then exit
	if(gbFullscreen==true)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);

		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd,&wpPrev);

		SetWindowPos(
					  ghwnd,
					  HWND_TOP,
					  0,0,0,0,
					  SWP_NOMOVE|
					  SWP_NOSIZE|
					  SWP_NOOWNERZORDER|
					  SWP_NOZORDER|
					  SWP_FRAMECHANGED
					);
		ShowCursor(TRUE);
	}

	// destroy Vao
	if (gVao_Pyramid)
	{
		glDeleteVertexArrays(1,&gVao_Pyramid);
		gVao_Pyramid = 0;
	}
	if (gVao_Cube)
	{
		glDeleteVertexArrays(1,&gVao_Cube);
		gVao_Cube = 0;
	}

	//destroy Vbo for Pyramid's Position and Texture
	if (gVbo_pyramid_position)
	{
		glDeleteBuffers(1,&gVbo_pyramid_position);
		gVbo_pyramid_position = 0;

	}
	if (gVbo_pyramid_texture)
	{
		glDeleteBuffers(1, &gVbo_pyramid_texture);
		gVbo_pyramid_texture = 0;

	}

	//destroy Vbo for Cube's Position and Texture
	if (gVbo_cube_position)
	{
		glDeleteBuffers(1,&gVbo_cube_position);
		gVbo_cube_position = 0;

	}
	if (gVbo_cube_texture)
	{
		glDeleteBuffers(1,&gVbo_cube_texture);
		gVbo_cube_texture = 0;
	}

	//detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject,gVertexShaderObject);
	
	//detach Fragment shader object from shader program object
	glDetachShader(gShaderProgramObject,gFragmentShaderObject);	
	
	//Delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject=0;
	
	//Delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject=0;
	
	//Now delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject=0;
	
	//unlink shader program
	glUseProgram(0);

	//Deselect the rendering context
	wglMakeCurrent(NULL,NULL);

	//Delete the rendering context
	wglDeleteContext(ghrc);
	ghrc=NULL;

	//Delete the device context
	ReleaseDC(ghwnd,ghdc);
	ghdc=NULL;

	if(gpFile)
	{
		fprintf(gpFile,"Log File Closed Successfully!!");
		fclose(gpFile);
		gpFile=NULL;
	}
	DestroyWindow(ghwnd);
}//end of uninitialize()