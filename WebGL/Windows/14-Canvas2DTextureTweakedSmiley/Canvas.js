/**********************************************************************
Program: 2D Tweaked Smiley Texture Mapped To Square!!
**********************************************************************/
//Global variables
var canvas=null;
var gl=null;
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;
var gDigitKeyPressed;

//Shader Variables
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

//Variables representing "shader variables" in our program
const WebGLMacros=
{
	VDG_ATTRIBUTE_VERTEX:0, 
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3,
};

//Define Vao, Vbo and Uniform Variables
var vao_square;
var vbo_position;
var vbo_texture;
var mvpUniform;

//Variables for storing texture image and sampler uniform
var smiley_texture=0;
var uniform_texture0_sampler;

//Perspective Projection matrix
var perspectiveProjectionMatrix;

//To Start animation:
var requestAnimationFrame=
		window.requestAnimationFrame ||
		window.webkitRequestAnimationFrame ||
		window.mozRequestAnimationFrame ||
		window.oRequestAnimationFrame ||
		window.msRequestAnimationFrame;

//To Stop the animation
var cancelAnimationFrame=
		window.cancelAnimationFrame ||
		window.webkitCancelRequestAnimationFrame ||
		window.webkitCancelAnimationFrame ||
		window.mozCancelRequestAnimationFrame ||
		window.mozCancelAnimationFrame ||
		window.oCancelRequestAnimationFrame ||
		window.oCancelAnimationFrame ||
		window.msCancelRequestAnimationFrame ||
		window.msCancelAnimationFrame;


//onload function
function main()
{
	// Get the Canvas from the ID 
	canvas=document.getElementById("AMC");
	if(!canvas)
		console.log("\n Obtaining canvas failed!! \n");
	else
		console.log("\n Obtaining canvas succeeded!! \n");
	
	//Store the original height and width into global variables
	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;
	
	//Register the "Keyboard/Mouse" event handlers.
	// Last parameter of the addEventListener() is the special one. 
	// If it is "true" we can trap the event associated with that event!!
	// But we dont want to do it in this program, hence the parameter has been kept "false".
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);
	window.addEventListener("resize",resize,false);
	
	//Initialize function 
	init();
	
	//resize function call
	resize();
	
	//draw function call
	draw();
	
}//main()

function toggleFullscreen()
{
	//code
	var fullscreen_element=
		document.fullscreenElement ||
		document.webkitFullscreenElement ||
		document.mozFullScreenElement ||
		document.msFullscreenElement ||
		null;
	
	//if not in fullscreen mode, switch to it!
	if(fullscreen_element==null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
	
		bFullscreen=true;
		
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		
		bFullscreen=false;
	}
	
}//toggleFullscreen()

function keyDown(event)
{
	switch(event.keyCode)
	{
		case 70: //For 'f' or 'F'
					 toggleFullscreen();
					 break;
		case 27: //escape
					//uninitialize()
					uninitialize();
					//Close Application's tab
					window.close();
					break;
			
		case 49: //Digit=1
					  gDigitKeyPressed=1;
					  console.log("Digit=1 Key Pressed!!\n");
					  break;
			
		case 50: //Digit=2
					  gDigitKeyPressed=2;
					  console.log("Digit=2 Key Pressed!!\n");
					  break;
			
		case 51: //Digit=3
					  gDigitKeyPressed=3;
					  console.log("Digit=3 Key Pressed!!\n");
					  break;
			
		case 52: //Digit=4
					  gDigitKeyPressed=4;
					  console.log("Digit=4 Key Pressed!!\n");
					  break;		
	}//switch(event.keyCode)
}//keyDown()

function mouseDown()
{
	//code
	
}

function draw()
{
	//1
	gl.clear(gl.COLOR_BUFFER_BIT || gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
		var modelViewMatrix=mat4.create();
		var modelViewProjectionMatrix=mat4.create();
		var translationMatrix=mat4.create();
	
		//Draw Square
		mat4.identity(modelViewMatrix);
		mat4.identity(modelViewProjectionMatrix);
		mat4.identity(translationMatrix);
 
   	//Translate	
		mat4.translate(translationMatrix, translationMatrix, [0.0, 0.0, -5.0] );
		mat4.multiply(modelViewMatrix, translationMatrix, modelViewMatrix);
		//Project
		mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
		gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
		 
	     //****************************************************************************
		//Bind vao_square and provide the texture co-ordinates to it based upon the key press.!
		//****************************************************************************	
		gl.bindVertexArray(vao_square);
		
		var squareTexcoords=new Float32Array(8);
		
		if(gDigitKeyPressed==1)
		{
			squareTexcoords[0]=0.0; squareTexcoords[1]=0.5; //Left-half
			squareTexcoords[2]=0.0; squareTexcoords[3]=1.0; //Left-Bottom 
			squareTexcoords[4]=0.5; squareTexcoords[5]=1.0; //Right-half
			squareTexcoords[6]=0.5; squareTexcoords[7]=0.5; //Center point
			
			gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture);
			gl.bufferData(gl.ARRAY_BUFFER, squareTexcoords, gl.DYNAMIC_DRAW);
			gl.bindBuffer(gl.ARRAY_BUFFER, null);
		
		}//gDigitKeyPressed==1
		else if(gDigitKeyPressed==2) //Full Texture
		{
			squareTexcoords[0]=0.0; squareTexcoords[1]=0.0;
			squareTexcoords[2]=0.0; squareTexcoords[3]=1.0;
			squareTexcoords[4]=1.0; squareTexcoords[5]=1.0;
			squareTexcoords[6]=1.0; squareTexcoords[7]=0.0;
			
			gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture);
			gl.bufferData(gl.ARRAY_BUFFER, squareTexcoords, gl.DYNAMIC_DRAW);
			gl.bindBuffer(gl.ARRAY_BUFFER,null);
			
		}//gDigitKeyPressed==2
	  else if(gDigitKeyPressed==3) //Repeat mode
	  {
		  squareTexcoords[0]=0.0; squareTexcoords[1]=0.0;
		  squareTexcoords[2]=0.0; squareTexcoords[3]=2.0;
		  squareTexcoords[4]=2.0; squareTexcoords[5]=2.0;
		  squareTexcoords[6]=2.0; squareTexcoords[7]=0.0;

		  gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture);
		  gl.bufferData(gl.ARRAY_BUFFER, squareTexcoords, gl.DYNAMIC_DRAW);
		  gl.bindBuffer(gl.ARRAY_BUFFER, null);

	  }//gDigitKeyPressed==3
	  else if(gDigitKeyPressed==4) //Single Point
	  {
	  	 squareTexcoords[0]=0.5; squareTexcoords[1]=0.5;
		 squareTexcoords[2]=0.5; squareTexcoords[3]=0.5;
		 squareTexcoords[4]=0.5; squareTexcoords[5]=0.5;
		 squareTexcoords[6]=0.5; squareTexcoords[7]=0.5;
		 
		  gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture);
		  gl.bufferData(gl.ARRAY_BUFFER, squareTexcoords, gl.DYNAMIC_DRAW);
		  gl.bindBuffer(gl.ARRAY_BUFFER, null);
		  
	  }//gDigitKeyPressed==4
	
	    //Bind with texture!
	    gl.activeTexture(gl.TEXTURE0);
	 	gl.bindTexture(gl.TEXTURE_2D, smiley_texture);
	    gl.uniform1i(uniform_texture0_sampler, 0);

		//Now Draw the Square/Geometry(As you received Texture co-ordinates)
	     gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	
	gl.bindVertexArray(null); //Un-bind Vao for Square!

	gl.useProgram(null);

	//animation loop!
	requestAnimationFrame(draw,canvas);
}//draw()

function resize()
{
	if(bFullscreen==true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;
	}
	//set the viewport
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	//Perspective projection==>(target matrix, fov angle, aspect ration, near, far)
	mat4.perspective(perspectiveProjectionMatrix, 45.0, (parseFloat)(canvas.width)/(parseFloat)(canvas.height), 0.1, 100.0);
}//resize()


function init()
{
	//code
	//Get WebGL 2.0 Context
	// This context is nothing but"the GATEWAY" for all the WebGL functions, macro's and what not!
	// As like Windows/Linux/Android -> The function/macro's here do not follow the same style of naming
	// Ex. glClear() --> it is clear() in webGL.
	// But for our conveniece we have created the context itself of name: "gl" and we will be invoking methods/macros 
	// using this context variable, hence it will look like:
	// gl.clear() --> which at least gives you the feel that, you are invoking the OpenGL functions.
	
	gl=canvas.getContext("webgl2");
	
	if(gl==null)
	{
		console.log("\n Obtaining WebGL Rendenring Context Failed!! \n");
		return; //Terminate the program, as there is no use of proceeding further as no rendering context has been created!!
	}
	
	//After getting the context, define the viewport
	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;
	
	//Define the Vertex Shader
	var vertexShaderSourceCode=
		"#version 300 es"+
		"\n"+
		"in vec4 vPosition;"+
		"in vec2 vTexture0_Coord;"+
		"out vec2 out_texture0_coord;"+
		"uniform mat4 u_mvp_matrix;"+
		"void main(void)"+
		"{"+
				"gl_Position=u_mvp_matrix * vPosition;"+
				"out_texture0_coord=vTexture0_Coord;"+
		"}";
	
	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	
	//Compilation Error Handling
	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
		
	}	//Vertex shader compilation error handling
	
	//Define Fragment Shader
	var fragmentShaderSourceCode=
		"#version 300 es"+
		"\n"+
		"precision highp float;"+
		"in vec2 out_texture0_coord;"+
		"out vec4 FragColor;"+
		"uniform highp sampler2D u_texture0_sampler;"+
		"void main(void)"+
		"{"+
				"FragColor=texture(u_texture0_sampler, out_texture0_coord);" +
		"}";
	
	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	//Error handling
	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
		
	}//Error handling for fragment Shader 
	
	//Define the Shader Program
	shaderProgramObject=gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	//Before linking the program, initializing the shader variables
	
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");

	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");
	
	//Linking 
	gl.linkProgram(shaderProgramObject);
	//Handling Link Errors
	if(gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS)==false)
	{
		var error=gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}//Shader Program Link Error Handling
	//Post link, tranferring the data to shader's in the form on "uniforms"
	mvpUniform=gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");
	
	//Get texture0_sampler uniform location
	uniform_texture0_sampler=gl.getUniformLocation(shaderProgramObject, "u_texture0_sampler");
	
	//******************************************************************************
	//Load Smiley Texture!!
	//******************************************************************************
	smiley_texture=gl.createTexture();
	smiley_texture.image=new Image();
	smiley_texture.image.src="smiley.png";
	smiley_texture.image.onload=function()
	{
		gl.bindTexture(gl.TEXTURE_2D, smiley_texture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texImage2D(
									gl.TEXTURE_2D,		// Type of texture
									0,									//Mipmap?
									gl.RGBA,						// Image's format
									gl.RGBA,						// The  Pixel format which has already been provided by us
									gl.UNSIGNED_BYTE, //Type of data
									smiley_texture.image //Actual Image 
								);
		
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		
		gl.bindTexture(gl.TEXTURE_2D, null);
	}//smiley texture onload function()
	
	//******************************************************************************
	//Define the: Vao, Vbo, Geometry vertices etc.
	//******************************************************************************
	
	var squareVertices=new Float32Array(
																			[
																				-1.0, 1.0, 0.0, //Left Top
																				-1.0, -1.0, 0.0, //left bottom
																				1.0, -1.0, 0.0, //Right bottom
																			 	1.0, 1.0, 0.0  //Rigth top
																			]											
																		);
		
	var squareTexcoords=new Float32Array(
																				[	
																					0.0, 0.0, 
																					0.0, 1.0,  
																					1.0, 1.0,
																					1.0, 0.0
																				]				
																			  );
	//******************************************************************************
	// Define vao_square 
	//******************************************************************************
	
	vao_square=gl.createVertexArray();
	gl.bindVertexArray(vao_square);
	//******************************************************************************
	//vbo_position
		vbo_position=gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position);
		gl.bufferData(gl.ARRAY_BUFFER, squareVertices, gl.STATIC_DRAW);
	
		//Tikadam-1
		gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,
												3,
												gl.FLOAT,
												false,
												0,
												0
												);
	//Tikadam-3
		gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);	
	gl.bindBuffer(gl.ARRAY_BUFFER,null); 
	//end of vbo_position;
	
	//******************************************************************************
	//vbo_texture
	    vbo_texture=gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture);
		gl.bufferData(gl.ARRAY_BUFFER, squareTexcoords, gl.DYNAMIC_DRAW); //Need dynamic drawing
	
		//Tikadam-1
		gl.vertexAttribPointer( WebGLMacros.VDG_ATTRIBUTE_TEXTURE0,
												2,
												gl.FLOAT,
												false,
												0,
												0
				  							   ); 
	
		//Tikdam 2 
		gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0);
        //end of vbo_color
		gl.bindBuffer(gl.ARRAY_BUFFER, null);
	  gl.bindVertexArray(null);
	
	//Set the background color*********************************************
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	//Depth Options
	gl.enable(gl.DEPTH_TEST);

	//initialize perspective projection matrix
	perspectiveProjectionMatrix=mat4.create();
	
}//init()


function uninitialize()
{
	
	//code
	if(vao_square)
	{
		gl.deleteVertexArray(vao_square);
		vao_square=null;
	}
	if(vbo_position)
	{
		gl.deleteBuffer(vbo_position);
		vbo_position=null;
	}
	if(vbo_texture)
	{
		gl.deleteBuffer(vbo_texture);
		vbo_texture=null;
	}
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
		}
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
		
	}//shaderProgramObject
	
}//uninitialize()