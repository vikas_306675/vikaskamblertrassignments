/******************************************************************************************************
Program: WebGL:: Light::Per Fragment PHONG Lighting Model !!!

//****** Per Fragment Shading *******
# Vertex shader will perform basic operations:
  1. Calculating "Eye Coordinates"
  2. Calculating "Transformed Normals"
  3. Calculating "Viewer Vector"

# Fragment shader will take the o/p given by "Vertex shader" and perform further processing:
 1. Calculate "Ambient", "Diffuse", "Specular" components of the final "PHONG Light"

//****** Geometry: Sphere *********
1. Along X-Axis (Red),
    Along Y-Axis (Green) and
	Along Z-Axis (Blue) Lights will illuminate the "Steady Pyramid" with rotation! 

*******************************************************************************************************/

//****************************************Global variables***************************************//
var canvas=null;
var gl=null;
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;

//Variables representing "shader variables" in our program
const WebGLMacros=
{
	VDG_ATTRIBUTE_VERTEX:0, 
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3
};

//Shader Variables
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

//Uniforms
var modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
var laUniform_red, ldUniform_red, lsUniform_red, red_lightPositionUniform;   //For Light's: ambient, diffuse, specular and position properties.
var laUniform_blue, ldUniform_blue, lsUniform_blue, blue_lightPositionUniform; //For "Blue Light's" ambient, diffuse and specular properties.
var laUniform_greed, ldUniform_green, lsUniform_green, green_lightPositionUniform; //For "Blue Light's" ambient, diffuse and specular properties.


var kaUniform,	kdUniform, ksUniform, materialShinninessUniform;	// For Material's: ambient,diffuse, specular and shinniness properties.
var LKeyPressedUniform; //Enable light on "L" Key Press.

//Boolean Variable for "L" Key Press.
var bLKeyPressed=false;

//Geometry variable
var sphere;

//Angle variable for rotation
var angleOfRotation=0.0;

//Perspective Projection matrix
var perspectiveProjectionMatrix;
//*****************************************************************************************************//
//To Start animation: To have requestAnimationFrame() to be called "cross-browser" compatible.
var requestAnimationFrame=
		window.requestAnimationFrame ||
		window.webkitRequestAnimationFrame ||
		window.mozRequestAnimationFrame ||
		window.oRequestAnimationFrame ||
		window.msRequestAnimationFrame;

//To Stop the animation: To have cancelAnimationFrame() to be called "cross-browser" compatible.
var cancelAnimationFrame=
		window.cancelAnimationFrame ||
		window.webkitCancelRequestAnimationFrame ||
		window.webkitCancelAnimationFrame ||
		window.mozCancelRequestAnimationFrame ||
		window.mozCancelAnimationFrame ||
		window.oCancelRequestAnimationFrame ||
		window.oCancelAnimationFrame ||
		window.msCancelRequestAnimationFrame ||
		window.msCancelAnimationFrame;
//*****************************************Define the Properties of Light and Material****************************//

//Red Light
var red_light_ambient=[0.0, 0.0, 0.0];
var red_light_diffuse=[1.0, 0.0, 0.0];
var red_light_specular=[1.0, 0.0, 0.0];
var red_light_position=[0.0, 0.0, 0.0, 1.0];

//Green Light
var green_light_ambient=[0.0, 0.0, 0.0];
var green_light_diffuse=[0.0, 1.0, 0.0];
var green_light_specular=[0.0, 1.0, 0.0];
var green_light_position=[0.0, 0.0, 0.0, 1.0];

//Blue Light
var blue_light_ambient=[0.0, 0.0, 0.0];
var blue_light_diffuse=[0.0, 0.0, 1.0];
var blue_light_specular=[0.0, 0.0, 1.0];
var blue_light_position=[0.0, 0.0, 0.0, 1.0];

//Material Properties
var material_ambient=[0.0, 0.0, 0.0];
var material_diffuse=[1.0, 1.0, 1.0];
var material_specular=[1.0, 1.0, 1.0];
var material_shinniness=50.0;

//*****************************************onload function:main()*******************************************//
function main()
{
	// Get the Canvas Element from the ID !!
	canvas=document.getElementById("AMC");
	if(!canvas)
		console.log("\n Obtaining canvas failed!! \n");
	else
		console.log("\n Obtaining canvas succeeded!! \n");
	
	//Store the original height and width into global variables.
	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;
	
	//Register the "Keyboard/Mouse" event handlers.
	// Last parameter of the addEventListener() is the special one. 
	// If it is "true" we can trap the event associated with that event!!
	// But we dont want to do it in this program, hence the parameter has been kept "false".
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);
	window.addEventListener("resize",resize,false);
	
	//Initialize function to initialize WebGL!
	init();
	
	//resize function call
	resize();
	
	//draw function call
	draw();
	
}//main()

//***************************************************init()***********************************************//
function init()
{
	//code
	//Get WebGL 2.0 Context
	// This context is nothing but"the GATEWAY" for all the WebGL functions, macro's and what not!
	// As like Windows/Linux/Android -> The function/macro's here do not follow the same style of naming.
	// Ex. glClear() --> it is clear() in webGL. [Note: "gl" is not there and  "Capital C is written in small".]
	// But for our conveniece we have created the context itself of name: "gl" and we will be invoking methods/macros 
	// using this context variable, hence it will look like:
	// gl.clear() --> which at least gives you the feel that, you are invoking the OpenGL functions.
	
	gl=canvas.getContext("webgl2");
	
	if(gl==null)
	{
		console.log("\n Obtaining WebGL Rendenring Context Failed!! \n");
		return; //Terminate the program, as there is no use of proceeding further as no rendering context has been created!!
	}
	
	//After getting the context, define the viewport
	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;
	
	//*********************************************Define the Vertex Shader***************************//
	var vertexShaderSourceCode=
		"#version 300 es"+
		"\n"+
		"in vec4 vPosition;"+
		"in vec3 vNormal;"+
		"uniform mat4 u_model_matrix;"+
		"uniform mat4 u_view_matrix;"+
		"uniform mat4 u_projection_matrix;"+
		"uniform mediump int u_LKeyPressed;"+
		"uniform vec4 u_red_light_position;"+
		"uniform vec4 u_green_light_position;"+
		"uniform vec4 u_blue_light_position;"+
		"out vec3 transformed_normals;"+
		"out vec3 red_light_direction;"+
		"out vec3 green_light_direction;"+
		"out vec3 blue_light_direction;"+
		"out vec3 viewer_vector;"+
		"void main(void)"+
		"{"+
				"if(u_LKeyPressed==1)"+
				"{"+
						"vec4 eyeCoordinates=u_view_matrix * u_model_matrix * vPosition;"+                  // Eye Coordinates
						"transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;"+ 	//Normalize or "transformed normals"
						"red_light_direction=vec3(u_red_light_position) - eyeCoordinates.xyz;"+		// Red Light Direction
						"green_light_direction=vec3(u_green_light_position) - eyeCoordinates.xyz;"+
						"blue_light_direction=vec3(u_blue_light_position)-eyeCoordinates.xyz;"+		//Blue light  direction
						"viewer_vector= -eyeCoordinates.xyz ;"+
				"}"+
				"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
		"}";
	
	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	
	//Compilation Error Handling
	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
		
	}	//Vertex shader compilation error handling
	
	//*************************************************Define Fragment Shader*******************//
	var fragmentShaderSourceCode=
		"#version 300 es"+
		"\n"+
		"precision highp float;"+
		"in vec3 transformed_normals;"+
		"in vec3 red_light_direction;"+
		"in vec3 green_light_direction;"+
		"in vec3 blue_light_direction;"+
		"in vec3 viewer_vector;"+
		"uniform vec3 u_La_red;"+
		"uniform vec3 u_Ld_red;"+
		"uniform vec3 u_Ls_red;"+
		"uniform vec3 u_La_green;"+
		"uniform vec3 u_Ld_green;"+
		"uniform vec3 u_Ls_green;"+
		"uniform vec3 u_La_blue;"+
		"uniform vec3 u_Ld_blue;"+
		"uniform vec3 u_Ls_blue;"+
		"uniform vec3 u_Ka;"+
		"uniform vec3 u_Kd;"+
		"uniform vec3 u_Ks;"+
		"uniform float u_material_shinniness;"+
		"uniform mediump int u_LKeyPressed;"+
		"out vec4 FragColor;"+
		"void main(void)"+
		"{"+
				"vec3 phong_ads_color_red;"+
				"vec3 phong_ads_color_green;"+
				"vec3 phong_ads_color_blue;"+
				"vec3 phong_ads_color;"+
		
				"if(u_LKeyPressed==1)"+
				"{"+
						"vec3 normalized_transformed_normals=normalize(transformed_normals);"+
						
						"vec3 normalized_red_light_direction=normalize(red_light_direction);"+
						"vec3 normalized_green_light_direction=normalize(green_light_direction);"+
						"vec3 normalized_blue_light_direction=normalize(blue_light_direction);"+
						
						"vec3 normalized_viewer_vector=normalize(viewer_vector);"+
						
						"float tn_dot_ld_red_light=max(dot(normalized_transformed_normals, normalized_red_light_direction),0.0);"+
						"float tn_dot_ld_green_light=max(dot(normalized_transformed_normals, normalized_green_light_direction),0.0);"+		
						"float tn_dot_ld_blue_light=max(dot(normalized_transformed_normals, normalized_blue_light_direction),0.0);"+
						
						"vec3 reflection_vector_red_light=reflect(-normalized_red_light_direction, normalized_transformed_normals);"+
						"vec3 reflection_vector_green_light=reflect(-normalized_green_light_direction, normalized_transformed_normals);"+
						"vec3 reflection_vector_blue_light=reflect(-normalized_blue_light_direction, normalized_transformed_normals);"+
						
						"vec3 ambient=u_La_red * u_Ka;"+
						"vec3 diffuse=u_Ld_red * u_Kd * tn_dot_ld_red_light;"+ //
						"vec3 specular=u_Ls_red * u_Ks * pow(max(dot(reflection_vector_red_light, normalized_viewer_vector),0.0), u_material_shinniness);"+
						
						"phong_ads_color_red=ambient + diffuse + specular;"+
			
						"ambient=u_La_green * u_Ka;"+
						"diffuse=u_Ld_green * u_Kd * tn_dot_ld_green_light;"+ //
						"specular=u_Ls_green* u_Ks * pow(max(dot(reflection_vector_green_light, normalized_viewer_vector),0.0), u_material_shinniness);"+
						
						"phong_ads_color_green=ambient + diffuse + specular;"+
				
						"ambient=u_La_blue * u_Ka;"+
						"diffuse=u_Ld_blue * u_Kd * tn_dot_ld_blue_light;"+
						"specular=u_Ls_blue * u_Ks * pow(max(dot(reflection_vector_blue_light, normalized_viewer_vector), 0.0),u_material_shinniness);"+
						
						"phong_ads_color_blue=ambient + diffuse + specular;"+
			
						"phong_ads_color=phong_ads_color_red +phong_ads_color_green+ phong_ads_color_blue;"+
				"}"+
				"else"+
				"{"+
						"phong_ads_color=vec3(1.0, 1.0, 1.0);"+
				"}"+
				"FragColor=vec4(phong_ads_color, 1.0);" +
		"}";
	
	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	//Error handling
	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
		
	}//Error handling for fragment Shader 
	
	//***************************************Define the Shader Program***********************//
	shaderProgramObject=gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	//Tikadam-2
	//Before linking the program, initializing the shader variables
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");
	
	//Linking 
	gl.linkProgram(shaderProgramObject);
	//Handling Link Errors
	if(gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS)==false)
	{
		var error=gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}//Shader Program Link Error Handling
	//Post link, tranferring the data to shader's in the form on "uniforms!!"
	//Get Model Matrix Uniform Location
	modelMatrixUniform=gl.getUniformLocation(shaderProgramObject, "u_model_matrix");
	//Get Model Matrix Uniform Location
	viewMatrixUniform=gl.getUniformLocation(shaderProgramObject, "u_view_matrix");
	//Get Projection Matrix Uniform Location
	projectionMatrixUniform=gl.getUniformLocation(shaderProgramObject, "u_projection_matrix");
	//Get Uniform location for "L" Key Press!
	LKeyPressedUniform=gl.getUniformLocation(shaderProgramObject, "u_LKeyPressed");
	
	//******************** Properties of Light ********************************************************//
	
	// Get Uniform location for "Ambient property of Light".
	laUniform_red=gl.getUniformLocation(shaderProgramObject, "u_La_red");
	laUniform_green=gl.getUniformLocation(shaderProgramObject, "u_La_green");
	laUniform_blue=gl.getUniformLocation(shaderProgramObject, "u_La_blue");
	
	// Get Uniform location for "Diffuse property of Light".
	ldUniform_red=gl.getUniformLocation(shaderProgramObject, "u_Ld_red");
	ldUniform_green=gl.getUniformLocation(shaderProgramObject, "u_Ld_green");
	ldUniform_blue=gl.getUniformLocation(shaderProgramObject, "u_Ld_blue");
	
	// Get Uniform location for "Specular property of Light".
	lsUniform_red=gl.getUniformLocation(shaderProgramObject, "u_Ls_red");
	lsUniform_green=gl.getUniformLocation(shaderProgramObject, "u_Ls_green");
	lsUniform_blue=gl.getUniformLocation(shaderProgramObject, "u_Ls_blue");
	
	// Get Uniform location for position of the light!
	red_lightPositionUniform=gl.getUniformLocation(shaderProgramObject, "u_red_light_position");
	green_lightPositionUniform=gl.getUniformLocation(shaderProgramObject, "u_green_light_position");
	blue_lightPositionUniform=gl.getUniformLocation(shaderProgramObject, "u_blue_light_position");
	
	//******************** Properties of Material ********************************************************//

	// Get Uniform location for "Ambient Intensity of material"!
	kaUniform=gl.getUniformLocation(shaderProgramObject, "u_Ka");
	
	// Get Uniform location for "Diffuse Reflective Color Intensity of material"!
	kdUniform=gl.getUniformLocation(shaderProgramObject, "u_Kd");
	
	// Get Uniform location for "Specular property of material"!
	ksUniform=gl.getUniformLocation(shaderProgramObject, "u_Ks");
	
	// Get Uniform location for "Shinniness of material"!
	materialShinninessUniform=gl.getUniformLocation(shaderProgramObject, "u_material_shinniness");
	
	
	//************************Define the: Vao, Vbo, Geometry:: vertices, Color Normals etc. *****************//
	
	sphere=new Mesh();
	makeSphere(sphere,2.0, 30, 30);
	
	
	//Set the background color*********************************************
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	//Depth Options
	gl.enable(gl.DEPTH_TEST);

	//Depth Test to Do!
	gl.depthFunc(gl.LEQUAL);
	
	//CULL the Back Face!
	//gl.enable(gl.CULL_FACE);
	
	//initialize perspective projection matrix
	perspectiveProjectionMatrix=mat4.create();
	
}//init()

//******************************************Resize()function ************************************************//
function resize()
{
	if(bFullscreen==true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;
	}
	//set the viewport
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	//Perspective projection==>(target matrix, fov angle, aspect ration, near, far)
	mat4.perspective(perspectiveProjectionMatrix, 45.0, (parseFloat)(canvas.width)/(parseFloat)(canvas.height), 0.1, 100.0);
}//resize()

//**************************************************** draw() *********************************************//
function draw()
{
	//1
	gl.clear(gl.COLOR_BUFFER_BIT || gl.DEPTH_BUFFER_BIT || gl.STENCIL_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
	if(bLKeyPressed==true) //Means "Light enable" karaychi aahe, so sarw values pathwaychya aahet!
	{
		//Philyanda Light enable kar sangnara uniform pathwala!
		gl.uniform1i(LKeyPressedUniform, 1); 
			
		//Provide the uniforms depicting the properties of the light for "Red and Blue Light"!!
		gl.uniform3fv(laUniform_red, red_light_ambient); //Ambient intensity of light
		gl.uniform3fv(ldUniform_red, red_light_diffuse); //Diffuse intensity of light
		gl.uniform3fv(lsUniform_red, red_light_specular); //Specular intensity of light
		red_light_position[1]=10.0 * Math.sin(angleOfRotation);
		red_light_position[2]=10.0 * Math.cos(angleOfRotation);
		
		console.log("\n red[1]= \t red[2]=  \n", red_light_position[1], red_light_position[2]);
		
		gl.uniform4fv(red_lightPositionUniform, red_light_position); //Light's postion
		
		//For "Green Light"
		gl.uniform3fv(laUniform_green, green_light_ambient); //Ambient intensity of light
		gl.uniform3fv(ldUniform_green, green_light_diffuse); //Diffuse intensity of light
		gl.uniform3fv(lsUniform_green, green_light_specular); //Specular intensity of light
		
		green_light_position[0]=10.0 * Math.sin(angleOfRotation); 
		green_light_position[2]=10.0 * Math.cos(angleOfRotation); 
		
		console.log("\n green[1]= \t green[2]=  \n", green_light_position[0], green_light_position[2]);

		gl.uniform4fv(green_lightPositionUniform, green_light_position); //Light's postion
		
		//For "Blue Light"
		gl.uniform3fv(laUniform_blue, blue_light_ambient); //Ambient intensity of light
		gl.uniform3fv(ldUniform_blue, blue_light_diffuse); //Diffuse intensity of light
		gl.uniform3fv(lsUniform_blue, blue_light_specular); //Specular intensity of light

		blue_light_position[0]=10.0 * Math.sin(angleOfRotation);
		blue_light_position[1]=10.0 * Math.cos(angleOfRotation);
		
		console.log("\n blue[1]= \t blue[2]=  \n", blue_light_position[0], blue_light_position[1]);

		
		gl.uniform4fv(blue_lightPositionUniform, blue_light_position); //Light's postion
		
		//Setting material properties
		gl.uniform3fv(kaUniform, material_ambient); //Ambient property of material
		gl.uniform3fv(kdUniform, material_diffuse); //Diffuse reflectivity of the material.
		gl.uniform3fv(ksUniform, material_specular); //Specular property of material
		gl.uniform1f(materialShinninessUniform, material_shinniness); //Shinniness property of the material	
	}
	else
	{		
			gl.uniform1i(LKeyPressedUniform, 0); //Jyamule, default CUBE with "Light" and "Grey/White" color will be displayed.
	}
		//** Drawing(Ya common goshti aahet jya light "enabled asel" or "nasel" pathwaychya aahetch!****************************//
		var modelMatrix=mat4.create();
		var viewMatrix=mat4.create();
	
		//Translate	
		mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0] );
		
	   //To Project(which is done by shader in this case) pass the modelViewMatrix to "Vertex Shader"(he is waiting for that :) 
		gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
		gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
		gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	
		//*****************************Draw The Sphere!*****************************//
		
		sphere.draw();
		
		//*****************************End of drawing SPHERE!!!*****************************//
		
	
		gl.useProgram(null);
	
		angleOfRotation=angleOfRotation + 0.05;
		if(angleOfRotation > 360.0)
			angleOfRotation=0.0;
	
    	//animation loop!
	   requestAnimationFrame(draw,canvas);
	
}//draw()

//************************************ keyDown() *******************************************************//
function keyDown(event)
{
	switch(event.keyCode)
	{
		 case 70: //For 'f' or 'F'
					
					toggleFullscreen();
		
					break;
		
		case 27: //escape
					
					//uninitialize()
					uninitialize();
					
					//Close Application's tab: This will not work with "Firefox" but will work with Chrome/Safari
					window.close();
					
					break;
			
		case 76:// Light: 'l' or 'L'	
					
					if(bLKeyPressed==false)
					{
						bLKeyPressed=true;
					}
				else
					{
						bLKeyPressed=false;
					}
					break;
	}//switch(event.keyCode)
}//keyDown()

//************************************************** mouseDown()******************************************//
function mouseDown()
{
	//code
	
}

//**********************************************toggleFullscreen()******************************************//
function toggleFullscreen()
{
	//code
	var fullscreen_element=
		document.fullscreenElement ||
		document.webkitFullscreenElement ||
		document.mozFullScreenElement ||
		document.msFullscreenElement ||
		null;
	
	//if not in fullscreen mode, switch to it!
	if(fullscreen_element==null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
	
		bFullscreen=true;
		
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		
		bFullscreen=false;
	}
	
}//toggleFullscreen()

//*************************************************** Uninitialize()*************************************//
function uninitialize()
{

	if(sphere)
	{
		sphere.deallocate();
		sphere=null;
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
		}
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
		
	}//shaderProgramObject
	
}//uninitialize()