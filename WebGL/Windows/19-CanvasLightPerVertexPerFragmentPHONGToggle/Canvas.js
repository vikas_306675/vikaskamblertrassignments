/******************************************************************************************************
Program: WebGL:: Light::Per Vertex and Per Fragment PHONG Lighting Model Combined !!!
*******************************************************************************************************/

//****************************************Global variables***************************************//
var canvas=null;
var gl=null;
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;

//Variables representing "shader variables" in our program
const WebGLMacros=
{
	VDG_ATTRIBUTE_VERTEX:0, 
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3
};

//Shader Variables
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;;

//Uniforms
var modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
var laUniform, ldUniform, lsUniform, lightPositionUniform;   //For Light's: ambient, diffuse, specular and position properties.
var kaUniform,	kdUniform, ksUniform, materialShinninessUniform;	// For Material's: ambient,diffuse, specular and shinniness properties.
var LKeyPressedUniform; //Enable light on "L" Key Press.
var FKeyPressedUniform; //Enable "Fragment PHONG Lighting" if "f" key pressed.

//Boolean Variable for "L" Key Press.
var bLKeyPressed=false;
var bFKeyPressed=false;
var bVKeyPressed=false;

//Sphere Variable
var sphere;

//Perspective Projection matrix
var perspectiveProjectionMatrix;
//*****************************************************************************************************//
//To Start animation: To have requestAnimationFrame() to be called "cross-browser" compatible.
var requestAnimationFrame=
		window.requestAnimationFrame ||
		window.webkitRequestAnimationFrame ||
		window.mozRequestAnimationFrame ||
		window.oRequestAnimationFrame ||
		window.msRequestAnimationFrame;

//To Stop the animation: To have cancelAnimationFrame() to be called "cross-browser" compatible.
var cancelAnimationFrame=
		window.cancelAnimationFrame ||
		window.webkitCancelRequestAnimationFrame ||
		window.webkitCancelAnimationFrame ||
		window.mozCancelRequestAnimationFrame ||
		window.mozCancelAnimationFrame ||
		window.oCancelRequestAnimationFrame ||
		window.oCancelAnimationFrame ||
		window.msCancelRequestAnimationFrame ||
		window.msCancelAnimationFrame;
//*****************************************Define the Properties of Light and Material****************************//
var light_ambient=[0.0, 0.0, 0.0];
var light_diffuse=[1.0, 1.0, 1.0];
var light_specular=[1.0, 1.0, 1.0];
var light_position=[100.0, 100.0, 100.0, 1.0];

var material_ambient=[0.0, 0.0, 0.0];
var material_diffuse=[1.0, 1.0, 1.0];
var material_specular=[1.0, 1.0, 1.0];
var material_shinniness=50.0;

//*****************************************onload function:main()*******************************************//
function main()
{
	// Get the Canvas Element from the ID !!
	canvas=document.getElementById("AMC");
	if(!canvas)
		console.log("\n Obtaining canvas failed!! \n");
	else
		console.log("\n Obtaining canvas succeeded!! \n");
	
	//Store the original height and width into global variables.
	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;
	
	//Register the "Keyboard/Mouse" event handlers.
	// Last parameter of the addEventListener() is the special one. 
	// If it is "true" we can trap the event associated with that event!!
	// But we dont want to do it in this program, hence the parameter has been kept "false".
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);
	window.addEventListener("resize",resize,false);
	
	//Initialize function to initialize WebGL!
	init();
	
	//resize function call
	resize();
	
	//draw function call
	draw();
	
}//main()

//***************************************************init()***********************************************//
function init()
{
	//code
	//Get WebGL 2.0 Context
	// This context is nothing but"the GATEWAY" for all the WebGL functions, macro's and what not!
	// As like Windows/Linux/Android -> The function/macro's here do not follow the same style of naming.
	// Ex. glClear() --> it is clear() in webGL. [Note: "gl" is not there and  "Capital C is written in small".]
	// But for our conveniece we have created the context itself of name: "gl" and we will be invoking methods/macros 
	// using this context variable, hence it will look like:
	// gl.clear() --> which at least gives you the feel that, you are invoking the OpenGL functions.
	
	gl=canvas.getContext("webgl2");
	
	if(gl==null)
	{
		console.log("\n Obtaining WebGL Rendenring Context Failed!! \n");
		return; //Terminate the program, as there is no use of proceeding further as no rendering context has been created!!
	}
	
	//After getting the context, define the viewport
	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;
	
	//*********************************************Define the Vertex Shader***************************//
	var vertexShaderSourceCode=
		"#version 300 es"+
		"\n"+
		"in vec4 vPosition;"+
		"in vec3 vNormal;"+
		"uniform mat4 u_model_matrix;"+
		"uniform mat4 u_view_matrix;"+
		"uniform mat4 u_projection_matrix;"+
		"uniform mediump int u_LKeyPressed;"+
		"uniform mediump int u_FKeyPressed;"+
		"uniform vec3 u_La;"+
		"uniform vec3 u_Ld;"+
		"uniform vec3 u_Ls;"+
		"uniform vec4 u_light_position;"+
		"uniform vec3 u_Ka;"+
		"uniform vec3 u_Kd;"+
		"uniform vec3 u_Ks;"+
		"uniform float u_material_shinniness;"+
		"out vec3 transformed_normals;"+
		"out vec3 light_direction;"+
		"out vec3 viewer_vector;"+
		"out vec3 vertex_phong_ads_color;"+
		"void main(void)"+
		"{"+
				"if(u_LKeyPressed==1)"+
				"{"+
						"if(u_FKeyPressed==1)"+ //Follow Frament PHONG Shading model
						"{"+
								"vec4 eyeCoordinates=u_view_matrix * u_model_matrix * vPosition;"+                  // Eye Coordinates
								"transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"+ 	//Normalize or "transformed normals"
								"light_direction=normalize(vec3(u_light_position) - eyeCoordinates.xyz);"+
								"viewer_vector=normalize(-eyeCoordinates.xyz);"+
						"}"+
						"else"+
						"{"+
								"vec4 eyeCoordinates=u_view_matrix * u_model_matrix * vPosition;"+                  // Eye Coordinates
								"vec3 transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"+ 	//Normalize or "transformed normals"
								"vec3 light_direction=normalize(vec3(u_light_position) - eyeCoordinates.xyz);"+
								"float tn_dot_ld=max(dot(transformed_normals, light_direction),0.0);"+
								"vec3 ambient=u_La * u_Ka;"+						//ambient = La * Ka;		
								"vec3 diffuse=u_Ld * u_Kd * tn_dot_ld;"+ //Diffuse=Ld * Kd * (dot product of transformed normals and light direction)
								"vec3 reflection_vector=reflect(-light_direction, transformed_normals);"+ //Note: -ve light_direction(Since vector coming out of surface of obejct)
								"vec3 viewer_vector=normalize(-eyeCoordinates.xyz);"+
								"vec3 specular=u_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector),0.0), u_material_shinniness);"+ 					//specular=Ls * Ks * dot(reflection vector, viewer_vector)^material_shinniness;
								"vertex_phong_ads_color=ambient + diffuse + specular;"+
							"}"+
				"}"+
				"else"+
				"{"+
						"vertex_phong_ads_color=vec3(1.0, 1.0, 1.0);"+
				"}"+
				"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
		"}";
	
	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	
	//Compilation Error Handling
	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
		
	}	//Vertex shader compilation error handling
	
	//*************************************************Define Fragment Shader*******************//
	var fragmentShaderSourceCode=
		"#version 300 es"+
		"\n"+
		"precision highp float;"+
		"in vec3 vertex_phong_ads_color;"+
		"in vec3 transformed_normals;"+
		"in vec3 light_direction;"+
		"in vec3 viewer_vector;"+
		"uniform vec3 u_La;"+
		"uniform vec3 u_Ld;"+
		"uniform vec3 u_Ls;"+
		"uniform vec3 u_Ka;"+
		"uniform vec3 u_Kd;"+
		"uniform vec3 u_Ks;"+
		"uniform float u_material_shinniness; "+
		"uniform int u_FKeyPressed;"+
		"out vec4 FragColor;"+
		"void main(void)"+
		"{"+
				"vec3 phong_ads_color;"+
				"if(u_FKeyPressed==1)"+
				"{"+
						"vec3 normalized_transformed_normals=normalize(transformed_normals);"+
						"vec3 normalized_light_direction=normalize(light_direction);"+
						"vec3 normalized_viewer_vector=normalize(viewer_vector);"+

						"vec3 ambient=u_La * u_Ka;"+
						"float tn_dot_ld=max(dot(normalized_transformed_normals,normalized_light_direction),0.0);"+
						"vec3 diffuse=u_Ld * u_Kd * tn_dot_ld;"+
						"vec3 reflection_vector=reflect(-normalized_light_direction,normalized_transformed_normals);"+
						"vec3 specular=u_Ls * u_Ks * pow(max(dot(reflection_vector,normalized_viewer_vector),0.0),u_material_shinniness);"+
	
						"phong_ads_color=ambient + diffuse + specular;"+
				"}"+
				"else"+
				"{"+
		
				"phong_ads_color=vertex_phong_ads_color;" +
		
				"}"+
		
				"FragColor=vec4(phong_ads_color, 1.0);"+
		"}";
	
	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	//Error handling
	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
		
	}//Error handling for fragment Shader 
	
	//***************************************Define the Shader Program***********************//
	shaderProgramObject=gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	//Tikadam-2
	//Before linking the program, initializing the shader variables
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");
	
	//Linking 
	gl.linkProgram(shaderProgramObject);
	//Handling Link Errors
	if(gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS)==false)
	{
		var error=gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}//Shader Program Link Error Handling
	//Post link, tranferring the data to shader's in the form on "uniforms!!"
	//Get Model Matrix Uniform Location
	modelMatrixUniform=gl.getUniformLocation(shaderProgramObject, "u_model_matrix");
	//Get Model Matrix Uniform Location
	viewMatrixUniform=gl.getUniformLocation(shaderProgramObject, "u_view_matrix");
	//Get Projection Matrix Uniform Location
	projectionMatrixUniform=gl.getUniformLocation(shaderProgramObject, "u_projection_matrix");
	//Get Uniform location for "L" Key Press!
	LKeyPressedUniform=gl.getUniformLocation(shaderProgramObject, "u_LKeyPressed");
	
	FKeyPressedUniform=gl.getUniformLocation(shaderProgramObject, "u_FKeyPressed");
	//******************** Properties of Light ********************************************************//
	
	// Get Uniform location for "Ambient property of Light".
	laUniform=gl.getUniformLocation(shaderProgramObject, "u_La");

	// Get Uniform location for "Diffuse property of Light".
	ldUniform=gl.getUniformLocation(shaderProgramObject, "u_Ld");
	
	// Get Uniform location for "Specular property of Light".
	lsUniform=gl.getUniformLocation(shaderProgramObject, "u_Ls");
	
	// Get Uniform location for position of the light!
	lightPositionUniform=gl.getUniformLocation(shaderProgramObject, "u_light_position");
	
	//******************** Properties of Material ********************************************************//

	// Get Uniform location for "Ambient Intensity of material"!
	kaUniform=gl.getUniformLocation(shaderProgramObject, "u_Ka");
	
	// Get Uniform location for "Diffuse Reflective Color Intensity of material"!
	kdUniform=gl.getUniformLocation(shaderProgramObject, "u_Kd");
	
	// Get Uniform location for "Specular property of material"!
	ksUniform=gl.getUniformLocation(shaderProgramObject, "u_Ks");
	
	// Get Uniform location for "Shinniness of material"!
	materialShinninessUniform=gl.getUniformLocation(shaderProgramObject, "u_material_shinniness");
	
	
	//************************Define the: Vao, Vbo, Geometry:: vertices, Color Normals etc. *****************//
	
	sphere=new Mesh();
	makeSphere(sphere,2.0, 30, 30);
	
	
	//Set the background color*********************************************
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	//Depth Options
	gl.enable(gl.DEPTH_TEST);

	//Depth Test to Do!
	gl.depthFunc(gl.LEQUAL);
	
	//CULL the Back Face!
	//gl.enable(gl.CULL_FACE);
	
	//initialize perspective projection matrix
	perspectiveProjectionMatrix=mat4.create();
	
}//init()

//******************************************Resize()function ************************************************//
function resize()
{
	if(bFullscreen==true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;
	}
	//set the viewport
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	//Perspective projection==>(target matrix, fov angle, aspect ration, near, far)
	mat4.perspective(perspectiveProjectionMatrix, 45.0, (parseFloat)(canvas.width)/(parseFloat)(canvas.height), 0.1, 100.0);
}//resize()

//**************************************************** draw() *********************************************//
function draw()
{
	//1
	gl.clear(gl.COLOR_BUFFER_BIT || gl.DEPTH_BUFFER_BIT || gl.STENCIL_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
	if(bLKeyPressed==true) //Means "Light enable" karaychi aahe, so sarw values pathwaychya aahet!
	{
		if(bFKeyPressed==true) //Send the uniform for enabling per fragment phong lighting.
		{
			
			gl.uniform1i(FKeyPressedUniform,1);
			
		}
		else
		{
			gl.uniform1i(FKeyPressedUniform,0);
			
		}
		
			
		//Philyanda Light enable kar sangnara uniform pathwala!
		gl.uniform1i(LKeyPressedUniform, 1); 
			
		//Provide the uniforms depicting the properties of the light
		gl.uniform3fv(laUniform, light_ambient); //Ambient intensity of light
		gl.uniform3fv(ldUniform, light_diffuse); //Diffuse intensity of light
		gl.uniform3fv(lsUniform, light_specular); //Specular intensity of light
		gl.uniform4fv(lightPositionUniform, light_position); //Light's postion
		
		//Setting material properties
		gl.uniform3fv(kaUniform, material_ambient); //Ambient property of material
		gl.uniform3fv(kdUniform, material_diffuse); //Diffuse reflectivity of the material.
		gl.uniform3fv(ksUniform, material_specular); //Specular property of material
		gl.uniform1f(materialShinninessUniform, material_shinniness); //Shinniness property of the material	
	}
	else
	{		
			gl.uniform1i(LKeyPressedUniform, 0); //Jyamule, default CUBE with "Light" and "Grey/White" color will be displayed.
			gl.uniform1i(FKeyPressedUniform, 0);
			bFKeyPressed=false;
	}
		//** Drawing(Ya common goshti aahet jya light "enabled asel" or "nasel" pathwaychya aahetch!****************************//
		var modelMatrix=mat4.create();
		var viewMatrix=mat4.create();
	
		//Translate	
		mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0] );
		
	   //To Project(which is done by shader in this case) pass the modelViewMatrix to "Vertex Shader"(he is waiting for that :) 
		gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
		gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
		gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	
		//*****************************Draw The Sphere!*****************************//
		
		sphere.draw();
		
		//*****************************End of drawing SPHERE!!!*****************************//
		
		gl.useProgram(null);
	
    	//animation loop!
	   requestAnimationFrame(draw,canvas);

}//draw()

//************************************ keyDown() *******************************************************//
function keyDown(event)
{
	switch(event.keyCode)
	{
		 case 69: //For 'e' or 'E'
					
					toggleFullscreen();
		
					break;
		case 70: //For 'f' or 'F' for Per Fragment Phong.
					if( bFKeyPressed==false)
					{
						bFKeyPressed=true;
						bVKeyPressed=false;
					}
					else
					{
						//bFKeyPressed=false;
					}
					break;
		case 86: //For 'v' or 'V' for Per Vertex PHONG.
					if(bVKeyPressed==false)
					{
						bVKeyPressed=true;
						bFKeyPressed=false; //Disable the "Fragment PHONG boolean variable, which leads to vertex PHONG code".
					}	
					else
					{
						
					}
					break;	
		case 27: //escape
					
					//uninitialize()
					uninitialize();
					
					//Close Application's tab: This will not work with "Firefox" but will work with Chrome/Safari
					window.close();
					
					break;
			
		case 76:// Light: 'l' or 'L'	
					
					if(bLKeyPressed==false)
					{
						bLKeyPressed=true;
					}
				else
					{
						bLKeyPressed=false;
					}
					break;
	}//switch(event.keyCode)
}//keyDown()

//************************************************** mouseDown()******************************************//
function mouseDown()
{
	//code
	
}

//**********************************************toggleFullscreen()******************************************//
function toggleFullscreen()
{
	//code
	var fullscreen_element=
		document.fullscreenElement ||
		document.webkitFullscreenElement ||
		document.mozFullScreenElement ||
		document.msFullscreenElement ||
		null;
	
	//if not in fullscreen mode, switch to it!
	if(fullscreen_element==null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
	
		bFullscreen=true;
		
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		
		bFullscreen=false;
	}
	
}//toggleFullscreen()

//*************************************************** Uninitialize()*************************************//
function uninitialize()
{

	if(sphere)
	{
		sphere.deallocate();
		sphere=null;
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
		}
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
		
	}//shaderProgramObject
	
}//uninitialize()