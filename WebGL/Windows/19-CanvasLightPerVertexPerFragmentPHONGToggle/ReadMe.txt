# HELP:
========
- Press L/l-> To Enable Lighting.
- Press E/e-> To Enable "Full screen".
- Press F/f-> To Enable Per Fragment PHONG.


##Note:
=====
# Below 3 files have been copied from previous(Diffused Light) program!!
1. Canvas.html
2. Canvas.js
3. gl-matrix-min.js 

# Below 2 files have been provided by Sir!!
1. Mesh.temp2
2. Sphere.temp2

## Sir's Expectation, which I want to surely fulfill:
1. Study Sphere.temp2 and Mesh.temp2 and try to create your own
   - DLL or Library of all the "Shapes"(Cube, Torus, Sphere, Cone etc. of geometrix objects
   - Use your own created "shapes" in your project, DO NOT COPY FROM ELSEWHERE!!