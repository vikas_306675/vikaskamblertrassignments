/******************************************************************************************************
Program: WebGL:: Light::Diffused Light!!
*******************************************************************************************************/
//*************************************************Global variables***************************************//
var canvas=null;
var gl=null;
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;

//Variables representing "shader variables" in our program
const WebGLMacros=
{
	VDG_ATTRIBUTE_VERTEX:0, 
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3
};

//Shader Variables
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

//Define Vao, Vbo and Uniform Variables
var vao_cube;
var vbo_cube_position;
var vbo_cube_normal;

//Uniforms
var mvpUniform;
var modelViewMatrixUniform, projectionMatrixUniform;
var ldUniform, kdUniform, lightPositionUniform;
var LKeyPressedUniform; //Enable light on "L" Key Press.

//Angle for Cube's Rotation
var angleCube=0.0;

//Boolean Variable for "L" Key Press.
var bLKeyPressed=false;

//Perspective Projection matrix
var perspectiveProjectionMatrix;

//To Start animation: To have requestAnimationFrame() to be called "cross-browser" compatible.
var requestAnimationFrame=
		window.requestAnimationFrame ||
		window.webkitRequestAnimationFrame ||
		window.mozRequestAnimationFrame ||
		window.oRequestAnimationFrame ||
		window.msRequestAnimationFrame;

//To Stop the animation: To have cancelAnimationFrame() to be called "cross-browser" compatible.
var cancelAnimationFrame=
		window.cancelAnimationFrame ||
		window.webkitCancelRequestAnimationFrame ||
		window.webkitCancelAnimationFrame ||
		window.mozCancelRequestAnimationFrame ||
		window.mozCancelAnimationFrame ||
		window.oCancelRequestAnimationFrame ||
		window.oCancelAnimationFrame ||
		window.msCancelRequestAnimationFrame ||
		window.msCancelAnimationFrame;

//*****************************************onload function:main()*******************************************//
function main()
{
	// Get the Canvas Element from the ID !!
	canvas=document.getElementById("AMC");
	if(!canvas)
		console.log("\n Obtaining canvas failed!! \n");
	else
		console.log("\n Obtaining canvas succeeded!! \n");
	
	//Store the original height and width into global variables.
	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;
	
	//Register the "Keyboard/Mouse" event handlers.
	// Last parameter of the addEventListener() is the special one. 
	// If it is "true" we can trap the event associated with that event!!
	// But we dont want to do it in this program, hence the parameter has been kept "false".
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);
	window.addEventListener("resize",resize,false);
	
	//Initialize function to initialize WebGL!
	init();
	
	//resize function call
	resize();
	
	//draw function call
	draw();
	
}//main()

//***************************************************init()***********************************************//
function init()
{
	//code
	//Get WebGL 2.0 Context
	// This context is nothing but"the GATEWAY" for all the WebGL functions, macro's and what not!
	// As like Windows/Linux/Android -> The function/macro's here do not follow the same style of naming.
	// Ex. glClear() --> it is clear() in webGL. [Note: "gl" is not there and  "Capital C is written in small".]
	// But for our conveniece we have created the context itself of name: "gl" and we will be invoking methods/macros 
	// using this context variable, hence it will look like:
	// gl.clear() --> which at least gives you the feel that, you are invoking the OpenGL functions.
	
	gl=canvas.getContext("webgl2");
	
	if(gl==null)
	{
		console.log("\n Obtaining WebGL Rendenring Context Failed!! \n");
		return; //Terminate the program, as there is no use of proceeding further as no rendering context has been created!!
	}
	
	//After getting the context, define the viewport
	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;
	
	//*********************************************Define the Vertex Shader***************************//
	var vertexShaderSourceCode=
		"#version 300 es"+
		"\n"+
		"in vec4 vPosition;"+
		"in vec3 vNormal;"+
		"uniform mat4 u_model_view_matrix;"+
		"uniform mat4 u_projection_matrix;"+
		"uniform mediump int u_LKeyPressed;"+
		"uniform vec3 u_Ld;"+
		"uniform vec3 u_Kd;"+
		"uniform vec4 u_light_position;"+
		"out vec3 diffuse_light;"+
		"void main(void)"+
		"{"+
				"if(u_LKeyPressed==1)"+
				"{"+
						"vec4 eyeCoordinates=u_model_view_matrix * vPosition;"+                  // Eye Coordinates
						"vec3 tnorm=normalize(mat3(u_model_view_matrix) * vNormal);"+ 	//Normalize or "transformed normals"
						"vec3 s=normalize(vec3(u_light_position-eyeCoordinates));"+
						"diffuse_light=u_Ld * u_Kd * max(dot(s, tnorm),0.0);"+
				"}"+
				"gl_Position=u_projection_matrix * u_model_view_matrix * vPosition;"+
		"}";
	
	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	
	//Compilation Error Handling
	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
		
	}	//Vertex shader compilation error handling
	
	//*************************************************Define Fragment Shader*******************//
	var fragmentShaderSourceCode=
		"#version 300 es"+
		"\n"+
		"precision highp float;"+
		"in vec3 diffuse_light;"+
		"out vec4 FragColor;"+
		"uniform int u_LKeyPressed;"+
		"void main(void)"+
		"{"+
				"vec4 color;"+
				"if(u_LKeyPressed==1)"+
				"{"+	
						"color=vec4(diffuse_light, 1.0);"+
				"}"+
				"else"+
				"{"+	
						"color=vec4(1.0, 1.0, 1.0, 1.0);"+
				"}"+
				"FragColor=color;" +
		"}";
	
	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	//Error handling
	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
		
	}//Error handling for fragment Shader 
	
	//***************************************Define the Shader Program***********************//
	shaderProgramObject=gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	//Tikadam-2
	//Before linking the program, initializing the shader variables
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");
	
	//Linking 
	gl.linkProgram(shaderProgramObject);
	//Handling Link Errors
	if(gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS)==false)
	{
		var error=gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}//Shader Program Link Error Handling
	//Post link, tranferring the data to shader's in the form on "uniforms!!"
	//Get Model View Matrix Uniform Location
	modelViewMatrixUniform=gl.getUniformLocation(shaderProgramObject, "u_model_view_matrix");
	//Get Projection Matrix Uniform Location
	projectionMatrixUniform=gl.getUniformLocation(shaderProgramObject, "u_projection_matrix");
	//Get Uniform location for "L" Key Press!
	LKeyPressedUniform=gl.getUniformLocation(shaderProgramObject, "u_LKeyPressed");
	// Get Uniform location for "Diffuse Color Intensity of Light".
	ldUniform=gl.getUniformLocation(shaderProgramObject, "u_Ld");
	// Get Uniform location for "Diffuse Reflective Color Intensity of material"!
	kdUniform=gl.getUniformLocation(shaderProgramObject, "u_Kd");
	// Get Uniform location for position of the light!
	lightPositionUniform=gl.getUniformLocation(shaderProgramObject, "u_light_position");
	
	
	//************************Define the: Vao, Vbo, Geometry:: vertices, Color Normals etc. *****************//
	var cubeVertices=new Float32Array(
																		[	
																			//Top Surface
																			1.0, 1.0, -1.0, //Top's Right Top
																			-1.0, 1.0, -1.0, //Top's Left Top
																			-1.0, 1.0, 1.0, //Top's Left Bottom
																			1.0, 1.0, 1.0,  //Top's Right bottom
																		
																			//Bottom Surface
																			1.0, -1.0, -1.0, //Bottom's Right Top
																			-1.0, -1.0, -1.0, //Bottom's Left Top
																			-1.0, -1.0, 1.0, //Bottom's Left Bottom
																			1.0, -1.0, 1.0, //Bottom's Right Bottom
																			
																			// Front Face
																			1.0, 1.0, 1.0, 
																			-1.0, 1.0, 1.0,
																			-1.0, -1.0, 1.0,
																			1.0, -1.0, 1.0,
																			
																			//Left Face
																			-1.0, 1.0, 1.0,
																			-1.0, 1.0, -1.0,		
																			-1.0, -1.0, -1.0,
																			-1.0, -1.0, 1.0,
																			
																			//Back Face
																			-1.0, 1.0, -1.0,
																			 1.0, 1.0, -1.0,
																			 1.0, -1.0, -1.0,
																			-1.0, -1.0, -1.0,
																			
																			//Right Face
																			1.0, 1.0, -1.0,
																			1.0, 1.0, 1.0,
																			1.0, -1.0, 1.0,
																			1.0, -1.0, -1.0
																		] );
	
	//Note: Normal is nothing but a parpendicular to the surface!
	var cubeNormals=new Float32Array(
																	[
																			//Top 
																			0.0, 1.0, 0.0,
																			0.0, 1.0, 0.0,
																			0.0, 1.0, 0.0,
																			0.0, 1.0, 0.0,
		
																			//Bottom
																			0.0, -1.0, 0.0,
																			0.0, -1.0, 0.0,
																			0.0, -1.0, 0.0,
																			0.0, -1.0, 0.0,
																			
																			//front
																			0.0, 0.0, 1.0,
																			0.0, 0.0, 1.0,
																			0.0, 0.0, 1.0,
																			0.0, 0.0, 1.0,
		
																			//Left
																			-1.0, 0.0, 0.0,
																			-1.0, 0.0, 0.0,
																			-1.0, 0.0, 0.0,
																			-1.0, 0.0, 0.0,
																			
																			//Back
																			0.0, 0.0, -1.0,
																			0.0, 0.0, -1.0,
																			0.0, 0.0, -1.0,
																			0.0, 0.0, -1.0,
		
																			//Right
																			1.0, 0.0, 0.0,
																			1.0, 0.0, 0.0,
																			1.0, 0.0, 0.0,
																			1.0, 0.0, 0.0,
		
																	]);	
	
	//************** ***************************Define vao_cube ******************************//
	vao_cube=gl.createVertexArray();
	gl.bindVertexArray(vao_cube);
	
	//vbo_cube_position
	vbo_cube_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_cube_position);
	gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
	
	//tikadam-1
	gl.vertexAttribPointer( 	WebGLMacros.VDG_ATTRIBUTE_VERTEX,
												3, //3 is for (x,y,z) coordinates of vertex position
												gl.FLOAT,
												false,
												0, 
												0	
											);
	
	//Tikadam-3
	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	//vbo_cube_normal
	vbo_cube_normal=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_cube_normal);
	gl.bufferData(gl.ARRAY_BUFFER, cubeNormals, gl.STATIC_DRAW);

	//Tikadam-1
	gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_NORMAL,
											3, //3 if for (x,y,z) coordinates for a normal
											gl.FLOAT,
											false,
											0,
											0
											);
	//Tikadam-3
	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_NORMAL)
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	gl.bindVertexArray(null); //End  of Vao for Cube!!
	
	 //Set the background color*********************************************
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	//Depth Options
	gl.enable(gl.DEPTH_TEST);

	//Depth Test to Do!
	gl.depthFunc(gl.LEQUAL);
	
	//CULL the Back Face!
	//gl.enable(gl.CULL_FACE);
	
	//initialize perspective projection matrix
	perspectiveProjectionMatrix=mat4.create();
	
}//init()

//******************************************Resize()function ************************************************//
function resize()
{
	if(bFullscreen==true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;
	}
	//set the viewport
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	//Perspective projection==>(target matrix, fov angle, aspect ration, near, far)
	mat4.perspective(perspectiveProjectionMatrix, 45.0, (parseFloat)(canvas.width)/(parseFloat)(canvas.height), 0.1, 100.0);
}//resize()

//**************************************************** draw() *********************************************//
function draw()
{
	//1
	gl.clear(gl.COLOR_BUFFER_BIT || gl.DEPTH_BUFFER_BIT || gl.STENCIL_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
	if(bLKeyPressed==true) //Means "Light enable" karaychi aahe, so sarw values pathwaychya aahet!
	{
		//Philyanda Light enable kar sangnara uniform pathwala!
		gl.uniform1i(LKeyPressedUniform, 1); 
			
		//Setup light Properties
		gl.uniform3f(ldUniform, 1.0, 1.0, 1.0); //Diffuse intensity of light
		
		//Setting material properties
		gl.uniform3f(kdUniform, 0.5, 0.5, 0.5); //Diffuse reflectivity of the material.
		
		//Define the light  position
		var lightPosition=[0.0, 0.0, 2.0, 1.0];	
		
		gl.uniform4fv(lightPositionUniform, lightPosition); //Light's postion
			
	}
	else
	{		
			gl.uniform1i(LKeyPressedUniform, 0); //Jyamule, default CUBE with "Light" and "Grey/White" color will be displayed.
	}
		//** Drawing(Ya common goshti aahet jya light "enabled asel" or "nasel" pathwaychya aahetch!****************************//
		var modelViewMatrix=mat4.create();
		
		//Translate	
		mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -4.0] );
		
		//Rotate the CUBE Across all the axes!
		mat4.rotateX(modelViewMatrix, modelViewMatrix, degToRadians(angleCube)); //Rotation along "X-axis"
		mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRadians(angleCube)); //Rotation along "Y-axis"
		mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRadians(angleCube)); //Rotation along "Z-axis"
	
	   //To Project(which is done by shader in this case) pass the modelViewMatrix to "Vertex Shader"(he is waiting for that :) 
		gl.uniformMatrix4fv(modelViewMatrixUniform, false, modelViewMatrix);
		gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	
		//*****************************Bind vao_normal_square and draw it!*****************************//
		gl.bindVertexArray(vao_cube);
	
				//Draw the Cube either using: gl.drawArrays() or gl.drawElements() or gl.drawTriangles()!
				// Actually if we look closely, 2 triangles for 1 "square" so total vertices for 2 Triangles=3x2=6.
				// But if we observe closely, 2 vertices are common amongst the 2 triangles, so effective vertices 
				// are: 6-2=4 (Hence we are jumping across using 4 vertices).
				gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
				gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
				gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
				gl.drawArrays(gl.TRIANGLE_FAN, 12,4);
				gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
				gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
		
		 gl.bindVertexArray(null);
		//*****************************End of drawing CUBE!!!*****************************//
		
		gl.useProgram(null);
		
		// Increment the angleCube here itself/write a separate function and call from here.
		update();
	
    	//animation loop!
	   requestAnimationFrame(draw,canvas);

}//draw()

//**************************************************update() ********************************************//
function update()
{
	angleCube=angleCube + 1.0;
	if(angleCube > 360.0)
		angleCube=0.0;
}//update()

//*************************************************** degToRadians()************************************//
// Note: We directly receive parameter into "degress" which is not even declared of type "var" ! (Seems to be implicit type conversion as the passed parameter is already of type "var")!
// Note: Even though the function returns something, there is no traditional "return type" to the function!
function degToRadians(degrees)
{
	//code
	return (degrees * Math.PI/180);
} //degToRadians()

//************************************ keyDown() *******************************************************//
function keyDown(event)
{
	switch(event.keyCode)
	{
		 case 70: //For 'f' or 'F'
					
					toggleFullscreen();
		
					break;
		
		case 27: //escape
					
					//uninitialize()
					uninitialize();
					
					//Close Application's tab: This will not work with "Firefox" but will work with Chrome/Safari
					window.close();
					
					break;
			
		case 76:// Light: 'l' or 'L'	
					
					if(bLKeyPressed==false)
					{
						bLKeyPressed=true;
					}
				else
					{
						bLKeyPressed=false;
					}
					break;
	}//switch(event.keyCode)
}//keyDown()

//************************************************** mouseDown()******************************************//
function mouseDown()
{
	//code
	
}

//**********************************************toggleFullscreen()******************************************//
function toggleFullscreen()
{
	//code
	var fullscreen_element=
		document.fullscreenElement ||
		document.webkitFullscreenElement ||
		document.mozFullScreenElement ||
		document.msFullscreenElement ||
		null;
	
	//if not in fullscreen mode, switch to it!
	if(fullscreen_element==null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
	
		bFullscreen=true;
		
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		
		bFullscreen=false;
	}
	
}//toggleFullscreen()

//*************************************************** Uninitialize()*************************************//
function uninitialize()
{
	
	//code
	if(vao_cube)
	{
		gl.deleteVertexArray(vao_cube);
		vao_cube=null;
	}
	
	if(vbo_cube_position)
	{
		gl.deleteBuffer(vbo_cube_position);
		vbo_cube_position=null;
	}
	if(vbo_cube_normal)
	{
		gl.deleteBuffer(vbo_cube_normal);
		vbo_cube_normal=null;
	}
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
		}
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
		
	}//shaderProgramObject
	
}//uninitialize()