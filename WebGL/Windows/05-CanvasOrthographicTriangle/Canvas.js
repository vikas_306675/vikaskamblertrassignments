/**************************************************************
//Program: Canvas:: Triangle With Orthographics Projection!!!
**************************************************************/

//Global Variables
var canvas=null;    // "null" is literally "null" on the contrary to Windows where it is #define null 0
var gl=null;				//OpenGL Context (We will catch into this variable)
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;

//Shader Attribute Variables in our program
const WebGLMacros= //When whole 'WebGLMacros' is 'const', all inside it are automatically 'const'
{
	VDG_ATTRIBUTE_VERTEX: 0,
	VDG_ATTRIBUTE_COLOR: 1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3
};

//Shader Objects.
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

//Vao and Vbo (Supported from WebGL 2.0)
var vao;
var vbo;
var mvpUniform;

//Orthographic Projection Matrix
var orthographicProjectionMatrix;

//To Start animation: To have requestAnimationFrame() to be called "cross-browser" compatible.
var requestAnimationFrame=
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame;

//To Stop animation: To have cancelAnimationFrame() to be called "cross-browser" compatible.
var cancelAnimationFrame=
	window.cancelAnimationFrame ||
	window.webkitCancelRequestAnimationFrame ||
	window.webkitCancelAnimationFrame ||
	window.mozCancelRequestAnimationFrame ||
	window.mozCancelAnimationFrame ||
	window.oCancelRequestAnimationFrame ||
	window.oCancelAnimationFrame ||
	window.msCancelRequestAnimationFrame ||
	window.msCancelAnimationFrame;


//onload function
function main()
{
	//get <canvas> element through the ID of it.
	canvas=document.getElementById("AMC");
	if(!canvas)
		console.log("\n Obtaining Canvas Failed!\n");
	else
		console.log("\n Obtaining Canvas Succeeded!\n");
	
	//Store these global variables to original values of width and height, which will be used during resize code
	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;
	
	//Register keyboard's "keydown" event handler
	// The last parameter is the special one, which if "true" will help us grab that event ! Parent will also get notified 
	// if that event occurs, but we don't want to do that, hence it is kept as "false".
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);
	window.addEventListener("resize",resize,false);
	
	//initialize WebGL [Sir has maintained the parity of the coding style across platform by doing this and other functions]
	init();
	
	//Start drawing here as warming-up
	resize();
	
	draw();
	
}//onload()

function init()
{
	//code
	//Get WebGL 2.0 Context
	// This context is nothing but"the GATEWAY" for all the WebGL functions, macro's and what not!
	// As like Windows/Linux/Android -> The function/macro's here do not follow the same style of naming
	// Ex. glClear() --> it is clear() in webGL.
	// But for our conveniece we have created the context itself of name: "gl" and we will be invoking methods/macros 
	// using this context variable, hence it will look like:
	// gl.clear() --> which at least gives you the feel that, you are invoking the OpenGL functions.
	
	gl=canvas.getContext("webgl2");
	
	if(gl==null)	//Failed to get the context 
	{
		console.log("\n Failed to get the rendering context for WenGL!\n");
		return; 	//As nothing can be done if we didn't receive the context itself.
	}
	// Once the context has been received-->Set the viewport width and height
	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;
	
	//Vertex Shader
	var vertexShaderSourceCode=
		"#version 300 es"+
		"\n"+
		"in vec4 vPosition;"+
		"uniform mat4 u_mvp_matrix;"+
		"void main(void)"+
		"{"+
		"gl_Position = u_mvp_matrix * vPosition;"+
		"}";
	
	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	//Error handling after vertex shader compilation
	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//Fragment shader
	var fragmentShaderSourceCode=
		"#version 300 es"+
		"\n"+
		"precision highp float;"+
		"out vec4 FragColor;"+
		"void main(void)"+
		"{"+
			"FragColor = vec4 (1.0, 1.0, 1.0, 1.0);"+
		"}";
	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	//Error handling for Fragment Shader Compilation
	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//Shader Program Object
	shaderProgramObject=gl.createProgram();
	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);
	
	//Pre-Link binding of shader program object with vertex shader attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
	
	//Linking of the ShaderProgram
	gl.linkProgram(shaderProgramObject);
		
	//Error handling for linking
	if(! gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error=gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	//Post link: get MVP uniform location
	mvpUniform=gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");
	
	/////////////////////// Geometry vertices, colors, shader attributes, vao, vbo initializations//////////////////////
	var triangleVertices=new Float32Array(	 [
																			   0.0, 50.0, 0.0,      // Apex 
																			 -50.0, -50.0, 0.0, //Left bottom		
																			   50.0, -50.0, 0.0 //Right bottom
																			] 
																		);
	//Vao
	vao=gl.createVertexArray();
	gl.bindVertexArray(vao);
	
	//vbo
	vbo=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo);
	gl.bufferData(gl.ARRAY_BUFFER, triangleVertices, gl.STATIC_DRAW);
	
	//Tikadam-1
	gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX, 
											3, //for (x,y,z) co-ordinates in our triangleVertices array
											gl.FLOAT,
											false,
											0,
											0
											);
	//Tikadam-3
	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);
	
	//Set the Clear color to "Blue"
	gl.clearColor(0.0, 0.0, 1.0, 1.0); //blue
	
	//initialize the projection matrix
	orthographicProjectionMatrix=mat4.create();
	
}//init()

function resize()
{
	//Code
	// There is a challenge for this piece of code when we will have 24 sphere assignment where
	// Top and bottom row's will leave more than expected margin from the screen bezel.
	// Sir has ordered to work on the logic to make it uniform (in looking) though it comes to be hard coded 
	// logic.
	if(bFullscreen==true) //Application is already "fullscreen"-->Resize it to inner width and height of canvas!
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else //Application is not in fullscreen-->Make it fullscreen/switch to original width and height
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;
	}
	//set the viewport to match, since there is not default "repaint()" or "redraw()" function supported in Javascript.
	gl.viewport(0,0,canvas.width,canvas.height);
		
	//Orthographic Projection==> (Left, right, bottom, up, near, far) 
	if(canvas.width <= canvas.height)
	{
		mat4.ortho(orthographicProjectionMatrix, -100.0, 100.0, (-100.0 * (canvas.height / canvas.width )), (100.0 * (canvas.height / canvas.width)), -100.0, 100.0);
	}
	else
	{
		mat4.ortho(orthographicProjectionMatrix, -100.0, 100.0, (-100.0 * (canvas.width / canvas.height)), (100.0 * (canvas.width / canvas.height)), -100.0, 100.0);
	}

}//resize()


function draw()
{
	//code
	// Following the naming convention in Java script: GL_COLOR_BUFFER_BIT-->Remove GL_ part and rest of the name
	// can be used.
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
		var modelViewMatrix=mat4.create();
		var modelViewProjectionMatrix=mat4.create();
		
		mat4.multiply(modelViewProjectionMatrix, orthographicProjectionMatrix, modelViewMatrix);
		gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
		//Vao binding for displaying geometry
		gl.bindVertexArray(vao);
			
			gl.drawArrays(gl.TRIANGLES,0,3);

	    gl.bindVertexArray(null);

	gl.useProgram(null);
	
	//animation loop!
	requestAnimationFrame(draw, canvas);
}//draw()

function toggleFullscreen()
{
	//code
	var fullscreen_element=
		document.fullscreenElement || 
		document.webkitFullscreenElement ||
		document.mozFullScreenElement ||
		document.msFullscreenElement ||
		null;
	
	//if not fullscreen
	if(fullscreen_element==null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen=true;
	}
	else //if fullscreen already exists
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullscreen=false;
	}		
	
}//toggleFullscreen()

function keyDown(event)
{
	switch(event.keyCode)
	{
		case 70: //For 'F' or 'f'
					  toggleFullscreen();
					  break;
			
		case 27: //escape	
						//uninitialize
						uninitialize();
						//Close our application's tab
						window.close(); //May not work in FireFox but works for Safari and Chrome
						break;	
	}
}//keyDown()

function uninitialize()
{
	//code
	if(vao)
	{
		gl.deleteVertexArray(vao);
		vao=null;
	}
	if(vbo)
	{
		gl.deleteBuffer(vbo);
		vbo=null;
	}
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
		}
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
	}
}//uninitialize()

function mouseDown()
{
	//code
	
}//mouseDown()

function uninitialize()
{
	//code
	
}//uninitialize()
