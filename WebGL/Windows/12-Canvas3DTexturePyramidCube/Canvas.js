/**********************************************************************
Program: Rotating 3D Textured Pyramid and Cube !!
**********************************************************************/
//Global variables
var canvas=null;
var gl=null;						//For WebGL Context
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;
var anglePyramid=0.0;
var angleCube=0.0;

//Texture related variables
var pyramid_texture=0;
var cube_texture=0;
var unifrom_texture0_sampler=0;

//Shader Variables
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

//Variables representing "shader variables" in our program
// When whole 'WebGLMacros' is 'const', all inside it are automatically 'const'.
const WebGLMacros=
{
	VDG_ATTRIBUTE_VERTEX:0, 
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3,
};

//Define Vao, Vbo and Uniform Variables
var vao_pyramid;
var vao_cube;
var vbo_position;
var vbo_texture;
var mvpUniform;

//Perspective Projection matrix
var perspectiveProjectionMatrix;

//To Start animation: To have requestAnimationFrame() to be called "cross-browser" compatible.
var requestAnimationFrame=
		window.requestAnimationFrame ||
		window.webkitRequestAnimationFrame ||
		window.mozRequestAnimationFrame ||
		window.oRequestAnimationFrame ||
		window.msRequestAnimationFrame;

//To Stop the animation: To have cancelAnimationFrame() to be called "cross-browser" compatible.
var cancelAnimationFrame=
		window.cancelAnimationFrame ||
		window.webkitCancelRequestAnimationFrame ||
		window.webkitCancelAnimationFrame ||
		window.mozCancelRequestAnimationFrame ||
		window.mozCancelAnimationFrame ||
		window.oCancelRequestAnimationFrame ||
		window.oCancelAnimationFrame ||
		window.msCancelRequestAnimationFrame ||
		window.msCancelAnimationFrame;


//onload function
function main()
{
	// Get the Canvas from the ID 
	canvas=document.getElementById("AMC");
	if(!canvas)
		console.log("\n Obtaining canvas failed!! \n");
	else
		console.log("\n Obtaining canvas succeeded!! \n");
	
	//Store the original height and width into global variables
	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;
	
	//Register the "Keyboard/Mouse" event handlers.
	// Last parameter of the addEventListener() is the special one. 
	// If it is "true" we can trap the event associated with that event!!
	// But we dont want to do it in this program, hence the parameter has been kept "false".
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);
	window.addEventListener("resize",resize,false);
	
	//Initialize function 
	init();
	
	//resize function call
	resize();
	
	//draw function call
	draw();
	
}//main()

function toggleFullscreen()
{
	//code
	var fullscreen_element=
		document.fullscreenElement ||
		document.webkitFullscreenElement ||
		document.mozFullScreenElement ||
		document.msFullscreenElement ||
		null;
	
	//if not in fullscreen mode, switch to it!
	if(fullscreen_element==null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
	
		bFullscreen=true;
		
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		
		bFullscreen=false;
	}
	
}//toggleFullscreen()

function keyDown(event)
{
	switch(event.keyCode)
	{
		case 70: //For 'f' or 'F'
					 toggleFullscreen();
					 break;
		case 27: //escape
					//uninitialize()
					uninitialize();
					//Close Application's tab
					window.close();
					break;
	}//switch(event.keyCode)
}//keyDown()

function mouseDown()
{
	//code
	
}

function draw()
{
	//1
	gl.clear(gl.COLOR_BUFFER_BIT || gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
		var modelViewMatrix=mat4.create();
		var modelViewProjectionMatrix=mat4.create();
		var translationMatrix=mat4.create();
		var rotationMatrix=mat4.create();
	
		//Draw Triangle
		//Translate
		mat4.translate(translationMatrix, translationMatrix, [-1.5, 0.0, -6.0]);
		mat4.multiply(modelViewMatrix, translationMatrix, modelViewMatrix);
		//Rotate
		mat4.rotateY(rotationMatrix, rotationMatrix, degreeToRadians(anglePyramid) );
		mat4.multiply(modelViewMatrix, modelViewMatrix, rotationMatrix);	
		// Note: In above statement, sequence if reversed like:
		// mat4.multiply(modelViewMatrix, rotationMatrix, modelViewMatrix); It will lead to Triangle 
	   // going further translating out of the screen(you can check :P), so sequence matters and should be 
	   // aligned with the one mentioned above.
		//Project
		mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
		gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
		
		//bind the texture
	    gl.bindTexture(gl.TEXTURE_2D, pyramid_texture);
		gl.uniform1i(uniform_texture0_sampler, 0);
	
		//Bind to vao_pyramid and display the triangle
		gl.bindVertexArray(vao_pyramid);
	
				gl.drawArrays(gl.TRIANGLES,0,12);		
	
		gl.bindVertexArray(null);
		
		//Draw Cube
		mat4.identity(modelViewMatrix);
		mat4.identity(modelViewProjectionMatrix);
		mat4.identity(translationMatrix);
		mat4.identity(rotationMatrix);
		//Translate	
		mat4.translate(translationMatrix, translationMatrix, [1.5, 0.0, -6.0] );
		mat4.multiply(modelViewMatrix, translationMatrix, modelViewMatrix);
		//Rotate-Cube along all Axes.
		mat4.rotateX(rotationMatrix, rotationMatrix, degreeToRadians(angleCube));
		mat4.rotateY(rotationMatrix, rotationMatrix, degreeToRadians(angleCube));
		mat4.rotateZ(rotationMatrix, rotationMatrix, degreeToRadians(angleCube));
		mat4.multiply(modelViewMatrix,modelViewMatrix, rotationMatrix); //Sequence matters
		//Project
		mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
		gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	
		//bind with the texture: If you failed to do so, it lead lead to "Andhaar".
		gl.bindTexture(gl.TEXTURE_2D, cube_texture);
		gl.uniform1i(uniform_texture0_sampler, 0);	
	
	   //Bind vao_cube and draw it!
		gl.bindVertexArray(vao_cube);
				gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
				gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
				gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
				gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
				gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
				gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
		gl.bindVertexArray(null);
	
	gl.useProgram(null);
	//call to update() function
	update();
	
	//animation loop!
	requestAnimationFrame(draw,canvas);
}//draw()

function update()
{
	anglePyramid=anglePyramid + 1.0;
	
	if(anglePyramid> 360.0)
		anglePyramid=0.0;
	
	angleCube=angleCube + 1.0;
	
	if(angleCube > 360.0)
		angleCube=0.0;
	
}//update()

function degreeToRadians(angleInDegrees)
{
	
	return(angleInDegrees * Math.PI/180.0);
	
}//degrees()

function resize()
{
	if(bFullscreen==true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;
	}
	//set the viewport
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	//Perspective projection==>(target matrix, fov angle, aspect ration, near, far)
	mat4.perspective(perspectiveProjectionMatrix, 45.0, (parseFloat)(canvas.width)/(parseFloat)(canvas.height), 0.1, 100.0);
}//resize()


function init()
{
	//code
	//Get WebGL 2.0 Context
	// This context is nothing but"the GATEWAY" for all the WebGL functions, macro's and what not!
	// As like Windows/Linux/Android -> The function/macro's here do not follow the same style of naming
	// Ex. glClear() --> it is clear() in webGL.
	// But for our conveniece we have created the context itself of name: "gl" and we will be invoking methods/macros 
	// using this context variable, hence it will look like:
	// gl.clear() --> which at least gives you the feel that, you are invoking the OpenGL functions.
	
	gl=canvas.getContext("webgl2");
	
	if(gl==null)
	{
		console.log("\n Obtaining WebGL Rendenring Context Failed!! \n");
		return; //Terminate the program, as there is no use of proceeding further as no rendering context has been created!!
	}
	
	//After getting the context, define the viewport
	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;
	
	//Define the Vertex Shader
	var vertexShaderSourceCode=
		"#version 300 es"+
		"\n"+
		"in vec4 vPosition;"+
		"in vec2 vTexture0_Coord;"+
		"out vec2 out_texture0_coord;"+
		"uniform mat4 u_mvp_matrix;"+
		"void main(void)"+
		"{"+
				"gl_Position=u_mvp_matrix * vPosition;"+
				"out_texture0_coord=vTexture0_Coord;"+
		"}";
	
	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	
	//Compilation Error Handling
	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
		
	}	//Vertex shader compilation error handling
	
	//Define Fragment Shader
	var fragmentShaderSourceCode=
		"#version 300 es"+
		"\n"+
		"precision highp float;"+
		"in vec2 out_texture0_coord;"+
		"uniform highp sampler2D u_texture0_sampler;"+
		"out vec4 FragColor;"+
		"void main(void)"+
		"{"+
				"FragColor=texture(u_texture0_sampler, out_texture0_coord);" +
		"}";
	
	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	//Error handling
	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
		
	}//Error handling for fragment Shader 
	
	//Define the Shader Program
	shaderProgramObject=gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	//Before linking the program, initializing the shader variables
	
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");

	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");
	
	//Linking 
	gl.linkProgram(shaderProgramObject);
	//Handling Link Errors
	if(gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS)==false)
	{
		var error=gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}//Shader Program Link Error Handling
	//Post link, tranferring the data to shader's in the form on "uniforms"
	mvpUniform=gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");
	
	//************** Load the Texture from the Images ********************//
	// Pyramid
	pyramid_texture=gl.createTexture();// Note the change: genTexture()-->createTexture()
	pyramid_texture.image=new Image(); //Image is the inner class
	pyramid_texture.image.src="stone.png"	;
	pyramid_texture.image.onload=function() //This is as good as "Lambda" in C++.
	{
		gl.bindTexture(gl.TEXTURE_2D, pyramid_texture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texImage2D(
									gl.TEXTURE_2D, //Type of the texture 
									0,              // Mip-Map's Level of  details(LOD)
									gl.RGBA, // Internal format of the image
									gl.RGBA, // The format which we give in our program  
									gl.UNSIGNED_BYTE, //Image's data type
									pyramid_texture.image //Actual image data
								);
		//Note: "Image class" actually ful-filled the "Width, Height, BorderWidth" characteristics of Image.
		// When you have ready made image-->You can go ahead with above call, but when there is a raw image data in
		// "bytes" you may need to specify the "Width, height and border width" to bound those bytes in the form of texture.
		
		gl.texParameteri(
									gl.TEXTURE_2D, 
									gl.TEXTURE_MAG_FILTER,
									gl.NEAREST
									);
			
		gl.texParameteri(
									 gl.TEXTURE_2D, 
									gl.TEXTURE_MIN_FILTER,
									gl.NEAREST
									);
		
		gl.bindTexture(gl.TEXTURE_2D, null); //"Passing null" as a parameter is the way we "un-bind" in JavaScript.
		
	}//onload()
	//Load the texture for CUBE!!
	cube_texture=gl.createTexture();
	cube_texture.image=new Image(); //Getting a memory for "Texture object"(which is nothing but returned by gl.createTexture())" does not mean "Image class's object got the memory.
	cube_texture.image.src="Vijay_Kundali.png";
	cube_texture.image.onload=function()
	{
		gl.bindTexture(gl.TEXTURE_2D, cube_texture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texImage2D(	
										gl.TEXTURE_2D, //Texture image type
										0, 						// Mip-Map LOD level	
										gl.RGBA,				// Internal format of the image
										gl.RGBA,				// The Pixel format which we specify.
										gl.UNSIGNED_BYTE,		//Actual type of the image's data
										cube_texture.image		// Actual image data
								);
		gl.texParameteri(
										gl.TEXTURE_2D,
										gl.TEXTURE_MAG_FILTER,
										gl.NEAREST
										);
		gl.texParameteri(
										gl.TEXTURE_2D,
										gl.TEXTURE_MIN_FILTER,
										gl.NEAREST
									 );
		gl.bindTexture(gl.TEXTURE_2D, null);	
		
	}//End of Cube texture's onload() code.
	//************************** Get texture0 sampler uniform location ******//
	
	uniform_texture0_sampler=gl.getUniformLocation(shaderProgramObject, "u_texture0_sampler");
	
	//****************************Define the: Vao, Vbo, Geometry vertices etc.*****//
	var pyramidVertices=new Float32Array(
																		 [
																			//Front Face
																			0.0, 1.0, 0.0,
																			-1.0, -1.0, 1.0,
																			1.0, -1.0, 1.0,
			
																			//Left Face																	
																			0.0, 1.0, 0.0,
																		    -1.0, -1.0, -1.0,
																			-1.0, -1.0, 1.0,
		
																			//Back Face
																			0.0, 1.0, 0.0,
																			1.0, -1.0, -1.0,
																			-1.0, -1.0, -1.0,
																							
																			//Right face
																			0.0, 1.0, 0.0,
																			1.0, -1.0, 1.0,
																			1.0, -1.0, -1.0
																		 ]								
																		);	
	
	var pyramidTexcoords=new Float32Array(
																					[
																						0.5, 1.0, //front-top						
																						1.0, 0.0, //front-left
																						0.0, 0.0, //front-right
																								
																						0.5, 1.0, //Right-top
																						1.0, 0.0, //Right-left
																						0.0, 0.0, //Right-right
																							
																						0.5, 1.0, //back-top
																						1.0, 0.0,	// back-left
																						0.0, 0.0, //back-right
									
																						0.5, 1.0, //Left-Top
																						1.0, 0.0,   //Left-Left			
																						0.0, 0.0  //Left-Right
																					]		
																				);
	var cubeVertices=new Float32Array(
																			[
																				//Front Face
																				-1.0, 1.0, 1.0, //Left Top
																				-1.0, -1.0, 1.0, //left bottom
																				1.0, -1.0, 1.0, //Right bottom
																				1.0, 1.0, 1.0,  //Rigth top
																				
																				//Left Face
																				-1.0, 1.0, -1.0,
																				-1.0, -1.0, -1.0,
																				-1.0, -1.0, 1.0,
																				-1.0, 1.0, 1.0,
																				
																				//Back Face
																				 1.0, 1.0, -1.0,
																				1.0, -1.0, -1.0,
																				-1.0, -1.0, -1.0,
																				-1.0, 1.0, -1.0,
																				
																				//Right Face
																				1.0, 1.0, 1.0,
																				1.0, -1.0, 1.0,
																				1.0, -1.0, -1.0,
																				1.0, 1.0, -1.0,
																				
																				//Top Face	
																			  -1.0,1.0, -1.0,
																			  -1.0, 1.0, 1.0,
																				1.0, 1.0, 1.0,
																				1.0, 1.0, -1.0,
		
																				//Bottom Face
																				-1.0, -1.0, -1.0,
																				-1.0, -1.0, 1.0,
																				1.0, -1.0, 1.0,
																				1.0, -1.0, -1.0
																			]											
																		);
		
	//In above -1.0 or +1.0 Values which make CUBE much larger than Pyramid, then follow the code in the following
	// loop which will convert all 1's and -1's to -0.75 or +0.75.
	for(var i=0; i<72; i++) //[3 components of a vertex * number of vertices].
	{
		if(cubeVertices[i] < 0.0 )
			cubeVertices[i]=cubeVertices[i] + 0.25;
		else if(cubeVertices[i]> 0.0)
			cubeVertices[i]=cubeVertices[i] - 0.25;
		else
			cubeVertices[i]=cubeVertices[i]; //no-change		
		
	}//for()
	
	var cubeTexcoords=new Float32Array(
																				[	
																					//Front Face (Refer Y flipped image, not the normal image)					
																					0.0, 0.0, //Left Top
																					1.0, 0.0, //Left Bottom
																					1.0, 1.0, //Right Bottom 
																					0.0, 1.0, //Right Top
				
																					//Left Face
																					0.0, 0.0,
																					1.0, 0.0,
																					1.0, 1.0,
																					0.0, 1.0,
																					
																					//Back face
																					0.0, 0.0,
																					1.0, 0.0,
																					1.0, 1.0,
																					0.0, 1.0,
		
																					//Right Face							
																					0.0, 0.0, 
																				    1.0, 0.0,
																					1.0, 1.0,
																				    0.0, 1.0,
										
																					//Top Face								
																					0.0, 0.0,
																					1.0, 0.0,
																					1.0, 1.0,
																					0.0, 1.0,
		
																					//Bottom Face
																					0.0, 0.0,
																				    1.0, 0.0,
																					1.0, 1,0,
																					0.0, 1.0			
																				]				
																			  );
	
	
	//************** Define vao_pyramid ******************************//
	vao_pyramid=gl.createVertexArray();
	gl.bindVertexArray(vao_pyramid);
	//vbo_position
		vbo_position=gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position);
		gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
	
		//Tikadam-1
		gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,
												3,
												gl.FLOAT,
												false,
												0,
												0
												);
	//Tikadam-3
		gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);	
	gl.bindBuffer(gl.ARRAY_BUFFER,null); 
	//end of vbo_position;
		
		//vbo_texture
		vbo_texture=gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture);
		gl.bufferData(gl.ARRAY_BUFFER, pyramidTexcoords, gl.STATIC_DRAW);
	
		//Tikadam-1
		gl.vertexAttribPointer( WebGLMacros.VDG_ATTRIBUTE_TEXTURE0,
												2,
												gl.FLOAT,
												false,
												0,
												0
				  							   ); 
	
		//Tikdam 2 
		gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0);
        //end of vbo_color
		gl.bindBuffer(gl.ARRAY_BUFFER, null);
	  gl.bindVertexArray(null); //End of vao_pyramid
	
	//********************** Define vao_cube ********************//
	vao_cube=gl.createVertexArray();
	gl.bindVertexArray(vao_cube);
		//vbo_position for square vertices
		vbo_position=gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position);
		gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
	
		//Tikadam-1
		gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,
												3, //(x,y,z) co-ordinates make up a single point/vertex hence this parameter is:3
												gl.FLOAT,
												false,
												0,
												0
												);
	//Tikadam-3
	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
	//unbind vbo
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	//vbo_texture for CUBE
		vbo_texture=gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER,vbo_texture);
		gl.bufferData(gl.ARRAY_BUFFER, cubeTexcoords, gl.STATIC_DRAW);
	
		//Tikadam-1
		gl.vertexAttribPointer( WebGLMacros.VDG_ATTRIBUTE_TEXTURE0,
												2,
												gl.FLOAT,
												false,
												0,
												0
				  							   ); 
	
		//Tikdam 2 
		gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0);
	//unbind vbo_texture
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	//unbind vao_cube
	gl.bindVertexArray(null);
	
	// Enable Depth Statements *******************************************
	//gl.shadeModel(gl.SMOOTH); //Smooth Shading
	
	gl.clearDepth(1.0); //Set up the "Depth Buffer"
	
	gl.enable(gl.DEPTH_TEST); //Enable Depth Testing
	
	gl.depthFunc(gl.LEQUAL); //Which depth test to do?
	
	//***********************************************************************
	//WebGL--> Chrome browser is throwing a warning for this line of code though the program behaves correctly.
	// Chrome browser says, "Canvas.js:589 WebGL: INVALID_ENUM: hint: invalid target".
	// Commenting this line will help get rid of that warning.
	// AI: Need to understand why the warning has been thrown?
	
	gl.hint(gl.PERSPECTIVE_CORRECTION_HINT, gl.NICEST); //Set really nice perspective calculations
	//***********************************************************************
	//gl.enable(gl.CULL_FACE); //We will always CULL back faces for better performance.
	// Uncommenting this line will cause one of the "Cube Face" be black, due to "CULLING" 
	// enablement. Since the geometry is rotating here, to improve perf we enable CULLING, but
	// in our case, this seems to be not required. (It is already not required if we refer same program
	// code on "Windows Platform".
	//***********************************************************************
	//Set the background color*********************************************
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	//initialize perspective projection matrix
	perspectiveProjectionMatrix=mat4.create();
	
}//init()


function uninitialize()
{
	
	//code
	if(vao_pyramid)
	{
		gl.deleteVertexArray(vao_pyramid);
		vao_pyramid=null;
	}	
	if(vao_cube)
	{
		gl.deleteVertexArray(vao_cube);
		vao_cube=null;
	}
	if(vbo_position)
	{
		gl.deleteBuffer(vbo_position);
		vbo_position=null;
	}
	if(vbo_texture)
	{
		gl.deleteBuffer(vbo_texture);
		vbo_texture=null;
	}
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
		}
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
		
	}//shaderProgramObject
	
}//uninitialize()