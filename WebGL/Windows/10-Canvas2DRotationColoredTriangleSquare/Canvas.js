/**********************************************************************
Program: Rotating Multi-Colored Triangle and Corn Colored Square !!
**********************************************************************/
//Global variables
var canvas=null;
var gl=null;
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;
var angleTriangle=0.0;
var angleSquare=0.0;
var angleInDegree;
var angleInRadians;

//Shader Variables
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

//Variables representing "shader variables" in our program
const WebGLMacros=
{
	VDG_ATTRIBUTE_VERTEX:0, 
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3,
};

//Define Vao, Vbo and Uniform Variables
var vao_triangle;
var vao_square;
var vbo_position;
var vbo_color;
var mvpUniform;

//Perspective Projection matrix
var perspectiveProjectionMatrix;

//To Start animation:
var requestAnimationFrame=
		window.requestAnimationFrame ||
		window.webkitRequestAnimationFrame ||
		window.mozRequestAnimationFrame ||
		window.oRequestAnimationFrame ||
		window.msRequestAnimationFrame;

//To Stop the animation
var cancelAnimationFrame=
		window.cancelAnimationFrame ||
		window.webkitCancelRequestAnimationFrame ||
		window.webkitCancelAnimationFrame ||
		window.mozCancelRequestAnimationFrame ||
		window.mozCancelAnimationFrame ||
		window.oCancelRequestAnimationFrame ||
		window.oCancelAnimationFrame ||
		window.msCancelRequestAnimationFrame ||
		window.msCancelAnimationFrame;


//onload function
function main()
{
	// Get the Canvas from the ID 
	canvas=document.getElementById("AMC");
	if(!canvas)
		console.log("\n Obtaining canvas failed!! \n");
	else
		console.log("\n Obtaining canvas succeeded!! \n");
	
	//Store the original height and width into global variables
	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;
	
	//Register the "Keyboard/Mouse" event handlers.
	// Last parameter of the addEventListener() is the special one. 
	// If it is "true" we can trap the event associated with that event!!
	// But we dont want to do it in this program, hence the parameter has been kept "false".
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);
	window.addEventListener("resize",resize,false);
	
	//Initialize function 
	init();
	
	//resize function call
	resize();
	
	//draw function call
	draw();
	
}//main()

function toggleFullscreen()
{
	//code
	var fullscreen_element=
		document.fullscreenElement ||
		document.webkitFullscreenElement ||
		document.mozFullScreenElement ||
		document.msFullscreenElement ||
		null;
	
	//if not in fullscreen mode, switch to it!
	if(fullscreen_element==null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
	
		bFullscreen=true;
		
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		
		bFullscreen=false;
	}
	
}//toggleFullscreen()

function keyDown(event)
{
	switch(event.keyCode)
	{
		case 70: //For 'f' or 'F'
					 toggleFullscreen();
					 break;
		case 27: //escape
					//uninitialize()
					uninitialize();
					//Close Application's tab
					window.close();
					break;
	}//switch(event.keyCode)
}//keyDown()

function mouseDown()
{
	//code
	
}

function draw()
{
	//1
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
		var modelViewMatrix=mat4.create();
		var modelViewProjectionMatrix=mat4.create();
		var translationMatrix=mat4.create();
		var rotationMatrix=mat4.create();
	
		//Draw Triangle
		//Translate
		mat4.translate(translationMatrix, translationMatrix, [-1.5, 0.0, -3.0]);
		mat4.multiply(modelViewMatrix, translationMatrix, modelViewMatrix);
		//Rotate
		mat4.rotateY(rotationMatrix, rotationMatrix, degreeToRadians(angleTriangle) );
		mat4.multiply(modelViewMatrix, modelViewMatrix, rotationMatrix);	
		// Note: In above statement, sequence if reversed like:
		// mat4.multiply(modelViewMatrix, rotationMatrix, modelViewMatrix); It will lead to Triangle 
	   // going further translating out of the screen(you can check :P), so sequence matters and should be 
	   // aligned with the one mentioned above.
		//Project
		mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
		gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
		
		//Bind to vao_triangle and display the triangle
		gl.bindVertexArray(vao_triangle);
	
				gl.drawArrays(gl.TRIANGLES,0,3);		
	
		gl.bindVertexArray(null);
		
		//Draw Square
		mat4.identity(modelViewMatrix);
		mat4.identity(modelViewProjectionMatrix);
		mat4.identity(translationMatrix);
		mat4.identity(rotationMatrix);
		//Translate	
		mat4.translate(translationMatrix, translationMatrix, [1.5, 0.0, -3.0] );
		mat4.multiply(modelViewMatrix, translationMatrix, modelViewMatrix);
		//Rotate
		mat4.rotateX(rotationMatrix, rotationMatrix, degreeToRadians(angleSquare));
		mat4.multiply(modelViewMatrix,modelViewMatrix, rotationMatrix); //Sequence matters
		//Project
		mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
		gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
		//Bind vao_square and draw it!
		gl.bindVertexArray(vao_square);
				gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
		gl.bindVertexArray(null);
	gl.useProgram(null);
	//call to update() function
	update();
	
	//animation loop!
	requestAnimationFrame(draw,canvas);
}//draw()

function update()
{
	angleTriangle=angleTriangle + 1.0;
	
	if(angleTriangle > 360.0)
		angleTriangle=0.0;
	
	angleSquare=angleSquare + 1.0;
	
	if(angleSquare > 360.0)
		angleSquare=0.0;
	
}//update()

function degreeToRadians(angleInDegrees)
{
	
	return(angleInDegrees * Math.PI/180.0);
	
}//degrees()

function resize()
{
	if(bFullscreen==true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;
	}
	//set the viewport
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	//Perspective projection==>(target matrix, fov angle, aspect ration, near, far)
	mat4.perspective(perspectiveProjectionMatrix, 45.0, (parseFloat)(canvas.width)/(parseFloat)(canvas.height), 0.1, 100.0);
}//resize()


function init()
{
	//code
	//Get WebGL 2.0 Context
	// This context is nothing but"the GATEWAY" for all the WebGL functions, macro's and what not!
	// As like Windows/Linux/Android -> The function/macro's here do not follow the same style of naming
	// Ex. glClear() --> it is clear() in webGL.
	// But for our conveniece we have created the context itself of name: "gl" and we will be invoking methods/macros 
	// using this context variable, hence it will look like:
	// gl.clear() --> which at least gives you the feel that, you are invoking the OpenGL functions.
	
	gl=canvas.getContext("webgl2");
	
	if(gl==null)
	{
		console.log("\n Obtaining WebGL Rendenring Context Failed!! \n");
		return; //Terminate the program, as there is no use of proceeding further as no rendering context has been created!!
	}
	
	//After getting the context, define the viewport
	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;
	
	//Define the Vertex Shader
	var vertexShaderSourceCode=
		"#version 300 es"+
		"\n"+
		"in vec4 vPosition;"+
		"in vec4 vColor;"+
		"out vec4 out_color;"+
		"uniform mat4 u_mvp_matrix;"+
		"void main(void)"+
		"{"+
				"gl_Position=u_mvp_matrix * vPosition;"+
				"out_color=vColor;"+
		"}";
	
	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	
	//Compilation Error Handling
	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
		
	}	//Vertex shader compilation error handling
	
	//Define Fragment Shader
	var fragmentShaderSourceCode=
		"#version 300 es"+
		"\n"+
		"precision highp float;"+
		"in vec4 out_color;"+
		"out vec4 FragColor;"+
		"void main(void)"+
		"{"+
				"FragColor=out_color;" +
		"}";
	
	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	//Error handling
	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
		
	}//Error handling for fragment Shader 
	
	//Define the Shader Program
	shaderProgramObject=gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	//Before linking the program, initializing the shader variables
	
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");

	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_COLOR, "vColor");
	
	//Linking 
	gl.linkProgram(shaderProgramObject);
	//Handling Link Errors
	if(gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS)==false)
	{
		var error=gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}//Shader Program Link Error Handling
	//Post link, tranferring the data to shader's in the form on "uniforms"
	mvpUniform=gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");
	
	//Define the: Vao, Vbo, Geometry vertices etc.
	var triangleVertices=new Float32Array(
																		 [
																			0.0, 0.5, 0.0,
																			-0.5, -0.5, 0.0,
																			0.5, -0.5, 0.0
																		 ]								
																		);	
	
	var triangleColorValues=new Float32Array(
																					[
																						1.0, 0.0, 0.0, //Red
																						0.0, 1.0, 0.0, //Green
																						0.0, 0.0, 1.0  //Blue
																					]		
																				);
	var squareVertices=new Float32Array(
																			[
																				0.5, 0.5, 0.0, //Rigth top
																				-0.5, 0.5, 0.0, //Left Top
																				-0.5, -0.5, 0.0, //left bottom
																				0.5, -0.5, 0.0 //Right bottom
																			]											
																		);
		
	var squareColorValues=new Float32Array(
																				[	
																					0.38, 0.59, 0.92, //Right Top
																					0.38, 0.59, 0.92, //Left Top
																					0.38, 0.59, 0.92, //Left Bottom
																					0.38, 0.59, 0.92 //Right bottom
																				]				
																			  );
	
	
	//************** Define vao_triangle ******************************//
	vao_triangle=gl.createVertexArray();
	gl.bindVertexArray(vao_triangle);
	//vbo_position
		vbo_position=gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position);
		gl.bufferData(gl.ARRAY_BUFFER, triangleVertices, gl.STATIC_DRAW);
	
		//Tikadam-1
		gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,
												3,
												gl.FLOAT,
												false,
												0,
												0
												);
	//Tikadam-3
		gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);	
	gl.bindBuffer(gl.ARRAY_BUFFER,null); 
	//end of vbo_position;
		
		//vbo_color
		vbo_color=gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, vbo_color);
		gl.bufferData(gl.ARRAY_BUFFER, triangleColorValues, gl.STATIC_DRAW);
	
		//Tikadam-1
		gl.vertexAttribPointer( WebGLMacros.VDG_ATTRIBUTE_COLOR,
												3,
												gl.FLOAT,
												false,
												0,
												0
				  							   ); 
	
		//Tikdam 2 
		gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_COLOR);
        //end of vbo_color
		gl.bindBuffer(gl.ARRAY_BUFFER, null);
	  gl.bindVertexArray(null);
	//********************** Define vao_square ********************//
	vao_square=gl.createVertexArray();
	gl.bindVertexArray(vao_square);
		//vbo_position for square vertices
		vbo_position=gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position);
		gl.bufferData(gl.ARRAY_BUFFER, squareVertices, gl.STATIC_DRAW);
	
		//Tikadam-1
		gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,
												3, //(x,y,z) co-ordinates make up a single point/vertex hence this parameter is:3
												gl.FLOAT,
												false,
												0,
												0
												);
	//Tikadam-3
	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
	//unbind vbo
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	//vbo_color
		vbo_color=gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, vbo_color);
		gl.bufferData(gl.ARRAY_BUFFER, squareColorValues, gl.STATIC_DRAW);
	
		//Tikadam-1
		gl.vertexAttribPointer( WebGLMacros.VDG_ATTRIBUTE_COLOR,
												3,
												gl.FLOAT,
												false,
												0,
												0
				  							   ); 
	
		//Tikdam 2 
		gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_COLOR);
	//unbind vbo_color
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	//unbind vao_square
	gl.bindVertexArray(null);
	
	//Set the background color*********************************************
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	//initialize perspective projection matrix
	perspectiveProjectionMatrix=mat4.create();
	
}//init()


function uninitialize()
{
	
	//code
	if(vao_triangle)
	{
		gl.deleteVertexArray(vao_triangle);
		vao_triangle=null;
	}	
	if(vao_square)
	{
		gl.deleteVertexArray(vao_square);
		vao_square=null;
	}
	if(vbo_position)
	{
		gl.deleteBuffer(vbo_position);
		vbo_position=null;
	}
	if(vbo_color)
	{
		gl.deleteBuffer(vbo_color);
		vbo_color=null;
	}
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
		}
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
		
	}//shaderProgramObject
	
}//uninitialize()