//Global Variables (Because these will be used across the functions)
var canvas=null;
var context=null;

//onload function
function main()
{
	//get <canvas> element
	canvas=document.getElementById ("AMC");
	if(!canvas)
		console.log("Obtaining the canvas failed!! \n");
    else
		console.log("Obtaining the canvas succeeded!! \n");
	
	//Print  the canvas width and height on console
	console.log("Canvas Width :  "+ canvas.width + "And Canvas Height : "+canvas.height);
	
	//Get the 2D Context
	context=canvas.getContext("2d");
	if(!context)
		console.log("Obtaining the 2D Context Failed!! \n");
	else
		console.log("Obtaining the 2D Context Succeeded!! \n");
	
	//Fill canvas with "Black" color
	context.fillStyle="black";		//"#000000"
	context.fillRect(0,0,canvas.width,canvas.height);
	
	//Draw Text
	drawText("Hello World !! ");
	
	//Register keyboard's keydown event handler
	// window-->One of the available "global" object!
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);
	
}//main()


// In earlier program the code contained in the below function was part of main() function.
// This has been contained in the function because "Normal drawing madhe(javascript chya)
// reDraw() nahi pan WebGL sathi te implement kele aahe.

function drawText(text)
{
	// code
	// Center the text
	context.textalign="center"; //Center horizontally
	context.textBaseline="middle"; //Center vertically
	
	//text font
	context.font="48px sans-serif";
	
	//text color
	context.fillStyle="white"; //"#ffffff"

	//display the text in center
	context.fillText(text, canvas.width/2, canvas.height/2);
	
}//drawText()

function toggleFullScreen()
{
	var fullscreen_element=
		document.fullscreenElement ||
		document.webkitFullscreenElement ||
		document.mozFullscreenElement ||
		document.msFullscreenElement ||
		null;
	
	//if not fullscreen
	if(fullscreen_element==null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
	}
	else //if already fullscreen
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
	}
	
}//toggleFullScreen()

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 70: //for 'F' or 'f'
					   toggleFullScreen();
						//repaint
						drawText("Hello World !!");
						break;
	}
	
}//keyDown()

function mouseDown()
{
	//code
	
}//mouseDown()


