//Onload Function

// This function's name is "main" just for convention
function main()
{
	//get <canvas> element
	var canvas=document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed!! \n");
	else
		console.log("Obtaining Canvas Succeeded!!\n");
	
	//Print Canvas Width and Height to Console
	console.log("\n Canvas Width : "+canvas.width + "Canvas Height: "+ canvas.height);
	
	//Get 2D Context
	var context=canvas.getContext("2d");
	if(!context)
		console.log("Obtaining 2D Context Failed!!\n");
	else
		console.log("Obtaining 2D Context Succeeded!! \n");
	
	//Fill canvas with "BLACK" Color
	context.fillStyle="black"; //OR Use: #000000(black) #ffffff(White)
	context.fillRect(0,0,canvas.width,canvas.height);

	//Center the text
	context.textAlign="center"; //Center horizontally
	context.textBaseLine="middle"; //Center vertically
	
	//text
	var str="Hello World!!";
	
	//text font
	context.font="48px sans-serif";
	
	//text-color
	context.fillStyle="white"; //OR Use: "#ffffff"
	
	//display the text in center
	context.fillText(str,canvas.width/2,canvas.height/2);
}