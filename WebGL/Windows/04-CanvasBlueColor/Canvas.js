/**************************************************************
//Program: Canvas Blue color!
**************************************************************/

//Global Variables
var canvas=null;    // "null" is literally "null" on the contrary to Windows where it is #define null 0
var gl=null;				//OpenGL Context (We will catch into this variable)
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;

//To Start animation: To have requestAnimationFrame() to be called "cross-browser" compatible.
var requestAnimationFrame=
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame;

//To Stop animation: To have cancelAnimationFrame() to be called "cross-browser" compatible.
var cancelAnimationFrame=
	window.cancelAnimationFrame ||
	window.webkitCancelRequestAnimationFrame ||
	window.webkitCancelAnimationFrame ||
	window.mozCancelRequestAnimationFrame ||
	window.mozCancelAnimationFrame ||
	window.oCancelRequestAnimationFrame ||
	window.oCancelAnimationFrame ||
	window.msCancelRequestAnimationFrame ||
	window.msCancelAnimationFrame;


//onload function
function main()
{
	//get <canvas> element through the ID of it.
	canvas=document.getElementById("AMC");
	if(!canvas)
		console.log("\n Obtaining Canvas Failed!\n");
	else
		console.log("\n Obtaining Canvas Succeeded!\n");
	
	//Store these global variables to original values of width and height, which will be used during resize code
	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;
	
	//Register keyboard's "keydown" event handler
	// The last parameter is the special one, which if "true" will help us grab that event ! Parent will also get notified 
	// if that event occurs, but we don't want to do that, hence it is kept as "false".
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);
	window.addEventListener("resize",resize,false);
	
	//initialize WebGL [Sir has maintained the parity of the coding style across platform by doing this and other functions]
	init();
	
	//Start drawing here as warming-up
	resize();
	
	draw();
	
}//onload()

function init()
{
	//code
	//Get WebGL 2.0 Context
	// This context is nothing but"the GATEWAY" for all the WebGL functions, macro's and what not!
	// As like Windows/Linux/Android -> The function/macro's here do not follow the same style of naming
	// Ex. glClear() --> it is clear() in webGL.
	// But for our conveniece we have created the context itself of name: "gl" and we will be invoking methods/macros 
	// using this context variable, hence it will look like:
	// gl.clear() --> which at least gives you the feel that, you are invoking the OpenGL functions.
	
	gl=canvas.getContext("webgl2");
	
	if(gl==null)	//Failed to get the context 
	{
		console.log("\n Failed to get the rendering context for WenGL!\n");
		return; 	//As nothing can be done if we didn't receive the context itself.
	}
	// Once the context has been received-->Set the viewport width and height
	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;
	
	//Set the Clear color to "Blue"
	gl.clearColor(0.0, 0.0, 1.0, 1.0); //blue
	
}//init()

function resize()
{
	//Code
	// There is a challenge for this piece of code when we will have 24 sphere assignment where
	// Top and bottom row's will leave more than expected margin from the screen bezel.
	// Sir has ordered to work on the logic to make it uniform (in looking) though it comes to be hard coded 
	// logic.
	if(bFullscreen==true) //Application is already "fullscreen"-->Resize it to inner width and height of canvas!
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else //Application is not in fullscreen-->Make it fullscreen/switch to original width and height
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;
	}
	//set the viewport to match, since there is not default "repaint()" or "redraw()" function supported in Javascript.
	gl.viewport(0,0,canvas.width,canvas.height);
	
}//resize()

function draw()
{
	//code
	// Following the naming convention in Java script: GL_COLOR_BUFFER_BIT-->Remove GL_ part and rest of the name
	// can be used.
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	//animation loop!
	requestAnimationFrame(draw, canvas);
}//draw()

function toggleFullscreen()
{
	//code
	var fullscreen_element=
		document.fullscreenElement || 
		document.webkitFullscreenElement ||
		document.mozFullScreenElement ||
		document.msFullscreenElement ||
		null;
	
	//if not fullscreen
	if(fullscreen_element==null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen=true;
	}
	else //if fullscreen already exists
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullscreen=false;
	}		
	
}//toggleFullscreen()

function keyDown(event)
{
	switch(event.keyCode)
	{
		case 70: //For 'F' or 'f'
					  toggleFullscreen();
					  break;
			
		case 27: //escape	
					  return;	
	}
}//keyDown()

function mouseDown()
{
	//code
	
}//mouseDown()

function uninitialize()
{
	//code
	
}//uninitialize()
