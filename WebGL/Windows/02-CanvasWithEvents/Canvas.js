/**********************************
//Program: Canvas with all events!!
***********************************/

//Global variables
var canvas=null; //This value "null" is literally null inside.
var context=null;

//Onload function

function main()
{
	
	//Get the Canvas Element:
	canvas=document.getElementById("AMC");
	if(!canvas)
		console.log("\n Obtaining Canvas Failed!! \n");
	else
		console.log("\n Obtaining Canvas Succeeded!! \n");
	
	//Print Canvas Width and Height on Console!
	console.log("\n Canvas Width : "+canvas.width + "And Canvas Height : "+canvas.height);
	
	//Get 2D Context
	context=canvas.getContext("2d");
	if(!context)
		console.log("\n Obtaining 2D Context Failed!! \n");
	else
		console.log("\n Obtaining 2D Context Succeeded!! \n");
	
	//Fill Canvas with BLACK COLOR 
	context.fillStyle="black"; //OR You can use: "#000000"
	context.fillRect(0,0, canvas.width, canvas.height);
	
	//Center the TEXT
	context.textAlign="center"; //Center horizontally
	context.textBaseline="middle"; //Center Vertically
	
	//Text
	var str="Hello World!!";
	
	//Text Font
	context.font="48px sans-serif";
	
	// Text Color
	context.fillStyle="white"; // OR Use "#FFFFFF"

	//Display the text in center
	context.fillText(str, canvas.width/2, canvas.height/2);
	
	//Register keyboard's keydown event handler
	window.addEventListener("keydown",keyDown, false);
	window.addEventListener("click", mouseDown, false);
	
}//main()

function keyDown(event)
{
	//code
	alert("A Key Is Pressed!!");

}//keyDown()

function mouseDown()
{
	//code
	alert("Mouse Is Clicked!!");
	
}//mouseDown()



