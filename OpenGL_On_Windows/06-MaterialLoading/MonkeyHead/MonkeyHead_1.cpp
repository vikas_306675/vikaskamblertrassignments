/**************************************************************************************************************************

Assignment::OBJ File Parser Which will display "Monkey Head" from the OBJ File!!

****************************************************************************************************************************/
/*
Need:=
========
1. We can create any sort of shape or close to reality shape through RTR
   APIs. But, it takes significant time to create a model/shape which is
   close to reality. 
   How shall we reduce this time and focus more upon the content as a whole?

2. Third party softwares have been developed in such a way that,
   a. You can create any sort of shape/model using those softwares.
   b. You can use those created models in your RTR project/code programmatically.

3. In order to use these models in our own code programatically, we need to,
   a. Understand the format of the model extract in terms of: .OBJ file.
   b. Understand how the actual or primitive information has been stored in 
      these .OBJ files. Ex. Vertex coords, Texture Coords, Normals etc.
4. Once we understand the format of such .OBJ file, we can write a program 
   which will parse or skim through OBJ file and will provide the data 
   - Vertex data
   - Texture data
   - Normal data
   - face value data which can be used programatically in our project/code.

5. So, this is the reason we are writing the .OBJ file parser for one of such 
  .OBJ file parsing created through third party software named "Blender"

6. What we will achieve out of this.
   6a. Having a parser program ready for the .OBJ files generated for models 
      created in Blender we can:
	  6.1a. Save the time which we would have invested in creating a singular shape
	  6.2a. We can create endless shapes/models in Blender and parse them into
	        our own code to achieve the ultimate project goal.

======= Necessary functions invlved for Depth===============
glClearDepth();
glEnable();

glDepthFunc();
============================================================
Optional Depth correction functions for:
1. Distortion: glShadeModel();

2. Correction function: glHint();
============================================================
glRotatef(angle,x,y,z): 
- angle: Angle of rotation
- x,y,z: The axis around which rotation to be performed.

============================================================
Concept of Normal:
Perpendicular to the center of the Surface

Function:
glNormal3f(x,y,z);

****************************************************************************************************************************/

////////////////// Header Files/////////////////////////////
//For Window Context
#include<windows.h>

#include<stdio.h> //for file i/o

#include<gl/GL.h> //For OpenGL APIs
#include<gl/glu.h>

#include<vector> //C++ Template

////////////////// Macro's Definition////////////////////////

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define BUFFER_SIZE 256 //Buffer size of line by line read of OBJ File.
#define S_EQUAL 0		//Return value of strcmp() when both strings match

#define NR_POINT_COORDS   3   //Number of coordinates for vertex
#define NR_TEXTURE_COORDS 2 //Number of texture coordinates
#define NR_NORMAL_COORDS  3 //Number of Normal Coordinates
#define NR_FACE_TOKENS	  3   //Number of tokens for Face Data

#define MONKEYHEAD_X_TRANSLATE 0.0f  //X-translation of monkeyhead
#define MONKEYHEAD_Y_TRANSLATE -0.0f //Y-translation of monkeyhead
#define MONKEYHEAD_Z_TRANSLATE -5.0f //Z-translation of monkeyhead


#define MONKEYHEAD_X_SCALE_FACTOR 1.5f //X-scale factor of monkeyhead
#define MONKEYHEAD_Y_SCALE_FACTOR 1.5f //Y-scale factor of monkeyhead
#define MONKEYHEAD_Z_SCALE_FACTOR 1.5f  //Z-scale factor of monkeyhead 

#define START_ANGLE_POS 0.0f	//Beginning angle 
#define END_ANGLE_POS 360.0f
#define MONKEYHEAD_ANGLE_INCREMENT 1.0f //Increment angle for monkeyhead

/////////////////// Import Libraries////////////////////////
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"gdi32.lib")

/////////////////// Global Variables/////////////////////////
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;
bool gbLighting = false; //global flag controlling the lighting.

char line[BUFFER_SIZE]; //Character array while will hold the line read by fgets() from OBJ File.
float g_rotate;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev= {sizeof(WINDOWPLACEMENT)};

HWND ghwnd;
HDC ghdc;
HGLRC ghrc; 

FILE *gpFile=NULL;
FILE *gpMeshLogFile = NULL;
FILE *gpMeshFile = NULL; //Global pointer to Mesh File(OBJ File)

//Vector of vector of floats to hold vertex data
std::vector<std::vector<float>>g_vertices;

//Vector of vector of floats to hold texture data
std::vector<std::vector<float>> g_texture;

//Vector of vector of floats to hold "normal data"
std::vector<std::vector<float>> g_normals;

//Vector of vector of int to hold index data in g_vertices
std::vector<std::vector<int>> g_face_tri, g_face_texture, g_face_normals;

/////////////////////////////////global arrays representing Light Work!//////////////////////////

GLfloat light_ambient[] = {1.0f,1.0f,1.0f,0.0f};
GLfloat light_diffuse[] = {1.0f,1.0f,1.0f,0.0f};
GLfloat light_specular[] = {1.0f,1.0f,1.0f,0.0f};
GLfloat light_position[] = {0.0f,0.0f,1.0f,0.0f};

GLfloat material_specular[] = {1.0f,1.0f,1.0f,1.0f};
GLfloat material_shininess = 50.0f;

HRESULT CALLBACK WndProc(HWND hwnd,UINT message ,WPARAM wParam,LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR nCmdLine,int nCmdShow)
{
	//function prototypes
	void initialize(void);
	void update(void);
	void display(void);
	void uninitialize(void);
	
	//Variables
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	bool bDone=false;
	TCHAR szAppName[]=TEXT("Assignment_OBJ_Parser");

	if(fopen_s(&gpFile,"Assignment_OBJ_Parser_Log.txt","w")!=0)
	{
		MessageBox(NULL,TEXT("Log file could not be created..exiting!!"),TEXT("Error"),MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile,"Log File Created Successfully!!\n");
	}

	//code
	wndClass.cbSize=sizeof(WNDCLASSEX);
	wndClass.style=CS_HREDRAW | CS_VREDRAW | CS_OWNDC; //CS_OWNDC-->Non-purgable DC
	wndClass.cbClsExtra=0;
	wndClass.cbWndExtra=0;

	wndClass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hInstance=hInstance;
	wndClass.hIcon=LoadIcon(NULL,IDI_APPLICATION); //load the default icon
	wndClass.hIconSm=LoadIcon(NULL,IDI_APPLICATION); //Icon for "System menu"
	wndClass.hCursor=LoadCursor(NULL,IDC_ARROW);	 //Load default cursor, you can specific the cursor type while creating a game application (Confirm?)

	wndClass.lpfnWndProc=WndProc;
	wndClass.lpszClassName=szAppName;
	wndClass.lpszMenuName=NULL;

	//Register the created class
	RegisterClassEx(&wndClass);

	//create the window
	hwnd=CreateWindowEx(
						 WS_EX_APPWINDOW, //exclusive full-screen window
						 szAppName,
						 TEXT("Assignment::OBJ File Parser(Monkey Head)!!"),
						 WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS|WS_VISIBLE,
						 0,
						 0,
						 WIN_WIDTH,
						 WIN_HEIGHT,
						 NULL,		
						 NULL,
						 hInstance,
						 NULL
						);

	ghwnd=hwnd;	//For further use of the global window handle

	initialize(); //Call to initialize(Do the preparation of the "Picture element" (Pixel)

	ShowWindow(hwnd,nCmdShow);//Show the created window
	SetForegroundWindow(hwnd);//Bring that window in foreground
	SetFocus(hwnd);//Set the focus on the window

	//Game loop
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}//Events further generating the messages
		else
		{
			if(gbActiveWindow==true)
			{
				if(gbEscapeKeyIsPressed==true)
				{
					bDone=true;
				}
				update(); //Logically: First update() and then display().
				display();
			}
			
		}//game will run here

	}//end of while
	uninitialize();
	return msg.wParam; 
}//WinMain

/*Initialize:
  1. PIXELFORMATDESCRIPTOR inialize
  2. ChoosePixelFormat
  3. SetPixelFormat
  4. Get the device context from OS
  5. Create OpenGL Context:wglCreateContext()
  6. HandOver control from OS DC to Rendering DC: wglMakeCurrent()
  7. Clear the window with desired color
  8. Give a warmup call to resize()

*/
void initialize(void)
{
	//function prototypes
 	void resize(int,int);
	void LoadMeshData(void);
	void uninitialize(void);

	//Variables
	PIXELFORMATDESCRIPTOR pfd;	//a structure which we need to fill will possible set of values from our side and rest can be achieved from OS
	int iPixelFormatIndex;

	//code

	if(fopen_s(&gpMeshLogFile, "MonkeyHead_Mesh_Log.txt", "w") != 0)
	{

		MessageBox(NULL,TEXT("Could not open Log file for MonkeyHead"),TEXT("Error"),MB_OK);
		exit(0);
	}
	else
	{

		fprintf(gpMeshLogFile,"Mesh Log File Created Successfully!!\n");

	}

	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));//Make the 26-membered structure to 0

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags= PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; //dword flags depicting pixel characteristics
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cGreenBits=8;
	pfd.cBlueBits=8;
	pfd.cAlphaBits=8;
	pfd.cDepthBits=32;		//equivalent call to this function in order to achieve the depth.

	ghdc=GetDC(ghwnd);//Get the regular device context from OS (this is non-purgeable)

	iPixelFormatIndex=ChoosePixelFormat(ghdc,&pfd);//After getting the DC, "Choose the pixel format" which is the closest match to the values which we have provides

	if(iPixelFormatIndex==0)//if the pixelformatindex is empty due to function could not succeed
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd)==false) //Set the chosen matching closest pixel format
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	ghrc=wglCreateContext(ghdc);//Now handover the device context to OpenGL's "Rendering context" through WGL

	if(ghrc==NULL)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	if(wglMakeCurrent(ghdc,ghrc)==NULL)//Once the handover is successful-->Make "Rendering context" as the current context through WGL
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	glClearColor(0.0f,0.0f,0.0f,0.0f); //Now clear the window with desired color

	///////////////////////Depth/////////////////////////////////////

	glClearDepth(1.0f);					  //First call w.r.t depth, Range of values:0.0f-1.0f, Here we have just specified the value for depth, actual action will be taken into display() 
	
	glEnable(GL_DEPTH_TEST);		     // Enable the Depth test 

    glDepthFunc(GL_LEQUAL);				 //Actual depth function which will act after the drawing is finished.

	glShadeModel(GL_SMOOTH);			//Optioncal call to have "Smooth shade of the light"

	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST); //Optional call to handle "distortion" if any

	////////////////////// Function call to LoadMeshData() function////////////////////////////////////
	LoadMeshData();
	
	/*
	//////////////////////// Light///////////////////////////////////
	glLightfv(GL_LIGHT0,GL_AMBIENT,light_ambient);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,light_diffuse);
	glLightfv(GL_LIGHT0,GL_SPECULAR,light_specular);
	glLightfv(GL_LIGHT0,GL_POSITION,light_position);

	glMaterialfv(GL_FRONT,GL_SPECULAR,material_specular);
	glMaterialf(GL_FRONT,GL_SHININESS,material_shininess);

	glEnable(GL_LIGHT0);
	*/
	resize(WIN_WIDTH,WIN_HEIGHT); //a warmup call to resize function--In the case of "Projection" this call is of crucial importance as this call will help keep the viewing volume ready before drawing happens.
								  //Otherwise, in this case, application is launching in windowed mode with some weird output(as viewing volume will not be ready if this call is commentedout) and after pressing "F"(for fullscreen) desired output comes.	
								  // This warm up call to resize() goig to be crucial in case of DirectX
}//end of initialize()

//Resize function:
// Projection functions are conventionally called inside the function which deals with the change in the size of the window

void resize(int width,int height)
{
	if(height==0)
		height=1;
	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);

}//end of resize

//CALLBACK Function
HRESULT CALLBACK WndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam)
{
	//function declarations
	void resize(int,int);
	void uninitialize(void);
	void ToggleFullscreen(void);
	//Code
	switch(message)
	{

	case WM_CREATE:
					break;

	case WM_ACTIVATE:
					 if(HIWORD(wParam)==0)
						 gbActiveWindow=true;
					 else
						 gbActiveWindow=false;
					break;

	case WM_KEYDOWN:
					switch(wParam)
					{
					case VK_ESCAPE:
									gbEscapeKeyIsPressed=true;
									break;

						case 0x46: //for fullscreen
									if(gbFullscreen==false)
									{
										ToggleFullscreen();
										gbFullscreen=true;
									}
									else
									{
										ToggleFullscreen();
										gbFullscreen=false;
									}
									break;
						case 0x51: //Q
									uninitialize();
									PostQuitMessage(0);
									break;
						
						/*
						case 0x4C:
									if (gbLighting == false)
									{
									glEnable(GL_LIGHTING);
									gbLighting = true;
									}
									else
									{
										glDisable(GL_LIGHTING);
										gbLighting = false;
									}

								break;*/

					}
					break;

	case WM_SIZE:
					resize(LOWORD(lParam),HIWORD(lParam));
					break;

	case WM_DESTROY:
					PostQuitMessage(0);
					break;

	}
	return(DefWindowProc(hwnd,message,wParam,lParam));

}//End of call back function

/*ToggleFullscreen function
	1. Get the window style using GetWindowLong()
	2. If the "Current window" type contains the "WS_OVERLAPPEDWINDOW" -->Remove it.
	3. Store the current window position
	4. Get the monitor info(Primary display)
	5. Set the window position
*/
void ToggleFullscreen(void)
{
	//variable declaration
	MONITORINFO mi={sizeof(MONITORINFO)};
	bool IsWindowPlacement;
	bool IsMonitorInfo;
	HMONITOR hMonitor;

	//code
	if(gbFullscreen==false) //windowed to full-screen
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE); //Get the window style/entire information of the window
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			//a) Get the window Placement and Monitor information then and then toggle to fullscreen
			IsWindowPlacement=GetWindowPlacement(ghwnd,&wpPrev);

			//b) Now get the monitor first 
			hMonitor=MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY); //the primary monitor in case of extended view

			//c) Get and store the monitor information from the monitor which is been just received
			IsMonitorInfo=GetMonitorInfo(hMonitor,&mi);

			//
			if(IsWindowPlacement && IsMonitorInfo)//both information have been received, now set the window to the top of the screen
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~ WS_OVERLAPPEDWINDOW);

				//Set the window position
				
				SetWindowPos(
							  ghwnd,
							  HWND_TOP,
							  mi.rcMonitor.left,
							  mi.rcMonitor.top,
							  mi.rcMonitor.right-mi.rcMonitor.left, //width
							  mi.rcMonitor.bottom-mi.rcMonitor.top, //height
							  SWP_NOZORDER | SWP_FRAMECHANGED       //ignore any of the Z order and trigger frame change event
							);

			}
			ShowCursor(FALSE);
		}
	}
	else //full-screen to windowed mode
	{
		// Add the removed style to the window style
		// Set the window placement/position to back to the saved position
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd,&wpPrev);

		SetWindowPos(
						ghwnd,
						HWND_TOP,
						0,0,0,0,
						SWP_NOMOVE|
						SWP_NOSIZE|
						SWP_NOOWNERZORDER|
						SWP_NOZORDER|
						SWP_FRAMECHANGED
					);
		ShowCursor(TRUE);
	}
	
}//end of ToggleFullscreen()

//Loading the OBJ File Data: Function LoadMeshData()

void LoadMeshData(void)
{
	//function prototypes
	void uninitialize(void);

	//variables
	// Token Pointers
	// Character pointer for holding first word in a line
	char *firstToken = NULL;

	// Separater strings
	// String holding space separator for strtok()
	char *sep_space = " ";

	//String holding "forward slash" separator for strtok()
	char *sep_fslash = "/";

	//String holding forward slash separator for strtok()
	char *token = NULL;

	// Array of character pointers to hold strings of face entries
	// Face entries can be variable. In some files they are three 
	// and in some files they are four.
	char *face_tokens[NR_FACE_TOKENS]; 

	//Number of non-NULL tokens in the vector.
	int nr_tokens=0;

	//Character pointer for holding string associated with
	//vertex index
	char *token_vertex_index = NULL;

	//Character pointer for holding string associated with
	//texture index
	char *token_texture_index = NULL;

	//Character pointer for holding string associated with
	//normal index
	char *token_normal_index = NULL;

	//code
	//Open mesh file(OBJ File), name of mesh file can be parameterized
	//gpMeshFile = fopen("MonkeyHead.OBJ","r");
	fprintf(gpFile,"In LoadMeshData()==========================================\n");
	
	if (fopen_s(&gpMeshFile,"MonkeyHead.OBJ","r")!=0)
	{
		fprintf(gpFile, "Failed to Open Mesh File!!!\n");
		uninitialize();
	}
	else
	{
		fprintf(gpFile,"Successfully opened the .OBJ file for parsing!!\n");
	}

	//While there is a line in the file
	while (fgets(line,BUFFER_SIZE,gpMeshFile)!=NULL)
	{
		//Get the first token out of the line read
		firstToken = strtok(line,sep_space); //possible values of firstToken=v/vt/vn/f    

		//If the first token indicates "Vertex" data
		if (strcmp(firstToken,"v")==S_EQUAL)//S_EQUAL=0 defined globally
		{
			//create a vector of NR_POINT_COORDS number of floats
			//to hold coordinates
			std::vector<float> vec_point_coord(NR_POINT_COORDS); //NR_POINT_COORDS=3 (as there can be (x,y,z) coords for the given point/vertex in 3D-Space:defined globally)

			//Do following NR_POINT_COORDS time
			//Step:1: Get the next token
			//Step:2: Feed it to atof() to get floating point number out of it (Because strtok() will read and return the content in string form)
			//Step:3: Add the floating point number(Which is actually currently in string format)
			//End Loop
			//Step:4: At the end of the loop vector is constructed, add it to
			//        global vector of vector of floats, ex.g_vertices

			for (int i = 0; i != NR_POINT_COORDS; i++)
			{
				vec_point_coord[i] = atof(strtok(NULL,sep_space));
			}
			g_vertices.push_back(vec_point_coord); //global vector of these per vertex based vectors (float vectors) will hold all vertices for your model.

		}
		else if (strcmp(firstToken,"vt")==S_EQUAL) //If the first token indicated "texture data"
		{
			//Create a vector which will hold texture data
			std::vector<float> vec_texture_coord(NR_TEXTURE_COORDS); //NR_TEXTURE_COORDS=2 (As there can be only 2 texture coords for the given vertex:defined globally)

			//Do following NR_POINT_COORDS time
			//Step:1: Get the next token
			//Step:2: Feed it to atof() to get floating point number out of it
			//Step:3: Add the floating point number(Which is actually currently in string format)
			//End Loop
			//Step:4: At the end of the loop vector is constructed, add it to
			//        global vector of vector of floats, ex.g_vertices
			for (int i=0; i!=NR_TEXTURE_COORDS;i++)
			{
				vec_texture_coord[i] = atof(strtok(NULL,sep_space));
			}
			g_texture.push_back(vec_texture_coord);//global vector of these per vertex texture vectors (float type vectors) will hold texture data for entire model.
		}
		else if (strcmp(firstToken,"vn")==S_EQUAL) //Of the first token depicts "normal" data
		{
			//Create a vector for normal data
			std::vector<float> vec_normal_coord(NR_NORMAL_COORDS);//NR_NORMAL_COORDS=3 (per vertex normal with 3 coords:defined globally)

			//Do following NR_POINT_COORDS time
			//Step:1: Get the next token
			//Step:2: Feed it to atof() to get floating point number out of it
			//Step:3: Add the floating point number(Which is actually currently in string format)
			//End Loop
			//Step:4: At the end of the loop vector is constructed, add it to
			//        global vector of vector of floats, ex.g_vertices
			for (int i = 0; i != NR_NORMAL_COORDS; i++)
			{
				vec_normal_coord[i] = atof(strtok(NULL, sep_space));
			}
			g_normals.push_back(vec_normal_coord); //global vector of these per vertex "normal" vectors (float type vectors) which will hold "Normal" data for the entire model.  
		}
		else if (strcmp(firstToken,"f")==S_EQUAL) //If the first token indicates face data: (Refer .OBJ file to know how the face data has been kept in it)
		{
			//Define three vector of integers with length 3 to hold indices
			//of triangle's positional coordinates, texture coordinates,
			//and normal coordinates in g_vertices,g_textures and g_normals respectively.
			std::vector<int> triangle_vertex_indices(3), texture_vertex_indices(3), normal_vertex_indices(3);

			 //Initialize all char pointers in face tokens to NULL
			memset((void *)face_tokens,0,NR_FACE_TOKENS); //NR_FACE_TOKENS=3(defined globally)

			// Extract three fields of information in face_tokens
			// and increment nr_tokens accordingly (Instead of for loop we have used while contruct for skimming through the "face" values)
			nr_tokens = 0;
			while (token=strtok(NULL,sep_space))//Sample face value:[f (vertex index/texture data index of this vertex/normal data index for this vertex) ] f 28/496/907 24/476/907 22/501/907 [This while loop will store the these 3 tokens into face_tokens array first]
			{
				if (strlen(token) < 3)
					break;
				face_tokens[nr_tokens] = token;
				nr_tokens++;
			}

			//Every face data entry is going to have minimum three fields
			//therefore, construct a triangle out of it with,
			// Step:1:S1: triangle coordinate data and
			// Step:2:S2: texture coordinate index data
			// Step:3:S3: normal coordinate index data
			// Step:4:S4: Put the data in triangle_vertex_indices, texture_vertex_indices
			//            normal_vertex_indices. Vectors will be constructed at the end of the loop.

			for (int i=0;i!=NR_FACE_TOKENS;++i) //NR_FACE_TOKENS=3 (defines globally)
			{
				token_vertex_index = strtok(face_tokens[i],sep_fslash);
				token_texture_index = strtok(NULL,sep_fslash);
				token_normal_index = strtok(NULL,sep_fslash);
				triangle_vertex_indices[i] = atoi(token_vertex_index);
				texture_vertex_indices[i] = atoi(token_texture_index);
				normal_vertex_indices[i] = atoi(token_normal_index);
			}

			//Add constructed vectors to global face vectors
			//Why we have such index data ?
			// a. We have constructure a model-->which is composed on "triangles, triangles, triangles" onlu.
			// b. As we go on increasing the complexity of model, accordingly trianles will increase, accordingl number of vertices,texture coords,normals will increase
			// c. As the model is constructed out of trianles connected to each other, means we have lot of vertices "in-common" across triangles.
			// d. So in short, we have finite number of vertices jyana aapan index wise refer karun respective triangle chi face tayar karto, rather than putting the vertex coords each time 
			// e. So indexing mule aapan easily sangu shakto ki ya index la (this index is defined in the .Obj file as we keep the v/vt/vn data one by one)
			//    jo vertex aahe, je texture aahe je normal aahe te wapar...kadhi?
			// kadhi?: while recontructing the model programmatically.
			// Note: DONT FORGET THAT WE ARE DEALING WITH MODELS NOT THE PRIMITIVE SHAPES ANYMORE.

			g_face_tri.push_back(triangle_vertex_indices); 
			g_face_texture.push_back(texture_vertex_indices);
			g_face_normals.push_back(normal_vertex_indices);
		 }
		 //Initialize line buffer to NULL, to avoid any garbage data/
		 memset((void *)line,(int)'\0',BUFFER_SIZE);
	}//while(): So per iteration of while loop we will separate out vertex/texture/normal/face value data from .OBJ file as we encounter in it.

	//Close meshfile and make file pointer NULL
	if (gpMeshFile)
	{
		fclose(gpMeshFile);
		gpMeshFile = NULL;
	}
	//Log count of:vertex, texture and face data in log file.
	fprintf(gpMeshLogFile,"g_vertices:%zu g_texture:%zu g_normals:%zu g_face_tri:%zu \n",
		    g_vertices.size(),g_texture.size(),g_normals.size(),g_face_tri.size());

	fprintf(gpFile, "Exiting LoadMeshData()==========================================\n");

}//LoadMeshData():Whose ultimate goal to create data structures out of .OBJ file data which we will use to construct the model in our project/code.

//Display function
void display(void)
{
	//function prototypes
	void uninitialize(void);

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	//Model Transformation
	//S1:	Translate the MonkeyHead in X,Y and Z directions respectively
	//		by MONKEY_X_TRANSLATE, MONLEY_Y_TRANSLATE,MONKEY_Z_TRANSLATE
	//S2:   Rotate the Model around Y axis by means of incremental angle using update()
	//S3:   Perform scaling transformations along X,Y and Z respectively.
	//      Using: MONKEY_X_SCALE_FACTOR,MONKEY_Y_SCALE_FACTOR,MONKEY_Z_SCALE_FACTOR

	glTranslatef(MONKEYHEAD_X_TRANSLATE, MONKEYHEAD_Y_TRANSLATE,MONKEYHEAD_Z_TRANSLATE);
	glRotatef(g_rotate,0.0f,1.0f,0.0f);
	glScalef(MONKEYHEAD_X_SCALE_FACTOR,MONKEYHEAD_Y_SCALE_FACTOR,MONKEYHEAD_Z_SCALE_FACTOR);

	//Keep counter-clockwise winding of vertices of the geometry
	glFrontFace(GL_CCW); //CCW:Counter clock wise

	//Set Polygon mode mentioning front and back faces and GL_LINE
	glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);

	//Start Drawing the faces in the form of triangles,
	// Whose vertex indixes are kept in: g_face_tri vector and
	// Actual vertex coords are kept in: g_vertices vector.
	//Step:1: for every face index maintained in triangular form in g_face_tri
    // Do below steps
	//      Step2: Set geometry primitive to GL_TRIANGLES
	//		Step3: Extract triangle from the outer loop index 
	//			   for every point of triangle
	//			 Step4: Calculate the index in g_vertices into variable:VI
	//			 Step5: Calculate x,y,z coords of point for the given index VI
	//			 Step6: Send the x,y,z to glVertex3f()
	//		Remark:   In Step4, we have to subtract g_face_tri[i][j] by 1 because
	//				  in mesh file, indexing starts from 1 and in case of arrays/vectors
	//				  indexing starts from 0.

	for (int i = 0; i != g_face_tri.size(); ++i)//Step1
	{
		glBegin(GL_TRIANGLES);//Step2
			for (int j = 0; j != g_face_tri[i].size(); j++)//Step3
			{
				int VI = g_face_tri[i][j] - 1; //Step4:VI: Vertex Index, As index in .OBJ file starts with 1 and array index starts with 0
				glVertex3f(g_vertices[VI][0],g_vertices[VI][1],g_vertices[VI][2]);//Step5
			}//Inner for loop: For each g_face_tri entry-->Extract one index -->means one vertex-->Loop to collect 3 vertices 
			// in glBegin()..glEnd() to construct the triangle.
		glEnd();
	}//Outer for loop: Each iteration means, triangle for each face.

	SwapBuffers(ghdc);

}//end of display function

void update(void)
{
	//Increment rotation angle by a factor MONKEYHEAD_ANGLE_INCREMENT
	g_rotate = g_rotate + MONKEYHEAD_ANGLE_INCREMENT;

	//If rotation angle equals or exceeds END_ANGLE_POS then
	// reset to START_ANGLE_POS
	if (g_rotate >= END_ANGLE_POS)
		g_rotate = START_ANGLE_POS;

}//end of update function

//Uninitialize function
void uninitialize(void)
{
	//if window is in fullscreen mode-->Switch back to windowed mode and then exit
	if(gbFullscreen==true)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);

		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd,&wpPrev);

		SetWindowPos(
					  ghwnd,
					  HWND_TOP,
					  0,0,0,0,
					  SWP_NOMOVE|
					  SWP_NOSIZE|
					  SWP_NOOWNERZORDER|
					  SWP_NOZORDER|
					  SWP_FRAMECHANGED
					);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL,NULL);

	wglDeleteContext(ghrc);
	ghrc=NULL;

	ReleaseDC(ghwnd,ghdc);
	ghdc=NULL;
	if(gpMeshLogFile)
	{
	
		fprintf(gpMeshLogFile,"Mesh Log File Closed Successfully!!\n");
		fclose(gpMeshLogFile);
		gpMeshLogFile = NULL;
	}
	if(gpMeshFile)
	{
		fprintf(gpFile, "Mesh File Load Completed Successfully!!!\n");
		fclose(gpMeshFile);
		gpMeshFile = NULL;
	}
	if(gpFile)
	{

		fprintf(gpFile,"Log File Closed Successfully!!");
		fclose(gpFile);
		gpFile=NULL;
	}
	DestroyWindow(ghwnd);
}//end of uninitialize()