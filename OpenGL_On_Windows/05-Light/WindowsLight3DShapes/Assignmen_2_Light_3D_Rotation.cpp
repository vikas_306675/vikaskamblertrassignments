/**************************************************************************************************************************

Assignment::Draw a "3D Cube and Pyramid Rotate:
			 a. Pyramid along "Y Axis".
			 b. Cube around all axes.
======= Necessary functions invlved for Depth===============
glClearDepth();

glEnable();

glDepthFunc();
============================================================
Optional Depth correction functions for:
1. Distortion: glShadeModel();

2. Correction function: glHint();
============================================================
glRotatef(angle,x,y,z): 
					  - angle: Angle of rotation
					  - x,y,z: The axis around which rotation to be performed.

============================================================
Concept of Normal:
Perpendicular to the center of the Surface

Function:
glNormal3f(x,y,z);

****************************************************************************************************************************/
#include<windows.h>
#include<stdio.h> //for file i/o
#include<gl/GL.h>
#include<gl/glu.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

bool gbActiveWindow=false;
bool gbEscapeKeyIsPressed=false;
bool gbFullscreen=false;
bool gbLighting = false; //global flag controlling the lighting.

float angleCube=0.0f;
float anglePyramid=0.0f;

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"gdi32.lib")

DWORD dwStyle;
WINDOWPLACEMENT wpPrev= {sizeof(WINDOWPLACEMENT)};

HWND ghwnd;
HDC ghdc;
HGLRC ghrc; 

FILE *gpFile=NULL;

//global arrays representing Light Work!

GLfloat light_ambient[] = {0.5f,0.5f,0.5f,1.0f};
GLfloat light_diffuse[] = {1.0f,1.0f,1.0f,1.0f};
GLfloat light_specular[] = {1.0f,1.0f,1.0f,1.0f};
GLfloat light_position[] = {0.0f,0.0f,1.0f,0.0f};

HRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR nCmdLine,int nCmdShow)
{
	//function prototypes
	void display(void);
	void initialize(void);
	void uninitialize(void);
	void update(void);

	//Variables
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	bool bDone=false;
	TCHAR szAppName[]=TEXT("Assignment_Light_Pyarmid_Cube_Rotation");

	if(fopen_s(&gpFile,"AssignmentFFP_2_Lighting_3D_Rotation.txt","w")!=0)
	{
		MessageBox(NULL,TEXT("Log file could not be created..exiting!!"),TEXT("Error"),MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile,"Log File Created Successfully!!\n");
	}

	//code
	wndClass.cbSize=sizeof(WNDCLASSEX);
	wndClass.style=CS_HREDRAW | CS_VREDRAW | CS_OWNDC; //CS_OWNDC-->Non-purgable DC
	wndClass.cbClsExtra=0;
	wndClass.cbWndExtra=0;

	wndClass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hInstance=hInstance;
	wndClass.hIcon=LoadIcon(NULL,IDI_APPLICATION); //load the default icon
	wndClass.hIconSm=LoadIcon(NULL,IDI_APPLICATION); //Icon for "System menu"
	wndClass.hCursor=LoadCursor(NULL,IDC_ARROW);	 //Load default cursor, you can specific the cursor type while creating a game application (Confirm?)

	wndClass.lpfnWndProc=WndProc;
	wndClass.lpszClassName=szAppName;
	wndClass.lpszMenuName=NULL;

	//Register the created class
	RegisterClassEx(&wndClass);

	//create the window
	hwnd=CreateWindowEx(
						 WS_EX_APPWINDOW, //exclusive full-screen window
						 szAppName,
						 TEXT("Assignment::Light:: 3D Cube Pyramid Rotation!!"),
						 WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS|WS_VISIBLE,
						 0,
						 0,
						 WIN_WIDTH,
						 WIN_HEIGHT,
						 NULL,		
						 NULL,
						 hInstance,
						 NULL
						);

	ghwnd=hwnd;	//For further use of the global window handle

	initialize(); //Call to initialize(Do the preparation of the "Picture element" (Pixel)

	ShowWindow(hwnd,nCmdShow);//Show the created window
	SetForegroundWindow(hwnd);//Bring that window in foreground
	SetFocus(hwnd);//Set the focus on the window

	//Game loop
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}//Events further generating the messages
		else
		{
			if(gbActiveWindow==true)
			{
				if(gbEscapeKeyIsPressed==true)
				{
					bDone=true;
				}
				update(); //Logically: First update() and then display().
				display();
			}
			
		}//game will run here

	}//end of while
	uninitialize();
	return msg.wParam; 
}//WinMain

/*Initialize:
  1. PIXELFORMATDESCRIPTOR inialize
  2. ChoosePixelFormat
  3. SetPixelFormat
  4. Get the device context from OS
  5. Create OpenGL Context:wglCreateContext()
  6. HandOver control from OS DC to Rendering DC: wglMakeCurrent()
  7. Clear the window with desired color
  8. Give a warmup call to resize()

*/
void initialize(void)
{
	//function prototypes

	void resize(int,int);

	//Variables
	PIXELFORMATDESCRIPTOR pfd;	//a structure which we need to fill will possible set of values from our side and rest can be achieved from OS
	int iPixelFormatIndex;
	//code
	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));//Make the 26-membered structure to 0

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags= PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; //dword flags depicting pixel characteristics
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cGreenBits=8;
	pfd.cBlueBits=8;
	pfd.cAlphaBits=8;
	pfd.cDepthBits=32;		//equivalent call to this function in order to achieve the depth.

	ghdc=GetDC(ghwnd);//Get the regular device context from OS (this is non-purgeable)

	iPixelFormatIndex=ChoosePixelFormat(ghdc,&pfd);//After getting the DC, "Choose the pixel format" which is the closest match to the values which we have provides

	if(iPixelFormatIndex==0)//if the pixelformatindex is empty due to function could not succeed
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd)==false) //Set the chosen matching closest pixel format
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	ghrc=wglCreateContext(ghdc);//Now handover the device context to OpenGL's "Rendering context" through WGL

	if(ghrc==NULL)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	if(wglMakeCurrent(ghdc,ghrc)==NULL)//Once the handover is successful-->Make "Rendering context" as the current context through WGL
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	glClearColor(0.0f,0.0f,0.0f,0.0f); //Now clear the window with desired color

	///////////////////////Depth/////////////////////////////////////

	glClearDepth(1.0f);					  //First call w.r.t depth, Range of values:0.0f-1.0f, Here we have just specified the value for depth, actual action will be taken into display() 
	
	glEnable(GL_DEPTH_TEST);		     // Enable the Depth test 

    glDepthFunc(GL_LEQUAL);				 //Actual depth function which will act after the drawing is finished.

	glShadeModel(GL_SMOOTH);			//Optioncal call to have "Smooth shade of the light"

	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST); //Optional call to handle "distortion" if any

	//////////////////////// Light///////////////////////////////////
	glLightfv(GL_LIGHT0,GL_AMBIENT,light_ambient);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,light_diffuse);
	glLightfv(GL_LIGHT0,GL_SPECULAR,light_specular);
	glLightfv(GL_LIGHT0,GL_POSITION,light_position);

	resize(WIN_WIDTH,WIN_HEIGHT); //a warmup call to resize function--In the case of "Projection" this call is of crucial importance as this call will help keep the viewing volume ready before drawing happens.
								  //Otherwise, in this case, application is launching in windowed mode with some weird output(as viewing volume will not be ready if this call is commentedout) and after pressing "F"(for fullscreen) desired output comes.	
								  // This warm up call to resize() goig to be crucial in case of DirectX
}//end of initialize()

//Resize function:
// Projection functions are conventionally called inside the function which deals with the change in the size of the window

void resize(int width,int height)
{
	if(height==0)
		height=1;
	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);

}//end of resize

//CALLBACK Function
HRESULT CALLBACK WndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam)
{
	//function declarations
	void resize(int,int);
	void uninitialize(void);
	void ToggleFullscreen(void);
	//Code
	switch(message)
	{

	case WM_CREATE:
					break;

	case WM_ACTIVATE:
					 if(HIWORD(wParam)==0)
						 gbActiveWindow=true;
					 else
						 gbActiveWindow=false;
					break;

	case WM_KEYDOWN:
					switch(wParam)
					{
					case VK_ESCAPE:
									gbEscapeKeyIsPressed=true;
									break;

						case 0x46: //for fullscreen
									if(gbFullscreen==false)
									{
										ToggleFullscreen();
										gbFullscreen=true;
									}
									else
									{
										ToggleFullscreen();
										gbFullscreen=false;
									}
									break;
						case 0x51: //Q
									uninitialize();
									PostQuitMessage(0);
									break;
						case 0x4C:
									if (gbLighting == false)
									{
									glEnable(GL_LIGHTING);
									gbLighting = true;
									}
									else
									{
										glDisable(GL_LIGHTING);
										gbLighting = false;
									}

								break;

					}
					break;

	case WM_SIZE:
					resize(LOWORD(lParam),HIWORD(lParam));
					break;

	case WM_DESTROY:
					PostQuitMessage(0);
					break;

	}
	return(DefWindowProc(hwnd,message,wParam,lParam));

}//End of call back function

/*ToggleFullscreen function
	1. Get the window style using GetWindowLong()
	2. If the "Current window" type contains the "WS_OVERLAPPEDWINDOW" -->Remove it.
	3. Store the current window position
	4. Get the monitor info(Primary display)
	5. Set the window position
*/
void ToggleFullscreen(void)
{
	//variable declaration
	MONITORINFO mi={sizeof(MONITORINFO)};
	bool IsWindowPlacement;
	bool IsMonitorInfo;
	HMONITOR hMonitor;

	//code
	if(gbFullscreen==false) //windowed to full-screen
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE); //Get the window style/entire information of the window
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			//a) Get the window Placement and Monitor information then and then toggle to fullscreen
			IsWindowPlacement=GetWindowPlacement(ghwnd,&wpPrev);

			//b) Now get the monitor first 
			hMonitor=MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY); //the primary monitor in case of extended view

			//c) Get and store the monitor information from the monitor which is been just received
			IsMonitorInfo=GetMonitorInfo(hMonitor,&mi);

			//
			if(IsWindowPlacement && IsMonitorInfo)//both information have been received, now set the window to the top of the screen
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~ WS_OVERLAPPEDWINDOW);

				//Set the window position
				
				SetWindowPos(
							  ghwnd,
							  HWND_TOP,
							  mi.rcMonitor.left,
							  mi.rcMonitor.top,
							  mi.rcMonitor.right-mi.rcMonitor.left, //width
							  mi.rcMonitor.bottom-mi.rcMonitor.top, //height
							  SWP_NOZORDER | SWP_FRAMECHANGED       //ignore any of the Z order and trigger frame change event
							);

			}
			ShowCursor(FALSE);
		}
	}
	else //full-screen to windowed mode
	{
		// Add the removed style to the window style
		// Set the window placement/position to back to the saved position
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd,&wpPrev);

		SetWindowPos(
						ghwnd,
						HWND_TOP,
						0,0,0,0,
						SWP_NOMOVE|
						SWP_NOSIZE|
						SWP_NOOWNERZORDER|
						SWP_NOZORDER|
						SWP_FRAMECHANGED
					);
		ShowCursor(TRUE);
	}
	
}//end of ToggleFullscreen()

//Display function

void display(void)
{

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);			//Change as per 
	//Draw a colored cube:
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//Draw Pyramid 
	glTranslatef(-1.5f,0.0f,-6.0f);//This will push the Shape towards:inside screen (-6.0f) and towards -ve X-axis
	glRotatef(anglePyramid,0.0f,1.0f,0.0f); //Perform rotation around all axes
	glColor3f(1.0f, 1.0f, 1.0f); 
	glBegin(GL_TRIANGLES);
	
		//FRONT FACE OF Pyramid
		glNormal3f(0.0f,0.0f,1.0f);
		glVertex3f(0.0f,1.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,1.0f);
		glVertex3f(1.0f,-1.0f,1.0f);

		//LEFT FACE OF Pyramid
		glNormal3f(-1.0f,0.0f,0.0f);
		glVertex3f(0.0f,1.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,1.0f);
		glVertex3f(-1.0f,-1.0f,-1.0f);
		
		//BACK FACE OF Pyramid
		glNormal3f(0.0f,0.0f,-1.0f);
		glVertex3f(0.0f,1.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,-1.0f);
		glVertex3f(1.0f,-1.0f,-1.0f);

		// RIGHT FACE OF PYRAMID
		glNormal3f(1.0f,0.0f,0.0f);
		glVertex3f(0.0f,1.0f,0.0f);
		glVertex3f(1.0f,-1.0f,-1.0f);
		glVertex3f(1.0f,-1.0f,1.0f);
	
	glEnd();

	//Draw Cube
	glLoadIdentity();			  //Missing this call causes-->Cube to rotate around "Pyramid" as good as "Earth is rotating around sun"
	glTranslatef(1.5f,0.0f,-6.0f);//This will push the cube towards:inside screen (-6.0f)

	glScalef(0.75f,0.75f,0.75f); //Scale the Cube along X,Y,Z axes.

	glRotatef(angleCube,1.0f,1.0f,1.0f); //Perform rotation around all axes
	
	glBegin(GL_QUADS);
	
		//TOP FACE-R+G
		
		glNormal3f(0.0f,1.0f,0.0f);

		glVertex3f(-1.0f,1.0f,1.0f);
		glVertex3f(1.0f,1.0f,1.0f);
		glVertex3f(1.0f,1.0f,-1.0f);
		glVertex3f(-1.0f,1.0f,-1.0f);

		//BOTTOM FACE-G+B
		
		glNormal3f(0.0f,-1.0f,0.0f);

		glVertex3f(-1.0f,-1.0f,1.0f);
		glVertex3f(1.0f,-1.0f,1.0f);
		glVertex3f(1.0f,-1.0f,-1.0f);
		glVertex3f(-1.0f,-1.0f,-1.0f);

		//FRONT FACE OF CUBE-R
		
		glNormal3f(0.0f,0.0f,1.0f);

		glVertex3f(-1.0f,1.0f,1.0f);
		glVertex3f(1.0f,1.0f,1.0f);
		glVertex3f(1.0f,-1.0f,1.0f);
		glVertex3f(-1.0f,-1.0f,1.0f);

		//LEFT FACE OF CUBE-G

		glNormal3f(-1.0f,0.0f,0.0f);

		glVertex3f(-1.0f,1.0f,-1.0f);
		glVertex3f(-1.0f,1.0f,1.0f);
		glVertex3f(-1.0f,-1.0f,1.0f);
		glVertex3f(-1.0f,-1.0f,-1.0f);

		//BACK FACE OF CUBE-B

		glNormal3f(0.0f,0.0f,-1.0f);

		glVertex3f(-1.0f,1.0f,-1.0f);
		glVertex3f(1.0f,1.0f,-1.0f);
		glVertex3f(1.0f,-1.0f,-1.0f);
		glVertex3f(-1.0f,-1.0f,-1.0f);

		// RIGHT FACE OF CUBE

		glNormal3f(1.0f,0.0f,0.0f);

		glVertex3f(1.0f,1.0f,1.0f);
		glVertex3f(1.0f,1.0f,-1.0f);
		glVertex3f(1.0f,-1.0f,-1.0f);
		glVertex3f(1.0f,-1.0f,1.0f);

	glEnd();
	SwapBuffers(ghdc);

}//end of display function

void update(void)
{
	angleCube=angleCube+1.0f;
	anglePyramid=anglePyramid+1.0f;

	if(angleCube>=360.0f)
		angleCube=0.0f;

	if(anglePyramid>=360.0f)
		anglePyramid=0.0f;
}//end of update function


//Uninitialize function
void uninitialize(void)
{
	//if window is in fullscreen mode-->Switch back to windowed mode and then exit
	if(gbFullscreen==true)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);

		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd,&wpPrev);

		SetWindowPos(
					  ghwnd,
					  HWND_TOP,
					  0,0,0,0,
					  SWP_NOMOVE|
					  SWP_NOSIZE|
					  SWP_NOOWNERZORDER|
					  SWP_NOZORDER|
					  SWP_FRAMECHANGED
					);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL,NULL);

	wglDeleteContext(ghrc);
	ghrc=NULL;

	ReleaseDC(ghwnd,ghdc);
	ghdc=NULL;

	if(gpFile)
	{

		fprintf(gpFile,"Log File Closed Successfully!!");
		fclose(gpFile);
		gpFile=NULL;
	}
	DestroyWindow(ghwnd);
}//end of uninitialize()