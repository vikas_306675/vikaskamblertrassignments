/**************************************************************

 Assignment:Light: Material Demo!

 **************************************************************/
#include<windows.h>
#include<stdio.h> //for file i/o

#include<gl/GL.h>
#include<gl/glu.h> //for gluPerspective()

#define WIN_WIDTH 1920
#define WIN_HEIGHT 1080

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"gdi32.lib")

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;
bool gbLighting = false;  //for toggling light



float angleAxisRotation = 0.0f;

float XRotation = 0.0f;
float YRotation = 0.0f;
float ZRotation = 0.0f;

int aroundAlteringAxis = 0;


DWORD dwStyle;
WINDOWPLACEMENT wpPrev= {sizeof(WINDOWPLACEMENT)};

HWND ghwnd;
HDC ghdc;
HGLRC ghrc; 

FILE *gpFile=NULL;

/////////////////////////////////////////////////// GLOBAL ARRAY OF WHITE LIGHT/////////////////////////////////////////////////

GLfloat light_ambient[] = {1.0f,1.0f,1.0f,0.0f};  //Ambient property decides the "General Light"!!      
GLfloat light_diffuse[] = {1.0f,1.0f,1.0f,0.0f};  // DIFFUSE decides the color of the light!!!
GLfloat light_specular[] = {1.0f,1.0f,1.0f,0.0f}; // SPECULAR decides the "Highlight of the light"!!
GLfloat light_position[] = {0.0f,0.0f,0.0f,0.0f}; //It specifies that light is coming from +ve Z-Axis.

GLfloat light_model_ambient[] = {0.2f,0.2f,0.2f,0.0f};
GLfloat light_model_local_viewer=0.0f;

/////////////////////////////////////////////////// GLOBAL ARRAY DECLARATION FOR MATERIALS OF 24 SPHERES////////////////////////

///////////////Emerald::Material For Sphere[1][1]////////////////////////////////////
GLfloat material_sphere_1_1_ambient[] = {0.0215f,0.1745f,0.0215f,1.0f};
GLfloat material_sphere_1_1_diffuse[] = {0.07568f,0.61424f,0.07568f,1.0f};
GLfloat material_sphere_1_1_specular[] = {0.633f,0.727811f,0.633f,1.0f};
GLfloat material_sphere_1_1_shininess=0.6*50.0f;

///////////////Jade:: Material For Sphere[2][1]////////////////////////////////////
GLfloat material_sphere_2_1_ambient[] = { 0.135f,0.2225f,0.1575f,1.0f };
GLfloat material_sphere_2_1_diffuse[] = { 0.54f,0.89f,0.63f,1.0f };
GLfloat material_sphere_2_1_specular[] = { 0.316228f,0.316228f,0.316228f,1.0f };
GLfloat material_sphere_2_1_shininess = 0.1*128.0f;

///////////////Obsidian::Material For Sphere[3][1]////////////////////////////////////
GLfloat material_sphere_3_1_ambient[] = { 0.05375f,0.05f,0.06625f,1.0f };
GLfloat material_sphere_3_1_diffuse[] = { 0.18275f,0.17f,0.22525f,1.0f };
GLfloat material_sphere_3_1_specular[] = { 0.332741f,0.328634f,0.346435f,1.0f };
GLfloat material_sphere_3_1_shininess = 0.3*128.0f;

///////////////Pearl::Material For Sphere[4][1]////////////////////////////////////
GLfloat material_sphere_4_1_ambient[] = { 0.25f,0.20725f,0.20725f,1.0f };
GLfloat material_sphere_4_1_diffuse[] = { 1.0f,0.829f,0.829f,1.0f };
GLfloat material_sphere_4_1_specular[] = { 0.296648f,0.296648f,0.296648f,1.0f };
GLfloat material_sphere_4_1_shininess = 0.088*128.0f;

///////////////Ruby::Material For Sphere[5][1]////////////////////////////////////
GLfloat material_sphere_5_1_ambient[] = { 0.1745f,0.01175f,0.01175f,1.0f };
GLfloat material_sphere_5_1_diffuse[] = { 0.61424f,0.04136f,0.04136f,1.0f };
GLfloat material_sphere_5_1_specular[] = { 0.0727811f,0.626959f,0.626959f,1.0f };
GLfloat material_sphere_5_1_shininess = 0.6*128.0f;

///////////////Turquoise::Material For Sphere[6][1]////////////////////////////////////
GLfloat material_sphere_6_1_ambient[] = { 0.1f,0.18725f,0.1745f,1.0f };
GLfloat material_sphere_6_1_diffuse[] = { 0.396f,0.74151f,0.69102f,1.0f };
GLfloat material_sphere_6_1_specular[] = { 0.297254f,0.30829f,0.306678f,1.0f };
GLfloat material_sphere_6_1_shininess = 0.1*128.0f;

///////////////Brass::Material For Sphere[1][2]////////////////////////////////////
GLfloat material_sphere_1_2_ambient[] = { 0.329412f,0.223529f,0.027451f,1.0f };
GLfloat material_sphere_1_2_diffuse[] = { 0.780392f,0.568627f,0.113725f,1.0f };
GLfloat material_sphere_1_2_specular[] = { 0.992157f,0.941176f,0.807843f,1.0f };
GLfloat material_sphere_1_2_shininess = 0.21794872f*128.0f;

///////////////Bronze::Material For Sphere[2][2]////////////////////////////////////
GLfloat material_sphere_2_2_ambient[] = { 0.2125f,0.1275f,0.054f,1.0f };
GLfloat material_sphere_2_2_diffuse[] = { 0.714f,0.4284f,0.18144f,1.0f };
GLfloat material_sphere_2_2_specular[] = { 0.393548f,0.271906f,0.166721f,1.0f };
GLfloat material_sphere_2_2_shininess = 0.2f*128.0f;

///////////////Chrome::Material For Sphere[3][2]////////////////////////////////////
GLfloat material_sphere_3_2_ambient[] = { 0.25f,0.25f,0.25f,1.0f };
GLfloat material_sphere_3_2_diffuse[] = { 0.4f,0.4f,0.4f,1.0f };
GLfloat material_sphere_3_2_specular[] = { 0.774597f,0.774597f,0.744597f,1.0f };
GLfloat material_sphere_3_2_shininess = 0.6f*128.0f;

///////////////Copper::Material For Sphere[4][2]////////////////////////////////////
GLfloat material_sphere_4_2_ambient[] = { 0.19125f,0.0735f,0.0225f,1.0f };
GLfloat material_sphere_4_2_diffuse[] = { 0.7038f,0.27048f,0.0828f,1.0f };
GLfloat material_sphere_4_2_specular[] = { 0.256777f,0.137622f,0.086014f,1.0f };
GLfloat material_sphere_4_2_shininess = 0.1f*128.0f;

///////////////Gold::Material For Sphere[5][2]////////////////////////////////////
GLfloat material_sphere_5_2_ambient[] = { 0.24725f,0.1995f,0.0745f,1.0f };
GLfloat material_sphere_5_2_diffuse[] = { 0.75164f,0.60048f,0.22648f,1.0f };
GLfloat material_sphere_5_2_specular[] = { 0.628281f,0.555802f,0.366065f,1.0f };
GLfloat material_sphere_5_2_shininess = 0.4f*128.0f;

///////////////Silver::Material For Sphere[6][2]////////////////////////////////////
GLfloat material_sphere_6_2_ambient[] = { 0.19225f,0.19225f,0.19225f,1.0f };
GLfloat material_sphere_6_2_diffuse[] = { 0.50754f,0.50754f,0.50754f,1.0f };
GLfloat material_sphere_6_2_specular[] = { 0.508273f,0.508273f,0.508273f,1.0f };
GLfloat material_sphere_6_2_shininess = 0.4f*128.0f;

///////////////Black::Material For Sphere[1][3]////////////////////////////////////
GLfloat material_sphere_1_3_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_sphere_1_3_diffuse[] = { 0.01f,0.01f,0.01f,1.0f };
GLfloat material_sphere_1_3_specular[] = { 0.50f,0.50f,0.50f,1.0f };
GLfloat material_sphere_1_3_shininess = 0.25f*128.0f;

///////////////Cyan::Material For Sphere[2][3]////////////////////////////////////
GLfloat material_sphere_2_3_ambient[] = { 0.0f,0.1f,0.06f,1.0f };
GLfloat material_sphere_2_3_diffuse[] = { 0.0f,0.50980392f,0.50980392f,1.0f };
GLfloat material_sphere_2_3_specular[] = { 0.50196078f,0.50106078f,0.50196078f,1.0f };
GLfloat material_sphere_2_3_shininess = 0.25f*128.0f;

///////////////Green::Material For Sphere[3][3]////////////////////////////////////
GLfloat material_sphere_3_3_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_sphere_3_3_diffuse[] = { 0.1f,0.35f,0.1f,1.0f };
GLfloat material_sphere_3_3_specular[] = { 0.45f,0.55f,0.45f,1.0f };
GLfloat material_sphere_3_3_shininess = 0.25f*128.0f;

///////////////Red::Material For Sphere[4][3]////////////////////////////////////
GLfloat material_sphere_4_3_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_sphere_4_3_diffuse[] = { 0.5f,0.0f,0.0f,1.0f };
GLfloat material_sphere_4_3_specular[] = { 0.7f,0.6f,0.6f,1.0f };
GLfloat material_sphere_4_3_shininess = 0.25f*128.0f;

///////////////White::Material For Sphere[5][3]////////////////////////////////////
GLfloat material_sphere_5_3_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_sphere_5_3_diffuse[] = { 0.55f,0.55f,0.55f,1.0f };
GLfloat material_sphere_5_3_specular[] = { 0.70f,0.70f,0.70f,1.0f };
GLfloat material_sphere_5_3_shininess = 0.25f*128.0f;

///////////////Yellow Plastic::Material For Sphere[6][3]////////////////////////////////////
GLfloat material_sphere_6_3_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_sphere_6_3_diffuse[] = { 0.5f,0.5f,0.0f,1.0f };
GLfloat material_sphere_6_3_specular[] = { 0.60f,0.60f,0.50f,1.0f };
GLfloat material_sphere_6_3_shininess = 0.25f*128.0f;

///////////////Black::Material For Sphere[1][4]////////////////////////////////////
GLfloat material_sphere_1_4_ambient[] = { 0.02f,0.02f,0.02f,1.0f };
GLfloat material_sphere_1_4_diffuse[] = { 0.01f,0.01f,0.01f,1.0f };
GLfloat material_sphere_1_4_specular[] = { 0.4f,0.4f,0.4f,1.0f };
GLfloat material_sphere_1_4_shininess = 0.078125f*128.0f;

///////////////Cyan::Material For Sphere[2][4]////////////////////////////////////
GLfloat material_sphere_2_4_ambient[] = { 0.0f,0.05f,0.05f,1.0f };
GLfloat material_sphere_2_4_diffuse[] = { 0.4f,0.5f,0.5f,1.0f };
GLfloat material_sphere_2_4_specular[] = { 0.04f,0.7f,0.7f,1.0f };
GLfloat material_sphere_2_4_shininess = 0.078125f*128.0f;

///////////////Green::Material For Sphere[3][4]////////////////////////////////////
GLfloat material_sphere_3_4_ambient[] = { 0.0f,0.05f,0.0f,1.0f };
GLfloat material_sphere_3_4_diffuse[] = { 0.4f,0.5f,0.4f,1.0f };
GLfloat material_sphere_3_4_specular[] = { 0.04f,0.7f,0.04f,1.0f };
GLfloat material_sphere_3_4_shininess = 0.078125f*128.0f;

///////////////Red::Material For Sphere[4][4]////////////////////////////////////
GLfloat material_sphere_4_4_ambient[] = { 0.05f,0.0f,0.0f,1.0f };
GLfloat material_sphere_4_4_diffuse[] = { 0.5f,0.4f,0.4f,1.0f };
GLfloat material_sphere_4_4_specular[] = { 0.7f,0.04f,0.04f,1.0f };
GLfloat material_sphere_4_4_shininess = 0.078125f*128.0f;

///////////////White::Material For Sphere[5][4]////////////////////////////////////
GLfloat material_sphere_5_4_ambient[] = { 0.05f,0.05f,0.05f,1.0f };
GLfloat material_sphere_5_4_diffuse[] = { 0.5f,0.5f,0.5f,1.0f };
GLfloat material_sphere_5_4_specular[] = { 0.7f,0.7f,0.7f,1.0f };
GLfloat material_sphere_5_4_shininess = 0.078125f*128.0f;

///////////////Yellow Rubber::Material For Sphere[6][4]////////////////////////////////////
GLfloat material_sphere_6_4_ambient[] = { 0.05f,0.05f,0.0f,1.0f };
GLfloat material_sphere_6_4_diffuse[] = { 0.5f,0.5f,0.4f,1.0f };
GLfloat material_sphere_6_4_specular[] = { 0.7f,0.7f,0.04f,1.0f };
GLfloat material_sphere_6_4_shininess = 0.078125f*128.0f;



GLUquadric *quadric=NULL; //For 24 Spheres.

HRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR nCmdLine,int nCmdShow)
{
	//function prototypes
	void initialize(void);
	void update(void);
	void display(void);
	void uninitialize(void);

	//Variables
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	bool bDone=false;
	TCHAR szAppName[]=TEXT("Assignment_Material_Demo");

	if(fopen_s(&gpFile,"AssignmentFFP_Material_Demo_Log.txt","w")!=0)
	{
		MessageBox(NULL,TEXT("Log file could not be created..exiting!!"),TEXT("Error"),MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile,"Log File Created Successfully!!\n");
	}

	//code
	wndClass.cbSize=sizeof(WNDCLASSEX);
	wndClass.style=CS_HREDRAW | CS_VREDRAW | CS_OWNDC; //CS_OWNDC-->Non-purgable DC
	wndClass.cbClsExtra=0;
	wndClass.cbWndExtra=0;

	wndClass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hInstance=hInstance;
	wndClass.hIcon=LoadIcon(NULL,IDI_APPLICATION); //load the default icon
	wndClass.hIconSm=LoadIcon(NULL,IDI_APPLICATION); //Icon for "System menu"
	wndClass.hCursor=LoadCursor(NULL,IDC_ARROW);	 //Load default cursor, you can specific the cursor type while creating a game application (Confirm?)

	wndClass.lpfnWndProc=WndProc;
	wndClass.lpszClassName=szAppName;
	wndClass.lpszMenuName=NULL;

	//Register the created class
	RegisterClassEx(&wndClass);

	//create the window
	hwnd=CreateWindowEx(
						 WS_EX_APPWINDOW, //exclusive full-screen window
						 szAppName,
						 TEXT("AssignmentFFP: Light-Material Demo!!"),
						 WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS|WS_VISIBLE,
						 0,
						 0,
						 WIN_WIDTH,
						 WIN_HEIGHT,
						 NULL,		
						 NULL,
						 hInstance,
						 NULL
						);

	ghwnd=hwnd;	//For further use of the global window handle

	initialize(); //Call to initialize(Do the preparation of the "Picture element" (Pixel)

	ShowWindow(hwnd,nCmdShow);//Show the created window
	SetForegroundWindow(hwnd);//Bring that window in foreground
	SetFocus(hwnd);//Set the focus on the window



	//Game loop
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}//Events further generating the messages
		else
		{
			if(gbActiveWindow==true)
			{
				if(gbEscapeKeyIsPressed==true)
				{
					bDone=true;
				}
				update();
				display();
			}
			
		}//game will run here

	}//end of while
	uninitialize();
	return msg.wParam; 
}//WinMain

/*Initialize:
  1. PIXELFORMATDESCRIPTOR inialize
  2. ChoosePixelFormat
  3. SetPixelFormat
  4. Get the device context from OS
  5. Create OpenGL Context:wglCreateContext()
  6. HandOver control from OS DC to Rendering DC: wglMakeCurrent()
  7. Clear the window with desired color
  8. Give a warmup call to resize()

*/
void initialize(void)
{
	//function prototypes

	void resize(int,int);

	//Variables
	PIXELFORMATDESCRIPTOR pfd;	//a structure which we need to fill will possible set of values from our side and rest can be achieved from OS
	int iPixelFormatIndex;
	//code
	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));//Make the 26-membered structure to 0

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags= PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; //dword flags depicting pixel characteristics
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cGreenBits=8;
	pfd.cBlueBits=8;
	pfd.cAlphaBits=8;
	pfd.cDepthBits = 32;

	ghdc=GetDC(ghwnd);//Get the regular device context from OS (this is non-purgeable)

	iPixelFormatIndex=ChoosePixelFormat(ghdc,&pfd);//After getting the DC, "Choose the pixel format" which is the closest match to the values which we have provides

	if(iPixelFormatIndex==0)//if the pixelformatindex is empty due to function could not succeed
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd)==false) //Set the chosen matching closest pixel format
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	ghrc=wglCreateContext(ghdc);//Now handover the device context to OpenGL's "Rendering context" through WGL

	if(ghrc==NULL)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	if(wglMakeCurrent(ghdc,ghrc)==NULL)//Once the handover is successful-->Make "Rendering context" as the current context through WGL
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	glClearColor(0.25f,0.25f,0.25f,0.0f); //Dark Grey: Now clear the window with desired color

	//////////////////////// Enable Depth////////////////////////////////////////

	//Enable Depth options as it is a pre-requisute for Light!!
	//[Depth lets you decide on which surface of 3D object you dont want to enable light, this control comes from depth-->Mhnaje to bhag zakala janar-->yalach hidden surface removal].

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	////////////////////////Lighting Model Relared initialization////////////////
	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT,light_model_ambient);
	glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER,light_model_local_viewer);

	////////////////////// Initialize global light array/////////////////////////

	//Enable Lights/initialization
	glLightfv(GL_LIGHT0, GL_AMBIENT,light_ambient);//glLight(f:float v:vector)-->glLightfv()
	glLightfv(GL_LIGHT0, GL_DIFFUSE,light_diffuse);
	glLightfv(GL_LIGHT0,GL_SPECULAR,light_specular);
	//glLightfv(GL_LIGHT0,GL_POSITION,light_position); //--->Position of light will be animated

	glEnable(GL_LIGHT0);

	////////////////////// Create quadric objects for 24 spheres//////////////////////////

	quadric= gluNewQuadric();

	resize(WIN_WIDTH,WIN_HEIGHT); //a warmup call to resize function--In the case of "Projection" this call is of crucial importance as this call will help keep the viewing volume ready before drawing happens.

}//end of initialize()

//Resize function:
// Projection functions are conventionally called inside the function which deals with the change in the size of the window

void resize(int width,int height)
{
	if(height==0)
		height=1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION); //You nees "Projection matrix" to tell OpenGL everything about projection
	glLoadIdentity(); 
	
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);  //gluPerspective(fovy, width/height ratio,near,far);--> fovy:field of view in "Y" axis direction
		
}//end of resize

//CALLBACK Function
HRESULT CALLBACK WndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam)
{
	//function declarations
	void resize(int,int);
	void uninitialize(void);
	void ToggleFullscreen(void);
	//Code
	switch(message)
	{

	case WM_CREATE:
					break;

	case WM_ACTIVATE:
					 if(HIWORD(wParam)==0)
						 gbActiveWindow=true;
					 else
						 gbActiveWindow=false;
					break;

	case WM_KEYDOWN:
					switch(wParam)
					{
					case VK_ESCAPE:
									gbEscapeKeyIsPressed=true;
									break;

						case 0x46: //for fullscreen
									if(gbFullscreen==false)
									{
										ToggleFullscreen();
										gbFullscreen=true;
									}
									else
									{
										ToggleFullscreen();
										gbFullscreen=false;
									}
									break;

						case 0x4C: //L for lights
									fprintf(gpFile,"Entered in Light Key!\n");
									if (gbLighting == false)
									{
										//glEnable(GL_LIGHT0);
										glEnable(GL_LIGHTING); //This statement enables the "GLOBAL LIGHT SWITCH" without which LIGHT CANNOT WORK!!
										gbLighting = true;
										//aroundAlteringAxis = 2;
										light_position[2] = 2.0f;
									}
									else
									{
										//glDisable(GL_LIGHT0);
										glDisable(GL_LIGHTING); //ethe openGL chi lighting enable hoil
										gbLighting = false;
										//Rotation should stop once the light has been disabled 
										XRotation = 0.0f;
										YRotation = 0.0f;
										ZRotation = 0.0f;
									}
									break;
	
						case 0x58:// X for rotation of light around X-Axis
									fprintf(gpFile,"Under X Character\n");
									
									aroundAlteringAxis = 1; //Y-axis values will be altered during rotation around X-Axis.
									XRotation = 1.0f;
									YRotation = 0.0f;
									ZRotation = 0.0f;
									
									break;
						case 0x59:// Y for rotation of light around Y-Axis
									fprintf(gpFile,"Under Y Character\n");
									
									aroundAlteringAxis = 0; //X-Axis values will be altered during rotation around Y-Axis.
									XRotation = 0.0f;
									YRotation = 1.0f;
									ZRotation = 0.0f;
									
									break;
						case 0x5A:// Z for rotation of light around Z-Axis
									fprintf(gpFile,"Under Z Character\n");

									aroundAlteringAxis = 1; //Y-Axis values will be altered during rotation around Z-Axis.
									XRotation = 0.0f;
									YRotation = 0.0f;
									ZRotation = 1.0f;
									
									break;
						case 0x51: //Q
									uninitialize();
									PostQuitMessage(0);
									break;
						default:
									//aroundAlteringAxis = 2;
									//angle
									break;
					}
					break;

	case WM_SIZE:
					resize(LOWORD(lParam),HIWORD(lParam));
					break;

	case WM_DESTROY:
					PostQuitMessage(0);
					break;

	}
	return(DefWindowProc(hwnd,message,wParam,lParam));

}//End of call back function

/*ToggleFullscreen function
	1. Get the window style using GetWindowLong()
	2. If the "Current window" type contains the "WS_OVERLAPPEDWINDOW" -->Remove it.
	3. Store the current window position
	4. Get the monitor info(Primary display)
	5. Set the window position
*/
void ToggleFullscreen(void)
{
	//variable declaration
	MONITORINFO mi={sizeof(MONITORINFO)};
	bool IsWindowPlacement;
	bool IsMonitorInfo;
	HMONITOR hMonitor;

	//code
	if(gbFullscreen==false) //windowed to full-screen
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE); //Get the window style/entire information of the window
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			//a) Get the window Placement and Monitor information then and then toggle to fullscreen
			IsWindowPlacement=GetWindowPlacement(ghwnd,&wpPrev);

			//b) Now get the monitor first 
			hMonitor=MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY); //the primary monitor in case of extended view

			//c) Get and store the monitor information from the monitor which is been just received
			IsMonitorInfo=GetMonitorInfo(hMonitor,&mi);

			//
			if(IsWindowPlacement && IsMonitorInfo)//both information have been received, now set the window to the top of the screen
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~ WS_OVERLAPPEDWINDOW);

				//Set the window position
				
				SetWindowPos(
							  ghwnd,
							  HWND_TOP,
							  mi.rcMonitor.left,
							  mi.rcMonitor.top,
							  mi.rcMonitor.right-mi.rcMonitor.left, //width
							  mi.rcMonitor.bottom-mi.rcMonitor.top, //height
							  SWP_NOZORDER | SWP_FRAMECHANGED       //ignore any of the Z order and trigger frame change event
							);

			}
			ShowCursor(FALSE);
		}
	}
	else //full-screen to windowed mode
	{
		// Add the removed style to the window style
		// Set the window placement/position to back to the saved position
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd,&wpPrev);

		SetWindowPos(
						ghwnd,
						HWND_TOP,
						0,0,0,0,
						SWP_NOMOVE|
						SWP_NOSIZE|
						SWP_NOOWNERZORDER|
						SWP_NOZORDER|
						SWP_FRAMECHANGED
					);
		ShowCursor(TRUE);
	}
	
}//ToggleFullscreen()

void update(void)
{
	if(XRotation==1 || YRotation==1|| ZRotation==1)
		angleAxisRotation = angleAxisRotation + 1.0f;
	
	if (angleAxisRotation>=360.0f)
		angleAxisRotation = 0.0f;
}//update()

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();

	gluLookAt(0.0f, 0.0f, 5.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	
	if (XRotation == 1 || YRotation == 1 || ZRotation == 1)
	{
		glPushMatrix();
		glRotatef(angleAxisRotation, XRotation, YRotation, ZRotation);
		light_position[aroundAlteringAxis] = angleAxisRotation;
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		glPopMatrix();
	}
	else
	{
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	}

	
	glPushMatrix();
	///////////// Sphere[1][1]/////////////////////////////////////////////////////////////////////////////////////////////
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_sphere_1_1_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_sphere_1_1_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_sphere_1_1_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_sphere_1_1_shininess);
	
	glTranslatef(-4.0f, 2.7f, -8.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.50f, 100, 100);//gluSphere(GLUQuadric, radius,no of slices and number of stacks) 
	
	
    ///////////// Sphere[2][1]///////////////////////////////////////////
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_sphere_2_1_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_sphere_2_1_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_sphere_2_1_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_sphere_2_1_shininess);
	
	glTranslatef(-4.0f,1.6f,-8.0f);
	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	gluSphere(quadric,0.50,100,100);

	
	///////////// Sphere[3][1]///////////////////////////////////////////
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_sphere_3_1_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_sphere_3_1_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_sphere_3_1_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_sphere_3_1_shininess);
	
	glTranslatef(-4.0f,0.5f,-8.0f);
	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	gluSphere(quadric,0.50f,100,100);

	///////////// Sphere[4][1]///////////////////////////////////////////
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_sphere_4_1_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_sphere_4_1_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_sphere_4_1_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_sphere_4_1_shininess);

	glTranslatef(-4.0f,-0.6f,-8.0f);
	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	gluSphere(quadric,0.50f,100,100);

	///////////// Sphere[5][1]///////////////////////////////////////////
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_sphere_5_1_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_sphere_5_1_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_sphere_5_1_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_sphere_5_1_shininess);
	
	glTranslatef(-4.0f,-1.7f,-8.0f);
	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	gluSphere(quadric,0.50f,100,100);

	///////////// Sphere[6][1]///////////////////////////////////////////
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_sphere_6_1_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_sphere_6_1_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_sphere_6_1_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_sphere_6_1_shininess);

	glTranslatef(-4.0f,-2.8f,-8.0f);
	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	gluSphere(quadric,0.50f,100,100);

	///////////// Sphere[1][2]///////////////////////////////////////////////////////////////////////////////////////////////////
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_sphere_1_2_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_sphere_1_2_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_sphere_1_2_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_sphere_1_2_shininess);
	
	glTranslatef(-1.5f,2.7f,-8.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric,0.50f,100,100);//gluSphere(GLUQuadric, radius,no of slices and number of stacks) 
	
    ///////////// Sphere[2][2]///////////////////////////////////////////
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_sphere_2_2_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_sphere_2_2_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_sphere_2_2_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_sphere_2_2_shininess);

	glTranslatef(-1.5f, 1.6f, -8.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.50, 100, 100);

	///////////// Sphere[3][2]///////////////////////////////////////////
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_sphere_3_2_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_sphere_3_2_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_sphere_3_2_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_sphere_3_2_shininess);

	glTranslatef(-1.5f, 0.5f, -8.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.50f, 100, 100);

	///////////// Sphere[4][2]///////////////////////////////////////////

	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_sphere_4_2_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_sphere_4_2_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_sphere_4_2_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_sphere_4_2_shininess);

	glTranslatef(-1.5f, -0.6f, -8.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.50f, 100, 100);

	///////////// Sphere[5][2]///////////////////////////////////////////

	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_sphere_5_2_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_sphere_5_2_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_sphere_5_2_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_sphere_5_2_shininess);

	glTranslatef(-1.5f, -1.7f, -8.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.50f, 100, 100);

	///////////// Sphere[6][2]///////////////////////////////////////////
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_sphere_6_2_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_sphere_6_2_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_sphere_6_2_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_sphere_6_2_shininess);

	glTranslatef(-1.5f, -2.8f, -8.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.50f, 100, 100);

	///////////// Sphere[1][3]///////////////////////////////////////////////////////////////////////////////////////////////////
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_sphere_1_3_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_sphere_1_3_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_sphere_1_3_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_sphere_1_3_shininess);

	glTranslatef(1.0f, 2.7f, -8.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.50f, 100, 100);//gluSphere(GLUQuadric, radius,no of slices and number of stacks) 

	///////////// Sphere[2][3]///////////////////////////////////////////
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_sphere_2_3_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_sphere_2_3_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_sphere_2_3_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_sphere_2_3_shininess);

	glTranslatef(1.0f, 1.6f, -8.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.50, 100, 100);

	///////////// Sphere[3][3]///////////////////////////////////////////
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_sphere_3_3_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_sphere_3_3_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_sphere_3_3_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_sphere_3_3_shininess);

	glTranslatef(1.0f, 0.5f, -8.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.50f, 100, 100);

	///////////// Sphere[4][3]///////////////////////////////////////////
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_sphere_4_3_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_sphere_4_3_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_sphere_4_3_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_sphere_4_3_shininess);

	glTranslatef(1.0f, -0.6f, -8.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.50f, 100, 100);

	///////////// Sphere[5][3]///////////////////////////////////////////
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_sphere_5_3_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_sphere_5_3_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_sphere_5_3_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_sphere_5_3_shininess);

	glTranslatef(1.0f, -1.7f, -8.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.50f, 100, 100);

	///////////// Sphere[6][3]///////////////////////////////////////////
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_sphere_6_3_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_sphere_6_3_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_sphere_6_3_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_sphere_6_3_shininess);

	glTranslatef(1.0f, -2.8f, -8.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.50f, 100, 100);

	///////////// Sphere[1][4]///////////////////////////////////////////////////////////////////////////////////////////////////
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_sphere_1_4_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_sphere_1_4_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_sphere_1_4_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_sphere_1_4_shininess);

	glTranslatef(3.5f, 2.7f, -8.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.50f, 100, 100);//gluSphere(GLUQuadric, radius,no of slices and number of stacks) 

	///////////// Sphere[2][4]///////////////////////////////////////////
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_sphere_2_4_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_sphere_2_4_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_sphere_2_4_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_sphere_2_4_shininess);

	glTranslatef(3.5f, 1.6f, -8.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.50, 100, 100);

	///////////// Sphere[3][4]///////////////////////////////////////////
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_sphere_3_4_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_sphere_3_4_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_sphere_3_4_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_sphere_3_4_shininess);

	glTranslatef(3.5f, 0.5f, -8.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.50f, 100, 100);

	///////////// Sphere[4][4]///////////////////////////////////////////
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_sphere_4_4_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_sphere_4_4_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_sphere_4_4_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_sphere_4_4_shininess);

	glTranslatef(3.5f, -0.6f, -8.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.50f, 100, 100);

	///////////// Sphere[5][4]///////////////////////////////////////////
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_sphere_5_4_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_sphere_5_4_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_sphere_5_4_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_sphere_5_4_shininess);

	glTranslatef(3.5f, -1.7f, -8.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.50f, 100, 100);

	///////////// Sphere[6][4]///////////////////////////////////////////
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_sphere_6_4_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_sphere_6_4_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_sphere_6_4_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_sphere_6_4_shininess);

	glTranslatef(3.5f, -2.8f, -8.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.50f, 100, 100);
	glPopMatrix();

	glPopMatrix();
		
	SwapBuffers(ghdc);

}//end of display function

//Uninitialize function
void uninitialize(void)
{
	//if window is in fullscreen mode-->Switch back to windowed mode and then exit
	
	if(quadric)
		gluDeleteQuadric(quadric); //It will delete the quadric object.
	
	if(gbFullscreen==true)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);

		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd,&wpPrev);

		SetWindowPos(
					  ghwnd,
					  HWND_TOP,
					  0,0,0,0,
					  SWP_NOMOVE|
					  SWP_NOSIZE|
					  SWP_NOOWNERZORDER|
					  SWP_NOZORDER|
					  SWP_FRAMECHANGED
					);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL,NULL);

	wglDeleteContext(ghrc);
	ghrc=NULL;

	ReleaseDC(ghwnd,ghdc);
	ghdc=NULL;

	if(gpFile)
	{

		fprintf(gpFile,"Log File Closed Successfully!!");
		fclose(gpFile);
		gpFile=NULL;
	}
	DestroyWindow(ghwnd);
}//end of uninitialize()