/**************************************************************
FreeGLUT: Program:Kundali!!!
***************************************************************/

#include<GL/freeglut.h>
#include<GL/gl.h>

//global variable declaration
bool bgFullscreen=false; //Variable for fullscreen toggle

int main(int argc,char**argv)
{
	//function prototypes
	void display(void);
	void resize(int,int);
	void initialize(void);
	void uninitialize(void);
	void mouse(int,int,int,int);
	void keyboard(unsigned char,int,int);

	//code
	glutInit(&argc,argv); //Initialize the GLUT and and windowing 

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

	glutInitWindowSize(1024,768);	//initial window size
	glutInitWindowPosition(100,100);	//initial window position
	glutCreateWindow("FreeGLUT: Kundali!!");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	//return(0);

}//end of main()

//Display

void display(void)
{
	//variables
	GLfloat x1,y1,x2,y2,x3,y3,x4,y4,z1,z2,z3,z4;
	x1=-0.5f;
	y1=-0.5f;
    z1=0.0f;

	x2=-0.5f;
	y2= 0.5f;
	z2=0.0f;

	x3=0.5f;
	y3=0.5f;
	z3=0.0f;

	x4=0.5f;
	y4=-0.5f;
	z4=0.0f;
	
	//code
	/******Draw CornFlower Rectangle**************/
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glBegin(GL_LINE_STRIP);
		glVertex3f(x1,y1,z1);
		glVertex3f(x2,y2,z2);
		glVertex3f(x3,y3,z3);
		glVertex3f(x4,y4,z4);
		glVertex3f(x1,y1,z1);
	
		//Inner square
		glVertex3f((x1+x2)/2,(y1+y2)/2,(z1+z2)/2);
		glVertex3f((x2+x3)/2,(y2+y3)/2,(z2+z3)/2);
		glVertex3f((x3+x4)/2,(y3+y4)/2,(z3+z4)/2);
		glVertex3f((x1+x4)/2,(y1+y4)/2,(z1+z4)/2);
		glVertex3f((x1+x2)/2,(y1+y2)/2,(z1+z2)/2);
	glEnd();

	glBegin(GL_LINES);
		//Cross lines
		glVertex3f(x2,y2,z2);
		glVertex3f(x4,y4,z4);

		glVertex3f(x3,y3,z3);
		glVertex3f(x1,y1,z1);
	glEnd();

	glutSwapBuffers();
}

//Initialize
void initialize(void)
{
	//code
	glClearColor(0.0f,0.0f,0.0f,0.0f);

}//end of initialize()


//keyboard function
void keyboard(unsigned char key,int x,int y)
{
	switch(key)
	{
	case 27://escape
			glutLeaveMainLoop();
			break;
	case 'F':
	case 'f':
			 if(bgFullscreen==false)
			 {
				 glutFullScreen();
				 bgFullscreen=true;
			 }
			 else
			 {
				 glutLeaveFullScreen();
				 bgFullscreen=false;
			 }
			 break;
	default:
			 break;
	}
}

//mouse function
void mouse(int button,int state,int x,int y)
{
	//code

}//end of mouse

//resize function
void resize(int width,int height)
{
	if(height==0)
		height=1;

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

}

//uninitialize
void uninitialize(void)
{
	//code
}



