/**************************************************************
FreeGLUT: Program:Concentric Circles!!
***************************************************************/

#include<GL/freeglut.h>
#include<GL/gl.h>
#include<math.h>

#define PI 3.1415926535898

//global variable declaration
bool bgFullscreen=false; //Variable for fullscreen toggle

float R,G,B;

int main(int argc,char**argv)
{
	//function prototypes
	void display(void);
	void resize(int,int);
	void initialize(void);
	void uninitialize(void);
	void mouse(int,int,int,int);
	void keyboard(unsigned char,int,int);

	//code
	glutInit(&argc,argv); //Initialize the GLUT and and windowing 

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

	glutInitWindowSize(1024,768);	//initial window size
	glutInitWindowPosition(100,100);	//initial window position
	glutCreateWindow("FreeGLUT: Concentric Circles!!");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	//return(0);

}//end of main()

//Display

void display(void)
{
	//Function Prototypes
	void SetColorForCircle(int);
	
	//variables
	GLfloat Radius=0.1f,angle=0.0f;
	int StepCount=1;

	//code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	for(; StepCount<=10;StepCount++,Radius=Radius+0.1f)
	{
		SetColorForCircle(StepCount);
		glColor3f(R,G,B);

		glBegin(GL_POINTS);	
			for(angle=0.0f; angle<2.0f*PI; angle=angle+0.001f)
			{
				glVertex3f(Radius*cos(angle),Radius*sin(angle),0.0f);	

			}//For loop for drawing a circle
        glEnd();	
		

	}//Outer for loop to draw 10 Concentric Circles
	glutSwapBuffers();
}

//SetColorForTriangle

void SetColorForCircle(int StepCount)
{
	switch(StepCount)
	{
	case 1://Red
			R=1.0f; G=0.0f; B=0.0f;
			break;

    case 2://Green
			R=0.0f; G=1.0f; B=0.0f;
			break;

	case 3://Blue
			R=0.0f; G=0.0f; B=1.0f;
			break;

	case 4://Cyan

			R=0.0; G=1.0f; B=1.0f;
			break;

	case 5://Yellow

			R=1.0f; G=1.0f; B=0.0f;
			break;
	
	case 6: //Magenta
			
			R=1.0f; G=0.0f; B=1.0f;
			break;

	case 7://White
			
			R=1.0f; G=1.0f; B=1.0f;
			break;

	case 8://Orange

			R=1.0f; G=0.5f; B=0.0f;
			break;

	case 9://Dark Gray

			R=0.25f; G=0.25f; B=0.25f;
			break;

	case 10://Light Gray

			R=0.75f; G=0.75f; B=0.75f;
			break;
	}

}//end of SetColorForCircle

//Initialize
void initialize(void)
{
	//code
	glClearColor(0.0f,0.0f,0.0f,0.0f);

}//end of initialize()


//keyboard function
void keyboard(unsigned char key,int x,int y)
{
	switch(key)
	{
	case 27://escape
			glutLeaveMainLoop();
			break;
	case 'F':
	case 'f':
			 if(bgFullscreen==false)
			 {
				 glutFullScreen();
				 bgFullscreen=true;
			 }
			 else
			 {
				 glutLeaveFullScreen();
				 bgFullscreen=false;
			 }
			 break;
	default:
			 break;
	}
}

//mouse function
void mouse(int button,int state,int x,int y)
{
	//code

}//end of mouse

//resize function
void resize(int width,int height)
{
	if(height==0)
		height=1;

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

}

//uninitialize
void uninitialize(void)
{
	//code
}



