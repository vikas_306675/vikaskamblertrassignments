/**************************************************************
FreeGLUT: Program: Graph Paper With Yellow Circle With Points !
***************************************************************/

#include<GL/freeglut.h>
#include<GL/gl.h>
#include<math.h>

//global variable declaration
bool bgFullscreen=false; //Variable for fullscreen toggle

int main(int argc,char**argv)
{
	//function prototypes
	void display(void);
	void resize(int,int);
	void initialize(void);
	void uninitialize(void);
	void mouse(int,int,int,int);
	void keyboard(unsigned char,int,int);

	//code
	glutInit(&argc,argv); //Initialize the GLUT and and windowing 

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

	glutInitWindowSize(1024,1024);	//initial window size
	glutInitWindowPosition(100,100);	//initial window position
	glutCreateWindow("FreeGLUT:Graph Paper With Yellow Points!!");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	//return(0);

}//end of main()

//Display

void display(void)
{
		//Variables
	GLfloat fXCoOrdinate,fYCoOrdinate;
	const float PI=3.14f;
	float x=0.0f,y=0.0f;

	//code
	glClear(GL_COLOR_BUFFER_BIT); //this function is applicable in both fixed function and programmable pipeline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//*********** Horizontal Lines Code ****************

	//Horizontal 20 lines above centered RED LINE
	glColor3f(0.0f,0.0f,1.0f); //Blue color
	glLineWidth(1.0f);
	for(fYCoOrdinate=1.0f;fYCoOrdinate>0.0f;fYCoOrdinate=fYCoOrdinate-0.05f)
	{
			glBegin(GL_LINES);
				glVertex3f(-1.0f,fYCoOrdinate,0.0f);
				glVertex3f(1.0f,fYCoOrdinate,0.0f);
			glEnd();
	}
	//Horizontal Red Line
	glColor3f(1.0f,0.0f,0.0f); //Red color
	glLineWidth(3.0f);		   //Line width=3
	glBegin(GL_LINES);
		glVertex3f(-1.0f,0.0f,0.0f);
		glVertex3f(1.0f,0.0f,0.0f);
	glEnd();

	//Horizontal 20 lines below Centered RED Line
	glColor3f(0.0f,0.0f,1.0f); //Blue
	glLineWidth(1.0f);
	for(fYCoOrdinate=0.05f;fYCoOrdinate<=1.0f;fYCoOrdinate=fYCoOrdinate+0.05f)
	{
		glBegin(GL_LINES);
			glVertex3f(-1.0f,-fYCoOrdinate,0.0f);
			glVertex3f(1.0f,-fYCoOrdinate,0.0f);
		glEnd();
	}

	//*********** Vertical Lines Code ****************

	//Vertical 20 lines to the left of centered VERTICAL GREEN LINE
	glColor3f(0.0f,0.0f,1.0f); //Blue color
	glLineWidth(1.0f);
	for(fXCoOrdinate=1.0f;fXCoOrdinate>0.0f;fXCoOrdinate=fXCoOrdinate-0.05f)
	{
			glBegin(GL_LINES);
				glVertex3f(-fXCoOrdinate,1.0f,0.0f);
				glVertex3f(-fXCoOrdinate,-1.0f,0.0f);
			glEnd();
	}
	//Vertical Green Line
	glColor3f(0.0f,1.0f,0.0f); //Green color
	glLineWidth(3.0f);		   //Line width=3
	glBegin(GL_LINES);
		glVertex3f(0.0f,1.0f,0.0f);
		glVertex3f(0.0f,-1.0f,0.0f);
	glEnd();

	//Vertical 20 lines to the right side of Centered VERTICAL GREEN Line
	glColor3f(0.0f,0.0f,1.0f); //Blue
	glLineWidth(1.0f);
	for(fXCoOrdinate=0.05f; fXCoOrdinate<=1.0f; fXCoOrdinate=fXCoOrdinate+0.05f)
	{
		glBegin(GL_LINES);
			glVertex3f(fXCoOrdinate,1.0f,0.0f);
			glVertex3f(fXCoOrdinate,-1.0f,0.0f);
		glEnd();
	}
	//********** Yellow Bordered Circle Code *****************/
	glColor3f(1.0f,1.0f,0.0f);
	glBegin(GL_POINTS);
		
		for(float angle=0.0f; angle<2.0f*PI; angle=angle+0.001f)
		{
			glVertex3f(cos(angle),sin(angle),0.0f);
		}
	glEnd();
	
	glutSwapBuffers();
}


//Initialize
void initialize(void)
{
	//code
	glClearColor(0.0f,0.0f,0.0f,0.0f);

}//end of initialize()


//keyboard function
void keyboard(unsigned char key,int x,int y)
{
	switch(key)
	{
	case 27://escape
			glutLeaveMainLoop();
			break;
	case 'F':
	case 'f':
			 if(bgFullscreen==false)
			 {
				 glutFullScreen();
				 bgFullscreen=true;
			 }
			 else
			 {
				 glutLeaveFullScreen();
				 bgFullscreen=false;
			 }
			 break;
	default:
			 break;
	}
}

//mouse function
void mouse(int button,int state,int x,int y)
{
	//code

}//end of mouse

//resize function
void resize(int width,int height)
{
	if(height==0)
		height=1;

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

}

//uninitialize
void uninitialize(void)
{
	//code
}



