/**************************************************************
FreeGLUT: Program: Solar System.
***************************************************************/

#include<GL/freeglut.h>
#include<GL/gl.h>
#include<GL/glu.h> //for gluLookAt()

//global variable declaration
bool bgFullscreen=false; //Variable for fullscreen toggle

int year=0;
int day=0;

int main(int argc,char**argv)
{
	//function prototypes
	void display(void);
	void resize(int,int);
	void initialize(void);
	void uninitialize(void);
	void mouse(int,int,int,int);
	void keyboard(unsigned char,int,int);

	//code
	glutInit(&argc,argv); //Initialize the GLUT and and windowing 

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

	glutInitWindowSize(1024,1024);	//initial window size
	glutInitWindowPosition(100,100);	//initial window position
	glutCreateWindow("FreeGLUT:Solar System!!");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	//return(0);

}//end of main()

//Display

void display(void)
{
	//Function Prototypes
	//Variables
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //this function is applicable in both fixed function and programmable pipeline (Clearing the Color and Depth buffers)
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//**************************************
	//Perform View Transformation
	//9 Parameters-> Where is viewer(x,y,z) + Where viewer is looking (x,y,z) + Which is your up? (x,y,z)
	// If you do not perform this(view)transformation, then either you need to perform model transformation in such a way that the drawing occurs propery and in viewable manner
	// Here "Viewer" has been pushed back along +ve Z-axis, he is looking at the origin and his up is along "Y" axis.
	gluLookAt(0.0f,0.0f,5.0f,0.0f,0.0f,0.0f,0.0f,1.0f,0.0f); 
		
	//Large Sphere like Sun
	glColor3f(1.0f,1.0f,0.0f); //Yellow color
	
	glPushMatrix();			   //To save the current matrix state(internally) and re-use the same going forward(if needed).
	glutWireSphere(1.0, 20, 16);//draw-Sun
	glRotatef((GLfloat)year,0.0f,1.0f,0.0f); //Rotate the upcoming planet around Sun based upon year value.
	
	glTranslatef(2.0f,0.0f,0.0f);			 //Translate the earth along X axis to form the orbit
	
	glColor3f(0.4f,0.9f,1.0f);				 //Blue colored earth
	glRotatef((GLfloat)day,0.0f,1.0f,0.0f);	 //Earth will rotate along itself on "Day" basis
	glutWireSphere(0.2,10,8);				 //Draw small planet/earth
	
	glPopMatrix();

	glutSwapBuffers();
}


//Initialize
void initialize(void)
{
	//code
	glClearColor(0.0f,0.0f,0.0f,0.0f);
	glClearDepth(1.0f);		 //Setup Depth Buffer(Value range: 0.0f-1.0f)
	glEnable(GL_DEPTH_TEST);//Specify the depth test to be carried out						 
	glDepthFunc(GL_LEQUAL); //Depth test to be performed
	glShadeModel(GL_SMOOTH);			//To have smooth light shade after setting up the depth for models (Offcourse after the drawing)	
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST); //Optional function call, which will help reduce distortion if it occurs after specifying the depth
}//end of initialize() 


//keyboard function
void keyboard(unsigned char key,int x,int y)
{
	switch(key)
	{
	case 27://escape
			glutLeaveMainLoop();
			break;
	case 'F':
	case 'f':
			 if(bgFullscreen==false)
			 {
				 glutFullScreen();
				 bgFullscreen=true;
			 }
			 else
			 {
				 glutLeaveFullScreen();
				 bgFullscreen=false;
			 }
			 break;

	case 'D': //Day based rotation of earth:factor=6 (arbitrary) since day's rotation is faster or more compared to "year rotation factor"
			day=(day+6)%360; 
			glutPostRedisplay();
			break;

	case 'd'://To have rotation in opposite direction
			day=(day-6)%360;
			glutPostRedisplay();
			break;

	case 'Y'://To have revolution:factor=3(arbitrary) since year's rotation is slower as compared to "day's rotation factor"
			year=(year+3)%360;
			glutPostRedisplay();
			break;

	case 'y'://To have revolution in -ve direction
			year=(year-3)%360;
			glutPostRedisplay();
			break;

	default:
			 break;
	}
}

//mouse function
void mouse(int button,int state,int x,int y)
{
	//code

}//end of mouse

//resize function
void resize(int width,int height)
{
	if(height==0)
		height=1;
	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	glMatrixMode(GL_PROJECTION); //Projection transformation
	glLoadIdentity();

	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
}

//uninitialize
void uninitialize(void)
{
	//code
}



