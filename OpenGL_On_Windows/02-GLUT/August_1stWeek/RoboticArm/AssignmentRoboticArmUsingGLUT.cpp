/**************************************************************
FreeGLUT: Program: Solar System.
***************************************************************/

#include<GL/freeglut.h>
#include<GL/gl.h>
#include<GL/glu.h> //for gluLookAt()

//global variable declaration
bool bgFullscreen=false; //Variable for fullscreen toggle

int elbow=0;
int shoulder=0;

int main(int argc,char**argv)
{
	//function prototypes
	void display(void);
	void resize(int,int);
	void initialize(void);
	void uninitialize(void);
	void mouse(int,int,int,int);
	void keyboard(unsigned char,int,int);

	//code
	glutInit(&argc,argv); //Initialize the GLUT and and windowing 

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

	glutInitWindowSize(1024,1024);	//initial window size
	glutInitWindowPosition(100,100);	//initial window position
	glutCreateWindow("FreeGLUT:Robotic Arm!!");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	//return(0);

}//end of main()

//Display

void display(void)
{
	//Function Prototypes
	//Variables
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //this function is applicable in both fixed function and programmable pipeline (Clearing the Color and Depth buffers)
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	/****************************************************/
	glPushMatrix();
	
	glTranslatef(0.0f,0.0f,-12.0f);
	glRotatef((GLfloat)shoulder,0.0f,0.0f,1.0f); //Rotation along Z-axis (M->R)
	glTranslatef(1.0f,0.0f,0.0f);//(M->R->T)
	
	glPushMatrix();
	glScalef(2.0f,0.5f,1.0f);//(M->R->T->S)
	glutWireCube(1.0);
	glPopMatrix();//(M->R->T)

	glTranslatef(1.0f,0.0f,0.0f);//(M->R->T->T)
	glRotatef((GLfloat)elbow,0.0f,0.0f,1.0f);//(M->R->T->T->R)
	glTranslatef(1.0f,0.0f,0.0f);//(M->R->T->T->R->T)
	glPushMatrix();

	glScalef(2.0f,0.5f,1.0f);//(M->R->T->T->R->T->S)
	glutWireCube(1.0);
	glPopMatrix();//(M->R->T->T->R->T)

	glPopMatrix();//(M->R->T->T-R)
	/****************************************************/
	glutSwapBuffers();
}


//Initialize
void initialize(void)
{
	//code
	glClearColor(0.0f,0.0f,0.0f,0.0f);
	glClearDepth(1.0f);		 //Setup Depth Buffer(Value range: 0.0f-1.0f)
	glEnable(GL_DEPTH_TEST);//Specify the depth test to be carried out						 
	glDepthFunc(GL_LEQUAL); //Depth test to be performed
	glShadeModel(GL_SMOOTH);			//To have smooth light shade after setting up the depth for models (Offcourse after the drawing)	
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST); //Optional function call, which will help reduce distortion if it occurs after specifying the depth
}//end of initialize() 


//keyboard function
void keyboard(unsigned char key,int x,int y)
{
	switch(key)
	{
	case 27://escape
			glutLeaveMainLoop();
			break;
	case 'F':
	case 'f':
			 if(bgFullscreen==false)
			 {
				 glutFullScreen();
				 bgFullscreen=true;
			 }
			 else
			 {
				 glutLeaveFullScreen();
				 bgFullscreen=false;
			 }
			 break;

	case 'E': //Day based rotation of earth:factor=6 (arbitrary) since day's rotation is faster or more compared to "year rotation factor"
			elbow=(elbow + 3)%360;
			glutPostRedisplay();
			break;

	case 'e'://To have rotation in opposite direction
			elbow=(elbow-3)%360;
			glutPostRedisplay();
			break;

	case 'S'://To have revolution:factor=3(arbitrary) since year's rotation is slower as compared to "day's rotation factor"
			shoulder=(shoulder+3)%360;
			glutPostRedisplay();
			break;

	case 's'://To have revolution in -ve direction
			shoulder=(shoulder-3)%360;
			glutPostRedisplay();
			break;

	default:
			 break;
	}
}

//mouse function
void mouse(int button,int state,int x,int y)
{
	//code

}//end of mouse

//resize function
void resize(int width,int height)
{
	if(height==0)
		height=1;
	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	glMatrixMode(GL_PROJECTION); //Projection transformation
	glLoadIdentity();

	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
}

//uninitialize
void uninitialize(void)
{
	//code
}



