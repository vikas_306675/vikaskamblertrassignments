#include<windows.h>

//Entry point function for DLL

BOOL WINAPI DllMain(HANDLE hModule,DWORD dwReason,LPVOID lpvReserved)
{
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}//switch(dwReason)
	return(TRUE);
}//DllMain()

//Exported function from the DLL
int MySquare(int number)
{
	return(number*number);
}//MySquare()