#include<Windows.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//Global typedef 
typedef int(*MySquare)(int); //pointer to function to be accessed through DLL
MySquare pfn = NULL;

//Declaration of the CALLBACK function
LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdLine,int nCmdShow)
{
	//function prototypes
	
	//variables
	WNDCLASSEX wndClass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("MyExplicitClient");
	
	//code
	//Initialize the members of your own Window Class.
	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;

	wndClass.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH);
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL,IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL,IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL,IDC_ARROW);
	
	wndClass.lpfnWndProc = WndProc;
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = szAppName;

	//Register your window class.
	RegisterClassEx(&wndClass);

	//Create Window in memory
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
						   szAppName,
						   TEXT("RTR DLL Assignment!!"),
						   WS_OVERLAPPEDWINDOW,
						   100,
						   100,
						   WIN_WIDTH,
						   WIN_HEIGHT,
						   NULL,
						   NULL,
						   hInstance,
						   NULL
						);
	if (hwnd == NULL)
	{
		MessageBox(NULL,TEXT("Window Could not be Created!!"),TEXT("Error"),MB_OK);
		exit(0);
	}
	//Show the window
	ShowWindow(hwnd,nCmdShow);
	UpdateWindow(hwnd);

	//Message Loop
	while (GetMessage(&msg,NULL,0,0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}//MessageLoop
	return((int)msg.wParam);
}//WinMain()

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//variables
	HMODULE hdll = NULL;
	int num = 5,answer;
	TCHAR str[255];

	switch (iMsg)
	{
	case WM_CREATE:
					//Load the dll.
					hdll = LoadLibrary(TEXT("RTRAssignmentExplicitServer.dll"));
					if (hdll == NULL)
					{
						MessageBox(NULL,TEXT("Could not Load RTRAssignmentExplicitServe.dll !!"),TEXT("ERROR"),MB_OK);
						return -1;
					}
					//Find the address of the function to make a call.
					pfn =(MySquare)GetProcAddress(hdll,"MySquare");
					if (pfn == NULL)
					{
						MessageBox(NULL,TEXT("Could not obtain the address for MySquare()..!!"),TEXT("ERROR"),MB_OK);
						return -1;
					}
					//Now make an actual call to function through function pointer.
					answer=pfn(num);
					//Embed the output into a string through wsprintf()
					wsprintf(str,TEXT("MySquare(%d)=%d"),num,answer);
					//Display the output!
					MessageBox(NULL,str,TEXT("MySquare Output"),MB_OK);
					break;
	case WM_DESTROY:
					//Now unload the library
					if (hdll)
						FreeLibrary(hdll);
					PostQuitMessage(0);
					break;
	}//switch(iMsg)
	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}//WndProc()