/**************************************************************

 Assignment: Map viewports to different sections of the window 
			 based upon key event!!

 **************************************************************/
#include<windows.h>
#include<stdio.h> //for file i/o
#include<gl/GL.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

bool gbActiveWindow=false;
bool gbEscapeKeyIsPressed=false;
bool gbFullscreen=false;

int iKeyValue=1;
int iKeyPressed;

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"kernel32.lib")

DWORD dwStyle;
WINDOWPLACEMENT wpPrev= {sizeof(WINDOWPLACEMENT)};

HWND ghwnd;
HDC ghdc;
HGLRC ghrc; 

FILE *gpFile=NULL;

HRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR nCmdLine,int nCmdShow)
{
	//function prototypes
	void display(void);
	void initialize(void);
	void uninitialize(void);

	//Variables
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	bool bDone=false;
	TCHAR szAppName[]=TEXT("Assignment_Viewport_Mapping");

	if(fopen_s(&gpFile,"AssignmentFFP_Viewport_Mapping.txt","w")!=0)
	{
		MessageBox(NULL,TEXT("Log file could not be created..exiting!!"),TEXT("Error"),MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile,"Log File Created Successfully!!\n");
	}

	//code
	wndClass.cbSize=sizeof(WNDCLASSEX);
	wndClass.style=CS_HREDRAW | CS_VREDRAW | CS_OWNDC; //CS_OWNDC-->Non-purgable DC
	wndClass.cbClsExtra=0;
	wndClass.cbWndExtra=0;

	wndClass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hInstance=hInstance;
	wndClass.hIcon=LoadIcon(NULL,IDI_APPLICATION); //load the default icon
	wndClass.hIconSm=LoadIcon(NULL,IDI_APPLICATION); //Icon for "System menu"
	wndClass.hCursor=LoadCursor(NULL,IDC_ARROW);	 //Load default cursor, you can specific the cursor type while creating a game application (Confirm?)

	wndClass.lpfnWndProc=WndProc;
	wndClass.lpszClassName=szAppName;
	wndClass.lpszMenuName=NULL;

	//Register the created class
	RegisterClassEx(&wndClass);

	//create the window
	hwnd=CreateWindowEx(
						 WS_EX_APPWINDOW, //exclusive full-screen window
						 szAppName,
						 TEXT("AssignmentFFP: Mapping Viewport On Keyboard Event!"),
						 WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS|WS_VISIBLE,
						 0,
						 0,
						 WIN_WIDTH,
						 WIN_HEIGHT,
						 NULL,		
						 NULL,
						 hInstance,
						 NULL
						);

	ghwnd=hwnd;	//For further use of the global window handle

	initialize(); //Call to initialize(Do the preparation of the "Picture element" (Pixel)

	ShowWindow(hwnd,nCmdShow);//Show the created window
	SetForegroundWindow(hwnd);//Bring that window in foreground
	SetFocus(hwnd);//Set the focus on the window



	//Game loop
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}//Events further generating the messages
		else
		{
			if(gbActiveWindow==true)
			{
				if(gbEscapeKeyIsPressed==true)
				{
					bDone=true;
				}
				display();
			}
			
		}//game will run here

	}//end of while
	uninitialize();
	return msg.wParam; 
}//WinMain

/*Initialize:
  1. PIXELFORMATDESCRIPTOR inialize
  2. ChoosePixelFormat
  3. SetPixelFormat
  4. Get the device context from OS
  5. Create OpenGL Context:wglCreateContext()
  6. HandOver control from OS DC to Rendering DC: wglMakeCurrent()
  7. Clear the window with desired color
  8. Give a warmup call to resize()

*/
void initialize(void)
{
	//function prototypes

	void resize(int,int);

	//Variables
	PIXELFORMATDESCRIPTOR pfd;	//a structure which we need to fill will possible set of values from our side and rest can be achieved from OS
	int iPixelFormatIndex;
	//code
	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));//Make the 26-membered structure to 0

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags= PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; //dword flags depicting pixel characteristics
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cGreenBits=8;
	pfd.cBlueBits=8;
	pfd.cAlphaBits=8;

	ghdc=GetDC(ghwnd);//Get the regular device context from OS (this is non-purgeable)

	iPixelFormatIndex=ChoosePixelFormat(ghdc,&pfd);//After getting the DC, "Choose the pixel format" which is the closest match to the values which we have provides

	if(iPixelFormatIndex==0)//if the pixelformatindex is empty due to function could not succeed
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd)==false) //Set the chosen matching closest pixel format
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	ghrc=wglCreateContext(ghdc);//Now handover the device context to OpenGL's "Rendering context" through WGL

	if(ghrc==NULL)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	if(wglMakeCurrent(ghdc,ghrc)==NULL)//Once the handover is successful-->Make "Rendering context" as the current context through WGL
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	glClearColor(0.0f,0.0f,0.0f,0.0f); //Now clear the window with desired color

	resize(WIN_WIDTH,WIN_HEIGHT); //a warmup call to resize function

}//end of initialize()

//Resize function:

void resize(int width,int height)
{
	if(height==0)
		height=1;

	//glViewport(0,0,(GLsizei)width/2,(GLsizei)height/2);             --> Left-bottom-half-->(0,0,400,400)
	//glViewport(width/2,0,(GLsizei)width/2,(GLsizei)height/2);       -->Right-Bottom-half-->(400,0,400,400)
	//glViewport(0,height/2,(GLsizei)width/2,(GLsizei)height/2);      -->Left-Top-half--> (0,400,400,400)
	//glViewport(width/2,height/2,(GLsizei)width/2,(GLsizei)height/2);-->Right-Top-half-->(400,400,400,400)

	//glViewport(0,height/4,(GLsizei)width,(GLsizei)height);		  -->Top half-->(0,200,800,800)


	//glViewport(width/4,0,(GLsizei)width,(GLsizei)height); -->Right-half-->(200,0,800,800)
	//glViewport(width/4,height/4,(GLsizei)width,(GLsizei)height);-->Right-Top-half-->(200,200,800,800)
	
	switch(iKeyPressed)
	{
		
		case 1 ://Letf-Top-Half
				glViewport(0,(GLfloat)height/2,(GLsizei)width/2,(GLsizei)height/2);
				break;

		case 2 ://Right-Top-Half
				glViewport((GLfloat)width/2,(GLfloat)height/2,(GLsizei)width/2,(GLsizei)height/2);
				break;
		
		case 3 ://Left-bottom-Half
				glViewport(0,0,(GLsizei)width/2,(GLsizei)height/2);
				break;
		
		case 4 ://Right-bottom-Half
				glViewport((GLfloat)width/2,0,(GLsizei)width/2,(GLsizei)height/2);
				break;
		
		case 5 ://Left-Half
				glViewport(0,0,(GLsizei)width/2,height);
				break;
		
		case 6 ://Right-Half
				glViewport((GLfloat)width/2,0,(GLsizei)width/2,(GLsizei)height);
				break;
		
		case 7 ://Top-Half
				glViewport(0,(GLfloat)height/2,(GLsizei)width,(GLsizei)height/2);
				break;

		case 8 ://Bottom-Half
				glViewport(0,0,(GLsizei)width,(GLsizei)height/2);
				break;
		
		case 9 ://Center-Half
				glViewport((GLfloat)width/4,0,(GLsizei)width/2,(GLsizei)height);
				break;
		


		default:
				glViewport(0,0,width,height);
				break;
	}//switch


}//end of resize

//CALLBACK Function
HRESULT CALLBACK WndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam)
{
	//Variables
	int xWidth,yHeight;
	
	xWidth=GetSystemMetrics(SM_CXSCREEN);
	
	yHeight=GetSystemMetrics(SM_CYSCREEN);

	//function declarations
	void resize(int,int);
	void uninitialize(void);
	void ToggleFullscreen(void);

	//Code
	switch(message)
	{

	case WM_CREATE:
					break;

	case WM_ACTIVATE:
					 if(HIWORD(wParam)==0)
						 gbActiveWindow=true;
					 else
						 gbActiveWindow=false;
					break;

	case WM_KEYDOWN:
					switch(wParam)
					{
					case VK_ESCAPE:
									gbEscapeKeyIsPressed=true;
									break;

						case 0x46: //for fullscreen
									if(gbFullscreen==false)
									{
										ToggleFullscreen();
										gbFullscreen=true;
									}
									else
									{
										ToggleFullscreen();
										gbFullscreen=false;
									}
									break;

						  case 0x61://Map the viewport "Left-top-half of window"!	//1
									
									iKeyPressed=1;
									if(gbFullscreen==true)
										resize(xWidth,yHeight);
									else
										resize(WIN_WIDTH,WIN_HEIGHT);									
									break;

						 case 0x62://Map the viewport "Right-top-half of window"!	//2
									iKeyPressed=2;
									if(gbFullscreen==true)
										resize(xWidth,yHeight);
									else
										resize(WIN_WIDTH,WIN_HEIGHT);									
									break;

						 case 0x63://Map the viewport "Left-bottom-half of window"!	//3
									iKeyPressed=3;
									if(gbFullscreen==true)
										resize(xWidth,yHeight);
									else
										resize(WIN_WIDTH,WIN_HEIGHT);									
									break;
						 case 0x64://Map the viewport "Right-bottom-half of window"! //4
									iKeyPressed=4;
									if(gbFullscreen==true)
										resize(xWidth,yHeight);
									else
										resize(WIN_WIDTH,WIN_HEIGHT);									
									break;	

						 case 0x65://Map the viewport "Left-half of window"!	//5
									iKeyPressed=5;
									if(gbFullscreen==true)
										resize(xWidth,yHeight);
									else
										resize(WIN_WIDTH,WIN_HEIGHT);									
									break;

						 case 0x66://Map the viewport "Right-half of window"!	//6
									iKeyPressed=6;
									if(gbFullscreen==true)
										resize(xWidth,yHeight);
									else
										resize(WIN_WIDTH,WIN_HEIGHT);									
									break;
						 case 0x67://Map the viewport "Top-half of window"!	//7
									iKeyPressed=7;
									if(gbFullscreen==true)
										resize(xWidth,yHeight);
									else
										resize(WIN_WIDTH,WIN_HEIGHT);									
									break;

						 case 0x68://Map the viewport "bottom-half of window"!	//8
									iKeyPressed=8;
									if(gbFullscreen==true)
										resize(xWidth,yHeight);
									else
										resize(WIN_WIDTH,WIN_HEIGHT);									
									break;

						 case 0x69://Map the viewport "Center of window"!	//9
									iKeyPressed=9;
									if(gbFullscreen==true)
										resize(xWidth,yHeight);
									else
										resize(WIN_WIDTH,WIN_HEIGHT);									
									break;

						case 0x51: //Q
									uninitialize();
									PostQuitMessage(0);
									break;
					}
					break;

	case WM_SIZE:
					resize(LOWORD(lParam),HIWORD(lParam));
					break;

	case WM_DESTROY:
					PostQuitMessage(0);
					break;

	}
	return(DefWindowProc(hwnd,message,wParam,lParam));

}//End of call back function

/*ToggleFullscreen function
	1. Get the window style using GetWindowLong()
	2. If the "Current window" type contains the "WS_OVERLAPPEDWINDOW" -->Remove it.
	3. Store the current window position
	4. Get the monitor info(Primary display)
	5. Set the window position
*/
void ToggleFullscreen(void)
{
	//variable declaration
	MONITORINFO mi={sizeof(MONITORINFO)};
	bool IsWindowPlacement;
	bool IsMonitorInfo;
	HMONITOR hMonitor;

	//code
	if(gbFullscreen==false) //windowed to full-screen
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE); //Get the window style/entire information of the window
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			//a) Get the window Placement and Monitor information then and then toggle to fullscreen
			IsWindowPlacement=GetWindowPlacement(ghwnd,&wpPrev);

			//b) Now get the monitor first 
			hMonitor=MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY); //the primary monitor in case of extended view

			//c) Get and store the monitor information from the monitor which is been just received
			IsMonitorInfo=GetMonitorInfo(hMonitor,&mi);

			//
			if(IsWindowPlacement && IsMonitorInfo)//both information have been received, now set the window to the top of the screen
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~ WS_OVERLAPPEDWINDOW);

				//Set the window position
				
				SetWindowPos(
							  ghwnd,
							  HWND_TOP,
							  mi.rcMonitor.left,
							  mi.rcMonitor.top,
							  mi.rcMonitor.right-mi.rcMonitor.left, //width
							  mi.rcMonitor.bottom-mi.rcMonitor.top, //height
							  SWP_NOZORDER | SWP_FRAMECHANGED       //ignore any of the Z order and trigger frame change event
							);

			}
			ShowCursor(FALSE);
		}
	}
	else //full-screen to windowed mode
	{
		// Add the removed style to the window style
		// Set the window placement/position to back to the saved position
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd,&wpPrev);

		SetWindowPos(
						ghwnd,
						HWND_TOP,
						0,0,0,0,
						SWP_NOMOVE|
						SWP_NOSIZE|
						SWP_NOOWNERZORDER|
						SWP_NOZORDER|
						SWP_FRAMECHANGED
					);
		ShowCursor(TRUE);
	}
	
}//end of ToggleFullscreen()

//Display function

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//glTranslatef(0.0f,0.0f,-3.0f);
	glBegin(GL_TRIANGLES);
		glColor3f(1.0f,0.0f,0.0f); //Red
		glVertex3f(0.0f,1.0f,0.0f);

		glColor3f(0.0f,1.0f,0.0f); //Green
		glVertex3f(-1.0f,-1.0f,0.0f);

		glColor3f(0.0f,0.0f,1.0f); //Blue
		glVertex3f(1.0f,-1.0f,0.0f);
	glEnd();

	SwapBuffers(ghdc);
}//end of display function

//Uninitialize function
void uninitialize(void)
{
	//if window is in fullscreen mode-->Switch back to windowed mode and then exit
	if(gbFullscreen==true)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);

		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd,&wpPrev);

		SetWindowPos(
					  ghwnd,
					  HWND_TOP,
					  0,0,0,0,
					  SWP_NOMOVE|
					  SWP_NOSIZE|
					  SWP_NOOWNERZORDER|
					  SWP_NOZORDER|
					  SWP_FRAMECHANGED
					);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL,NULL);

	wglDeleteContext(ghrc);
	ghrc=NULL;

	ReleaseDC(ghwnd,ghdc);
	ghdc=NULL;

	if(gpFile)
	{

		fprintf(gpFile,"Log File Closed Successfully!!");
		fclose(gpFile);
		gpFile=NULL;
	}
	DestroyWindow(ghwnd);
}//end of uninitialize()