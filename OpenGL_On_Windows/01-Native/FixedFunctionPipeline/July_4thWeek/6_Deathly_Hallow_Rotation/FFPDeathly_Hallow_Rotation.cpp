/***********************************************************************************

 Assignment: Draw a "Deathly Hallow" a symbol from Harry Potter movie and perform rotations for
			 - Cicle 
			 - Triangle.
 ***********************************************************************************/
#include<windows.h>
#include<stdio.h> //for file i/o
#include<gl/GL.h>
#include<math.h> //for cos() and sin() functions

#define WIN_WIDTH 1024
#define WIN_HEIGHT 1024
#define PI 3.1415926535898

bool gbActiveWindow=false;
bool gbEscapeKeyIsPressed=false;
bool gbFullscreen=false;

float angleTriangle=1.0f;
float angleCircle=360.0f;

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"kernel32.lib")

DWORD dwStyle;
WINDOWPLACEMENT wpPrev= {sizeof(WINDOWPLACEMENT)};

HWND ghwnd;
HDC ghdc;
HGLRC ghrc; 

FILE *gpFile=NULL;

HRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR nCmdLine,int nCmdShow)
{
	//function prototypes
	void display(void);
	void initialize(void);
	void uninitialize(void);
	void update(void);

	//Variables
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	bool bDone=false;
	TCHAR szAppName[]=TEXT("Assignment_Deathly_Hallow-Rotation");

	if(fopen_s(&gpFile,"AssignmentFFP_Deathly_Hallow.txt","w")!=0)
	{
		MessageBox(NULL,TEXT("Log file could not be created..exiting!!"),TEXT("Error"),MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile,"Log File Created Successfully!!\n");
	}

	//code
	wndClass.cbSize=sizeof(WNDCLASSEX);
	wndClass.style=CS_HREDRAW | CS_VREDRAW | CS_OWNDC; //CS_OWNDC-->Non-purgable DC
	wndClass.cbClsExtra=0;
	wndClass.cbWndExtra=0;

	wndClass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hInstance=hInstance;
	wndClass.hIcon=LoadIcon(NULL,IDI_APPLICATION); //load the default icon
	wndClass.hIconSm=LoadIcon(NULL,IDI_APPLICATION); //Icon for "System menu"
	wndClass.hCursor=LoadCursor(NULL,IDC_ARROW);	 //Load default cursor, you can specific the cursor type while creating a game application (Confirm?)

	wndClass.lpfnWndProc=WndProc;
	wndClass.lpszClassName=szAppName;
	wndClass.lpszMenuName=NULL;

	//Register the created class
	RegisterClassEx(&wndClass);

	//create the window
	hwnd=CreateWindowEx(
						 WS_EX_APPWINDOW, //exclusive full-screen window
						 szAppName,
						 TEXT("AssignmentFFP:Deathly Hallow Rotation!!"),
						 WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS|WS_VISIBLE,
						 0,
						 0,
						 WIN_WIDTH,
						 WIN_HEIGHT,
						 NULL,		
						 NULL,
						 hInstance,
						 NULL
						);

	ghwnd=hwnd;	//For further use of the global window handle

	initialize(); //Call to initialize(Do the preparation of the "Picture element" (Pixel)

	ShowWindow(hwnd,nCmdShow);//Show the created window
	SetForegroundWindow(hwnd);//Bring that window in foreground
	SetFocus(hwnd);//Set the focus on the window



	//Game loop
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}//Events further generating the messages
		else
		{
			if(gbActiveWindow==true)
			{
				if(gbEscapeKeyIsPressed==true)
				{
					bDone=true;
				}
				update(); // This function will take care of the logic: first rotate() and then display().
				display();
			}
			
		}//game will run here

	}//end of while
	uninitialize();
	return msg.wParam; 
}//WinMain

/*Initialize:
  1. PIXELFORMATDESCRIPTOR inialize
  2. ChoosePixelFormat
  3. SetPixelFormat
  4. Get the device context from OS
  5. Create OpenGL Context:wglCreateContext()
  6. HandOver control from OS DC to Rendering DC: wglMakeCurrent()
  7. Clear the window with desired color
  8. Give a warmup call to resize()

*/
void initialize(void)
{
	//function prototypes

	void resize(int,int);

	//Variables
	PIXELFORMATDESCRIPTOR pfd;	//a structure which we need to fill will possible set of values from our side and rest can be achieved from OS
	int iPixelFormatIndex;
	//code
	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));//Make the 26-membered structure to 0

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags= PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; //dword flags depicting pixel characteristics
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cGreenBits=8;
	pfd.cBlueBits=8;
	pfd.cAlphaBits=8;

	ghdc=GetDC(ghwnd);//Get the regular device context from OS (this is non-purgeable)

	iPixelFormatIndex=ChoosePixelFormat(ghdc,&pfd);//After getting the DC, "Choose the pixel format" which is the closest match to the values which we have provides

	if(iPixelFormatIndex==0)//if the pixelformatindex is empty due to function could not succeed
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd)==false) //Set the chosen matching closest pixel format
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	ghrc=wglCreateContext(ghdc);//Now handover the device context to OpenGL's "Rendering context" through WGL

	if(ghrc==NULL)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	if(wglMakeCurrent(ghdc,ghrc)==NULL)//Once the handover is successful-->Make "Rendering context" as the current context through WGL
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	glClearColor(0.0f,0.0f,0.0f,0.0f); //Now clear the window with desired color

	//resize(WIN_WIDTH,WIN_HEIGHT); //a warmup call to resize function

}//end of initialize()

//Resize function:

void resize(int width,int height)
{
	if(height==0)
		height=1;

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

}//end of resize

//CALLBACK Function
HRESULT CALLBACK WndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam)
{
	//function declarations
	void resize(int,int);
	void uninitialize(void);
	void ToggleFullscreen(void);
	//Code
	switch(message)
	{

	case WM_CREATE:
					break;

	case WM_ACTIVATE:
					 if(HIWORD(wParam)==0)
						 gbActiveWindow=true;
					 else
						 gbActiveWindow=false;
					break;

	case WM_KEYDOWN:
					switch(wParam)
					{
					case VK_ESCAPE:
									gbEscapeKeyIsPressed=true;
									break;

						case 0x46: //for fullscreen
									if(gbFullscreen==false)
									{
										ToggleFullscreen();
										gbFullscreen=true;
									}
									else
									{
										ToggleFullscreen();
										gbFullscreen=false;
									}
									break;
						case 0x51: //Q
									uninitialize();
									PostQuitMessage(0);
									break;
					}
					break;

	case WM_SIZE:
					resize(LOWORD(lParam),HIWORD(lParam));
					break;

	case WM_DESTROY:
					PostQuitMessage(0);
					break;

	}
	return(DefWindowProc(hwnd,message,wParam,lParam));

}//End of call back function

/*ToggleFullscreen function
	1. Get the window style using GetWindowLong()
	2. If the "Current window" type contains the "WS_OVERLAPPEDWINDOW" -->Remove it.
	3. Store the current window position
	4. Get the monitor info(Primary display)
	5. Set the window position
*/
void ToggleFullscreen(void)
{
	//variable declaration
	MONITORINFO mi={sizeof(MONITORINFO)};
	bool IsWindowPlacement;
	bool IsMonitorInfo;
	HMONITOR hMonitor;

	//code
	if(gbFullscreen==false) //windowed to full-screen
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE); //Get the window style/entire information of the window
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			//a) Get the window Placement and Monitor information then and then toggle to fullscreen
			IsWindowPlacement=GetWindowPlacement(ghwnd,&wpPrev);

			//b) Now get the monitor first 
			hMonitor=MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY); //the primary monitor in case of extended view

			//c) Get and store the monitor information from the monitor which is been just received
			IsMonitorInfo=GetMonitorInfo(hMonitor,&mi);

			//
			if(IsWindowPlacement && IsMonitorInfo)//both information have been received, now set the window to the top of the screen
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~ WS_OVERLAPPEDWINDOW);

				//Set the window position
				
				SetWindowPos(
							  ghwnd,
							  HWND_TOP,
							  mi.rcMonitor.left,
							  mi.rcMonitor.top,
							  mi.rcMonitor.right-mi.rcMonitor.left, //width
							  mi.rcMonitor.bottom-mi.rcMonitor.top, //height
							  SWP_NOZORDER | SWP_FRAMECHANGED       //ignore any of the Z order and trigger frame change event
							);

			}
			ShowCursor(FALSE);
		}
	}
	else //full-screen to windowed mode
	{
		// Add the removed style to the window style
		// Set the window placement/position to back to the saved position
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd,&wpPrev);

		SetWindowPos(
						ghwnd,
						HWND_TOP,
						0,0,0,0,
						SWP_NOMOVE|
						SWP_NOSIZE|
						SWP_NOOWNERZORDER|
						SWP_NOZORDER|
						SWP_FRAMECHANGED
					);
		ShowCursor(TRUE);
	}
	
}//end of ToggleFullscreen()

//Display function

void display(void)
{
	//Function Prototypes
	
	//variables
	GLfloat Radius=0.0f,angle=0.0f, AreaOfTriangle=0.0f,SemiPerimeterOfTriangle,PerimeterOfTriangle;
	float x1=0.0f,y1=0.5f,x2=-0.5f,y2=-0.5f,x3=0.5f,y3=-0.5f,a,b,c,Ox,Oy;
	//code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glRotatef(angleTriangle,0.0f,1.0f,0.0f);    //Rotate the triangle along Y-axis
	//Draw a triangle
	glBegin(GL_LINE_LOOP);
		glVertex3f(x1,y1,0.0f);
		glVertex3f(x2,y2,0.0f);
		glVertex3f(x3,y3,0.0f);
	glEnd();

	//Calculate the lengths of the sides of the triangle
	float lAB=sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
	fprintf(gpFile,"lAB=c=%f\n",lAB);

	float lBC=sqrt((x2-x3)*(x2-x3)+(y2-y3)*(y2-y3));
	fprintf(gpFile,"lBC=a=%f\n",lBC);

	float lAC=sqrt((x3-x1)*(x3-x1)+(y3-y1)*(y3-y1));
	fprintf(gpFile,"lAC=b=%f\n",lAC);

	c=lAB;
	a=lBC;
	b=lAC;

	//Calculate perimeter of the triangle
	PerimeterOfTriangle=lAB + lBC +lAC;
	fprintf(gpFile,"Perimeter=%f\n",PerimeterOfTriangle);

	//Calculate SemiPerimeter
	SemiPerimeterOfTriangle=PerimeterOfTriangle/2; //SemiPerimeter=Perimeter received from function/2
	fprintf(gpFile,"SemiPerimeter=%f\n",SemiPerimeterOfTriangle);

	//Calculate Area Of Triangle:=sqrt(s*(s-a)*(s-b)*(s-c)))
	AreaOfTriangle=sqrt(SemiPerimeterOfTriangle*(SemiPerimeterOfTriangle-a)*(SemiPerimeterOfTriangle-b)*(SemiPerimeterOfTriangle-c));
	fprintf(gpFile,"Area of Triangle=%f\n",AreaOfTriangle);

	
	//Radius=(2*AreaOfTriangle)/PerimeterOfTriangle;
	Radius=(2*AreaOfTriangle)/PerimeterOfTriangle;
	fprintf(gpFile,"Radius calculated=%f\n",Radius);

	/*Calculate the "Incenter of the Triangle"-->Which should be the center for the circle/in-circle

	  Formula: Given the co-ordinates of the vertices of triangle ABC, the co-ordinates for "incenter" O are:

			  Ox=(a*Ax + b*Bx + c*Cx)/P and Oy=(a*Ay + b*By + c*Cy)/P

			  Where, 
			       Ax,Ay: X and Y Co-ordinates of A.
				   a,b,c: are the side lengths opposite to vertex A,B,C
				   P: Perimeter of the triangle
	*/
		
	//Calculate the incenter of the triangle
	Ox=(a*x1 + b*x2 + c*x3)/PerimeterOfTriangle;

	Oy=(a*y1 + b*y2 + c*y3)/PerimeterOfTriangle;

	fprintf(gpFile,"Co-Ordinates of In-Center:(Ox,Ox)=(%f,%f)\n",Ox,Oy);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glRotatef(angleCircle,0.0f,1.0f,0.0f); //Rotate the Circle around Y-axis in reverse direction
	//Draw the inscrined circle
	glBegin(GL_POINTS);	
		for(angle=0.0f; angle<2.0f*PI; angle=angle+0.001f)
		{
			glVertex3f(Radius*cos(angle)+Ox,Radius*sin(angle)+Oy,0.0f);	

		}//For loop for drawing a circle			
	glEnd();	

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//Draw "Perpendicular  from Vertex A to opposite side of A"	
	glBegin(GL_LINES);
		glVertex3f(x1,y1,0.0f);
		glVertex3f((x2+x3)/2,(y2+y3)/2,0.0f);
	glEnd();

	
	SwapBuffers(ghdc);
}//end of display function

void update(void)
{

	angleTriangle=angleTriangle+1.0f;

	if(angleTriangle>=360.0f)
		angleTriangle=0.0f;

	angleCircle=angleCircle-1.0f;

	if(angleCircle<=0.0f)
		angleCircle=360.0f;

}//end of update()

//Uninitialize function
void uninitialize(void)
{
	//if window is in fullscreen mode-->Switch back to windowed mode and then exit
	if(gbFullscreen==true)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);

		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd,&wpPrev);

		SetWindowPos(
					  ghwnd,
					  HWND_TOP,
					  0,0,0,0,
					  SWP_NOMOVE|
					  SWP_NOSIZE|
					  SWP_NOOWNERZORDER|
					  SWP_NOZORDER|
					  SWP_FRAMECHANGED
					);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL,NULL);

	wglDeleteContext(ghrc);
	ghrc=NULL;

	ReleaseDC(ghwnd,ghdc);
	ghdc=NULL;

	if(gpFile)
	{

		fprintf(gpFile,"Log File Closed Successfully!!");
		fclose(gpFile);
		gpFile=NULL;
	}
	DestroyWindow(ghwnd);
}//end of uninitialize()