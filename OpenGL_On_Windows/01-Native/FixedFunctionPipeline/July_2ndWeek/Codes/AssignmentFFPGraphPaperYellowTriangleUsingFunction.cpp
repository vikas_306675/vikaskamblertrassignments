/************************
Triangle using function!
*************************/

#include<windows.h>
#include<gl/GL.h>

#define WIN_WIDTH 1024
#define WIN_HEIGHT 768

#pragma comment(lib,"opengl32.lib")

//Global variable declaration
DWORD dwStyle;
WINDOWPLACEMENT wpPrev={ sizeof(WINDOWPLACEMENT) }; //previous window placement

bool gbFullscreen=false;
bool gbActivateWindow=false;
bool gbEscapeKeyIsPressed=false;

HWND ghwnd=NULL;
HDC ghdc=NULL;   //handle to device context
HGLRC ghrc=NULL; //handle to OpenGL Rendering context 

//Prototype of CALLBACK Function
HRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,LPSTR lpszCmdLine,int nCmdShow)
{
	//function prototype
	void initialize(void);
	void display(void);
	void uninitialize(void);

	//variable declaration
	WNDCLASSEX wndClass;
	MSG msg;
	HWND hwnd;
	TCHAR szClassName[]=TEXT("OpenGL_FFP_Graph_Paper_Yellow_Triangle");
	bool bDone=false;

	//code
	wndClass.cbSize=sizeof(WNDCLASSEX); //size of the custom window class
	wndClass.style=CS_HREDRAW | CS_VREDRAW | CS_OWNDC; //style of the window drawing
	wndClass.cbClsExtra=0;
	wndClass.cbWndExtra=0;

	wndClass.hInstance=hInstance;								//Initializing class's member with the window
	wndClass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH); //background window color
	wndClass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndClass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndClass.hCursor=LoadCursor(NULL,IDC_ARROW);

	wndClass.lpfnWndProc=WndProc;
	wndClass.lpszClassName=szClassName;
	wndClass.lpszMenuName=NULL;

	//Register the created window class
	RegisterClassEx(&wndClass);

	//create the window in memory
	hwnd=CreateWindowEx(
					  WS_EX_APPWINDOW,				//Exclusive window which will also hide out the task bar(above all)
					  szClassName,					//class name
					  TEXT("OpenGL FFP Assignment:Yellow Triangle Graph Paper Using Functions!"),	//Caption bar title
					  WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN |
					  WS_CLIPSIBLINGS | WS_VISIBLE,			//basic draw style
					  0,				//x
					  0,				//y
					  WIN_WIDTH,				//width
					  WIN_HEIGHT,				//height
					  NULL,					//parent window handle in this case 
					  NULL,
					  hInstance,
					  NULL
						);

	//copy hwnd to global handle to use it across different functions
	ghwnd=hwnd;

	//call to initialize()
	initialize();

	//Show-window
	ShowWindow(hwnd,nCmdShow); 
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	//UpdateWindow(hwnd);    //this call has been replaced with above two function calls.


	//Game Loop: Where the game-logic will keep on runnning
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE)) //PM_REMOVE : Depicts, after processing the message->Remove it from message queue->dont send it back to OS.
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true; //application is going to get close
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if(gbActivateWindow==true)
			{
				if(gbEscapeKeyIsPressed==true)
				{
					bDone=true;
				}
				display();
			}
		}

	}//while

	uninitialize(); //At this line, application is going to get close, hence uninitialize all the contexts etc.
	return((int)msg.wParam);

}//WinMain


void uninitialize(void)
{
	//code
	//Before un-initializing the variables or structures used in the program, if window is in Fullscreen mode, lets get it back to "windowed" mode.
	if(gbFullscreen==true)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);

		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd,&wpPrev);

		SetWindowPos(
					 ghwnd,
					 HWND_TOP,
					 0,0,0,0,
					 SWP_NOMOVE |
					 SWP_NOSIZE |
					 SWP_NOOWNERZORDER | 
					 SWP_NOZORDER | 
					 SWP_FRAMECHANGED	
					);
		ShowCursor(TRUE);
	}

	//Release OpenGL rendering context and an association with "window's device context"
	wglMakeCurrent(NULL,NULL);

	wglDeleteContext(ghrc); //Delet OpenGL rendering context
	ghrc=NULL;

	ReleaseDC(ghwnd,ghdc); //release device context for this window
	ghdc=NULL;

	DestroyWindow(ghwnd);

}//uninitialize


void initialize(void)
{
	//function prototype
	void resize(int,int);

	//variable declaration

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));

	//Initialize the members of PIXELFORMATDESCRIPTOR's object
	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags=PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER; //Without specifying PFD_DOUBLE_BUFFERED --> This application will be single buffered
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cBlueBits=8;
	pfd.cGreenBits=8;
	pfd.cAlphaBits=8;

	//Get regular device context from windows
	ghdc=GetDC(ghwnd);

	//choose the pixel format
	iPixelFormatIndex=ChoosePixelFormat(ghdc,&pfd);

	if(iPixelFormatIndex==0)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	//set the chosen pixel format
	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd)==false)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	//now handover the drawing/coloring context to "OpenGL rendering context"
	ghrc=wglCreateContext(ghdc);

	if(ghrc==NULL)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	//now make OpenGL rendering context as the current context
	if(wglMakeCurrent(ghdc,ghrc)==NULL)
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	//you reach this line means you are able to make OpenGL context as the current context

	glClearColor(0.0f,0.0f,0.0f,0.0f); //This function we have already used in FFP, and, it is also applicable in Programmable pipeline

	//resize(WIN_WIDTH,WIN_HEIGHT); //warm up call: Before actual rendering starts

}//initialize

void resize(int width, int height)
{
	//code
	if(height==0)
		height=1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

}//resize

HRESULT CALLBACK WndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam)
{
	//code
	void display(void);
	void ToggleFullscreen(void);

	switch(message)
	{
	
	case WM_CREATE: //Fired first of all and only once
					break;

	case WM_ACTIVATE:
					//Window activation in case of "extended mode": in the sense when you have dual view set between 2 displays
					if(HIWORD(wParam)==0)
						gbActivateWindow=true;
					else
						gbActivateWindow=false;
					break;
	
	case WM_PAINT: //if "display()" function has been invoked in WM_PAINT context, then this will be as good as keeping compatibility with FFP?? (HOW?)
				 display(); 
				 break;

	case WM_ERASEBKGND: //Keep brush and background : Why do we keep brush?-> To use it later on, after returning from OpenGL to immediate mode Ex. OpenGL ne return keleli bitmap
				return(0);

	case WM_SIZE:
				resize(LOWORD(lParam),HIWORD(lParam)); //background should not be black while we resize the window
				break;
	
	case WM_KEYDOWN:
					switch(wParam)
					{
					case VK_ESCAPE: //Escape key press event
									if(gbEscapeKeyIsPressed==false)
										gbEscapeKeyIsPressed=true;
									break;
					case 0x46://for 'f' or 'F' for fullscreen toggle
									if(gbFullscreen==false) //Window mode-->Fullscreen mode
									{
										ToggleFullscreen();
										gbFullscreen=true;
									}
									else //fullscreen-->Windowed Mode
									{
										ToggleFullscreen();
										gbFullscreen=false;
									}
									break;

					}//switch
					break;

	case WM_DESTROY:
					PostQuitMessage(0);
					break;
	}

	return(DefWindowProc(hwnd,message,wParam,lParam));

}//callback

void ToggleFullscreen(void)
{
	//variable declaration
	MONITORINFO mi={sizeof(MONITORINFO)};
	//code

	if(gbFullscreen==false) //window is already in "Windowed mode"
	{
		//get the window long draw style
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
		if(dwStyle & WS_OVERLAPPEDWINDOW)//why do we have to check for "WS_OVERLAPPEDWINDOW" in the dwStyle?? (Question needs to be asked/digged deep)
		{
			 //mi={sizeof(MONITORINFO)};
			//Save the "current window placement" and "get the information of primary monitor" (always)
			if(GetWindowPlacement(ghwnd,&wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghwnd, 
							 HWND_TOP,//HWND_TOP: Top of all the windows
							 mi.rcMonitor.left,//x
							 mi.rcMonitor.top, //y
							 mi.rcMonitor.right-mi.rcMonitor.left, //width
							 mi.rcMonitor.bottom-mi.rcMonitor.top, //height
							 SWP_NOZORDER | SWP_FRAMECHANGED
							 ); 
			}
		}
		ShowCursor(FALSE); //Do not show the cursor in Full screen mode
	}
	else //window is already in "Fullscreen mode"
	{
		//code
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW); //add "WS_OVERLAPPEDWINDOW" to convert back to Windowed mode.

		SetWindowPlacement(ghwnd,&wpPrev);//set the window placement to the saved one

		SetWindowPos(ghwnd,
					 HWND_TOP,
					 0,0,0,0,
					 SWP_NOMOVE|
					 SWP_NOSIZE |
					 SWP_NOOWNERZORDER|
					 SWP_NOZORDER|
					 SWP_FRAMECHANGED
					);

	   ShowCursor(TRUE); //show the cursor in windowed mode
	}
}//ToggleFullscreen

void display(void)
{
	//Function Prototypes
	
	void DrawHorizontalLines(void);
	void DrawVerticalLines(void);
	void DrawTriangle(void);

	//Variables
	GLfloat fXCoOrdinate,fYCoOrdinate;

	//code
	glClear(GL_COLOR_BUFFER_BIT); //this function is applicable in both fixed function and programmable pipeline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//*********** Horizontal Lines Code ****************
	DrawHorizontalLines();
	//*********** Vertical Lines Code ****************
	DrawVerticalLines();
	//********** Yellow Bordered Triangle Code *****************/
	DrawTriangle(); 

	SwapBuffers(ghdc);
	//glFlush();

}//display

//Function To Draw Horizontal Lines:

void DrawHorizontalLines(void)
{
	GLfloat fXCoOrdinate,fYCoOrdinate;

	glColor3f(0.0f,0.0f,1.0f); //Blue color
	glLineWidth(1.0f);
	for(fYCoOrdinate=1.0f;fYCoOrdinate>0.0f;fYCoOrdinate=fYCoOrdinate-0.05f)
	{
			glBegin(GL_LINES);
				glVertex3f(-1.0f,fYCoOrdinate,0.0f);
				glVertex3f(1.0f,fYCoOrdinate,0.0f);
			glEnd();
	}
	//Horizontal Red Line
	glColor3f(1.0f,0.0f,0.0f); //Red color
	glLineWidth(3.0f);		   //Line width=3
	glBegin(GL_LINES);
		glVertex3f(-1.0f,0.0f,0.0f);
		glVertex3f(1.0f,0.0f,0.0f);
	glEnd();

	//Horizontal 20 lines below Centered RED Line
	glColor3f(0.0f,0.0f,1.0f); //Blue
	glLineWidth(1.0f);
	for(fYCoOrdinate=0.05f;fYCoOrdinate<=1.0f;fYCoOrdinate=fYCoOrdinate+0.05f)
	{
		glBegin(GL_LINES);
			glVertex3f(-1.0f,-fYCoOrdinate,0.0f);
			glVertex3f(1.0f,-fYCoOrdinate,0.0f);
		glEnd();
	}
}//end of DrawHorizontalLines()

//Draw Vertical Lines

void DrawVerticalLines(void)
{
	GLfloat fXCoOrdinate, fYCoOrdinate;

	//Vertical 20 lines to the left of centered VERTICAL GREEN LINE
	glColor3f(0.0f,0.0f,1.0f); //Blue color
	glLineWidth(1.0f);
	for(fXCoOrdinate=1.0f;fXCoOrdinate>0.0f;fXCoOrdinate=fXCoOrdinate-0.05f)
	{
			glBegin(GL_LINES);
				glVertex3f(-fXCoOrdinate,1.0f,0.0f);
				glVertex3f(-fXCoOrdinate,-1.0f,0.0f);
			glEnd();
	}
	//Vertical Green Line
	glColor3f(0.0f,1.0f,0.0f); //Green color
	glLineWidth(3.0f);		   //Line width=3
	glBegin(GL_LINES);
		glVertex3f(0.0f,1.0f,0.0f);
		glVertex3f(0.0f,-1.0f,0.0f);
	glEnd();

	//Vertical 20 lines to the right side of Centered VERTICAL GREEN Line
	glColor3f(0.0f,0.0f,1.0f); //Blue
	glLineWidth(1.0f);
	for(fXCoOrdinate=0.05f; fXCoOrdinate<=1.0f; fXCoOrdinate=fXCoOrdinate+0.05f)
	{
		glBegin(GL_LINES);
			glVertex3f(fXCoOrdinate,1.0f,0.0f);
			glVertex3f(fXCoOrdinate,-1.0f,0.0f);
		glEnd();
	}

}//end of DrawVerticalLines()

//Draw Yellow Bordered Triangle

void DrawTriangle(void)
{
	glColor3f(1.0f,1.0f,0.0f);
	glBegin(GL_LINE_LOOP);
		glVertex3f(0.0f,0.5f,0.0f);
		glVertex3f(-0.5f,-0.5f,0.0f);
		glVertex3f(0.5f,-0.5f,0.0f);
	glEnd();

}//end of DrawTriangle()