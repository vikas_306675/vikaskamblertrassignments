/**************************************************************
FreeGLUT: Program: Deathly Hallows Symbol!!
***************************************************************/

#include<GL/freeglut.h>
#include<GL/gl.h>
#include<math.h>

#define PI 3.1415926535898

//global variable declaration
bool bgFullscreen=false; //Variable for fullscreen toggle

float R,G,B;

int main(int argc,char**argv)
{
	//function prototypes
	void display(void);
	void resize(int,int);
	void initialize(void);
	void uninitialize(void);
	void mouse(int,int,int,int);
	void keyboard(unsigned char,int,int);

	//code
	glutInit(&argc,argv); //Initialize the GLUT and and windowing 

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

	glutInitWindowSize(1024,1024);	//initial window size
	glutInitWindowPosition(100,100);	//initial window position
	glutCreateWindow("FreeGLUT:Deathly Hallows!!");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	//return(0);

}//end of main()

//Display

void display(void)
{
	//Function Prototypes
	
	//variables
	GLfloat Radius=0.0f,angle=0.0f, AreaOfTriangle=0.0f,SemiPerimeterOfTriangle,PerimeterOfTriangle;
	float x1=0.0f,y1=0.5f,x2=-0.5f,y2=-0.5f,x3=0.5f,y3=-0.5f,a,b,c,Ox,Oy;
	//code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//Draw a triangle
	glBegin(GL_LINE_LOOP);
		glVertex3f(x1,y1,0.0f);
		glVertex3f(x2,y2,0.0f);
		glVertex3f(x3,y3,0.0f);
	glEnd();

	//Calculate the lengths of the sides of the triangle
	float lAB=sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));

	float lBC=sqrt((x2-x3)*(x2-x3)+(y2-y3)*(y2-y3));

	float lAC=sqrt((x3-x1)*(x3-x1)+(y3-y1)*(y3-y1));

	c=lAB;
	a=lBC;
	b=lAC;

	//Calculate perimeter of the triangle
	PerimeterOfTriangle=lAB + lBC +lAC;

	//Calculate SemiPerimeter
	SemiPerimeterOfTriangle=PerimeterOfTriangle/2; //SemiPerimeter=Perimeter received from function/2

	//Calculate Area Of Triangle:=sqrt(s*(s-a)*(s-b)*(s-c)))
	AreaOfTriangle=sqrt(SemiPerimeterOfTriangle*(SemiPerimeterOfTriangle-a)*(SemiPerimeterOfTriangle-b)*(SemiPerimeterOfTriangle-c));

	//Radius=(2*AreaOfTriangle)/PerimeterOfTriangle;
	Radius=(2*AreaOfTriangle)/PerimeterOfTriangle;

	/*Calculate the "Incenter of the Triangle"-->Which should be the center for the circle/in-circle

	  Formula: Given the co-ordinates of the vertices of triangle ABC, the co-ordinates for "incenter" O are:

			  Ox=(a*Ax + b*Bx + c*Cx)/P and Oy=(a*Ay + b*By + c*Cy)/P

			  Where, 
			       Ax,Ay: X and Y Co-ordinates of A.
				   a,b,c: are the side lengths opposite to vertex A,B,C
				   P: Perimeter of the triangle
	*/
		
	//Calculate the incenter of the triangle
	Ox=(a*x1 + b*x2 + c*x3)/PerimeterOfTriangle;

	Oy=(a*y1 + b*y2 + c*y3)/PerimeterOfTriangle;

	//Draw the inscrined circle
	glBegin(GL_POINTS);	
		for(angle=0.0f; angle<2.0f*PI; angle=angle+0.001f)
		{
			glVertex3f(Radius*cos(angle)+Ox,Radius*sin(angle)+Oy,0.0f);	

		}//For loop for drawing a circle
			
	glEnd();	

	//Draw "Perpendicular  from Vertex A to opposite side of A"	
	glBegin(GL_LINES);
		glVertex3f(x1,y1,0.0f);
		glVertex3f((x2+x3)/2,(y2+y3)/2,0.0f);
	glEnd();

	glutSwapBuffers();
}

//Initialize
void initialize(void)
{
	//code
	glClearColor(0.0f,0.0f,0.0f,0.0f);

}//end of initialize()


//keyboard function
void keyboard(unsigned char key,int x,int y)
{
	switch(key)
	{
	case 27://escape
			glutLeaveMainLoop();
			break;
	case 'F':
	case 'f':
			 if(bgFullscreen==false)
			 {
				 glutFullScreen();
				 bgFullscreen=true;
			 }
			 else
			 {
				 glutLeaveFullScreen();
				 bgFullscreen=false;
			 }
			 break;
	default:
			 break;
	}
}

//mouse function
void mouse(int button,int state,int x,int y)
{
	//code

}//end of mouse

//resize function
void resize(int width,int height)
{
	if(height==0)
		height=1;

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

}

//uninitialize
void uninitialize(void)
{
	//code
}



