#include <windows.h>
#include <windowsx.h>
#include <stdio.h>


//Global variable declarations
FILE *gpFile=NULL;
int giPaintFlag=-1;

//Callback function declared
LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdLine, int nCmdShow)
{
	//Variable declaration
	 WNDCLASSEX wndClass;
	 TCHAR szAppName[]=TEXT("Keydown_Window_Color_Change");
	 HWND hwnd;
	 MSG msg;

	//code
	 if(fopen_s(&gpFile,"Log_Assignment_Keydown_Color_Change.txt","w")!=0)
	 {
		 MessageBox(NULL,TEXT("Log File cannot be created..\nExiting.."),TEXT("Error"),MB_OK);
		 exit(0);
	 }
	 else
	 {
		 fprintf(gpFile,"Log file created successfully and Opened!\n");
	 }
	wndClass.cbSize=sizeof(WNDCLASSEX);			//sizeof the custom class
	wndClass.style=CS_HREDRAW | CS_VREDRAW;		//class style
	
	wndClass.cbClsExtra=0;						
	wndClass.cbWndExtra=0;

	wndClass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndClass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndClass.hCursor=LoadCursor(NULL,IDC_ARROW);
	wndClass.hInstance=hInstance;
	

	wndClass.lpfnWndProc=WndProc;
	wndClass.lpszClassName=szAppName;
	wndClass.lpszMenuName=NULL;

	//Register the custom created class
	RegisterClassEx(&wndClass);

	//Create the window in memory
	hwnd=CreateWindow(
						szAppName, //Name of the custom class
						TEXT("Assignment Keydown Window Color Change"), //caption bar title of the window
						WS_OVERLAPPEDWINDOW,			 //Window style
						CW_USEDEFAULT,					 //x
						CW_USEDEFAULT,					 //y
						CW_USEDEFAULT,					 //windth
						CW_USEDEFAULT,					 //height
						NULL,							 //handle to the parent window
						NULL,							 
						hInstance,						 //
						NULL
					 );

	//Show the window
	ShowWindow(hwnd,nCmdShow);
	UpdateWindow(hwnd);

	//Message loop

	while(GetMessage(&msg,NULL,0,0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;

}//WinMain

/*
WM_LBUTTONDOWN:
				wParam: this will help understand which virtual key has been pressed. (MK_CONTROL, MK_LBUTTON, MK_MBUTTON, MK_RBUTTON etc.)
				lParam: this will help get more information about this message
				        a. Low order word specifies the "X-Co-ordinate" 
						b. High Order word specifies the "Y-Co-Ordinate"
*/

LRESULT CALLBACK WndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam)
{
	
	HDC hdc; //handle to the device context
	PAINTSTRUCT ps;	
	RECT rc;
	HBRUSH hBrush;
	

	switch(message)
	{
	case WM_CREATE:	
					break;

	case WM_LBUTTONDOWN:
					
					break;

	case WM_PAINT:	/*
					1. Get the handle to device context
					2. Get the client region in RECT variable.
					3. Based upon the keypress:
						1. Create respective brush of respective color.
						2. Fill the client rectangle with the "colored brush" created in above step
					Color Values:
					------------
					0,0,0 Black
					255,255,255 white
					255,0,0 Red
					0,255,0 Green
					0,0,255 Blue
					0,255,255 Cyan
					255,0,255 Magenta
					255,255,0 Yellow

					BOOL WINAPI GetClientRect(		
												_IN_ HWND hwnd,
												_Out_ LPRECT lpRect
											 );
					COLORREF RGB(
									BYTE byRed,
									BYTE byGreen,
									BYTE byBlue
								);
					HBRUSH CreateSolidBrush(_IN_ COLORREF crColor); //created solid brush of the color value given

					int FillRect(
								_IN_ HDC hdc,
								_IN_ const RECT *lprc,
								_IN_ HBRUSH hbr
								);

					*/
					hdc=BeginPaint(hwnd,&ps);
					GetClientRect(hwnd,&rc);

					if(giPaintFlag==1) //black
					{
						hBrush=CreateSolidBrush(RGB(0,0,0));
						FillRect(hdc,&rc,hBrush);
						giPaintFlag=-1;
					}
					else if(giPaintFlag==2) //White
					{
						hBrush=CreateSolidBrush(RGB(255,255,255));
						FillRect(hdc,&rc,hBrush);
						giPaintFlag=-1;
					}
					else if(giPaintFlag==3) //Red
					{
						hBrush=CreateSolidBrush(RGB(255,0,0));
						FillRect(hdc,&rc,hBrush);
						giPaintFlag=-1;
					}
					else if(giPaintFlag==4) //Green
					{
						hBrush=CreateSolidBrush(RGB(0,255,0));
						FillRect(hdc,&rc,hBrush);
						giPaintFlag=-1;
					}
					else if(giPaintFlag==5) //Blue
					{
						hBrush=CreateSolidBrush(RGB(0,0,255));
						FillRect(hdc,&rc,hBrush);
						giPaintFlag=-1;
					}
					else if(giPaintFlag==6) //Cyan
					{
						hBrush=CreateSolidBrush(RGB(0,255,255));
						FillRect(hdc,&rc,hBrush);
						giPaintFlag=-1;
					}
					else if(giPaintFlag==7) //Magenta
					{
						hBrush=CreateSolidBrush(RGB(255,0,255));
						FillRect(hdc,&rc,hBrush);
						giPaintFlag=-1;
					}
					else if(giPaintFlag==8) //Yellow
					{
						hBrush=CreateSolidBrush(RGB(255,255,0));
						FillRect(hdc,&rc,hBrush);
						giPaintFlag=-1;
					}
					DeleteObject(hBrush);
					EndPaint(hwnd,&ps);
					break;

	case WM_KEYDOWN://Based upon the key pressed: change the color for the window.*/
					/*
					WM_KEYDOWN: wParam: Will help get the answer to: "Which key (virtual keycode) has been pressed?"
								lParam: Will help provide an extra information: repeat count, scan code, extended key flag, context code, previous key state flag, transistion state flag etc.
				
					BOOL InvalidateRect(
										_IN_ HWND hwnd,
										_IN_ const RECT *lpRect,
										_IN_ BOOL bErase
										);

						--> This function invalidates rectangular region of the display and generate a WM_PAINT message.
						--> hwnd :Handle to the window whose update region has changed.
								  if the value specified is NULL-->This funcion will redraw all the windows.
						-->lpRect: a pointer to RECT structure which contains the co-ordinates of the rectangle to be added to the update region
						--> bErase: Specifies whether the background needs to be erased when "update region" is processed.
						            True: value will help erase the background
									False: Value will keep the background as it is (i.e unchanged)
						--> CALL TO THIS FUNCTION WILL TRIGGER WM_PAINT MESSAGE AND HENCE THE WM_PAINT BLOCK WILL BE EXECUTED.
					*/
					fprintf(gpFile,"Entered WM_KEYDOWN!!\n");	
					
					switch(wParam) //specifying wParam is quivalent to LOWORD(wParam)--> Which gives you the virtual key code value for the key press
					{
						
						case 0x41: //A-Black
									giPaintFlag=1;			
									InvalidateRect(hwnd,NULL,TRUE);
									break;

						case 0x57: //W-White
									giPaintFlag=2;
									InvalidateRect(hwnd,NULL,TRUE);
									break;

						case 0x52:	//R-Red
									giPaintFlag=3;
									InvalidateRect(hwnd,NULL,TRUE);
									break;

						case 0x47:	//G-Green
									giPaintFlag=4;		
									InvalidateRect(hwnd,NULL,TRUE);
									break;

					    case 0x42: //B-Blue
									giPaintFlag=5;
									InvalidateRect(hwnd,NULL,TRUE);
									break;

					    case 0x43://C-Cyan (0,255,255)
									giPaintFlag=6;
									InvalidateRect(hwnd,NULL,TRUE);
									break;
			
						case 0x4D: //M-Magenta (255,0,255)
									giPaintFlag=7;
									InvalidateRect(hwnd,NULL,TRUE);
									break;
						
						case 0x59: // Y-Yellow (255,255,0)
									giPaintFlag=8;
									InvalidateRect(hwnd,NULL,TRUE);
									break;
											
						case 0x51:	//Q: to quit the application
									MessageBox(NULL,TEXT("WM_KEYDOWN: Q has been pressed, Quitting the application..!!"),TEXT("Keydown Color Change"),MB_OK);		
									PostQuitMessage(0);
									break;

						case VK_ESCAPE: //Escape: Quit the application
									MessageBox(NULL,TEXT("WM_KEYDOWN: Escape Key has been Pressed!!..Quitting the application!!"),TEXT("Keydown Color Change"),MB_OK);
									PostQuitMessage(0);
									break;

						default:	MessageBox(NULL,TEXT("Different key has been pressed,[Press:A/W/R/G/B/C/M/Y] to display color!!"),TEXT("Keydown Color Change"),MB_OK);
							
					}//internal switch
					fprintf(gpFile,"Exiting WM_KEYDOWN!\n");
					break;

	case WM_DESTROY:
					fprintf(gpFile,"Entered WM_DESTROY!!\n");
					fprintf(gpFile,"Log file closed successfully!!\n");
					fclose(gpFile);
					gpFile=NULL;
					PostQuitMessage(0);
					break;

	}
	return(DefWindowProc(hwnd,message,wParam,lParam));
}//WndProc







