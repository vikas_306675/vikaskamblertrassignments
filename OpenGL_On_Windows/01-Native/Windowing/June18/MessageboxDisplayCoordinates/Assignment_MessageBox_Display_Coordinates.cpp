#include <windows.h>
#include <windowsx.h>
#include <stdio.h>

//Global variable declarations
FILE *gpFile=NULL;

//Callback function declared
LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdLine, int nCmdShow)
{
	//Variable declaration
	 WNDCLASSEX wndClass;
	 TCHAR szAppName[]=TEXT("MessageBox Program");
	 HWND hwnd;
	 MSG msg;

	//code
	 if(fopen_s(&gpFile,"Log_Assignment_MessageBox.txt","w")!=0)
	 {
		 MessageBox(NULL,TEXT("Log File cannot be created..\nExiting.."),TEXT("Error"),MB_OK);
		 exit(0);
	 }
	 else
	 {
		 fprintf(gpFile,"Log file created successfully and Opened!\n");
	 }
	wndClass.cbSize=sizeof(WNDCLASSEX);			//sizeof the custom class
	wndClass.style=CS_HREDRAW | CS_VREDRAW;		//class style
	
	wndClass.cbClsExtra=0;						
	wndClass.cbWndExtra=0;

	wndClass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndClass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndClass.hCursor=LoadCursor(NULL,IDC_ARROW);
	wndClass.hInstance=hInstance;
	

	wndClass.lpfnWndProc=WndProc;
	wndClass.lpszClassName=szAppName;
	wndClass.lpszMenuName=NULL;

	//Register the custom created class
	RegisterClassEx(&wndClass);

	//Create the window in memory
	hwnd=CreateWindow(
						szAppName, //Name of the custom class
						TEXT("Assignment Messagebox"), //caption bar title of the window
						WS_OVERLAPPEDWINDOW,			 //Window style
						CW_USEDEFAULT,					 //x
						CW_USEDEFAULT,					 //y
						CW_USEDEFAULT,					 //windth
						CW_USEDEFAULT,					 //height
						NULL,							 //handle to the parent window
						NULL,							 
						hInstance,						 //
						NULL
					 );

	//Show the window
	ShowWindow(hwnd,nCmdShow);
	UpdateWindow(hwnd);

	//Message loop

	while(GetMessage(&msg,NULL,0,0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;

}//WinMain

/*
WM_LBUTTONDOWN:
				wParam: this will help understand which virtual key has been pressed. (MK_CONTROL, MK_LBUTTON, MK_MBUTTON, MK_RBUTTON etc.)
				lParam: this will help get more information about this message
				        a. Low order word specifies the "X-Co-ordinate" 
						b. High Order word specifies the "Y-Co-Ordinate"
*/

LRESULT CALLBACK WndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam)
{
	int xPos,yPos;
	TCHAR szString[1024]=TEXT("");

	switch(message)
	{
	case WM_CREATE:
					MessageBox(hwnd,TEXT("WM_CREATE Message Received!!"),TEXT("WM_CREATE"),MB_OK);
					break;

	case WM_LBUTTONDOWN:
					fprintf(gpFile,"Entered WM_LBUTTONDOWN!!\n");
					//To display co-ordinates of the click
					xPos=GET_X_LPARAM(lParam);
					yPos=GET_Y_LPARAM(lParam);

					wsprintf(szString,"WM_LBUTTONDOWN received at(x,y)=(%d,%d)!",xPos,yPos);

					MessageBox(NULL,szString,TEXT("LButtonDown"),MB_OK);
					
					fprintf(gpFile,"Exiting WM_LBUTTONDOWN!\n");

					break;

	case WM_KEYDOWN://B,F,L,T,Q
					/*
					WM_KEYDOWN: wParam: Will help get the answer to: "Which key (virtual keycode) has been pressed?"
								lParam: Will help provide an extra information: repeat count, scan code, extended key flag, context code, previous key state flag, transistion state flag etc.
					*/
					fprintf(gpFile,"Entered WM_KEYDOWN!!\n");	
					
					switch(wParam) //specifying wParam is quivalent to LOWORD(wParam)--> Which gives you the virtual key code value for the key press
					{
						
						case 0x41: //A
									MessageBox(NULL,TEXT("WM_KEYDOWN: A has been Pressed!"),TEXT("Keydown"),MB_OK);
									break;
						case 0x42: //B
									MessageBox(NULL,TEXT("WM_KEYDOWN: B has been pressed!"),TEXT("Keydown"),MB_OK);
									break;
						case 0x44: //D
									MessageBox(NULL,TEXT("WM_KEYDOWN: D has been pressed!"),TEXT("Keydown"),MB_OK);
									break;
						case 0x46: //F: For Full screen
									MessageBox(NULL,TEXT("WM_KEYDOWN: F has been pressed!"),TEXT("Keydown"),MB_OK);
									break;
						case 0x4C: //L: For toggling Light ON/OFF
									MessageBox(NULL,TEXT("WM_KEYDOWN: L has been pressed!"),TEXT("Keydown"),MB_OK);
									break;	
						case 0x51:	//Q: to quit the application
									MessageBox(NULL,TEXT("WM_KEYDOWN: Q has been pressed, Quitting the application..!!"),TEXT("Keydown"),MB_OK);		
									PostQuitMessage(0);
									break;
						case 0x53: //S
									MessageBox(NULL,TEXT("WM_KEYDOWN: S has been pressed!"),TEXT("Keydown"),MB_OK);
									break;				
						case 0x54: //T: Texture on/off
									MessageBox(NULL,TEXT("WM_KEYDOWN: T has been pressed!"),TEXT("Keydown"),MB_OK);
									break;
						case 0x57: //W
									MessageBox(NULL,TEXT("WM_KEYDOWN: W has been pressed!"),TEXT("Keydown"),MB_OK);
									break;
						
						case VK_SPACE://spacebar 
									MessageBox(NULL,TEXT("WM_KEYDOWN: Spacebar has been pressed!"),TEXT("Keydown"),MB_OK);
									break;
						case VK_ESCAPE: 
									MessageBox(NULL,TEXT("WM_KEYDOWN: Escape Key has been Pressed!!..Quitting the application!!"),TEXT("Keydown"),MB_OK);
									PostQuitMessage(0);
									break;

						default:	MessageBox(NULL,TEXT("WM_KEYDOWN: Different key has been pressed!"),TEXT("Keydown"),MB_OK);
							
					}//internal switch
					fprintf(gpFile,"Exiting WM_KEYDOWN!\n");
					break;

	case WM_DESTROY:
					fprintf(gpFile,"Entered WM_DESTROY!!\n");
					fprintf(gpFile,"Log file closed successfully!!\n");
					fclose(gpFile);
					gpFile=NULL;
					PostQuitMessage(0);
					break;

	}
	return(DefWindowProc(hwnd,message,wParam,lParam));
}//WndProc







