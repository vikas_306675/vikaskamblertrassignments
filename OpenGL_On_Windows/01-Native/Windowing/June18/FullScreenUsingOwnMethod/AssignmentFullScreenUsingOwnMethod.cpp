/********************************************************
Program: Toggle FullScreen Using Own Method

Steps:
1. Check whether current state of the window is "fullscreen" or not.
2. Get the current window style:
   GetWindowLong(); //Function which helps you get whatever information you want to use or manipulate 
					  anything related to Window in WndProc().
	DWORD dwStyle;
	dwStyle=GetWindowLong(hwnd,GWL_STYLE);

3. Check whether the style which we have received in step-2 includes "WS_OVERLAPPEDWINDOW" or not?
	 
	if(dwStyle & WS_OVERLAPPEDWINDOW) //& bitwise operator internally does multiplication of the results received: 1*1
	{

	}

4. If "WS_OVERLAPPEDWINDOW" is present in the style, from step-3, then "Evaluate current window placement/get the current window placement".
	
	WINDOWPLACEMENT wpPrev;

	//Initialize the wpPrev:
	wpPrev.length=sizeof(WINDOWPLACEMENT);

	bool bIsWindowPlacement= GetWindowPlacement(hwnd,&wpPrev); //returns boolean result so catch it.

5. Get information about monitor screen:
	
   BOOL bIsMonitorInfo=GetMonitorInfo(hMonitor,MONITORINFO *); //(handle to monitor, monitorinfo's structure)
   
   5a. To get hMonitor call:: MonitorFromWindow();
	   HMONITOR MonitorFromWindow(hwnd,MONITORINFOF_PRIMARY); F-stands for Flag so it is read as"Monitor info flag primary"

   5b. So overall code will be:
	   HMONITOR hMonitor;
	   hMonitor=MonitorFromWindow(hwnd,MONITORINFOF_PRIMARY);

	   MONITORINFO mi;
	   mi.cbSize=sizeof(MONITORINFO);

	   BOOL bIsMonitorInfo=GetMonitorInfo(hMonitor,&mi);

6. Check whether: Both boolean variables are TRUE or not?
	
	  if(bIsWindowPlacement && bIsMonitorInfo)
	  {
			//then and then go ahead for step-7 and 8
	  }

7. Set window style accordingly using: "SetWindowLong()"

	SetWindowLong(hwnd,dwStyle & ~WS_OVERLAPPEDWINDOW);

   //WS_OVERLAPPED, WS_CAPTION, WS_SYSMENU, WS_MAXIMIZEBOX, WS_MINIMIZEBOX, WS_THICKFRAME-->Are non needed things when window switched to Full-screen, you dont see any of them
   //But, WS_OVERLAPPED is been taken care in next step

8. Set Window Position accordingly:
	
	SetWindowPos(
				 hwnd,
				 HWND_TOP, //this is the place where WS_OVERLAPPED has been taken care
				 mi.rcMonitor.left, //x of top
				 mi.rcMonitor.top,  //y of top
				 mi.rcMonitor.right-mi.rcMonitor.left, //width
				 mi.rcMonitor.bottom-mi.rcMonitor.top, //height
				 SWP_NOZORDER, //means ekda top la aanli window ki z-ordering kru nako, aahe ti top la tashich asu de 
				 SWP_FRAMECHANGED // yamule WM_NCCALCSIZE trigger hoto-->Which internally calls WM_SIZE
				);

======Switching from Full-Screen to windowed mode========

1. Set the window style back with "WS_OVERLAPPEDWINDOW" added into the style:

	SetWindowLong(hwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);

2. Set the window placement back to previous placement i.e. before going into "Fullscreen"

   SetWindowPlacement(hwnd,&wpPrev);

3. Set Window Position:

   SetWindowPos(
				hwnd,
				HWND_TOP,
				0,0,0,0,  //These value need not be set now, since "SetWindowPlacement has already took care of this
				SWP_NOZORDER|  //Do not create/follow z order by your own
				SWP_NOOWNERZORDER| //do not follow z-order specified by owner if the current window is a child one
				SWP_FRAMECHANGED|  //trigger the change in window size-->WM_NCCALCSIZE-->triggering WM_SIZE
				SWP_NOSIZE|		   //do no resize or change size on your own
				SWP_NOMOVE		   //do not move at all
				);
*********************************************************/
#include <windows.h>
#include <windowsx.h>
#include <stdio.h>

//Global variable declarations

FILE *gpFile=NULL;
bool gbIsFullscreen=false; //to toggle the fullscreen 
HWND ghwnd;

//Callback function declared
LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdLine, int nCmdShow)
{
	//Variable declaration
	 WNDCLASSEX wndClass;
	 TCHAR szAppName[]=TEXT("FullScreen With Own Method");
	 HWND hwnd;
	 MSG msg;

	//code
	 if(fopen_s(&gpFile,"Log_Assignment_Fullscreen_OwnMethod.txt","w")!=0)
	 {
		 MessageBox(NULL,TEXT("Log File cannot be created..\nExiting.."),TEXT("Error"),MB_OK);
		 exit(0);
	 }
	 else
	 {
		 fprintf(gpFile,"Log file created successfully and Opened!\n");
	 }
	wndClass.cbSize=sizeof(WNDCLASSEX);			//sizeof the custom class
	wndClass.style=CS_HREDRAW | CS_VREDRAW;		//class style
	
	wndClass.cbClsExtra=0;						
	wndClass.cbWndExtra=0;

	wndClass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndClass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndClass.hCursor=LoadCursor(NULL,IDC_ARROW);
	wndClass.hInstance=hInstance;
	

	wndClass.lpfnWndProc=WndProc;
	wndClass.lpszClassName=szAppName;
	wndClass.lpszMenuName=NULL;

	//Register the custom created class
	RegisterClassEx(&wndClass);

	//Create the window in memory
	hwnd=CreateWindowEx(
						WS_EX_APPWINDOW,  //This extra parameter to create window means: Try to bring the window on top of "Taskbar" as well. (Exclusive app window)
						szAppName, //Name of the custom class
						TEXT("Assignment Fullscreen with Own Method"), //caption bar title of the window
						WS_OVERLAPPEDWINDOW,			 //Window style
						CW_USEDEFAULT,					 //x
						CW_USEDEFAULT,					 //y
						CW_USEDEFAULT,					 //windth
						CW_USEDEFAULT,					 //height
						NULL,							 //handle to the parent window
						NULL,							 
						hInstance,						 
						NULL
					 );

	//Copy this handle to global handle which can be used in different functions ex. ToggleFullscreen()
	ghwnd=hwnd;

	//Show the window
	ShowWindow(hwnd,nCmdShow);
	UpdateWindow(hwnd);

	//Message loop

	while(GetMessage(&msg,NULL,0,0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;

}//WinMain

LRESULT CALLBACK WndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam)
{
	//declarations
	void ToggleFullscreen(void);

	//code
	switch(message)
	{
	case WM_CREATE:	break;

	case WM_LBUTTONDOWN:break;

	case WM_PAINT:	break;

	case WM_KEYDOWN://Based upon the key pressed: change the color for the window.*/
					/*
					WM_KEYDOWN: wParam: Will help get the answer to: "Which key (virtual keycode) has been pressed?"
								lParam: Will help provide an extra information: repeat count, scan code, extended key flag, context code, previous key state flag, transistion state flag etc.
				
					BOOL InvalidateRect(
										_IN_ HWND hwnd,
										_IN_ const RECT *lpRect,
										_IN_ BOOL bErase
										);

						--> This function invalidates rectangular region of the display and generate a WM_PAINT message.
						--> hwnd :Handle to the window whose update region has changed.
								  if the value specified is NULL-->This funcion will redraw all the windows.
						-->lpRect: a pointer to RECT structure which contains the co-ordinates of the rectangle to be added to the update region
						--> bErase: Specifies whether the background needs to be erased when "update region" is processed.
						            True: value will help erase the background
									False: Value will keep the background as it is (i.e unchanged)
						--> CALL TO THIS FUNCTION WILL TRIGGER WM_PAINT MESSAGE AND HENCE THE WM_PAINT BLOCK WILL BE EXECUTED.
					*/
					fprintf(gpFile,"Entered WM_KEYDOWN!!\n");	
					
					switch(wParam) //specifying wParam is quivalent to LOWORD(wParam)--> Which gives you the virtual key code value for the key press
					{
						
						case 0x46:  //F: Fullscreen toggle
									if(gbIsFullscreen==false)
									{
										ToggleFullscreen();
										gbIsFullscreen=true;
									}
									else
									{
										ToggleFullscreen();
										gbIsFullscreen=false;
									}
									break;
											
						case 0x51:	//Q: to quit the application
									MessageBox(NULL,TEXT("WM_KEYDOWN: Q has been pressed, Quitting the application..!!"),TEXT("Keydown Color Change"),MB_OK);		
									PostQuitMessage(0);
									break;

						case VK_ESCAPE: //Escape: Quit the application
									MessageBox(NULL,TEXT("WM_KEYDOWN: Escape Key has been Pressed!!..Quitting the application!!"),TEXT("Keydown Color Change"),MB_OK);
									PostQuitMessage(0);
									break;
				
					}//internal switch
					fprintf(gpFile,"Exiting WM_KEYDOWN!\n");
					break;

	case WM_DESTROY:
					fprintf(gpFile,"Entered WM_DESTROY!!\n");
					fprintf(gpFile,"Log file closed successfully!!\n");
					fclose(gpFile);
					gpFile=NULL;
					PostQuitMessage(0);
					break;

	}
	return(DefWindowProc(hwnd,message,wParam,lParam));
}//WndProc

//This will toggle the window to full-screen or windowed mode based upon the flag
void ToggleFullscreen(void)
{
	DWORD dwStyle;
	WINDOWPLACEMENT wpPrev;
	BOOL bIsWindowPlacement;
	BOOL bIsMonitorInfo;
	HMONITOR hMonitor;
	MONITORINFO mi;

	if(gbIsFullscreen==false) //Windowed-->Fullscreen mode toggle [Step-1]
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE); //get the style of the window [Step-2]
		
		if(dwStyle & WS_OVERLAPPEDWINDOW) //if the style acquired just now, has "WS_OVERLAPPEDWINDOW in it or not? [Step-3]
		{
			wpPrev.length=sizeof(WINDOWPLACEMENT); 
			bIsWindowPlacement=GetWindowPlacement(ghwnd,&wpPrev);	//backup/save current window placement [Step-4]

			mi.cbSize=sizeof(MONITORINFO);							//Get monitor information [Step-5]
			hMonitor=MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY); //[Step-5a]--Get the monitor handle from window
			bIsMonitorInfo=GetMonitorInfo(hMonitor,&mi);           //[Step-5b]-->Get monitor information from monitor handle

			if(bIsWindowPlacement && bIsMonitorInfo)//[Step-6]:If both information points are available then perform these next 2 steps
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~WS_OVERLAPPEDWINDOW); //[Step-7]://set window style accordingly (without WS_OVERLAPPEDWINDOW)

				SetWindowPos(										 //Now set the window to full-screen [Step-8]
							  ghwnd,
							  HWND_TOP, //ha parameter warchya step madhe "WS_OVERLAPPED"(which is one of 6 component style of WS_OVERLAPPEDWINDOW) che removal balance karto
							  mi.rcMonitor.left, //Top-x
							  mi.rcMonitor.top,  //Top-y
							  mi.rcMonitor.right-mi.rcMonitor.left,//width
							  mi.rcMonitor.bottom-mi.rcMonitor.top,//height
							  SWP_NOZORDER| //do not obey any of the Z-order 
							  SWP_FRAMECHANGED // will trigger WM_NCCALCSIZE message which internally calls upon WM_SIZE message
							  );
			
			}//if-bIsWindowPlacement

		}//if-dwStyle
		ShowCursor(FALSE); //Convention: Not to show cursor in full-screen mode

	}//if-gbIsFullscreen
	else //Fullscreen-->Windowed mode toggle
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle|WS_OVERLAPPEDWINDOW ); //Add earlier removed "WS_OVERLAPPEDWINDOW" style to the window style [Step-1]
		SetWindowPlacement(ghwnd,&wpPrev);							   //Set window placement to earlier value, which was backed up  [Step-2]	
		SetWindowPos(
					 ghwnd,
					 HWND_TOP,
					 0,0,0,0, //as this setting has been taken care by "SetWindowPlacement" function call
					 SWP_NOZORDER | //Do not follow/create your own Z-order
					 SWP_NOOWNERZORDER| //Do not follow Owner's Z-order, if the current window is a child window
					 SWP_FRAMECHANGED | //this flag is as it is, will trigger WM_NCCALCSIZE message-->Triggering WM_SIZE internally
					 SWP_NOSIZE|        //Do not change size
					 SWP_NOMOVE		//Do not move
					 );
		ShowCursor(TRUE);
	}
}





