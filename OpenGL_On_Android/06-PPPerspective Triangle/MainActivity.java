/************************************************************************************

  Program 05: Programmable Pipeline: Orthographic Triangle !
  1. This program includes code for Orthographic Triangle (Using Vertex and Fragment Shader!)
 *************************************************************************************/
//Package ID:
//It represents unique ID for every application. 
// Whichever java file has this package id, it will be compiled during compilation too!
// It means all .java files having this package id will be compiled during compilation.
//Package ID needs to be specified in: 
//        			1) MainActivity.java  OR All .java files as a part of this project.
//					2) andoid-manifest.xml 
//					3) app/build.gradle
package com.astromedicomp.PPPerspectiveTriangle;
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Default Supplied Packages By Android SDK
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
import android.app.Activity;	// Activity which encloses "Window" Class. (One of the default Supplied package by android SDK)
import android.os.Bundle;		// This contains "Global environment variables etc.". (One of the default Supplied package by android SDK)

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Later  Added Packages
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
import android.content.Context;  //For "context" related drawing
import android.content.pm.ActivityInfo; //For "ActivityInfo" class.
import android.view.Window;	    // For "requestWindowFeature().
import android.view.WindowManager; //For Setting orientation of the Andoid window
import android.widget.TextView;	// For All Widget related classes.
import android.graphics.Color;	// For Color related method/Constants.
import android.view.Gravity; // For "Gravity"(position/location) of the text etc. to be displayed on the screen/view.


//Activity: 
//		- It is the class which wraps the "Window" class
//     - It has/keeps the information related to anything related to "device" Ex. Orientation of the device: (he activity la kalte pan window la nahi)      


public class MainActivity extends Activity 				//Class is "public" which indicates that "Object" can be created for this class.
{

	//Defining your own class's object as a private member of the class
	private GLESView glesView; 

    @Override
    protected void onCreate(Bundle savedInstanceState) //Equivalent to "WM_CREATE" 
	{
		//////////////// Parent's Method + Child's extention to it /////////
		// Call to Parent's "onCreate() method, which child wants to extend, hence 
		// first give a call to parent's method followed by 
		// the code child wants to write in the function(which is nothing but extention to parent's code!)
		// Method: 
		//			super.onCreate(); 
		  
        super.onCreate(savedInstanceState);           
		
		//////////////// Fullscreen Window //////////////////////////////////
		// Step:1) Full-screen sathi, 'Window che title' kadhle.
		//				  Plus "Actionbar removal"-->Ya function call sathi package import karawe lagel
		// Step:2) Fullscreen->Set Fullscreen flag: setFlags(Flag, Mask); In case of this program, both are same!
		
		this.requestWindowFeature(Window.FEATURE_NO_TITLE); 
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);	
		
		/////////////// Set Background Color to the window //////////////////
		// Theory: Pratyek window la swatacha ek view asto tyala "DecorView" mhantat.
		// Method: 
		//			getWindow().getDecorView().setBackgroundColor(Color);
		
		getWindow().getDecorView().setBackgroundColor(Color.rgb(0,0,0));
		
		/////////////// Set the Orientation:Landscape ///////////////////////
		// Theory: This needs to be told to "Activity" class why?
		//          1) Device tumhi "Landscape" kiwa "Portrait" kelyashiway orientation change hot nahi.
		//			2) Hee device related information (Orientation etc.) hee Activity class kade aste (Tyane WRAP kelelya Window class kade nahi)
		// Method:
		//		    MainActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		// 		   
		//	        Portrait mode sathi: ActivityInfo.SCREEN_ORIENTATION.PORTRAIT flag.
		//
		// Note:   Need to import android.view.WindowManager; package.
		
		MainActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		
		/////////////// CREATE YOUR OWN VIEW:(AS YOU WILL SHOW TEXT USING YOUR OWN DEFINED VIEW//
		//	TextView: import android.widget.TextView;
		//  Methods: 
		//			  TextView myTextView=new TextView(this);
		//				- Here 'this' parameter gets typecasted to "Context context" since
		//				- "Context" is such a class in "Andoid" which "stores everything global related to application"	
		//				- "TextView" extends "View" class.
		//
		//			  setText("Text to be shown on the window-view");
		//			  setTextSize(Integer size of the text to be displayed);
		//			  setTextColor(Color);
		//			  setGravity(Where to display the text i.e its position across the area of thw window);		
		//					- This method is not only for "Text", it can be used for "Shapes" as well.
		//			  setContentView(Your own defined view);
		
		//TextView myTextView = new TextView(this); //this: Activity class cha object.
		
		//myTextView.setText("myTextView:: Hello World!!");
		//myTextView.setTextSize(60);	
		//myTextView.setTextColor(Color.GREEN);
		//myTextView.setGravity(Gravity.CENTER);
		//setContentView(R.layout.activity_main); //Earlier we used this "Ready-made"view call
  				
		glesView=new GLESView(this);
	
	   //set view as content view of the activity
		setContentView(glesView);	
    } //onCreate()
	
	@Override
	protected void onPause()
	{
		super.onPause();	
	}//onPause()
	
	@Override
	protected void onResume()
	{
		super.onResume();
	}//onResume()
	
}//MainActivity-Class
