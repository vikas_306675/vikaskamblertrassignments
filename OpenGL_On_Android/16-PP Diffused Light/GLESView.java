/************************************************************************************

  Program 16: To Create your own "GLESView" class.
  						 It will contain the code for "3D Cube With Diffused Light" which is created through 
						 - Vertex and 
						 - Fragment Shader!

 OpenGLES : OpenGL Embedded System
  
 ************************************************************************************/
//It represents unique ID for every application. 
// Whichever java file has this package id, it will be compiled during compilation too!
// Means all .java files having this package id will be compiled during compilation.
//To be specified in: 
//        			1) MainActivity.java 
//					2) andoid-manifest.xml 
//					3) app/build.gradle
//				    4) Any other .java file 
// ************************************************************************************/
package com.astromedicomp.PPDiffusedLightCube;
// ************************************************************************************/

import android.view.Window;	    // For "requestWindowFeature().
import android.view.WindowManager; //For Setting orientation of the Andoid window
import android.content.Context;  //For "context" related drawing
import android.app.Activity;	// Activity which encloses "Window" Class.
import android.content.pm.ActivityInfo; //For "ActivityInfo" class.
import android.os.Bundle;		// This contains "Global environment variables etc.".
import android.widget.TextView;	// For All Widget related classes.
import android.graphics.Color;	// For Color related method/Constants.
import android.view.Gravity; // For "Gravity"(position/location) of the text etc. to be displayed on the screen/view.

import android.view.MotionEvent; // for "MotionEvent"
import android.view.GestureDetector; //for GestureDetector
import android.view.GestureDetector.OnGestureListener; //for OnGestureListener
import android.view.GestureDetector.OnDoubleTapListener; //for OnDoubleTapListener

import android.opengl.GLSurfaceView; //for OpenGL Surface View and related drawing
import android.opengl.GLES32;			 // for OpenGLES 3.2
import javax.microedition.khronos.opengles.GL10; // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;   // for EGLConfig needed as parameter type EGLConfig.

//for vbo (nio:non blocking IO: Which is OK for memory IO but not OK to FILE IO which is always "Blocking IO")
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

//For Matrix Manipulations: Mathematics
import android.opengl.Matrix;

//Implementing a "feature" as a separate class.
// Extends: Keyword depicts that, "GLSurfaceView" which is a parent class, would be extended by its child.
// Implements: Depicts, "we" are going to write the implementation of both the Listener's
// OnGestureListener-->Listener Class
// GLSurfaceView.Renderer--> Adapter Class

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	//Class Members
	private final Context context;
	private GestureDetector gestureDetector;
	
	//Shader variables
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
	
	private int[] vao_cube=new int[1];   //vao for Cube
	private int[] vbo_cube_position=new int[1];    //vbo for Cube position
	private int[] vbo_cube_normal=new int[1];         //vbo for Cube Normal
	
	private int modelViewMatrixUniform, projectionMatrixUniform;
	private int ldUniform, kdUniform, lightPositionUniform; //for light's diffuse property, material's diffuse property and light's position property
	
	private int doubleTapUniform;
	
	private float perspectiveProjectionMatrix[]=new float[16]; //4x4 Matrix
	
	//Angle variables
	private float angleCube=0.0f;
	
	private int singleTap; //for animation
	private int doubleTap; //for lights
	
	//Constructor
	public GLESView(Context drawingContext)
	{
		// A call to Parent's constructor passing "context" as a parameter to it.
		// This constructor will extend "Parent's" constructor hence 
		//  - first call represents a call to Parent's constructor
		//	- Within constructor itself we will define the view for the text.
		// Methods:
		//			setTextView();
		//			setTextSize();
		//			setTextColor();
		//			setGravity();
		// Note:	
		//		 While calling above mentioned methods, here we need not use the "this" pointer since 
		//	     whenever an object of this class will come into existence, first of all contrcutor 
		//       will be executed which in turn will be applicable for the object for whom it is getting executed!
		
		super(drawingContext);
		
		context=drawingContext;
		
		/* Step-1)  Version Negotiation Step: accordingly set EGLContext to current Supported version of OpenGL-ES
            - In this step we actually tell Android NDK to select the appropriate OpenGL Context
			  (Which is supported by current Android OS Version + GPU )
			- NDK will take the given parameter as the minimum version of the OpenGL ES and will set respective version
			  (Ex. Either ES3.1 OR ES3.2 ) based upon the search taken.
			- In short it actually checks for the "AEP" .
       */
		setEGLContextClientVersion(3);
		
		/*Step-2) Set Renderer for drawing on GLSurfaceView: Konala renderer banwayche?? 
		    - This is a function of GLSurfaceView Class which is nothing but a parent class.
			- Make "this" class as the "Renderer" class.
		*/ 
		setRenderer(this);
		
		/* Step-3) Render the view only when there is a change in the drawing data.
			- Ethe aapan tharwato ki 1. OnScreenRendering karayche ki 
			- 											 2. Off Screen Rendering ?
			- RENDERMODE_WHEN_DIRTY: Jeva mazi window 'repaint' karanyasathi asel teva!
		*/
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
				
		//setText("MyView:Hello World!!");
		//setTextSize(60);
		//setTextColor(Color.rgb(255,128,0)); // color		
		//setGravity(Gravity.CENTER);			//Centered display of the Text
		
		//Gesture
		// context: Set of all global variables
		//  this: Refers to the "class" which is going to use this GestureDetector!
		// null: If you have any "event handler"defined by you, then specify the name of the class enclosing the same method
		// false: The reserved parameter
		// this means 'handler' i.e. who is going to handle 
		gestureDetector= new GestureDetector(context,this,null,false);
		
		gestureDetector.setOnDoubleTapListener(this); //"this" means handler i.e. who is going to handle it.
	} //Constructor
	
	////////////////Override the methods for GLSurfaceView OR GLSurfaceView.Renderer/////////
	/************Override method of GLSurfaceView.Renderer(INIT Code)*******/
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		//OpenGL-ES Version Check [Je actually constructor madhlya setEGLContextClientVersion() ne select kele te!]
		String version=gl.glGetString(GL10.GL_VERSION);
		System.out.println("VDG: OpenGL ES Version:="+version);
		
		//Get GLSL Version
		String glslVersion=gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("VDG: GLSL Version:="+glslVersion);
		
		initialize(gl);
	}//onSurfaceCreated()
	
	/***********Override method of GLSurfaceView.Renderer (ChangeSize code/resize())*****/
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width,height);
		
	}//onSurfaceChanged()
	
	/********** Override method of GLSurfaceView.Renderer( Rendering Code)*************/
	@Override
	public void onDrawFrame(GL10 unused)
	{
		
		draw();
		
	}//onDrawFrame()
	
	//////////////// Touch Events/Event Code///////////////////////////////////
	//Handing 'OnTouchEvent' is of greate importance, because it triggers "ALL" Gesture and Tap Events.
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventaction=event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}//onTouchEvent
	
	///////////////// onDoubleTap////////////////////////////////////
	//Abstract method from OnDoubleTapListener so it must be implemented.
	
	@Override   	
	public boolean onDoubleTap(MotionEvent e)
	{
		//setText("Double Tap!");
		System.out.println("VDG: "+"Double Tap");
		doubleTap++;
		if(doubleTap>1)
			doubleTap=0;
		return(true);
	
	}//onDoubleTap()
	
	//////////////// onDoubleTapEvent()/////////////////////////////////////
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{	
		//DO NOT WRITE ANY CODE HERE!! BECAUSE ALREADY WRITTEN "OnDoubleTap"
		return(true);	
	}//onDoubleTapEvent()
	
	//////////////// onSingleTapConfirmed() /////////////////////////////////////
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		//code
		singleTap++;
		if(singleTap>1)
		   singleTap=0;
		   
		return(true);	
	}//OnSingleTapConfirmed()
	
	//////////////// OnDown()/////////////////////////////////////
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		//DO NOT WRITE ANY CODE HERE!! BECAUSE ALREADY WRITTEN "OnSingleTapConfirmed()"
		return(true);
	}//OnDown()
	
	//////////////// OnFling()/////////////////////////////////////
	
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2, float velocityX,float velocityY)
	{
		return(true);
	}//OnFling()

	//////////////// OnLongPress() /////////////////////////////////////
  	
	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("VDG: "+"Long Press");
	}//OnLongPress()
	
	//////////////// OnScroll() /////////////////////////////////////
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2,float distanceX,float distanceY)
	{
		uninitialize();
		System.exit(0); // We have opted one way to "Exit the application on scroll event"
		return(true);
	}//OnScroll()

 //////////////// OnShowPress() /////////////////////////////////////
	
	@Override
	public void onShowPress(MotionEvent e)
	{
	}//OnShowPress()
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}//OnSingleTapUp()

	////////////// Lets implement: initialize(), resize(), draw() of  our own!!///
	private void initialize(GL10 gl)
	{
			// Set the background color for frame
			// First get to know the OpenGL ES version supported on your device [Keep initialize(), resize(), draw() as empty]
		   //  Ex. GLES30 / GLES31 / GLES32 --> As per the version supported on your device and android OS.
		
		  //********************************************************
		  // VERTEX SHADER
		  //********************************************************
		  
		  //Create Shader
		  vertexShaderObject=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		  
		  //Vertex shader source code
		  				// final is synonymous to "const"
		  				// const GLchar *-->This does not work in Android.
		 final String vertexShaderSourceCode=String.format
		 (
		 	"#version 320 es"+
			"\n"+
			"in vec4 vPosition;"+
			"in vec3 vNormal;"+
			"uniform mat4 u_model_view_matrix;"+
			"uniform mat4 u_projection_matrix;" +
			"uniform mediump int u_double_tap;"+
			"uniform vec3 u_Ld;"+
			"uniform vec3 u_Kd;"+
			"uniform vec4 u_light_position;"+
			"out vec3 diffuse_light;"+
			"void main(void)"+
			"{"+
			"if(u_double_tap==1)"+
			"{"+
			"vec4 eyeCoordinates=u_model_view_matrix * vPosition;"+
			"vec3 transformed_normal=normalize(mat3(u_model_view_matrix)*vNormal);"+
			"vec3 light_source_direction=normalize(vec3(u_light_position-eyeCoordinates));"+
			"diffuse_light=u_Ld * u_Kd * max(dot(light_source_direction,transformed_normal),0.0);"+
			"}"+  
			"gl_Position = u_projection_matrix * u_model_view_matrix * vPosition;"+
			"}"
		 );
		
		//Provide source code to Shader
		GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

		//Compile shader & check for errors
		GLES32.glCompileShader(vertexShaderObject);
		
		int [] iShaderCompiledStatus =new int[1]; 
		int [] iInfoLogLength = new int[1];
		String szInfoLog=null;
		
		GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0); //Rule: Array name followed by '0'(from where to start in the array)	
		if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);

			if(iInfoLogLength[0]>0)
			{
				szInfoLog=GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("VDG: Vertex Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}//iInfoLogLength[0]>0
						
		}//iShaderCompiledStatus[0]==GLES32.GL_FALSE
		
		//************************************************************
		// FRAGMENT SHADER
		//************************************************************
		//Create shader
		fragmentShaderObject=GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		//Fragment shader source code
				//"precision highp float": This line is specifically for ARM which applies to all single variables of type float which provides "high precision" value for float variables		
		final String fragmentShaderSourceCode=String.format
		(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
			"in vec3 diffuse_light;"+
			"out vec4 FragColor;"+
			"uniform int u_double_tap;"+
			"void main(void)"+
			"{"+
			"vec4 color;"+
			"if(u_double_tap==1)"+
			"{"+
				
				"color=vec4(diffuse_light,1.0);"+
			  
			"}"+
			"else"+
			"{"+
				
					"color=vec4(1.0 ,1.0 ,1.0 ,1.0);"+	
			
			"}"+
			  "FragColor=color;"+
			 "}"
		);		
		//Provide source code to shader object
		GLES32.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);
		
		//Compile Shader and check for errors
		GLES32.glCompileShader(fragmentShaderObject);
		
		iShaderCompiledStatus[0]=0; //re-initialize
		iInfoLogLength[0]=0;				// re-initialize
		szInfoLog=null;							// re-initialize
		
		GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			
			if(iInfoLogLength[0]>0)
			{
				szInfoLog=GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("VDG: Fragment Shader Compilation Log: = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}//iInfoLogLength[0]>0
			
		}//iShaderCompiledStatus[0]==GLES32.GL_FALSE		
		
		//*****************************************************************
		//Create Program which will attach Vertex and Fragment shader objects
		//*****************************************************************
		//Create Shader Program
		shaderProgramObject=GLES32.glCreateProgram();
		
		//attach Vertex Shader to shader program
		GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);
		
		//attach Fragment Shader to Shader Program
		GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);
		
		//*****************************************************************
		//pre-link binding of shader program object with vertex shader attributes
		//*****************************************************************
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");
		
		//*****************************************************************
		//Link the two shaders together to shader program object
		//*****************************************************************
		GLES32.glLinkProgram(shaderProgramObject);
		
		int[] iShaderProgramLinkStatus=new int[1];
	    iInfoLogLength[0]=0; //re-initialize
		szInfoLog=null;	          // re-initialize
		
		GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
		if(iShaderProgramLinkStatus[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0]>0)
			{
				szInfoLog=GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("VDG: Shader Program Link Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}//iInfoLogLength[0]>0

		}//iShaderProgramLinkStatus==GLES32.GL_FALSE;
				
		//***********************************************************
		// get ModelView and Projection Uniform Location
		//***********************************************************
		modelViewMatrixUniform=GLES32.glGetUniformLocation(shaderProgramObject,"u_model_view_matrix");
		
		projectionMatrixUniform=GLES32.glGetUniformLocation(shaderProgramObject,"u_projection_matrix");
		
		doubleTapUniform=GLES32.glGetUniformLocation(shaderProgramObject,"u_double_tap");
		
		ldUniform=GLES32.glGetUniformLocation(shaderProgramObject,"u_Ld");
		
		kdUniform=GLES32.glGetUniformLocation(shaderProgramObject,"u_Kd");
		
		lightPositionUniform=GLES32.glGetUniformLocation(shaderProgramObject,"u_light_position");
		
		/*************** Cube vertices and Normals *******************************/
		
		final float cubeVertices[]=new float[]
		{
			 //Top Face
			1.0f,1.0f,-1.0f,
			-1.0f,1.0f,-1.0f,
			-1.0f,1.0f,1.0f,
			1.0f,1.0f,1.0f,
			 
			 //Bottom Face
			1.0f,-1.0f,1.0f,
			-1.0f,-1.0f,1.0f,
			-1.0f,-1.0f,-1.0f,
			1.0f,-1.0f,-1.0f,
			
			 //Front face
			1.0f, 1.0f,1.0f,     //left-top
			-1.0f,1.0f,1.0f, //left bottom 
			-1.0f,-1.0f,1.0f,   //right bottom
			1.0f,-1.0f,1.0f,    //right top 
			
			//Back Face
			1.0f,-1.0f,-1.0f,
			-1.0f,-1.0f,-1.0f,
			-1.0f,1.0f,-1.0f,
			1.0f,1.0f,-1.0f,
	  
	        //Left Face
			-1.0f,1.0f,1.0f,
			-1.0f,1.0f,-1.0f,
			-1.0f,-1.0f,-1.0f,
			-1.0f,-1.0f,1.0f,
			
			//Right Face
			1.0f,1.0f,-1.0f,
			1.0f,1.0f,1.0f,
			1.0f,-1.0f,1.0f,
			1.0f,-1.0f,-1.0f
		};
		
		//if above -1.0f or +1.0f Values make Cube Much Larger than Pyramid, then follow the code in following loop which will convert all 1s and -1s to -0.75 or +0.75
		for(int i=0;i<72;i++)
		{
			if(cubeVertices[i]<0.0f)
				cubeVertices[i]=cubeVertices[i]+0.25f;
			else if(cubeVertices[i]>0.0f)	
			    cubeVertices[i]=cubeVertices[i]-0.25f;
			else
			    cubeVertices[i]=cubeVertices[i];
		}
		
		final float cubeNormals[]=new float[]
		{
		    0.0f,1.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.0f,1.0f,0.0f,
			
			0.0f,-1.0f,0.0f,
			0.0f,-1.0f,0.0f,
			0.0f,-1.0f,0.0f,
			0.0f,-1.0f,0.0f,
		
			0.0f,0.0f,1.0f,
			0.0f,0.0f,1.0f,
			0.0f,0.0f,1.0f,
			0.0f,0.0f,1.0f,
				
			0.0f,0.0f,-1.0f,
			0.0f,0.0f,-1.0f,
			0.0f,0.0f,-1.0f,
			0.0f,0.0f,-1.0f,
			
			-1.0f,0.0f,0.0f,
			-1.0f,0.0f,0.0f,
			-1.0f,0.0f,0.0f,
			-1.0f,0.0f,0.0f,
				
			1.0f,0.0f,0.0f,
			1.0f,0.0f,0.0f,
			1.0f,0.0f,0.0f,
			1.0f,0.0f,0.0f
			
		};
		
		/*************** Cube vertices and Normals *******************************/
		GLES32.glGenVertexArrays(1,vao_cube,0); //vao is a single element array followed by the position from where to start in array!
		GLES32.glBindVertexArray(vao_cube[0]);
		
		///////////////// vbo_cubePosition////////////////////
		
		GLES32.glGenBuffers(1,vbo_cube_position,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_cube_position[0]);
		
		//*********** Now we need to create "Cooked buffer" to solve our purpose!***************
		// Ka?
		// OpenGL la byte buffer lagto, java madhe asa buffer with memory locations milat nahi..as there is no concept of pointer here.
		// Rules/Steps:
		//===========
		//	1. ByteBuffer tayar kara, aani allocate it enough memory.
		//	2. Tell Java which byte order you want to give to this buffer data!
		// 	==> Appan JVM la sangto ki khalcha CPU tu detect kar aani tyachi ji native byte order asel ti ghe!
		//	    ==> Left to right wachayche ki right to left wachayche (Little Endian and Big Endian)
		//    ==> Most of the Dekstop PC's are "Little Endian"
		//   ==> for ARM-->It will be "Big Endian" but it also has ability to be of "Little endian" if required
		// 3. Tya byte buffer la tumhala jya data type madhe convert karaychey tyamadhe convert kara
		// 4. Actually put the data into the buffer.
		// 5. Tya buffer madhe mag aata suruwat kuthun karaychi!
		
		//Step-1) Allocate such a buffer whose all memory locations are zeroed out (as good as memset)
		ByteBuffer byteBufferCube=ByteBuffer.allocateDirect(cubeVertices.length*4); //(24*4)==>4 : number for bytes per float
		
		//Step-2) tuzya platform chi native order ghe tya buffer sathi: (Little Endian or Big endian)
		byteBufferCube.order(ByteOrder.nativeOrder()); 
		
		//Step-3) Conversion into "Float" type buffer.
		FloatBuffer cubeVerticesBuffer=byteBufferCube.asFloatBuffer();
		
		//Step-4) Actually put the data into it.
		cubeVerticesBuffer.put(cubeVertices);
		
		//Step-5) After putting the data into buffer, from where shall we start.
		cubeVerticesBuffer.position(0);
		
		GLES32.glBufferData( GLES32.GL_ARRAY_BUFFER,
												cubeVertices.length*4,
												cubeVerticesBuffer,
												GLES32.GL_STATIC_DRAW		
											);
		//Tikadam-1
		GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,
															 3,
															 GLES32.GL_FLOAT,
															 false, //Normalize karun gheu ka? 
															 0, //No Dhanga!
															 0 //No Dhanga, hence every array should start from 0th element! 
															 );
		//	Tikadam-3												 
		GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);	
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		///////////// vbo_cube Normal//////////////////////////////
		GLES32.glGenBuffers(1,vbo_cube_normal,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_cube_normal[0]);
		
		//*********** Now we need to create "Cooked buffer" to solve our purpose!***************
		
		//Step-1) Allocate such a buffer whose all memory locations are zeroed out (as good as memset)
		ByteBuffer byteBufferCubeNormal=ByteBuffer.allocateDirect(cubeNormals.length*4); //(24*4)==>4 : number for bytes per float
		
		//Step-2) tuzya platform chi native order ghe tya buffer sathi: (Little Endian or Big endian)
		byteBufferCubeNormal.order(ByteOrder.nativeOrder()); 
		
		//Step-3) Conversion into "Float" type buffer.
		FloatBuffer cubeNormalBuffer=byteBufferCubeNormal.asFloatBuffer();
		
		//Step-4) Actually put the data into it.
		cubeNormalBuffer.put(cubeNormals);
		
		//Step-5) After putting the data into buffer, from where shall we start.
		cubeNormalBuffer.position(0);
		
		GLES32.glBufferData( GLES32.GL_ARRAY_BUFFER,
												cubeNormals.length*4,
												cubeNormalBuffer,
												GLES32.GL_STATIC_DRAW		
											);
		//Tikadam-1
		GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_NORMAL,
															 3,
															 GLES32.GL_FLOAT,
															 false, //Normalize karun gheu ka? 
															 0, //No Dhanga!
															 0 //No Dhanga, hence every array should start from 0th element! 
															 );
		//	Tikadam-3												 
		GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_NORMAL);	
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);
	    /************************ End of Cube  vao ******************************/
		
		/////////////////////Enable Depth Testing/////////////////////////////////////////////////////////////
		// There is no "smooth test"(GL_SMOOTH)" supported and no GL_PERSPECTIVE_CORRECTION_HINT
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		
		//depth test to do
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		
		//we will always cull back faces for better performance
		//But During Rotation: we will "Turn it OFF"
		GLES32.glEnable(GLES32.GL_CULL_FACE);
														 
		//Set background color!
		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f); //Blue color
		
		//initialization
		singleTap=0;
		doubleTap=0;
		
		//Set Projection matrix to identity matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
				
	}//initialize()
	
	/************************ Resize()******************************/
	private void resize(int width,int height)
	{
		//Adjust the viewport based on geometry changes such as screen rotation
		GLES32.glViewport(0,0,width,height);
		
		//Perspective Projection==>(fov angle, aspect ratio,near,far)
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,(float)width/(float)height,0.1f,100.0f);
		
	}//resize()
	
	/************************ draw() ******************************/
	public void draw()
	{
		//Draw background color
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		//Use the shader program
		GLES32.glUseProgram(shaderProgramObject);
		
		if(doubleTap==1)
		{
			GLES32.glUniform1i(doubleTapUniform,1);
	
			//Setting light properties
			GLES32.glUniform3f(ldUniform,1.0f, 1.0f, 1.0f); //diffuse intensity of light
			GLES32.glUniform3f(kdUniform,0.5f,0.5f,0.5f); //diffuse reflectivity of material
			float[] lightPosition={0.0f,0.0f,2.0f,1.0f};
			GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0); //light position
			
		}//if(doubleTap==1)
		else
		{
			GLES32.glUniform1i(doubleTapUniform,0);
		}
		
		//OpenGL-ES Drawing!
		float modelMatrix[]=new float[16];
		float modelViewMatrix[]=new float[16];
		float rotationMatrix[]=new float[16];
	
		
		//set modelView and modelViewProjection matrices tp identity
		Matrix.setIdentityM(modelMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(rotationMatrix,0);

		
		//**************** Draw Cube
		
		//Translate the cube deep into the screen by -5.0
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-5.0f);
		
		//Now Rotate the Matrix-All axis rotation
		Matrix.setRotateM(rotationMatrix,0,angleCube,1.0f,1.0f,1.0f);
		
		//Now Multiply the modelViewMatrix with the rotationMatrix to reflect the rotation
		Matrix.multiplyMM(modelViewMatrix,0,modelMatrix,0,rotationMatrix,0);	
		
		//Pass above modelView matrix to "u_model_view_matrix" shader variable
		GLES32.glUniformMatrix4fv(modelViewMatrixUniform,1,false,modelViewMatrix,0);
		
		//Pass projection matrix to the vertex shader in "u_projection_matrix" 
		GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
		
		
		/********************** Vao's ****************************************/
		//bind vao_cube
		GLES32.glBindVertexArray(vao_cube[0]);
		
		//draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,0,4); //4 (Each with its (x,y,z)) vertices in triangleVertices array
	    GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,4,4); //4 (Each with its (x,y,z)) vertices in triangleVertices array
	   	GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,8,4); //4 (Each with its (x,y,z)) vertices in triangleVertices array
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,12,4); //4 (Each with its (x,y,z)) vertices in triangleVertices array
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,16,4); //4 (Each with its (x,y,z)) vertices in triangleVertices array
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,20,4); //4 (Each with its (x,y,z)) vertices in triangleVertices array
	
		//unbind vao for square
		GLES32.glBindVertexArray(0);

		//un-use Shader Program
		GLES32.glUseProgram(0);													
													
		//Call the update function to increment respective angles
		if(singleTap==1)
		{
		angleCube=angleCube-0.75f;											
		}											
		//Render/flush-->Equivalent  to swapBuffers() 
		requestRender();	
	}//draw()

   /************************ uninitialize() ******************************/
	void uninitialize()
	{
		//code
		//destroy vao_cube
		if(vao_cube[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1,vao_cube,0);
			vao_cube[0]=0;
		}
	
		//destroy vbo_cube_position
		if(vbo_cube_position[0] != 0)
		{
			GLES32.glDeleteBuffers(1,vbo_cube_position,0);
			vbo_cube_position[0]=0;
		}
		
		//destroy vbo_cube_normal
		if(vbo_cube_normal[0] != 0)
		{
			GLES32.glDeleteBuffers(1,vbo_cube_normal,0);
			vbo_cube_normal[0]=0;	
		}
		
		if(shaderProgramObject != 0)
		{
				if(vertexShaderObject != 0)
				{
					//detach vertex shader from shader program object
					GLES32.glDetachShader(shaderProgramObject,vertexShaderObject);
					
					//delete vertexShaderObject
					GLES32.glDeleteShader(vertexShaderObject);
					vertexShaderObject=0;
				}
				
				if(fragmentShaderObject != 0)
				{
					
					//detach fragment shader from shader program object
					GLES32.glDetachShader(shaderProgramObject,fragmentShaderObject);
					
					//delete fragment shader object
					GLES32.glDeleteShader(fragmentShaderObject);
					fragmentShaderObject=0;
				}

		}//shaderProgramObject!=0
		
		//delete shader program object
		if(shaderProgramObject !=0)
		{
			
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject=0;
			
		} //shaderProgramObject!=0
		
	}//uninitialize()

}//MyTextView-Class-Defined by me.