/************************************************************************************

  Program: To Create your own "TextView".
  Methods: There are two ways to implement featured in Object Oriented Programming,
           a) Implement feature as a property of an object of a class OR
		   b) Implement feature as a "Separate Class"
		   
  Note: Here we implement feature as a "Object property" of a class by extending the
        functionality of the parent.		   	   
*************************************************************************************/
package com.astromedicomp.TextViewWindow;//It represents unique ID for every application. [To be specified in: 1) MainActivity.java 2) andoid-manifest.xml 3) app/build.gradle]

import android.content.Context;
import android.app.Activity;	// Activity which encloses "Window" Class.
import android.content.pm.ActivityInfo; //For "ActivityInfo" class.
import android.os.Bundle;		// This contains "Global environment variables etc.".
import android.view.Window;	    // For "requestWindowFeature().
import android.view.WindowManager; //For Setting orientation of the Andoid window
import android.widget.TextView;	// For All Widget related classes.
import android.graphics.Color;	// For Color related method/Constants.
import android.view.Gravity;

/*
Activity: 
		- It is the class which wraps the "Window" class
        - It has/keeps the information related to anything related to "device" Ex. Orientation of the device: (he activity la kalte pan window la nahi)      
*/

public class MainActivity extends Activity 				//Class is "public" which indicates that "Object" can be created for this class.
{

    @Override
    protected void onCreate(Bundle savedInstanceState) //Equivalent to "WM_CREATE" 
	{
		//////////////// Parent's Method + Child's extendsion to it /////////
		// Call to Parent's "onCreate() method, which child wants to extend hence 
		// first call upon parent's method followed by 
		// the code child wants to write in the function(which has been extended)
		// Method: 
		//			super.onCreate(); 
		  
        super.onCreate(savedInstanceState);           
		
		//////////////// Fullscreen Window //////////////////////////////////
		// Step:1) Full-screen sathi, 'Window che title' kadhle.Ya function call sathi package import karawe lagel
		// Step:2) Fullscreen->Set Fullscreen flag: setFlags(Flag, Mask); In case of this program, both are same!
		
		this.requestWindowFeature(Window.FEATURE_NO_TITLE); 
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);	
		
		/////////////// Set Background Color to the window //////////////////
		// Theory: Pratyek window la swatacha ek view asto tyala "DecorView" mhantat.
		// Method: 
		//			getWindow().getDecorView().setBackgroundColor(Color);
		
		getWindow().getDecorView().setBackgroundColor(Color.rgb(0,0,0));
		
		/////////////// Set the Orientation:Landscape ///////////////////////
		// Theory: This needs to be told to "Activity" class why?
		//          1) Device tumhi "Landscape" kiwa "Portrait" kelyashiway orientation change hot nahi.
		//			2) Hee device related information (Orientation etc.) hee Activity class kade aste (Tyane WRAP kelelya Window class kade nahi)
		// Method:
		//		    MainActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		// 		   
		//	        Portrait mode sathi: ActivityInfo.SCREEN_ORIENTATION.PORTRAIT flag.
		//
		// Note:   Need to import android.view.WindowManager; package.
		
		MainActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		
		/////////////// CREATE YOUR OWN VIEW:(AS YOU WILL SHOW TEXT USING YOUR OWN DEFINED VIEW//
		//	TextView: import android.widget.TextView;
		//  Methods: 
		//			  TextView myTextView=new TextView(this);
		//				- Here 'this' parameter gets typecasted to "Context context" since
		//				- "Context" is such a class in "Andoid" which "stores everything global related to application"	
		//				- "TextView" extends "View" class.
		//
		//			  setText("Text to be shown on the window-view");
		//			  setTextSize(Integer size of the text to be displayed);
		//			  setTextColor(Color);
		//			  setGravity(Where to display the text i.e its position across the area of thw window);		
		//					- This method is not only for "Text", it can be used for "Shapes" as well.
		//			  setContentView(Your own defined view);
		
		TextView myTextView = new TextView(this); //this: Activity class cha object.
		
		myTextView.setText("myTextView:: Hello World!!");
		myTextView.setTextSize(60);	
		myTextView.setTextColor(Color.GREEN);
		myTextView.setGravity(Gravity.CENTER);
		
		//setContentView(R.layout.activity_main); //Earlier we used this "Ready-made"view call
        setContentView(myTextView);	
		
    } //onCreate()
}//MainActivity-Class
