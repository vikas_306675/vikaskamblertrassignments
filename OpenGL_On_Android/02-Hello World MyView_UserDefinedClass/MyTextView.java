/************************************************************************************

  Program: To Create your own "TextView".
  Methods: There are two ways to implement featured in Object Oriented Programming,
           a) Implement feature as a property of an object of a class OR
		   b) Implement feature as a "Separate Class"
		   
  Note: Here we implement feature as a "Separate class defined by us" of a class by extending the
        functionality of the parent.		   	   
*************************************************************************************/
//It represents unique ID for every application. 
// Whichever java file has this package id, it will be compiled during compilation too!
// Means all .java files having this package id will be compiled during compilation.
//To be specified in: 
//        			1) MainActivity.java 
//					2) andoid-manifest.xml 
//					3) app/build.gradle
//				    4) Any other .java file 

package com.astromedicomp.MyViewWindow;

import android.content.Context;  //For "context" related drawing
import android.app.Activity;	// Activity which encloses "Window" Class.
import android.content.pm.ActivityInfo; //For "ActivityInfo" class.
import android.os.Bundle;		// This contains "Global environment variables etc.".
import android.view.Window;	    // For "requestWindowFeature().
import android.view.WindowManager; //For Setting orientation of the Andoid window
import android.widget.TextView;	// For All Widget related classes.
import android.graphics.Color;	// For Color related method/Constants.
import android.view.Gravity; // For "Gravity"(position/location) of the text etc. to be displayed on the screen/view.

//Implementing a "feature" as a separate class.
public class MyTextView extends TextView
{
	//Constructor
	MyTextView(Context context)
	{
		// A call to Parent's constructor passing "context" as a parameter to it.
		// This constructor will extend "Parent's" constructor hence 
		//  - first call represents a call to Parent's constructor
		//	- Within constructor itself we will define the view for the text.
		// Methods:
		//			setTextView();
		//			setTextSize();
		//			setTextColor();
		//			setGravity();
		// Note:	
		//		 While calling above mentioned methods, here we need not use the "this" pointer since 
		//	     whenever an object of this class will come into existence, first of all contrcutor 
		//       will be executed which in turn will be applicable for the object for whom it is getting executed!
		
		super(context);
		setText("MyTextView:Hello World!!");
		setTextSize(60);
		setTextColor(Color.rgb(255,128,0)); //Green color		
		setGravity(Gravity.CENTER);			//Centered display of the Text
	} //Constructor
	
}//MyTextView-Class-Defined by me.