/************************************************************************************

  Program 04: To Create your own "GLESView" class.
 
 OpenGLES : OpenGL Embedded System
  
 ************************************************************************************/
//It represents unique ID for every application. 
// Whichever java file has this package id, it will be compiled during compilation too!
// Means all .java files having this package id will be compiled during compilation.
//To be specified in: 
//        			1) MainActivity.java 
//					2) andoid-manifest.xml 
//					3) app/build.gradle
//				    4) Any other .java file 
// ************************************************************************************/
package com.astromedicomp.GLESViewDemo;
// ************************************************************************************/

import android.view.Window;	    // For "requestWindowFeature().
import android.view.WindowManager; //For Setting orientation of the Andoid window
import android.content.Context;  //For "context" related drawing
import android.app.Activity;	// Activity which encloses "Window" Class.
import android.content.pm.ActivityInfo; //For "ActivityInfo" class.
import android.os.Bundle;		// This contains "Global environment variables etc.".
import android.widget.TextView;	// For All Widget related classes.
import android.graphics.Color;	// For Color related method/Constants.
import android.view.Gravity; // For "Gravity"(position/location) of the text etc. to be displayed on the screen/view.

import android.view.MotionEvent; // for "MotionEvent"
import android.view.GestureDetector; //for GestureDetector
import android.view.GestureDetector.OnGestureListener; //for OnGestureListener
import android.view.GestureDetector.OnDoubleTapListener; //for OnDoubleTapListener

import android.opengl.GLSurfaceView; //for OpenGL Surface View and related drawing
import android.opengl.GLES32;			 // for OpenGLES 3.2
import javax.microedition.khronos.opengles.GL10; // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;   // for EGLConfig needed as parameter type EGLConfig.

//Implementing a "feature" as a separate class.
// Extends: Keyword depicts that, "GLSurfaceView" which is a parent class, would be extended by its child.
// Implements: Depicts, "we" are going to write the implementation of both the Listener's
// OnGestureListener-->Listener Class
// GLSurfaceView.Renderer--> Adapter Class

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	//Class Members
	private final Context context;
	private GestureDetector gestureDetector;
	
	//Constructor
	public GLESView(Context drawingContext)
	{
		// A call to Parent's constructor passing "context" as a parameter to it.
		// This constructor will extend "Parent's" constructor hence 
		//  - first call represents a call to Parent's constructor
		//	- Within constructor itself we will define the view for the text.
		// Methods:
		//			setTextView();
		//			setTextSize();
		//			setTextColor();
		//			setGravity();
		// Note:	
		//		 While calling above mentioned methods, here we need not use the "this" pointer since 
		//	     whenever an object of this class will come into existence, first of all contrcutor 
		//       will be executed which in turn will be applicable for the object for whom it is getting executed!
		
		super(drawingContext);
		
		context=drawingContext;
		
		/* Step-1)  Version Negotiation Step: accordingly set EGLContext to current Supported version of OpenGL-ES
            - In this step we actually tell Android NDK to select the appropriate OpenGL Context
			  (Which is supported by current Android OS Version + GPU )
			- NDK will take the given parameter as the minimum version of the OpenGL ES and will set respective version
			  (Ex. Either ES3.1 OR ES3.2 ) based upon the search taken.
			- In short it actually checks for the "AEP" .
       */
		setEGLContextClientVersion(3);
		
		/*Step-2) Set Renderer for drawing on GLSurfaceView: Konala renderer banwayche?? 
		    - This is a function of GLSurfaceView Class which is nothing but a parent class.
			- Make "this" class as the "Renderer" class.
		*/ 
		setRenderer(this);
		
		/* Step-3) Render the view only when there is a change in the drawing data.
			- Ethe aapan tharwato ki 1. OnScreenRendering karayche ki 
			- 											 2. Off Screen Rendering ?
			- RENDERMODE_WHEN_DIRTY: Jeva mazi window 'repaint' karanyasathi asel teva!
		*/
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
				
		//setText("MyView:Hello World!!");
		//setTextSize(60);
		//setTextColor(Color.rgb(255,128,0)); // color		
		//setGravity(Gravity.CENTER);			//Centered display of the Text
		
		//Gesture
		// context: Set of all global variables
		//  this: Refers to the "class" which is going to use this GestureDetector!
		// null: If you have any "event handler"defined by you, then specify the name of the class enclosing the same method
		// false: The reserved parameter
		// this means 'handler' i.e. who is going to handle 
		gestureDetector= new GestureDetector(context,this,null,false);
		
		gestureDetector.setOnDoubleTapListener(this); //"this" means handler i.e. who is going to handle it.
	} //Constructor
	
	////////////////Override the methods for GLSurfaceView OR GLSurfaceView.Renderer/////////
	/************Override method of GLSurfaceView.Renderer(INIT Code)*******/
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		//OpenGL-ES Version Check [Je actually constructor madhlya setEGLContextClientVersion() ne select kele te!]
		String version=gl.glGetString(GL10.GL_VERSION);
		System.out.println("VDG: "+version);
		
		initialize(gl);
	}//onSurfaceCreated()
	
	/***********Override method of GLSurfaceView.Renderer (ChangeSize code/resize())*****/
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width,height);
		
	}//onSurfaceChanged()
	
	/********** Override method of GLSurfaceView.Renderer( Rendering Code)*************/
	@Override
	public void onDrawFrame(GL10 unused)
	{
		
		draw();
		
	}//onDrawFrame()
	
	//////////////// Touch Events/Event Code///////////////////////////////////
	//Handing 'OnTouchEvent' is of greate importance, because it triggers "ALL" Gesture and Tap Events.
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventaction=event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}//onTouchEvent
	
	///////////////// onDoubleTap////////////////////////////////////
	//Abstract method from OnDoubleTapListener so it must be implemented.
	
	@Override   	
	public boolean onDoubleTap(MotionEvent e)
	{
		//setText("Double Tap!");
		System.out.println("VDG: "+"Double Tap");
		return(true);
	}//onDoubleTap()
	
	//////////////// onDoubleTapEvent()/////////////////////////////////////
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{	
		//DO NOT WRITE ANY CODE HERE!! BECAUSE ALREADY WRITTEN "OnDoubleTap"
		return(true);	
	}//onDoubleTapEvent()
	
	//////////////// onSingleTapConfirmed() /////////////////////////////////////
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		System.out.println("VDG: "+"Single Tap");
		return(true);	
	}//OnSingleTapConfirmed()
	
	//////////////// OnDown()/////////////////////////////////////
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		//DO NOT WRITE ANY CODE HERE!! BECAUSE ALREADY WRITTEN "OnSingleTapConfirmed()"
		return(true);
	}//OnDown()
	
	//////////////// OnFling()/////////////////////////////////////
	
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2, float velocityX,float velocityY)
	{
		return(true);
	}//OnFling()

	//////////////// OnLongPress() /////////////////////////////////////
  	
	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("VDG: "+"Long Press");
	}//OnLongPress()
	
	//////////////// OnScroll() /////////////////////////////////////
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2,float distanceX,float distanceY)
	{
		System.out.println("VDG: "+"Scroll");
		System.exit(0); // We have opted one way to "Exit the application on scroll event"
		return(true);
	}//OnScroll()

 //////////////// OnShowPress() /////////////////////////////////////
	
	@Override
	public void onShowPress(MotionEvent e)
	{
	}//OnShowPress()
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}//OnSingleTapUp()

	////////////// Lets implement: initialize(), resize(), draw() of  our own!!///
	private void initialize(GL10 gl)
	{
			// Set the background color for frame
			// First get to know the OpenGL ES version supported on your device [Keep initialize(), resize(), draw() as empty]
		   //  Ex. GLES30 / GLES31 / GLES32 --> As per the version supported on your device and android OS.
			GLES32.glClearColor(0.0f,0.0f,1.0f,0.0f); //Blue color
					
	}//initialize()
	
	private void resize(int width,int height)
	{
		//Adjust the viewport based on geometry changes such as screen rotation
		GLES32.glViewport(0,0,width,height);
	}//resize()
	
	public void draw()
	{
		//Draw background color
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
	}//draw()

}//MyTextView-Class-Defined by me.