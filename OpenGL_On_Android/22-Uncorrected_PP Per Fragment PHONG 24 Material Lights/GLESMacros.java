//***************************************************************
// This is a separate class which depicts the Attributes of the Shader
//***************************************************************
package com.astromedicomp.PP24Spheres3Lights;

public class GLESMacros 
{
	//Attribute index
	public static final int VDG_ATTRIBUTE_VERTEX=0;
	public static final int VDG_ATTRIBUTE_COLOR=1;
	public static final int VDG_ATTRIBUTE_NORMAL=2;
	public static final int VDG_ATTRIBUTE_TEXTURE0=3;
}

//Note:
// We cannot write enum data types in  JAVA!