/************************************************************************************

  Program 08: To Create your own "GLESView" class.
  						 It will contain the code for "Perspective Triangle" which is created through 
						 - Vertex and 
						 - Fragment Shader!

 OpenGLES : OpenGL Embedded System
  
 ************************************************************************************/
//It represents unique ID for every application. 
// Whichever java file has this package id, it will be compiled during compilation too!
// Means all .java files having this package id will be compiled during compilation.
//To be specified in: 
//        			1) MainActivity.java 
//					2) andoid-manifest.xml 
//					3) app/build.gradle
//				    4) Any other .java file 
// ************************************************************************************/
package com.astromedicomp.PPBWTriangleSquare;
// ************************************************************************************/

import android.view.Window;	    // For "requestWindowFeature().
import android.view.WindowManager; //For Setting orientation of the Andoid window
import android.content.Context;  //For "context" related drawing
import android.app.Activity;	// Activity which encloses "Window" Class.
import android.content.pm.ActivityInfo; //For "ActivityInfo" class.
import android.os.Bundle;		// This contains "Global environment variables etc.".
import android.widget.TextView;	// For All Widget related classes.
import android.graphics.Color;	// For Color related method/Constants.
import android.view.Gravity; // For "Gravity"(position/location) of the text etc. to be displayed on the screen/view.

import android.view.MotionEvent; // for "MotionEvent"
import android.view.GestureDetector; //for GestureDetector
import android.view.GestureDetector.OnGestureListener; //for OnGestureListener
import android.view.GestureDetector.OnDoubleTapListener; //for OnDoubleTapListener

import android.opengl.GLSurfaceView; //for OpenGL Surface View and related drawing
import android.opengl.GLES32;			 // for OpenGLES 3.2
import javax.microedition.khronos.opengles.GL10; // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;   // for EGLConfig needed as parameter type EGLConfig.

//for vbo (nio:non blocking IO: Which is OK for memory IO but not OK to FILE IO which is always "Blocking IO")
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

//For Matrix Manipulations: Mathematics
import android.opengl.Matrix;

//Implementing a "feature" as a separate class.
// Extends: Keyword depicts that, "GLSurfaceView" which is a parent class, would be extended by its child.
// Implements: Depicts, "we" are going to write the implementation of both the Listener's
// OnGestureListener-->Listener Class
// GLSurfaceView.Renderer--> Adapter Class

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	//Class Members
	private final Context context;
	private GestureDetector gestureDetector;
	
	//Shader variables
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
	private int[] vao_triangle =new int[1]; //vao for triangle
	private int[] vao_square=new int[1];   //vao for square
	private int[] vbo = new int[1];
	private int mvpUniform;
	
	private float perspectiveProjectionMatrix[]=new float[16];
	//4x4 Matrix
	
	//Constructor
	public GLESView(Context drawingContext)
	{
		// A call to Parent's constructor passing "context" as a parameter to it.
		// This constructor will extend "Parent's" constructor hence 
		//  - first call represents a call to Parent's constructor
		//	- Within constructor itself we will define the view for the text.
		// Methods:
		//			setTextView();
		//			setTextSize();
		//			setTextColor();
		//			setGravity();
		// Note:	
		//		 While calling above mentioned methods, here we need not use the "this" pointer since 
		//	     whenever an object of this class will come into existence, first of all contrcutor 
		//       will be executed which in turn will be applicable for the object for whom it is getting executed!
		
		super(drawingContext);
		
		context=drawingContext;
		
		/* Step-1)  Version Negotiation Step: accordingly set EGLContext to current Supported version of OpenGL-ES
            - In this step we actually tell Android NDK to select the appropriate OpenGL Context
			  (Which is supported by current Android OS Version + GPU )
			- NDK will take the given parameter as the minimum version of the OpenGL ES and will set respective version
			  (Ex. Either ES3.1 OR ES3.2 ) based upon the search taken.
			- In short it actually checks for the "AEP" .
       */
		setEGLContextClientVersion(3);
		
		/*Step-2) Set Renderer for drawing on GLSurfaceView: Konala renderer banwayche?? 
		    - This is a function of GLSurfaceView Class which is nothing but a parent class.
			- Make "this" class as the "Renderer" class.
		*/ 
		setRenderer(this);
		
		/* Step-3) Render the view only when there is a change in the drawing data.
			- Ethe aapan tharwato ki 1. OnScreenRendering karayche ki 
			- 											 2. Off Screen Rendering ?
			- RENDERMODE_WHEN_DIRTY: Jeva mazi window 'repaint' karanyasathi asel teva!
		*/
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
				
		//setText("MyView:Hello World!!");
		//setTextSize(60);
		//setTextColor(Color.rgb(255,128,0)); // color		
		//setGravity(Gravity.CENTER);			//Centered display of the Text
		
		//Gesture
		// context: Set of all global variables
		//  this: Refers to the "class" which is going to use this GestureDetector!
		// null: If you have any "event handler"defined by you, then specify the name of the class enclosing the same method
		// false: The reserved parameter
		// this means 'handler' i.e. who is going to handle 
		gestureDetector= new GestureDetector(context,this,null,false);
		
		gestureDetector.setOnDoubleTapListener(this); //"this" means handler i.e. who is going to handle it.
	} //Constructor
	
	////////////////Override the methods for GLSurfaceView OR GLSurfaceView.Renderer/////////
	/************Override method of GLSurfaceView.Renderer(INIT Code)*******/
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		//OpenGL-ES Version Check [Je actually constructor madhlya setEGLContextClientVersion() ne select kele te!]
		String version=gl.glGetString(GL10.GL_VERSION);
		System.out.println("VDG: OpenGL ES Version:="+version);
		
		//Get GLSL Version
		String glslVersion=gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("VDG: GLSL Version:="+glslVersion);
		
		initialize(gl);
	}//onSurfaceCreated()
	
	/***********Override method of GLSurfaceView.Renderer (ChangeSize code/resize())*****/
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width,height);
		
	}//onSurfaceChanged()
	
	/********** Override method of GLSurfaceView.Renderer( Rendering Code)*************/
	@Override
	public void onDrawFrame(GL10 unused)
	{
		
		draw();
		
	}//onDrawFrame()
	
	//////////////// Touch Events/Event Code///////////////////////////////////
	//Handing 'OnTouchEvent' is of greate importance, because it triggers "ALL" Gesture and Tap Events.
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventaction=event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}//onTouchEvent
	
	///////////////// onDoubleTap////////////////////////////////////
	//Abstract method from OnDoubleTapListener so it must be implemented.
	
	@Override   	
	public boolean onDoubleTap(MotionEvent e)
	{
		//setText("Double Tap!");
		System.out.println("VDG: "+"Double Tap");
		return(true);
	}//onDoubleTap()
	
	//////////////// onDoubleTapEvent()/////////////////////////////////////
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{	
		//DO NOT WRITE ANY CODE HERE!! BECAUSE ALREADY WRITTEN "OnDoubleTap"
		return(true);	
	}//onDoubleTapEvent()
	
	//////////////// onSingleTapConfirmed() /////////////////////////////////////
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		System.out.println("VDG: "+"Single Tap");
		return(true);	
	}//OnSingleTapConfirmed()
	
	//////////////// OnDown()/////////////////////////////////////
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		//DO NOT WRITE ANY CODE HERE!! BECAUSE ALREADY WRITTEN "OnSingleTapConfirmed()"
		return(true);
	}//OnDown()
	
	//////////////// OnFling()/////////////////////////////////////
	
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2, float velocityX,float velocityY)
	{
		return(true);
	}//OnFling()

	//////////////// OnLongPress() /////////////////////////////////////
  	
	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("VDG: "+"Long Press");
	}//OnLongPress()
	
	//////////////// OnScroll() /////////////////////////////////////
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2,float distanceX,float distanceY)
	{
		System.out.println("VDG: "+"Scroll");
		System.exit(0); // We have opted one way to "Exit the application on scroll event"
		return(true);
	}//OnScroll()

 //////////////// OnShowPress() /////////////////////////////////////
	
	@Override
	public void onShowPress(MotionEvent e)
	{
	}//OnShowPress()
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}//OnSingleTapUp()

	////////////// Lets implement: initialize(), resize(), draw() of  our own!!///
	private void initialize(GL10 gl)
	{
			// Set the background color for frame
			// First get to know the OpenGL ES version supported on your device [Keep initialize(), resize(), draw() as empty]
		   //  Ex. GLES30 / GLES31 / GLES32 --> As per the version supported on your device and android OS.
		
		  //********************************************************
		  // VERTEX SHADER
		  //********************************************************
		  
		  //Create Shader
		  vertexShaderObject=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		  
		  //Vertex shader source code
		  				// final is synonymous to "const"
		  				// const GLchar *-->This does not work in Android.
		 final String vertexShaderSourceCode=String.format
		 (
		 	"#version 320 es"+
			"\n"+
			"in vec4 vPosition;"+
			"uniform mat4 u_mvp_matrix;"+
			"void main(void)"+
			"{"+
			"gl_Position = u_mvp_matrix * vPosition;"+
			"}"
		 );
		
		//Provide source code to Shader
		GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

		//Compile shader & check for errors
		GLES32.glCompileShader(vertexShaderObject);
		
		int [] iShaderCompiledStatus =new int[1]; 
		int [] iInfoLogLength = new int[1];
		String szInfoLog=null;
		
		GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0); //Rule: Array name followed by '0'(from where to start in the array)	
		if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);

			if(iInfoLogLength[0]>0)
			{
				szInfoLog=GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("VDG: Vertex Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}//iInfoLogLength[0]>0
						
		}//iShaderCompiledStatus[0]==GLES32.GL_FALSE
		
		//************************************************************
		// FRAGMENT SHADER
		//************************************************************
		//Create shader
		fragmentShaderObject=GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		//Fragment shader source code
				//"precision highp float": This line is specifically for ARM which applies to all single variables of type float which provides "high precision" value for float variables		
		final String fragmentShaderSourceCode=String.format
		(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
			"out vec4 FragColor;"+
			"void main(void)"+
			"{"+
			  "FragColor=vec4(1.0, 1.0, 1.0, 1.0);"+
			 "}"
		);		
		//Provide source code to shader object
		GLES32.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);
		
		//Compile Shader and check for errors
		GLES32.glCompileShader(fragmentShaderObject);
		
		iShaderCompiledStatus[0]=0; //re-initialize
		iInfoLogLength[0]=0;				// re-initialize
		szInfoLog=null;							// re-initialize
		
		GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			
			if(iInfoLogLength[0]>0)
			{
				szInfoLog=GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("VDG: Fragment Shader Compilation Log: = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}//iInfoLogLength[0]>0
			
		}//iShaderCompiledStatus[0]==GLES32.GL_FALSE		
		
		//*****************************************************************
		//Create Program which will attach Vertex and Fragment shader objects
		//*****************************************************************
		//Create Shader Program
		shaderProgramObject=GLES32.glCreateProgram();
		
		//attach Vertex Shader to shader program
		GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);
		
		//attach Fragment Shader to Shader Program
		GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);
		
		//pre-link binding of shader program object with vertex shader attributes
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
		
		//Link the two shaders together to shader program object
		GLES32.glLinkProgram(shaderProgramObject);
		
		int[] iShaderProgramLinkStatus=new int[1];
	    iInfoLogLength[0]=0; //re-initialize
		szInfoLog=null;	          // re-initialize
		
		GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
		if(iShaderProgramLinkStatus[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0]>0)
			{
				szInfoLog=GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("VDG: Shader Program Link Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}//iInfoLogLength[0]>0

		}//iShaderProgramLinkStatus==GLES32.GL_FALSE;
				
		//***********************************************************
		// get MVP Uniform Location
		//***********************************************************
		mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");
		
		//***********************************************************
		// Vertices, colors, shader attribs, vao, vbo, initializations
		//***********************************************************
		final float triangleVertices[]=new float[]
		{
			0.0f, 1.0f,0.0f,     //apex
			-1.0f,-1.0f,0.0f, //left bottom 
			1.0f,-1.0f,0.0f   //right bottom
		};
		/*************** Vao for Triangle *******************************/
		GLES32.glGenVertexArrays(1,vao_triangle,0); //vao is a single element array followed by the position from where to start in array!
		GLES32.glBindVertexArray(vao_triangle[0]);
		
		GLES32.glGenBuffers(1,vbo,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo[0]);
		
		//*********** Now we need to create "Cooked buffer" to solve our purpose!***************
		// Ka?
		// OpenGL la byte buffer lagto, java madhe asa buffer with memory locations milat nahi..as there is no concept of pointer here.
		// Rules/Steps:
		//===========
		//	1. ByteBuffer tayar kara, aani allocate it enough memory.
		//	2. Tell Java which byte order you want to give to this buffer data!
		// 	==> Appan JVM la sangto ki khalcha CPU tu detect kar aani tyachi ji native byte order asel ti ghe!
		//	    ==> Left to right wachayche ki right to left wachayche (Little Endian and Big Endian)
		//    ==> Most of the Dekstop PC's are "Little Endian"
		//   ==> for ARM-->It will be "Big Endian" but it also has ability to be of "Little endian" if required
		// 3. Tya byte buffer la tumhala jya data type madhe convert karaychey tyamadhe convert kara
		// 4. Actually put the data into the buffer.
		// 5. Tya buffer madhe mag aata suruwat kuthun karaychi!
		
		//Step-1) Allocate such a buffer whose all memory locations are zeroed out (as good as memset)
		ByteBuffer byteBuffer=ByteBuffer.allocateDirect(triangleVertices.length*4); //(6*4)==>4 : number for bytes per float
		
		//Step-2) tuzya platform chi native order ghe tya buffer sathi: (Little Endian or Big endian)
		byteBuffer.order(ByteOrder.nativeOrder()); 
		
		//Step-3) Conversion into "Float" type buffer.
		FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
		
		//Step-4) Actually put the data into it.
		verticesBuffer.put(triangleVertices);
		
		//Step-5) After putting the data into buffer, from where shall we start.
		verticesBuffer.position(0);
		
		GLES32.glBufferData( GLES32.GL_ARRAY_BUFFER,
												triangleVertices.length*4,
												verticesBuffer,
												GLES32.GL_STATIC_DRAW		
											);
		//Tikadam-1
		GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,
															 3,
															 GLES32.GL_FLOAT,
															 false, //Normalize karun gheu ka? 
															 0, //No Dhanga!
															 0 //No Dhanga, hence every array should start from 0th element! 
															 );
		//	Tikadam-3												 
		GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);	
		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		GLES32.glBindVertexArray(0);
		
		
		/*************** Vao for Square  *******************************/
		final float squareVertices[]=new float[]
		{
			-1.0f, 1.0f,0.0f,     //left-top
			-1.0f,-1.0f,0.0f, //left bottom 
			1.0f,-1.0f,0.0f,   //right bottom
			1.0f,1.0f,0.0f    //right top 
		};
		GLES32.glGenVertexArrays(1,vao_square,0); //vao is a single element array followed by the position from where to start in array!
		GLES32.glBindVertexArray(vao_square[0]);
		
		GLES32.glGenBuffers(1,vbo,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo[0]);
		
		//*********** Now we need to create "Cooked buffer" to solve our purpose!***************
		// Ka?
		// OpenGL la byte buffer lagto, java madhe asa buffer with memory locations milat nahi..as there is no concept of pointer here.
		// Rules/Steps:
		//===========
		//	1. ByteBuffer tayar kara, aani allocate it enough memory.
		//	2. Tell Java which byte order you want to give to this buffer data!
		// 	==> Appan JVM la sangto ki khalcha CPU tu detect kar aani tyachi ji native byte order asel ti ghe!
		//	    ==> Left to right wachayche ki right to left wachayche (Little Endian and Big Endian)
		//    ==> Most of the Dekstop PC's are "Little Endian"
		//   ==> for ARM-->It will be "Big Endian" but it also has ability to be of "Little endian" if required
		// 3. Tya byte buffer la tumhala jya data type madhe convert karaychey tyamadhe convert kara
		// 4. Actually put the data into the buffer.
		// 5. Tya buffer madhe mag aata suruwat kuthun karaychi!
		
		//Step-1) Allocate such a buffer whose all memory locations are zeroed out (as good as memset)
		ByteBuffer byteBufferSquare=ByteBuffer.allocateDirect(squareVertices.length*4); //(6*4)==>4 : number for bytes per float
		
		//Step-2) tuzya platform chi native order ghe tya buffer sathi: (Little Endian or Big endian)
		byteBufferSquare.order(ByteOrder.nativeOrder()); 
		
		//Step-3) Conversion into "Float" type buffer.
		FloatBuffer verticesBufferSquare=byteBufferSquare.asFloatBuffer();
		
		//Step-4) Actually put the data into it.
		verticesBufferSquare.put(squareVertices);
		
		//Step-5) After putting the data into buffer, from where shall we start.
		verticesBufferSquare.position(0);
		
		GLES32.glBufferData( GLES32.GL_ARRAY_BUFFER,
												squareVertices.length*4,
												verticesBufferSquare,
												GLES32.GL_STATIC_DRAW		
											);
		//Tikadam-1
		GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,
															 3,
															 GLES32.GL_FLOAT,
															 false, //Normalize karun gheu ka? 
															 0, //No Dhanga!
															 0 //No Dhanga, hence every array should start from 0th element! 
															 );
		//	Tikadam-3												 
		GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);	
		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		GLES32.glBindVertexArray(0);
		
	    /************************ End of Triangle and Square vao ******************************/
		
		/////////////////////Enable Depth Testing/////////////////////////////////////////////////////////////
		// There is no "smooth test"(GL_SMOOTH)" supported and no GL_PERSPECTIVE_CORRECTION_HINT
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		
		//depth test to do
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		
		//we will always cull back faces for better performance
		GLES32.glEnable(GLES32.GL_CULL_FACE);
														 
		//Set background color!
		GLES32.glClearColor(0.0f,0.0f,0.0f,0.0f); //Blue color
		
		//Set Projection matrix to identity matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
				
	}//initialize()
	
	private void resize(int width,int height)
	{
		//Adjust the viewport based on geometry changes such as screen rotation
		GLES32.glViewport(0,0,width,height);
		
		//Perspective Projection==>(fov angle, aspect ratio,near,far)
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,(float)width/(float)height,0.1f,100.0f);
		
	}//resize()
	
	public void draw()
	{
		//Draw background color
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		//Use the shader program
		GLES32.glUseProgram(shaderProgramObject);
		
		//OpenGL-ES Drawing!
		float modelViewMatrix[]=new float[16];
		float modelViewProjectionMatrix[]=new float[16];
		
		//set modelView and modelViewProjection matrices tp identity
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		
		/**************** Draw Triangle ***********************************/
		
		//Translate the triangle to the left side
		Matrix.translateM(modelViewMatrix,0,-1.5f,0.0f,-6.0f);
		
		//Multiply the modelView and Projection matrix to get modelViewProjection Matrix
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0); //Left to right order
		
		// Pass above modelViewProjectionMatrix to vertex Shader in "u_mvp_matrix" shader variable whose position value we already 
		// calculated in initWithFrame() by using glGetUniformLocation().
		
		GLES32.glUniformMatrix4fv ( mvpUniform,
															1,
															false, //Do not transpose
															modelViewProjectionMatrix,
															0
															);
		//bind vao
		GLES32.glBindVertexArray(vao_triangle[0]);
		
		//draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES,0,3); //3 (Each with its (x,y,z)) vertices in triangleVertices array
		
		//unbind vao
		GLES32.glBindVertexArray(0);
	
	 	/**************** Draw Square***********************************/
	   	//set modelView and modelViewProjection matrices tp identity
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
	 
	    //Translate the square to the right side
		Matrix.translateM(modelViewMatrix,0,1.5f,0.0f,-6.0f);
		
		//Multiply the modelView and Projection matrix to get modelViewProjection Matrix
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0); //Left to right order
		
		// Pass above modelViewProjectionMatrix to vertex Shader in "u_mvp_matrix" shader variable whose position value we already 
		// calculated in initWithFrame() by using glGetUniformLocation().
		
		GLES32.glUniformMatrix4fv ( mvpUniform,
															1,
															false, //Do not transpose
															modelViewProjectionMatrix,
															0
															);
		//bind vao_square
		GLES32.glBindVertexArray(vao_square[0]);
		
		//draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,0,4); //4 (Each with its (x,y,z)) vertices in triangleVertices array
		
		//unbind vao for square
		GLES32.glBindVertexArray(0);
	
	  /********************** End  of Vao's ****************************************/
		
		//un-use Shader Program
		GLES32.glUseProgram(0);													
													
		//Render/flush-->Equivalent  to swapBuffers() 
		requestRender();	
	}//draw()

	void uninitialize()
	{
		//code
		//destroy vao_triangle
		if(vao_triangle[0] != 0)
		{
			
			GLES32.glDeleteVertexArrays(1,vao_triangle,0);
			vao_triangle[0]=0;
		}
		
		//destroy vao_square
		if(vao_square[0] != 0)
		{
			
			GLES32.glDeleteVertexArrays(1,vao_square,0);
			vao_square[0]=0;
		}
		//destroy vbo
		if(vbo[0] != 0)
		{
			GLES32.glDeleteBuffers(1,vbo,0);
			vbo[0]=0;
			
		}
		if(shaderProgramObject != 0)
		{
				if(vertexShaderObject != 0)
				{
					//detach vertex shader from shader program object
					GLES32.glDetachShader(shaderProgramObject,vertexShaderObject);
					
					//delete vertexShaderObject
					GLES32.glDeleteShader(vertexShaderObject);
					vertexShaderObject=0;
				}
				
				if(fragmentShaderObject != 0)
				{
					
					//detach fragment shader from shader program object
					GLES32.glDetachShader(shaderProgramObject,fragmentShaderObject);
					
					//delete fragment shader object
					GLES32.glDeleteShader(fragmentShaderObject);
					fragmentShaderObject=0;
				}

		}//shaderProgramObject!=0
		
		//delete shader program object
		if(shaderProgramObject !=0)
		{
			
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject=0;
			
		} //shaderProgramObject!=0
		
	}//uninitialize()

}//MyTextView-Class-Defined by me.