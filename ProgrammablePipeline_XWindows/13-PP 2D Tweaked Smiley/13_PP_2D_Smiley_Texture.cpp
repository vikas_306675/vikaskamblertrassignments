/********************************************************************************

Assignment Program:13: Programmable Pipeline: 2D Tweaked Smiley Texture!!! 

Learning:
# showkey -a (Enter)
  Press any key-->Whose Decimal/Octal/Hexadecimal value can be printed! 
*********************************************************************************/
///////////////////////// Headers////////////////////////////////////////////////

#include<iostream>
#include<stdio.h>  //For printf()
#include<stdlib.h> //For exit()
#include<memory.h> //For memset()

//Headers for XServer

#include<X11/Xlib.h>   //analogous to "windows.h"
#include<X11/Xutil.h>  //for visuals
#include<X11/XKBlib.h> //XkbKeycodeToKeysym()
#include<X11/keysym.h> //for 'Keysym'

#include<GL/glew.h> //Should be written before #include<gl/gl.h>

#include<GL/gl.h>
#include<GL/glx.h>     //for 'glx' functions

#include "vmath.h"  //For Matrix related function and variables.

#include<SOIL/SOIL.h> //THIRD PARTY IMAGE LOADING LIBRARY!!

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

/////////////////////////Global Variables ////////////////////////////////////////
FILE *gpFile=NULL;

Display *gpDisplay=NULL;

XVisualInfo *gpXVisualInfo=NULL;

Colormap gColormap;

Window gWindow;

GLXContext gGLXContext; //Parallel to HGLRC

typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*,GLXFBConfig,GLXContext,Bool,const int*);

glXCreateContextAttribsARBProc glXCreateContextAttribsARB=NULL;

GLXFBConfig gGLXFBConfig;

bool gbFullscreen=false;
int gKeyPressed=0;
//////////////////////// Texture Object Declaration /////////////////////////////////
GLuint gTexture_Smiley;

//////////////////////// Shader and Shader Program Objects///////////////////////////

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;
////////////////////////// Our own enum variable ////////////////////////////////////
using namespace vmath;

enum
{
	VDG_ATTRIBUTE_VERTEX=0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0
};


/////////////////////// Vao and Vbo /////////////////////////////////////////////////

GLuint gVao_square;   // Vao for Square
GLuint gVbo_position;		
GLuint gVbo_smiley_texture; //An attribute of the Vertex of final geometry	

GLuint gMVPUniform;
GLuint gTexture_Sampler_Uniform; //An uniform for the sampler!!

/////////////////////// gPerspectiveProjectionMatrix////////////////////////////////

mat4 gPerspectiveProjectionMatrix;

//////////////////////// Function ///////////////////////////////////////////////////

int main(int argc, char *argv[])
{

	//function prototypes

	void CreateWindow(void);
	void ToggleFullscreen(void);
	void initialize(void);
	void resize(int,int);
	void display(void);
	void uninitialize(void);	

	//code
	
	//Create a log file
 
	gpFile=fopen("13_PP_2D_Smiley_Texture_Log.txt","w");
	
	if(gpFile==NULL)
	{
		printf("\n\t\t Log File Cannot be Created..Exiting now...\n");
		exit(1);			
	}
	else
	{
		fprintf(gpFile,"\n Log file created and successfully opened!!");

	}
	
	//Create the window
	CreateWindow();

	//initialize
	initialize();

	//Message Loop
	
	//Variable declarations
	XEvent event; //Parallel to "MSG" structure in windows
	KeySym keySym;
	int winWidth;
	int winHeight;
	bool bDone=false;

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);//Parallel to "GetMessage()"
			
			switch(event.type) //parallel to 'iMsg'
			{
				case MapNotify: //Parallel to "WM_CREATE"
						
						break;

				 case KeyPress: //Parallel to WM_KEYDOWN
						keySym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
						
						switch(keySym)
						{
							
							case XK_Escape:
									bDone=true;
									break;

							     case XK_F:
							     case XK_f:		
									if(gbFullscreen==false)
									 {
									   ToggleFullscreen();
									   gbFullscreen=true;							
									 }
									else
									 {
									    ToggleFullscreen();									
		                                                	    gbFullscreen=false;	
									 }
									 break;
	
							    case 0x31: //1 Key Pressed
								     	
								        printf("\n\t\t 1 Key Pressed!!\n");				
								      
									gKeyPressed=1; 	 
								      
									break;
	  
							    case 0x32: //2	

								       printf("\n\t\t 2 Key Pressed!!\n");

								       gKeyPressed=2;		
															
								       break;			
	
							   case 0x33: //3
								      
								      printf("\n\t\t 3 Key Pressed!!\n");
	
								      gKeyPressed=3;			

								      break;

							   case 0x34: //4

								     printf("\n\t\t 4 Key Pressed!!\n");
									
								     gKeyPressed=4;

								     break;		

							   default:
							           		
								   break;			
						}//keysym switch.		
						break;

				case ButtonPress:
						switch(event.xbutton.button)
						{
							case 1://left button

								break;

							case 2://middle button
	
								break;

							case 3://right button
								
								break;

							default:
								break;	
						}			
						break;

			       case MotionNotify://Parallel to "WM_MOUSEMOVE"
						
						 break;

			       case ConfigureNotify://Parallel to "WM_SIZE"
                                                  
						 winWidth=event.xconfigure.width;
						 winHeight=event.xconfigure.height;
						 resize(winWidth,winHeight);			
					 	break;
			       case Expose: //parallel to "WM_PAINT"
						break;

			       case DestroyNotify://
						break;
	
			       case 33://close button, system menu-->Close
								
						bDone=true;
						break;
				default:  		
						break;
			}//switch

		}//inner while
		
		display(); //Render function

	}//End of Message Loop.
	
	uninitialize();	
	return 0;	
}//main()

///////////////////////////////////// CreateWindow() //////////////////////////////////////////
void CreateWindow(void)
{
	
	//function prototypes
	void uninitialize(void);
	
	//variable declarations
	XSetWindowAttributes winAttribs;
	GLXFBConfig *pGLXFBConfigs=NULL; //an array which will hold all the FB Configs
	GLXFBConfig bestGLXFBConfig;	
	XVisualInfo *pTempXVisualInfo=NULL; 
	int styleMask;
	int iNumFBConfigs=0;	
	int i;
	//Algorithm Step-1:Define "our own" frame buffer attributes!

	static int frameBufferAttributes[]={
						GLX_X_RENDERABLE, True,//Means,we want to have video rendering(not image rendering)enabled/True 
						GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR, //Equivanent to "TrueColor" parameter to XMatchVisualInfo()
						GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT, //Since it is video rendering hence "Window"drawable. 
						GLX_RENDER_TYPE, GLX_RGBA_BIT,	   //Single frame is of type "RGBA"
						GLX_DOUBLEBUFFER, True,		   //Application will be "Double buffered".
						GLX_RED_SIZE,8,
						GLX_GREEN_SIZE,8,
						GLX_BLUE_SIZE,8,
						GLX_ALPHA_SIZE,8,
						GLX_DEPTH_SIZE,24,//You have defined depth value, so you won't take it through any function..
						GLX_STENCIL_SIZE,8,
					     // GLX_SAMPLE_BUFFERS,1, //These are the golden lines which will enable you to create the "
					     // GLX_SAMPLES,4,        //"heaven" demo		 
						None		 //Specify end of an array	
			  		   }; //array must be terminated by 0
	//code
	
	gpDisplay=XOpenDisplay(NULL);
	
	if(gpDisplay==NULL)
	{
		printf("\nERROR: Unable to obtain X Display..");
		uninitialize();
		exit(1);
	}	
					
	//Algorithm:Step-2: Get the respective framebuffer configuration that meets our frame buffer attributes(defined above).

	//glxChooseFBConfig()-->will return more than one FB configurations hence we captured the returned values in pointer.	
	pGLXFBConfigs=glXChooseFBConfig(gpDisplay,DefaultScreen(gpDisplay),frameBufferAttributes,&iNumFBConfigs);			
	
	if(pGLXFBConfigs==NULL)
	{
	
		printf("\n Failed to get Valid Framebuffer Configurations..Exiting...");
		uninitialize();
		exit(1);
	}
		
	printf("\n %d Matching FB Configs Found. \n",iNumFBConfigs);

	// Algorithm:Step-3&4: Pick that FB Config/Visual which has most samples per pixel.
	// As we are not asking the system to choose or match one visual, we obtain/get multiple visuals because we get multiple
        // frame buffer configurations. 
	// Hence we loop through all obtained frame buffer configurations to get "the best" frame buffer configuration which is 
        // capable of giving maximum number of samples.

	int bestFrameBufferConfig=-1, worstFrameBufferConfig=-1, bestNumberOfSamples=1, worstNumberOfSamples=999;

	for(i=0;i<iNumFBConfigs;i++)
	{

		pTempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfigs[i]);
		
		//jar visual aale asel tarch pudhe ja, else exit from here itself as there is no point in going ahead
		//if there is no visual, there will be no samples and hence we cannot find the best visual with maximum no.of samples
		//Samples kuthe kuthe lagtat: 
		// 1. DEPTH
		// 2. Texture
		// 3. Tessellation etc.
		// 4. Motion Blur 
		// 5. Occlusion Culling.

		// Audio Gradient: Quittest quite sound to Loudest to loud madhala difference, 
		//		   ha jevadha jast tevadhi audio chi quality changli 
		// Video Gradient: Blackest black to whittest to white.
		if(pTempXVisualInfo)
		{
			int sampleBuffer,samples; //To receive the count of samplebuffers(locations to save samples) and respective count of samples
			// Visual aahe mhanun aat aalo aani he function wapru shakto.
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLE_BUFFERS,&sampleBuffer);
		
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLES,&samples);	
		
			printf("\n Matching FrameBuffer Config=%d:Visual ID=0x%lu:SAMPLE_BUFFERS=%d:SAMPLES=%d\n",i,pTempXVisualInfo->visualid,sampleBuffer,samples);
		
			if(bestFrameBufferConfig <0 || sampleBuffer && samples > bestNumberOfSamples)
			{
				bestFrameBufferConfig=i;

				bestNumberOfSamples=samples;
			}	
			if(worstFrameBufferConfig < 0 || !sampleBuffer||samples < worstNumberOfSamples);
			{
				worstFrameBufferConfig=i;
				worstNumberOfSamples=samples;
			}
			
			XFree(pTempXVisualInfo); //For each visual

		}//if(pTempXVisualInfo)
	
	}//end of looping through FBConfigs which will yield "best FB config"=FB config with highest number of samples. 


	//Algorithm Step:5:For this best FB config, obtain the visual matching to it, which is "Best Visual/Visual of highest quality"
	bestGLXFBConfig=pGLXFBConfigs[bestFrameBufferConfig]; //saved into local variable
	
	
       //Store this "The Best" GLXFBConfig in the global variable "gGLXFBConfig" which will be used in "initialize()" to create the context!!  
         gGLXFBConfig=bestGLXFBConfig;

	//be sure to free "pGLXFBConfigs" as it is not needed now, its job is done successfully!
	XFree(pGLXFBConfigs);
	
	gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);

	printf("\n Chosen Visual ID:0x%lu",gpXVisualInfo->visualid);
	
	//set window attributes now
		
	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
					    RootWindow(gpDisplay,gpXVisualInfo->screen), //"defaultscreen" can also be specified
					    gpXVisualInfo->visual,
					    AllocNone //for 'movable' memory allocation					
					   );
	winAttribs.event_mask=StructureNotifyMask | KeyPressMask | ButtonPressMask | ExposureMask | VisibilityChangeMask | PointerMotionMask;

	styleMask=CWBorderPixel | CWEventMask | CWColormap;
	
	gColormap=winAttribs.colormap;

	gWindow=XCreateWindow(gpDisplay,
			      RootWindow(gpDisplay,gpXVisualInfo->screen),
			      0,
			      0,
			      WIN_WIDTH,
			      WIN_HEIGHT,
			      0, //border width
			      gpXVisualInfo->depth, //depth of the visual (depth for colormap)
			      InputOutput, //class(type) of your window
			      gpXVisualInfo->visual,
			      styleMask,	
			      &winAttribs
			     );	
	
	if(!gWindow)
	{
	
		printf("\n Failure in window creation.");
		uninitialize();
		exit(1);
	}
	
	XStoreName(gpDisplay,gWindow,"Programmable Pipeline::2D Tweaked Smiley Texture!!!");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_WINDOW_DELETE",True);

	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);

}//CreateWindow()

/////////////////////////////////////////////// initialize() //////////////////////////////////////////
void initialize(void)
{
	
	//function declarations
	void resize(int,int);
	void loadGLTextures(GLuint *);
	void uninitialize(void); 

	//code
	//Create a new GL Context 4.5 for rendering:
	// Algorithm Step:6 &7:
	// Here we use "glxCreateContextAttribsARB() instead of "glXCreateContext()"
	// glxCreateContextAttribsARB() creates context from: 1]FrameBuffer Config 2]allows us to flexibly specify OpenGL version
	// Obtain highest supported openGL context not by asking the system to give the highest which it has but by forcing the 
        // system which highest you want.
	// For that purpose "manually" create "the context attribute" of "highest supported" openGL version.
	// Ex. OpenGL Version:4.5
	
	// We have 2 options to do this:
        // 1] Use glxCreateContext(): which gives you OpenGL Context from the chosen visual.
	// 2] Better way is to use: glxCreateCreateContextAttribsARB(): Because it will give OpenGL context from 
	// 	a) Frame Buffer config (not the visual)
	//	b) Allows us to flexibility to specify whichever OpenGL version we want.
  
	glXCreateContextAttribsARB=(glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");

	fprintf(gpFile,"\n Initialize():Before loading the function!");

	if(!glXCreateContextAttribsARB)
		fprintf(gpFile,"\n Could not load the function!");

	GLint attribs[]={
			  GLX_CONTEXT_MAJOR_VERSION_ARB,4,
			  GLX_CONTEXT_MINOR_VERSION_ARB,5,
			  GLX_CONTEXT_PROFILE_MASK_ARB,   //Mask out "low profiles" and "give me the richest profiles"
			  GLX_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB,
			  0	
			};//array must be terminated by 0	
	
	gGLXContext=glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,NULL,True,attribs); //(Display*,FB Config,"is it a shared context"?,HW rendering?,attribs array)	

	if(!gGLXContext) //fallback to safe old style 2.x context
	{
		
	  //When a context version below 3.0 is requested, implementations will return the newest context version compatible with OpenGL versions less than the version 3.0.
	
	  GLint low_version_attribs[]={
			   		GLX_CONTEXT_MAJOR_VERSION_ARB,1,
			   		GLX_CONTEXT_MINOR_VERSION_ARB,4,
			   		0
			  	      }; //array must be terminated by 0
	
	 printf("\n Failed to create GLX 4.5 context. Hence using old-style GLX Context.");
 	
	 gGLXContext=glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,NULL,True,low_version_attribs);

	 printf("\n GLX_VENDOR:%s\n",glXGetClientString(gpDisplay,GLX_VENDOR));
	 printf("\n GLX_VERSION:%s\n",glXGetClientString(gpDisplay,GLX_VERSION));

	}
	else //it means we have obtained OpenGL Context>version 4.1
	{
	  printf("\n OpenGL Context 4.5 Created!");
	
	}

	//Algorithm-Step:8:Check whether the obtained context is directly processable by GPU (HW rendering) Or we need a SW layer in between
        //(SW Rendering).
	// 
	//Verifying the context is a direct context
	
	if(!glXIsDirect(gpDisplay,gGLXContext))
	{
		printf("\n Indirect GLX Rendering Context Obtained!!!\n");
	}
	else
	{
	
		printf("\n Direct GLX Rendering Context Obtained!!\n");
	}

	glXMakeCurrent(gpDisplay,gWindow,gGLXContext); //make the obtained context as a current context

	
	////////////////// GLEW Code ///////////////////////////////////
	GLenum glew_error=glewInit();

	if(glew_error!=GLEW_OK)
	{
		
		fprintf(gpFile,"\n GLEW Initialization did not work! \n");	
		uninitialize();
		exit(1);
	}
	else
	{
		
		fprintf(gpFile,"\n GLEW Initialization done!!\n");

	}
	//////////////// Vertex Shader Code /////////////////////////////////////
	//Specialist who creates the shader
	
	gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER); 

	
	const GLchar *vertexShaderSourceCode="#version 130"\
					     "\n"
					     "in vec4 vPosition;"\
					     "in vec2 vTexture0_Coord;"\
					     "out vec2 out_vTexture0_Coord;"\
					     "uniform mat4 u_mvp_matrix;"\
					     "void main()"\
					     "{"\
						"gl_Position=u_mvp_matrix * vPosition;"\
						"out_vTexture0_Coord=vTexture0_Coord;"		
					     "}";
	
	glShaderSource(gVertexShaderObject,1,(const GLchar **)&vertexShaderSourceCode,NULL); //Attach the shader source code to shader object

	glCompileShader(gVertexShaderObject); //Compile the shader

	GLint iShaderCompiledStatus=0;
	GLint iInfoLogLength=0;
	char *szInfoLog=NULL;

	//Get the status of the compilation
	glGetShaderiv(gVertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);

	if(iShaderCompiledStatus == GL_FALSE)
	{
		//Get the Length of the Log which has been generated after compilation failure..OR..Error log
		glGetShaderiv(gVertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

		if(iInfoLogLength > 0)
		{
			szInfoLog=(char *)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{

				GLsizei written;

				glGetShaderInfoLog(gVertexShaderObject,iInfoLogLength,&written,szInfoLog);
				
				fprintf(gpFile,"\nVertex Shader Compilation Log:%s\n",szInfoLog);

				free(szInfoLog);

				uninitialize();

				exit(0);	

			}//szInfoLog!=NULL : Means malloc() succeeded to allocate memory

		}//iInfoLogLength>0 : Means The Log text exists, so get that text.

	}//iShaderCompiledStatus==GL_FALSE: Means Shader failed with an error during compilation
	
	
	/////////////// Fragment Shader Code////////////////////////////////////
	
	gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

	 
	const GLchar *fragmentShaderSourceCode=	"#version 130"\
						"\n"
						"in vec2 out_vTexture0_Coord;"\
						"out vec4 FragColor;"\
						"uniform sampler2D u_texture0_sampler;"		
						"void main()"\
						"{"\
							
						   "FragColor = texture(u_texture0_sampler, out_vTexture0_Coord);"	
						"}";

	glShaderSource(gFragmentShaderObject,1,(const GLchar **)&fragmentShaderSourceCode,NULL);
	
	glCompileShader(gFragmentShaderObject);

	//Handle fragment shader compilation errors as well
	
	//Get the status of compilation
	glGetShaderiv(gFragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
	
	if(iShaderCompiledStatus==GL_FALSE)
	{

		//Get the Length of the "error Log" to further dump into string.
		glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

		if(iInfoLogLength > 0)
		{
			szInfoLog=(char *)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				
				glGetShaderInfoLog(gFragmentShaderObject,iInfoLogLength,&written,szInfoLog);

				fprintf(gpFile,"\nFragment shader compilation Log:%s\n",szInfoLog);
	
				free(szInfoLog);

				uninitialize();

				exit(0);	

			}//szInfoLog!=NULL:: Means the malloc() call succeeded!				

		}//iInfoLogLength > 0:: Means the Error log exists

	}//iShaderCompiledStatus==GL_FALSE:: Means Shader compilation failed.	
	

	/////////////// Shader Program Object //////////////////////////////////
	
	gShaderProgramObject=glCreateProgram();

	glAttachShader(gShaderProgramObject,gVertexShaderObject);//Attach Vertex Shader to program object	
	glAttachShader(gShaderProgramObject,gFragmentShaderObject);//Attach Fragment Shader to Program Object


	//Pre-link binding of the "shader variable" with the program variable
	glBindAttribLocation(gShaderProgramObject,VDG_ATTRIBUTE_VERTEX,"vPosition"); //Tikadam-2
	glBindAttribLocation(gShaderProgramObject,VDG_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");


	glLinkProgram(gShaderProgramObject);//Link Shaders to program object 

	//Check for any linking failures
	GLint iProgramLinkStatus;

	//Get the linking status
	glGetProgramiv(gShaderProgramObject,GL_LINK_STATUS,&iProgramLinkStatus);

	if(iProgramLinkStatus == GL_FALSE)
	{

		//Get the Log Length for link failure
		glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength > 0)
		{
			szInfoLog=(char *)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject,iInfoLogLength,&written,szInfoLog);

				fprintf(gpFile,"\n Shader Program Link Failure Log:%s\n",szInfoLog);
			
				free(szInfoLog);

				uninitialize();

				exit(0);

			}//szInfoLog!=NULL			

		}//iInfoLogLength > 0::
	
	}//iProgramLinkStatus==GL_FALSE

	//Post-link binding of the "uniform variable" of the shader with the program uniform variable

	gMVPUniform = glGetUniformLocation(gShaderProgramObject,"u_mvp_matrix");	
	
	gTexture_Sampler_Uniform=glGetUniformLocation(gShaderProgramObject, "u_texture0_sampler");

	//Square: ///////////////////// Define our Square, Vao, Vbo and initialize them////////////////////////
	
	const GLfloat squareVertices[]=
	{
		-1.0f,  1.0f, 0.0f, //Left-Top
		-1.0f, -1.0f, 0.0f, //Left-Bottom
	 	 1.0f, -1.0f, 0.0f, //Right Bottom
		 1.0f,  1.0f, 0.0f  //Right Top
	};

	
	// Texture co-ordinates[We are using third party Image loading library and we have given texture coords as opposite to windows way
	// to specify the texture coords.
	// Since the texture co-ordinates need to be supplied dynamically, we will keep this array empty and we will fill it dynamically.
	const GLfloat smileyTextureCoords[]=
	{
	//	0.0f, 0.0f,
	//	0.0f, 1.0f,
	//	1.0f, 1.0f,
	//	1.0f, 0.0f
	};


	//******* Vao Square ******//
	glGenVertexArrays(1,&gVao_square);	
	glBindVertexArray(gVao_square);
		
		//****Vbo Square**//
		glGenBuffers(1,&gVbo_position);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_position);
			
			glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);

			glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);		
			
			glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

		glBindBuffer(GL_ARRAY_BUFFER,0);			

		//*** Vbo Texture ***//
		glGenBuffers(1, &gVbo_smiley_texture);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_smiley_texture);
		
			// Since the array has 8 elements
			glBufferData(GL_ARRAY_BUFFER, 8*sizeof(GLfloat), smileyTextureCoords, GL_DYNAMIC_DRAW );

			glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
			
			glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//////////////////// Enable DEPTH //////////////////////////////////////////////////////////	
	//initialize OpenGL related options
	glShadeModel(GL_SMOOTH);
	
	//set-up depth buffer
	glClearDepth(1.0f);

	//Enable depth testing
	glEnable(GL_DEPTH_TEST);

	//Which test to do?
	glDepthFunc(GL_LEQUAL);
	
	//set really nice perspective calculations?
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	//We will always cull back faces for better performance
	glEnable(GL_CULL_FACE);

	///////////////// Load the Texture Image thourgh our own defined function////////////////////	
	loadGLTextures(&gTexture_Smiley);	

	//Set the background color
	glClearColor(0.0f,0.0f,0.0f,0.0f);

	//Set the gPerspectiveProjectionMatrix to identity
	gPerspectiveProjectionMatrix=mat4::identity();
				
	//warmup resize call
	resize(WIN_WIDTH,WIN_HEIGHT);
			
}//initialize()

///////////////////////////////// loadGLTextures() /////////////////////////////////////////////////////
void loadGLTextures(GLuint *texture)
{
	//variable declaration

	int width, height;
	unsigned char *imageData=NULL;

	//code
	//Get the image data through SOIL(Simple OpenGL Image Library)
	imageData=SOIL_load_image(
					
				"/home/viks/RTR/XWindows/ProgrammablePipeline/Assignments/12_PP_2D_Smiley_Texture/Smiley.bmp",	//Image Path			 						
				 &width,
				 &height,
				 0,		//Image madhe kiti channels aahet? (R/B/G/A)-->0-->Default		
				 SOIL_LOAD_RGB	
				 );
	//Check!!
	fprintf(gpFile,"ImageData:%c\n", *imageData);

	//Texture Generation Code
	glGenTextures(1, texture); //Texture is nothing but an empty memory location where we will fill in actual texture/image data

	glBindTexture(GL_TEXTURE_2D, *texture); //(Texture Type, unique number returned by glGenTextures())

	glPixelStorei(GL_UNPACK_ALIGNMENT,4);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	glTexImage2D(
			GL_TEXTURE_2D, 		// Target Image Type
			0, 	       		// Level of depth/mipmap
			GL_RGB,	       		// Internal format of the image
			width,	       		// Image width
			height,	       		// Image height
			0,	       		// Border Width	
			GL_RGB,	       		// Pixel Format
			GL_UNSIGNED_BYTE,	// Data type of the image
			imageData		// Actual image Data
		    );
	
	//Create MIPMAPS for this texture for better image quality
	glGenerateMipmap(GL_TEXTURE_2D);

	SOIL_free_image_data(imageData);
	
}//loadGLTextures()

//////////////////////////// ToggleFullscreen() //////////////////////////////////////////

// 1. Create an atom which will store non-fullscreen state.
// 2. Since this will be a switch on state of the window, from our side we will fire the event created by us.
//    So create your own event, let the XServer know about it.
// 3. 

void ToggleFullscreen(void)
{
	//function prototypes
	//variables
	Atom wm_state;    //it will store the non-fullscreen state
	Atom fullscreen;  //it will store the fullscreen state
	XEvent eventForToggleFullscreen; //for our own event.

	//code

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	
	eventForToggleFullscreen.type=ClientMessage;     //Event type?
	eventForToggleFullscreen.xclient.window=gWindow;         //Kuthlya window sathi?
	eventForToggleFullscreen.xclient.message_type=wm_state;  //non-fullscreen state
	eventForToggleFullscreen.xclient.format=32;              //message format=32 bit.

	eventForToggleFullscreen.xclient.data.l[0]=gbFullscreen?0:1; //Ternary operator which decides what to store in l[0].

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	
	eventForToggleFullscreen.xclient.data.l[1]=fullscreen; //Storing the "fullscreen" state. 
	
	//Now we will send the event. Parallel to SendMessage() in windows.		
		
	XSendEvent(gpDisplay,
		   RootWindow(gpDisplay,gpXVisualInfo->screen),
		   False, //Do not send this message to sibling windows
		   StructureNotifyMask, //resizing mask(event_mask)
		   &eventForToggleFullscreen
		  );

}//ToggleFullscreen()

///////////////////////////////////////// resize() /////////////////////////////////////////////
void resize(int width,int height)
{

	if(height==0)
		height=1;
	
	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	//perspective(fovy,Aspect Ratio,near,far);	
	gPerspectiveProjectionMatrix = perspective(45.0f, (float)width / (float)height, 0.1f, 100.0f);

	
}//resize()

/////////////////////////////////////// display() //////////////////////////////////////////
void display(void)
{
	
	//code		
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	//Select using openGL program object
	glUseProgram(gShaderProgramObject);
	
		//In this block, there will be code for OpenGL Drawing. 
		//************************ SQUARE ************************************//
		mat4 modelViewMatrix=mat4::identity();
		mat4 modelViewProjectionMatrix=mat4::identity();

		modelViewMatrix=translate(0.0f, 0.0f, -5.0f);

		modelViewProjectionMatrix=gPerspectiveProjectionMatrix * modelViewMatrix;

		glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);		
	
		
		//****************** Drawing the geometry along with texture *******//
		//Based upon the key press change the texture mapping co-ordinates
	
		glBindVertexArray(gVao_square);		

		GLfloat smileyCoords[8];
		
		if(gKeyPressed==1)//Left-Bottom
		{
		
			smileyCoords[0]=0.0f; smileyCoords[1]=0.0f;
			smileyCoords[2]=0.0f; smileyCoords[3]=0.5f;
			smileyCoords[4]=0.5f; smileyCoords[5]=0.5f;
			smileyCoords[6]=0.5f; smileyCoords[7]=0.0f;	
			
			glBindBuffer(GL_ARRAY_BUFFER, gVbo_smiley_texture);

			glBufferData(GL_ARRAY_BUFFER, sizeof(smileyCoords), smileyCoords, GL_DYNAMIC_DRAW);

			glBindBuffer(GL_ARRAY_BUFFER,0);

		}
		else if(gKeyPressed==2)//Normal-Smiley
		{

			smileyCoords[0]=0.0f; smileyCoords[1]=0.0f;
			smileyCoords[2]=0.0f; smileyCoords[3]=1.0f;
			smileyCoords[4]=1.0f; smileyCoords[5]=1.0f;
			smileyCoords[6]=1.0f; smileyCoords[7]=0.0f;

			glBindBuffer(GL_ARRAY_BUFFER, gVbo_smiley_texture);

			glBufferData(GL_ARRAY_BUFFER, sizeof(smileyCoords),smileyCoords, GL_DYNAMIC_DRAW);

			glBindBuffer(GL_ARRAY_BUFFER,0);
		}
		else if(gKeyPressed==3)//Repeat Mode
		{
			smileyCoords[0]=0.0f; smileyCoords[1]=0.0f;
			smileyCoords[2]=0.0f; smileyCoords[3]=2.0f;
			smileyCoords[4]=2.0f; smileyCoords[5]=2.0f;
			smileyCoords[6]=2.0f; smileyCoords[7]=0.0f;

			glBindBuffer(GL_ARRAY_BUFFER, gVbo_smiley_texture);	
		
			glBufferData(GL_ARRAY_BUFFER, sizeof(smileyCoords), smileyCoords, GL_DYNAMIC_DRAW);

			glBindBuffer(GL_ARRAY_BUFFER, 0);
		}
		else if(gKeyPressed==4)//Center-point
		{
			smileyCoords[0]=0.5f; smileyCoords[1]=0.5f;
			smileyCoords[2]=0.5f; smileyCoords[3]=0.5f;
			smileyCoords[4]=0.5f; smileyCoords[5]=0.5f;
			smileyCoords[6]=0.5f; smileyCoords[7]=0.5f;

			glBindBuffer(GL_ARRAY_BUFFER, gVbo_smiley_texture);
			
			glBufferData(GL_ARRAY_BUFFER, sizeof(smileyCoords), smileyCoords, GL_DYNAMIC_DRAW);
	
			glBindBuffer(GL_ARRAY_BUFFER,0);
		}
	
		//3 Step binding of the texture
		
		glActiveTexture(GL_TEXTURE0);

		glBindTexture(GL_TEXTURE_2D, gTexture_Smiley);

		glUniform1i(gTexture_Sampler_Uniform,0);

		glDrawArrays(GL_TRIANGLE_FAN,0,4);
	
		glBindVertexArray(0);


	//Stop using "this" shader program object
	glUseProgram(0);

	glXSwapBuffers(gpDisplay,gWindow);	
}//display()

//////////////////////////////// uninitialize() ///////////////////////////////////////////

void uninitialize(void)
{
	
	//Code
	//Release the Vao and Vbo
	if(gVao_square)
	{

		glDeleteVertexArrays(1, &gVao_square);
		gVao_square=0;
	}
	if(gVbo_position)
	{

		glDeleteBuffers(1,&gVbo_position);
		gVbo_position=0;			

	}
	if(gVbo_smiley_texture)
	{
		glDeleteBuffers(1, &gVbo_smiley_texture);
		gVbo_smiley_texture=0;
	}
	//Releasing/detaching shader's from program object

	glDetachShader(gShaderProgramObject,gVertexShaderObject); //Vertex Shader Object

	glDetachShader(gShaderProgramObject,gFragmentShaderObject); //Fragment Shader Object
	
	glDeleteShader(gVertexShaderObject); //Delete Vertex Shader
	gVertexShaderObject=0;

	glDeleteShader(gFragmentShaderObject); //Delete Fragment Shader
	gFragmentShaderObject=0;

	glDeleteProgram(gShaderProgramObject);//Now delete the "shader program object" after detaching vertex and fragment shaders from it.
	gShaderProgramObject=0;

	//unlink the shader program
	glUseProgram(0);
	

	//Releasing OpenGL related and XWindow related objects.
	GLXContext currentContext=glXGetCurrentContext();

	if(currentContext!=NULL && currentContext==gGLXContext)	
	{
		
		glXMakeCurrent(gpDisplay,0,0);	

	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	
	}
	if(gWindow)
	{
		
		XDestroyWindow(gpDisplay,gWindow);

	}
	if(gColormap)
	{
	
		XFreeColormap(gpDisplay,gColormap);

	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{

		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}	
	if(gpFile)
	{
		fprintf(gpFile,"\n Log file is successfully closed!");
		fclose(gpFile);
		gpFile=NULL;	
	}
	
}//uninitialize()


						 
