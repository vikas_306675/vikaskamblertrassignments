//**************************************************************************************************************************

 //Programmable Pipeline: All Shapes!!!
 //[Conversion of FFP Program into Programmable pipeline!]
//***************************************************************************************************************************/
#include<windows.h>
#include<stdio.h> //for file i/o

#include<gl\glew.h> // for GLSL extensions.
					//This file should be included before below statement: #include<gl/gl.h>

#include<gl\GL.h>

#include "vmath.h"
//#include<gl/glu.h> //In Programmable pipeline, none of the functions under GLU work!
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"glew32.lib") //The GLEW library.
#pragma comment(lib,"opengl32.lib")
//#pragma comment(lib,"glu32.lib")

#define WIN_WIDTH 1024
#define WIN_HEIGHT 768

//***********************************************Global Veriables/Declarations************************************************************/

using namespace vmath;

enum
{
	VDG_ATTRIBUTE_VERTEX = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

bool gbActiveWindow=false;
bool gbEscapeKeyIsPressed=false;
bool gbFullscreen=false;


DWORD dwStyle;
WINDOWPLACEMENT wpPrev= {sizeof(WINDOWPLACEMENT)};
HWND ghwnd;
HDC ghdc;
HGLRC ghrc; 
FILE *gpFile=NULL;


//Prototype of WndProc()
LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

//Shader and Shader Program Objects
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVbo_Shape_Position;
GLuint gVbo_Shape_Color;

GLuint gVao_Shape_1_Points;
GLuint gVao_Shape_2;
GLuint gVao_Shape_3;
GLuint gVao_Shape_4;
GLuint gVao_Shape_5;
GLuint gVao_Shape_6_Quad_1;
GLuint gVao_Shape_6_Quad_2;
GLuint gVao_Shape_6_Quad_3;
GLuint gVao_Shape_6_Strip;


GLuint gMVPUniform;

mat4 gPerspectiveProjectionMatrix;

//**************************************************** WinMain() ***********************************************************************/
int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR nCmdLine,int nCmdShow)
{
	//function prototypes
	void initialize(void);
	void display(void);
	void uninitialize(void);

	//Variables
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	bool bDone=false;
	TCHAR szAppName[]=TEXT("PP Perspective!");

	if(fopen_s(&gpFile,"PP_All_Shapes_Log.txt","w")!=0)
	{
		MessageBox(NULL,TEXT("Log file could not be created..exiting!!"),TEXT("Error"),MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile,"Log File Created Successfully!!\n");
	}

	//code
	wndClass.cbSize=sizeof(WNDCLASSEX);
	wndClass.style=CS_HREDRAW | CS_VREDRAW | CS_OWNDC; //CS_OWNDC-->Non-purgable DC
	wndClass.cbClsExtra=0;
	wndClass.cbWndExtra=0;

	wndClass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hInstance=hInstance;
	wndClass.hIcon=LoadIcon(NULL,IDI_APPLICATION); //load the default icon
	wndClass.hIconSm=LoadIcon(NULL,IDI_APPLICATION); //Icon for "System menu"
	wndClass.hCursor=LoadCursor(NULL,IDC_ARROW);	 //Load default cursor, you can specific the cursor type while creating a game application (Confirm?)

	wndClass.lpfnWndProc=WndProc;
	wndClass.lpszClassName=szAppName;
	wndClass.lpszMenuName=NULL;

	//Register the created class
	RegisterClassEx(&wndClass);

	//create the window
	hwnd=CreateWindowEx(
						 WS_EX_APPWINDOW, //exclusive full-screen window
						 szAppName,
						 TEXT("PP::All Shapes!!!"),
						 WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS|WS_VISIBLE,
						 0,
						 0,
						 WIN_WIDTH,
						 WIN_HEIGHT,
						 NULL,		
						 NULL,
						 hInstance,
						 NULL
						);

	ghwnd=hwnd;	//For further use of the global window handle

	ShowWindow(hwnd,nCmdShow);//Show the created window
	SetForegroundWindow(hwnd);//Bring that window in foreground
	SetFocus(hwnd);//Set the focus on the window

	initialize(); //Call to initialize(Do the preparation of the "Picture element" (Pixel)

	//Game loop
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}//Events further generating the messages
		else
		{
			if(gbActiveWindow==true)
			{
				if(gbEscapeKeyIsPressed==true)
				{
					bDone=true;
				}
				display();
			}
			
		}//game will run here

	}//end of while
	uninitialize();
	return msg.wParam; 
}//WinMain

 //*********************************************************initialize() ******************************************************************/

/*Initialize:
  1. PIXELFORMATDESCRIPTOR inialize
  2. ChoosePixelFormat
  3. SetPixelFormat
  4. Get the device context from OS
  5. Create OpenGL Context:wglCreateContext()
  6. HandOver control from OS DC to Rendering DC: wglMakeCurrent()
  7. Clear the window with desired color
  8. Give a warmup call to resize()

*/
void initialize(void)
{
	//function prototypes
	void resize(int,int);
	void uninitialize(void);

	//Variables
	PIXELFORMATDESCRIPTOR pfd;	//a structure which we need to fill will possible set of values from our side and rest can be achieved from OS
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));//Make the 26-membered structure to 0

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags= PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; //dword flags depicting pixel characteristics
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cGreenBits=8;
	pfd.cBlueBits=8;
	pfd.cAlphaBits=8;
	pfd.cDepthBits = 32;

	ghdc=GetDC(ghwnd);//Get the regular device context from OS (this is non-purgeable)

	iPixelFormatIndex=ChoosePixelFormat(ghdc,&pfd);//After getting the DC, "Choose the pixel format" which is the closest match to the values which we have provides

	if(iPixelFormatIndex==0)//if the pixelformatindex is empty means function did not succeed!!
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd)==false) //Set the chosen matching closest pixel format
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	ghrc=wglCreateContext(ghdc);//Now handover the device context to OpenGL's "Rendering context" through WGL

	if(ghrc==NULL)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	if(wglMakeCurrent(ghdc,ghrc)==NULL)//Once the handover is successful-->Make "Rendering context" as the current context through WGL
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	/////////////////// The Place for glew //////////////////////////
	// (IMPORTANT: This piece should be here means, after creating the OpenGL context but before using any OpenGL Function).
	
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	fprintf(gpFile, "%s \n", glGetString(GL_VERSION)); //Get the OpenGL Version supported!

	fprintf(gpFile, "%s \n", glGetString(GL_SHADING_LANGUAGE_VERSION)); //Get the GLSL Version supported on this configuration!

	////////////////// VERTEX SHADER /////////////////////////////
	//Create Shader
	gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER); //glCreateShader()-->Specialist who writes the shader.
	
	//To Provide source code to shader, write it![Here you can specify the GLSL version supported by your graphics hardware and OS (Ex: "#version 440 core")]
	const GLchar *vertexShaderSourceCode = "#version 130" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 out_color;" \
		"void main(void)"\
		"{"\
		"gl_Position=u_mvp_matrix * vPosition;" \
		"out_color=vColor;" \
		"}";
	glShaderSource(gVertexShaderObject,1, (const GLchar **)&vertexShaderSourceCode,NULL);
	
	//Compile the shader
	glCompileShader(gVertexShaderObject);

	GLint iShaderCompiledStatus = 0;
	GLint iInfoLogLength = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
	if (iShaderCompiledStatus==GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Vertex Shader Compilation Log: %s\n",szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			} //szInfoLog!=NULL

		}//iInfoLogLength>0

	}//iShaderCompiledStatus==GL_FALSE
	
	////////////////// FRAGMENT SHADER///////////////////////
	//Create shader
	gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
	
	//Provide source code to Shader[Here you can specify the GLSL version supported by your graphics hardware and OS (Ex: "#version 440 core")]
	const GLchar *fragmentShaderSourceCode = " #version 130"
		"\n"\
		"in vec4 out_color;" \
		"out vec4 FragColor;"\
		"void main(void)"\
		"{"\
			"FragColor=out_color;"\
		"}";
	
	glShaderSource(gFragmentShaderObject,1,(const GLchar **)&fragmentShaderSourceCode,NULL);
																			   
	//Compile the shader
	glCompileShader(gFragmentShaderObject);																			   
	glGetShaderiv(gFragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog =(char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Fragment Shader Log: %s\n",szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			} //szInfoLog!=NULL

		}//iInfoLogLength>0

	}//iShaderCompiledStatus==GL_FALSE

	//////////////////SHADER PROGRAM/////////////////////////
	//Create the Shader Program Object
	gShaderProgramObject=glCreateProgram();
	
	//Attach Vertex Shader to Shader Program
	glAttachShader(gShaderProgramObject,gVertexShaderObject);
	
	//Attach fragment shader to Shader Program
	glAttachShader(gShaderProgramObject,gFragmentShaderObject);
	
	//Pre-link binding of "shader program object" with vertex shader position attribute
	glBindAttribLocation(gShaderProgramObject,VDG_ATTRIBUTE_VERTEX, "vPosition"); //Tikadam-2

	//Link shader
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;

	glGetProgramiv(gShaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);

	if (iShaderProgramLinkStatus==GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{

				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			} //szInfoLog!=NULL

		}//iInfoLogLength>0

	}//iShaderProgramLinkStatus==GL_FALSE

	//get MVP uniform location
	gMVPUniform = glGetUniformLocation(gShaderProgramObject,"u_mvp_matrix");

	////////////////////// Vertices, colors, shader attribs, vbo, vao initializations//////////////
	
	//SHAPE_1:
	const GLfloat shape_1_Points[]=
	{
		//Row-1
		-0.85f,0.75f,0.0f, //1st Row-1St Point
		-0.75f,0.75f,0.0f,
		-0.65f,0.75f,0.0f,
		-0.55f,0.75f,0.0f,
		
		//Row-2
		-0.85f,0.60f,0.0f,
		-0.75f,0.60f,0.0f,
		-0.65f,0.60f,0.0f,
		-0.55f,0.60f,0.0f,
		
		//Row-3
		-0.85f,0.45f,0.0f,
		-0.75f,0.45f,0.0f,
		-0.65f,0.45f,0.0f,
		-0.55f,0.45f,0.0f,
		
		//Row-4
		-0.85f,0.30f,0.0f,
		-0.75f,0.30f,0.0f,
		-0.65f,0.30f,0.0f,
		-0.55f,0.30f,0.0f
		
	};
	const GLfloat shape_1_Points_Color[] =
	{
		//Row-1
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		//Row-2
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		//Row-3
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		//Row-4
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
	};

    //Define Vao for shape-1
	glGenVertexArrays(1,&gVao_Shape_1_Points);
	glBindVertexArray(gVao_Shape_1_Points);
	
	//Define gVbo_Shape_1_Points
	//===========================
	glGenBuffers(1,&gVbo_Shape_Position);
	glBindBuffer(GL_ARRAY_BUFFER,gVbo_Shape_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(shape_1_Points), shape_1_Points, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL); //Tikadam-1

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX); //Tikadam-3

	glBindBuffer(GL_ARRAY_BUFFER,0);

	//Define gVbo_Shape_1_Points_Color
	//================================
	glGenBuffers(1, &gVbo_Shape_Color);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Shape_Color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(shape_1_Points_Color), shape_1_Points_Color, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL); //Tikadam-1

	glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR); //Tikadam-3

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//**********************************************//
	//SHAPE_2:
	const GLfloat shape_2_Position[] =
	{
		-0.15f,0.75f,0.0f,
		-0.15f,0.30f,0.0f,
		0.15f,0.75f,0.0f,
		-0.15f,0.75f,0.0f,
		
		-0.15f,0.60f,0.0f,
		-0.05f,0.75f,0.0f, //inner-1
	
		-0.05f,0.60f,0.0f,
		-0.15f,0.60f,0.0f,//inner-2
		-0.15f,0.45f,0.0f,
		-0.05f,0.60f,0.0f,//inner-3
		 0.05f,0.75f,0.0f,//inner-4
		 0.05f,0.60f,0.0f,
		-0.05f,0.60f,0.0f,//inner-5
		-0.05f,0.45f,0.0f,
		-0.15f,0.45f,0.0f,//inner-6

		-0.05f,0.45f,0.0f,
		-0.05f,0.30f,0.0f,//inner-7
		0.05f,0.45f,0.0f,
		0.15f,0.60f,0.0f,
		
		0.05f,0.60f,0.0f,
		0.05f,0.45f,0.0f,//outer-1
		
		-0.05f,0.45f,0.0f,//outer-2

		0.05f,0.45f,0.0f,
		0.05f,0.30f,0.0f,
		0.15f,0.45f,0.0f,
		0.05f,0.45f,0.0f//outer-3
	
	};

	//SHAPE-2: [COLOR]
	const GLfloat shape_2_color[]=
	{
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		
		1.0f, 1.0f, 1.0f,
		
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f
	};
	
	//Define Vao for shape-2
	glGenVertexArrays(1, &gVao_Shape_2);
	glBindVertexArray(gVao_Shape_2);

	//Define gVbo_Shape_2_Position
	//============================
	glGenBuffers(1, &gVbo_Shape_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Shape_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(shape_2_Position), shape_2_Position, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL); //Tikadam-1

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX); //Tikadam-3

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Define gVbo_Shape_2_Color
	//============================
	glGenBuffers(1, &gVbo_Shape_Color);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Shape_Color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(shape_2_color), shape_2_color, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);

	glBindBuffer(GL_ARRAY_BUFFER,0);

	glBindVertexArray(0);
	//**********************************************//

	//SHAPE_3:[POSITION]
	const GLfloat shape_3_position[]=
	{
		0.55f,0.75f,0.0f,
		0.65f,0.75f,0.0f,
		0.75f,0.75f,0.0f,
		0.85f,0.75f,0.0f,//Top-line

		0.85f,0.60f,0.0f,
		0.85f,0.45f,0.0f,
		0.85f,0.30f,0.0f,//Rightmost-line
		0.75f,0.30f,0.0f,
		0.65f,0.30f,0.0f,
		0.55f,0.30f,0.0f,//Bottommost-line
		0.55f,0.45f,0.0f,
		0.55f,0.60f,0.0f,
		0.55f,0.75f,0.0f,//Leftmost-line

		0.55f,0.60f,0.0f,
		0.65f,0.60f,0.0f,
		0.75f,0.60f,0.0f,
		0.85f,0.60f,0.0f,//Inner-Horizontal-line-1

		0.85f,0.45f,0.0f,
		0.75f,0.45f,0.0f,
		0.65f,0.45f,0.0f,
		0.55f,0.45f,0.0f,//Inner-Horizontal-line-2 

		0.55f,0.30f,0.0f,
		0.65f,0.30f,0.0f,
		0.65f,0.45f,0.0f,
		0.65f,0.60f,0.0f,
		0.65f,0.75f,0.0f,//left-Vertical-line-1

		0.75f,0.75f,0.0f,
		0.75f,0.60f,0.0f,
		0.75f,0.45f,0.0f,
		0.75f,0.30f,0.0f//Vertical-line-2

	};

	//SHAPE_3:[COLOR]
	const GLfloat shape_3_color[]=
	{
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f
	};

	//Define Vao for shape-3
	glGenVertexArrays(1, &gVao_Shape_3);
	glBindVertexArray(gVao_Shape_3);

	//Define gVbo_Shape_3_Position
	//============================
	glGenBuffers(1, &gVbo_Shape_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Shape_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(shape_3_position), shape_3_position, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL); //Tikadam-1

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX); //Tikadam-3

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Define gVbo_Shape_3_Color
	//============================
	glGenBuffers(1, &gVbo_Shape_Color);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Shape_Color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(shape_3_color), shape_3_color, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	//**********************************************//

	//SHAPE_4:[POSITION]

	const GLfloat shape_4_position[]=
	{
		-0.85f,-0.15f,0.0f,
		-0.85f,-0.60f,0.0f,
		-0.55f,-0.15f,0.0f,
		-0.85f,-0.15f,0.0f,


		-0.85f,-0.30f,0.0f,
		-0.75f,-0.15f,0.0f, //inner-1
		-0.75f,-0.30f,0.0f,
		-0.85f,-0.30f,0.0f,//inner-2
		-0.85f,-0.45f,0.0f,
		-0.75f,-0.30f,0.0f,//inner-3
		-0.65f,-0.15f,0.0f,//inner-4
		-0.65f,-0.30f,0.0f,
		-0.75f,-0.30f,0.0f,//inner-5
		-0.75f,-0.45f,0.0f,
		-0.85f,-0.45f,0.0f,//inner-6

		-0.75f,-0.45f,0.0f,
		-0.75f,-0.60f,0.0f,//inner-7
		-0.65f,-0.45f,0.0f,
		-0.55f,-0.30f,0.0f,
		-0.65f,-0.30f,0.0f,
		-0.65f,-0.45f,0.0f,//outer-1
		-0.75f,-0.45f,0.0f,//outer-2
		
		-0.65f,-0.45f,0.0f,
		-0.65f,-0.60f,0.0f,
		-0.55f,-0.45f,0.0f,
		-0.65f,-0.45f,0.0f,//outer-3	

		-0.75f,-0.45f,0.0f,
		-0.85f,-0.60f,0.0f,
		-0.75f,-0.60f,0.0f,
		-0.65f,-0.60f,0.0f,
		-0.55f,-0.60f,0.0f,//Bottom-most line

		-0.55f,-0.45f,0.0f,
		-0.55f,-0.30f,0.0f,
		-0.55f,-0.15f,0.0f//Right-most-line
	};

	//SHAPE_4:[COLOR]

	const GLfloat shape_4_color[]=
	{
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f

	};

	//Define Vao for shape-4
	glGenVertexArrays(1, &gVao_Shape_4);
	glBindVertexArray(gVao_Shape_4);

	//Define gVbo_Shape_4_Position
	//============================
	glGenBuffers(1, &gVbo_Shape_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Shape_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(shape_4_position), shape_4_position, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL); //Tikadam-1

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX); //Tikadam-3

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Define gVbo_Shape_4_Color
	//============================
	glGenBuffers(1, &gVbo_Shape_Color);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Shape_Color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(shape_4_color), shape_4_color, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	//**********************************************//
	
	//SHAPE_5:Square with lines:[POSITION]
	const GLfloat shape_5_position[]=
	{
		-0.15f,-0.15f,0.0f,
		-0.15f,-0.30f,0.0f,
		-0.15f,-0.45f,0.0f,
		-0.15f,-0.60f,0.0f, //Left-most line

	    -0.05f,-0.60f,0.0f,
		 0.05f,-0.60f,0.0f,
		 0.15f,-0.60f,0.0f, //Bottom-most line

		 0.15f,-0.45f,0.0f,
		 0.15f,-0.30f,0.0f,
		 0.15f,-0.15f,0.0f,//Right-most line

		 0.05f,-0.15f,0.0f,
		-0.05f,-0.15f,0.0f,
		-0.15f,-0.15f,0.0f,//Top-most line
		 0.15f,-0.30f,0.0f,//line-1
		 0.15f,-0.45f,0.0f,
		-0.15f,-0.15f,0.0f,//line-2
		 0.15f,-0.60f,0.0f,//line-3
		 0.05f,-0.60f,0.0f,
		-0.15f,-0.15f,0.0f,//line-4
		-0.05f,-0.60f,0.0f//line-5
	};


	//SHAPE_5:Square with lines:[COLOR]
	const GLfloat shape_5_color[]=
	{
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
	};

	//Define Vao for shape-5
	glGenVertexArrays(1, &gVao_Shape_5);
	glBindVertexArray(gVao_Shape_5);

	//Define gVbo_Shape_5_Position
	//============================
	glGenBuffers(1, &gVbo_Shape_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Shape_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(shape_5_position), shape_5_position, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL); //Tikadam-1

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX); //Tikadam-3

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Define gVbo_Shape_5_Color
	//============================
	glGenBuffers(1, &gVbo_Shape_Color);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Shape_Color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(shape_5_color), shape_5_color, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	//**********************************************//
	//SHAPE_6:Colored Quads!
	//**********************************************//

	//Quad_1****************************************//
	const GLfloat shape_6_quad_1_position[]=
	{
		0.55f, -0.60f, 0.0f,
		0.55f, -0.15f, 0.0f,
		0.65f, -0.15f, 0.0f,
		0.65f, -0.60f, 0.0f
	};

	const GLfloat shape_6_quad_1_color[] =
	{
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f

	};

	//Define Vao for shape-6_quad_1
	glGenVertexArrays(1, &gVao_Shape_6_Quad_1);
	glBindVertexArray(gVao_Shape_6_Quad_1);

	//Define gVbo_Shape_6_Position
	//============================
	glGenBuffers(1, &gVbo_Shape_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Shape_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(shape_6_quad_1_position), shape_6_quad_1_position, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL); //Tikadam-1

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX); //Tikadam-3

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Define gVbo_Shape_6_Color
	//============================
	glGenBuffers(1, &gVbo_Shape_Color);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Shape_Color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(shape_6_quad_1_color), shape_6_quad_1_color, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	//**********************************************//

	//Quad_2****************************************//
	const GLfloat shape_6_quad_2_position[]=
	{
		0.65f, -0.60f, 0.0f,
		0.65f, -0.15f, 0.0f,
		0.75f, -0.15f, 0.0f,
		0.75f, -0.60f, 0.0f
	};

	const GLfloat shape_6_quad_2_color[] =
	{
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f
	};

	//Define gVbo_Shape_6_Quad_2
	//============================
	glGenVertexArrays(1, &gVao_Shape_6_Quad_2);
	glBindVertexArray(gVao_Shape_6_Quad_2);

	glGenBuffers(1, &gVbo_Shape_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Shape_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(shape_6_quad_2_position), shape_6_quad_2_position, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL); //Tikadam-1

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX); //Tikadam-3

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Define gVbo_Shape_6_Color
	//============================
	glGenBuffers(1, &gVbo_Shape_Color);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Shape_Color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(shape_6_quad_2_color), shape_6_quad_2_color, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	//**********************************************//

	//Quad_3****************************************//
	const GLfloat shape_6_quad_3_position[] =
	{
		0.75f, -0.60f, 0.0f,
		0.75f, -0.15f, 0.0f,
		0.85f, -0.15f, 0.0f,
		0.85f, -0.60f, 0.0f

	};

	const GLfloat shape_6_quad_3_color[] =
	{
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 
		0.0f, 0.0f, 1.0f

	};

	//Define gVbo_Shape_6_Quad_3
	//============================
	glGenVertexArrays(1, &gVao_Shape_6_Quad_3);
	glBindVertexArray(gVao_Shape_6_Quad_3);

	glGenBuffers(1, &gVbo_Shape_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Shape_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(shape_6_quad_3_position), shape_6_quad_3_position, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL); //Tikadam-1

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX); //Tikadam-3

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Define gVbo_Shape_6_Color
	//============================
	glGenBuffers(1, &gVbo_Shape_Color);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Shape_Color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(shape_6_quad_3_color), shape_6_quad_3_color, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	//**********************************************//

	//SHAPE_6:Colored Quad STRIP:[POSITION]
	const GLfloat shape_6_strip_position[]=
	{
		0.55f,-0.15f,0.0f,
		0.65f,-0.15f,0.0f,
		0.75f,-0.15f,0.0f,
		0.85f,-0.15f,0.0f,//Top-line
		0.85f,-0.30f,0.0f,
		0.85f,-0.45f,0.0f,
		0.85f,-0.60f,0.0f,//Rightmost-line
		0.75f,-0.60f,0.0f,
		0.65f,-0.60f,0.0f,
		0.55f,-0.60f,0.0f,//Bottommost-line
		0.55f,-0.45f,0.0f,
		0.55f,-0.30f,0.0f,
		0.55f,-0.15f,0.0f,//Leftmost-line

		0.55f,-0.30f,0.0f,
		0.65f,-0.30f,0.0f,
		0.75f,-0.30f,0.0f,
		0.85f,-0.30f,0.0f,//Inner-Horizontal-line-1

		0.85f,-0.45f,0.0f,
		0.75f,-0.45f,0.0f,
		0.65f,-0.45f,0.0f,
		0.55f,-0.45f,0.0f,//Inner-Horizontal-line-2 
		
		0.55f,-0.60f,0.0f,
		0.65f,-0.60f,0.0f,
		0.65f,-0.45f,0.0f,
		0.65f,-0.30f,0.0f,
		0.65f,-0.15f,0.0f,//left-Vertical-line-1


		0.75f,-0.15f,0.0f,
		0.75f,-0.30f,0.0f,
		0.75f,-0.45f,0.0f,
		0.75f,-0.60f,0.0f//Vertical-line-2
	};



	//SHAPE_6:Colored Quad STRIP:[COLOR]
	const GLfloat shape_6_strip_color[]=
	{
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f


	};
	//Define Vao for shape-6
	glGenVertexArrays(1, &gVao_Shape_6_Strip);
	glBindVertexArray(gVao_Shape_6_Strip);

	//Define gVbo_Shape_6_Position
	//============================
	glGenBuffers(1, &gVbo_Shape_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Shape_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(shape_6_strip_position), shape_6_strip_position, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL); //Tikadam-1

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX); //Tikadam-3

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Define gVbo_Shape_6_Color
	//============================
	glGenBuffers(1, &gVbo_Shape_Color);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Shape_Color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(shape_6_strip_color), shape_6_strip_color, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	//**********************************************//

	//***************Define the depth initialization***************//
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //Now clear the window with desired color

	//Set perspectiveProjectionMatrix to Identity Matrix.
	gPerspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH,WIN_HEIGHT); //a warmup call to resize function--In the case of "Projection" this call is of crucial importance as this call will help keep the viewing volume ready before drawing happens.
								  //Otherwise, in this case, application is launching in windowed mode with some weird output(as viewing volume will not be ready if this call is commentedout) and after pressing "F"(for fullscreen) desired output comes.	
								  // This warm up call to resize() goig to be crucial in case of DirectX
}//end of initialize()

//**********************************************Resize function**********************************************//
// Projection functions are conventionally called inside the function which deals with the change in the size of the window
void resize(int width,int height)
{
	if(height==0)
		height=1;
	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	//gluPerspective(fovy,Aspect Ration,near,far);
	//gPerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
	
}//end of resize

//********************************************** WndProc **********************************************//
 //CALLBACK Function
LRESULT CALLBACK WndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam)
{
	//function declarations
	void resize(int,int);
	void uninitialize(void);
	void ToggleFullscreen(void);

	//Code
	switch(message)
	{

	case WM_CREATE:
					break;

	case WM_ACTIVATE:
					 if(HIWORD(wParam)==0)
						 gbActiveWindow=true;
					 else
						 gbActiveWindow=false;
					break;

	case WM_KEYDOWN:
					switch(wParam)
					{
					case VK_ESCAPE:
									gbEscapeKeyIsPressed=true;
									break;

						case 0x46: //for fullscreen
									if(gbFullscreen==false)
									{
										ToggleFullscreen();
										gbFullscreen=true;
									}
									else
									{
										ToggleFullscreen();
										gbFullscreen=false;
									}
									break;
						case 0x51: //Q
									uninitialize();
									PostQuitMessage(0);
									break;
						
					}
					break;

	case WM_SIZE:
					resize(LOWORD(lParam),HIWORD(lParam));
					break;

	case WM_DESTROY:
					PostQuitMessage(0);
					break;

	}
	return(DefWindowProc(hwnd,message,wParam,lParam));

}//End of call back function

//********************************* ToggleFullscreen **************************************//
/*ToggleFullscreen function
	1. Get the window style using GetWindowLong()
	2. If the "Current window" type contains the "WS_OVERLAPPEDWINDOW" -->Remove it.
	3. Store the current window position
	4. Get the monitor info(Primary display)
	5. Set the window position
*/
void ToggleFullscreen(void)
{
	//variable declaration
	MONITORINFO mi={sizeof(MONITORINFO)};
	bool IsWindowPlacement;
	bool IsMonitorInfo;
	HMONITOR hMonitor;

	//code
	if(gbFullscreen==false) //windowed to full-screen
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE); //Get the window style/entire information of the window
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			//a) Get the window Placement and Monitor information then and then toggle to fullscreen
			IsWindowPlacement=GetWindowPlacement(ghwnd,&wpPrev);

			//b) Now get the monitor first 
			hMonitor=MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY); //the primary monitor in case of extended view

			//c) Get and store the monitor information from the monitor which is been just received
			IsMonitorInfo=GetMonitorInfo(hMonitor,&mi);

			//
			if(IsWindowPlacement && IsMonitorInfo)//both information have been received, now set the window to the top of the screen
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~ WS_OVERLAPPEDWINDOW);

				//Set the window position
				
				SetWindowPos(
							  ghwnd,
							  HWND_TOP,
							  mi.rcMonitor.left,
							  mi.rcMonitor.top,
							  mi.rcMonitor.right-mi.rcMonitor.left, //width
							  mi.rcMonitor.bottom-mi.rcMonitor.top, //height
							  SWP_NOZORDER | SWP_FRAMECHANGED       //ignore any of the Z order and trigger frame change event
							);

			}
			ShowCursor(FALSE);
		}
	}
	else //full-screen to windowed mode
	{
		// Add the removed style to the window style
		// Set the window placement/position to back to the saved position
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd,&wpPrev);

		SetWindowPos(
						ghwnd,
						HWND_TOP,
						0,0,0,0,
						SWP_NOMOVE|
						SWP_NOSIZE|
						SWP_NOOWNERZORDER|
						SWP_NOZORDER|
						SWP_FRAMECHANGED
					);
		ShowCursor(TRUE);
	}
	
}//end of ToggleFullscreen()

//***************************************Display function***************************************//
void display(void)
{
	//Variables
	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	
	//Start using OpenGL Program Object
	glUseProgram(gShaderProgramObject);

	//OpenGL Drawing
	//== == == == == == == == == == == == == == == == == == == == == =//
	//Shape 1: Points!
	//== == == == == == == == == == == == == == == == == == == == == =//
	//set modelview & modelviewprojection matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	//Need to translate the Shape in order to see it..
	//modelViewMatrix = vmath::translate(-2.0f, 1.0f, -5.0f);

	//Multiply the modelview and perspective projection matrix to get modelviewprojection matrix!
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix; //ORDER IS IMPORTANT (Rigthmost to Leftmost asa sequence aahe)

	//Pass above modelViewProjectionMatrix to the "Vertex Shader" in "u_mvp_matrix" shader variable whose position value we already calculated
	// in "initWithFrame()" by using "glGetUniformLocation()"

	glUniformMatrix4fv(gMVPUniform,1,GL_FALSE,modelViewProjectionMatrix); //glUniformMatrix4fv(initialize madhe tayar karun thewlela,count,transpose karu ki nako, modelview projection matrix)

	// ****** Bind Vao For Position:1 ***********//
	glBindVertexArray(gVao_Shape_1_Points);
	glPointSize(5.0f);
	glDrawArrays(GL_POINTS, 0, 16);
	glBindVertexArray(0);					 
	//******* Unbind vao **********//	

	//== == == == == == == == == == == == == == == == == == == == == =//
	//Shape 2: LINE_STRIP!
	//== == == == == == == == == == == == == == == == == == == == == =//
	//set modelview & modelviewprojection matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	//Need to translate the Shape in order to see it..
	//modelViewMatrix = vmath::translate(-1.0f, 1.0f, -5.0f);

	//Multiply the modelview and perspective projection matrix to get modelviewprojection matrix!
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix; //ORDER IS IMPORTANT (Rigthmost to Leftmost asa sequence aahe)

																				//Pass above modelViewProjectionMatrix to the "Vertex Shader" in "u_mvp_matrix" shader variable whose position value we already calculated
																				// in "initWithFrame()" by using "glGetUniformLocation()"

	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix); //glUniformMatrix4fv(initialize madhe tayar karun thewlela,count,transpose karu ki nako, modelview projection matrix)


	// ****** Bind Vao For Shape:2 ***********//
	glBindVertexArray(gVao_Shape_2);
		glDrawArrays(GL_LINE_STRIP, 0, 26);
	glBindVertexArray(0);
	//******* Unbind vao **********//
	
	//== == == == == == == == == == == == == == == == == == == == == =//
	//Shape 3: QUAD_STRIP!
	//== == == == == == == == == == == == == == == == == == == == == =//

	// ****** Bind Vao For Shape:3 ***********//
	glBindVertexArray(gVao_Shape_3);
		glDrawArrays(GL_LINE_STRIP, 0, 30);
	glBindVertexArray(0);
	//******* Unbind vao **********//

	//== == == == == == == == == == == == == == == == == == == == == =//
	//Shape 4: QUAD + LINE_STRIP!!
	//== == == == == == == == == == == == == == == == == == == == == =//
	
	// ****** Bind Vao For Shape:4 ***********/
	glBindVertexArray(gVao_Shape_4);
		glDrawArrays(GL_LINE_STRIP, 0, 34);
	glBindVertexArray(0);
	//******* Unbind vao **********//

	//== == == == == == == == == == == == == == == == == == == == == =//
	//Shape 5: Square With Lines!!
	//== == == == == == == == == == == == == == == == == == == == == =//

	// ****** Bind Vao For Shape:5 ***********/
	glBindVertexArray(gVao_Shape_5);
		glDrawArrays(GL_LINE_STRIP, 0, 20);
	glBindVertexArray(0);
	//******* Unbind vao **********//

	//== == == == == == == == == == == == == == == == == == == == == =//
	//Shape 6: Colored Quads!!
	//== == == == == == == == == == == == == == == == == == == == == =//

	// ****** Bind Vao For Shape:6 ***********/
	glBindVertexArray(gVao_Shape_6_Quad_1);
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);
	//******* Unbind vao **********//

	glBindVertexArray(gVao_Shape_6_Quad_2);
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

	glBindVertexArray(gVao_Shape_6_Quad_3);
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

	//== == == == == == == == == == == == == == == == == == == == == =//
	//Shape 6: Colored Quad Strip!!
	//== == == == == == == == == == == == == == == == == == == == == =//

	// ****** Bind Vao For Shape:6 ***********/
	glBindVertexArray(gVao_Shape_6_Strip);
		glDrawArrays(GL_LINE_STRIP, 0, 30);
	glBindVertexArray(0);
	//******* Unbind vao **********//

	
	//Stop using OpenGL Program Object=================//
	glUseProgram(0);
	
	SwapBuffers(ghdc);
	
}//end of display function

//***************************************uninitialize()***************************************//
void uninitialize(void)
{	
	//if window is in fullscreen mode-->Switch back to windowed mode and then exit
	if(gbFullscreen==true)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);

		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd,&wpPrev);

		SetWindowPos(
					  ghwnd,
					  HWND_TOP,
					  0,0,0,0,
					  SWP_NOMOVE|
					  SWP_NOSIZE|
					  SWP_NOOWNERZORDER|
					  SWP_NOZORDER|
					  SWP_FRAMECHANGED
					);
		ShowCursor(TRUE);
	}

	// destroy Vao
	if (gVao_Shape_1_Points)
	{
		glDeleteVertexArrays(1,&gVao_Shape_1_Points);
		gVao_Shape_1_Points = 0;
	}

	//******** Shape-2 ********//
	if (gVao_Shape_2)
	{
		glDeleteBuffers(1,&gVao_Shape_2);
		gVao_Shape_2 = 0;

	}
	//******** Shape-3 ********//
	if (gVao_Shape_3)
	{
		glDeleteBuffers(1, &gVao_Shape_3);
		gVao_Shape_3 = 0;

	}
	//******** Shape-4 ********//
	if (gVao_Shape_4)
	{
		glDeleteBuffers(1, &gVao_Shape_4);
		gVao_Shape_4 = 0;

	}
	//******** Shape-5 ********//
	if (gVao_Shape_5)
	{
		glDeleteBuffers(1, &gVao_Shape_5);
		gVao_Shape_5 = 0;

	}
	//******** Shape-6_Strip ********//
	if (gVao_Shape_6_Strip)
	{
		glDeleteBuffers(1, &gVao_Shape_6_Strip);
		gVao_Shape_6_Strip = 0;

	}
	//******** Shape-6_Quads ********//
	if (gVao_Shape_6_Quad_1)
	{
		glDeleteBuffers(1, &gVao_Shape_6_Quad_1);
		gVao_Shape_6_Quad_1 = 0;

	}
	if (gVao_Shape_6_Quad_2)
	{
		glDeleteBuffers(1, &gVao_Shape_6_Quad_2);
		gVao_Shape_6_Quad_2 = 0;

	}
	if (gVao_Shape_6_Quad_3)
	{
		glDeleteBuffers(1, &gVao_Shape_6_Quad_3);
		gVao_Shape_6_Quad_3 = 0;

	}
	if (gVbo_Shape_Position)
	{
		glDeleteBuffers(1, &gVbo_Shape_Position);
		gVbo_Shape_Position = 0;
	}
	if (gVbo_Shape_Color)
	{
		glDeleteBuffers(1, &gVbo_Shape_Color);
		gVbo_Shape_Color = 0;
	}

	//detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject,gVertexShaderObject);
	
	//detach Fragment shader object from shader program object
	glDetachShader(gShaderProgramObject,gFragmentShaderObject);	
	
	//Delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject=0;
	
	//Delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject=0;
	
	//Now delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject=0;
	
	//unlink shader program
	glUseProgram(0);

	//Deselect the rendering context
	wglMakeCurrent(NULL,NULL);

	//Delete the rendering context
	wglDeleteContext(ghrc);
	ghrc=NULL;

	//Delete the device context
	ReleaseDC(ghwnd,ghdc);
	ghdc=NULL;

	if(gpFile)
	{
		fprintf(gpFile,"Log File Closed Successfully!!");
		fclose(gpFile);
		gpFile=NULL;
	}
	DestroyWindow(ghwnd);
}//end of uninitialize()
 //***************************************End Of Program!***************************************//