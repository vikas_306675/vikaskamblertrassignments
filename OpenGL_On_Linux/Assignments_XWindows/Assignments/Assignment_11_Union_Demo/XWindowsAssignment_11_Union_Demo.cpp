
#include<stdio.h>

int main(void)
{

	//variables

	char input_character;
	int int_choice;
	float float_choice;	

	union myUnion{
			int i;
			float f;
		
		     }u;

	printf("\n//////// Defined below Union////////////////\n");

	printf("\nunion myUnion{\n\tint i;\n\tfloat f;\n\t}u;");

	printf("\n\nEnter:\ni:for integer data\nf:for float data\n");
	scanf("%c",&input_character);

	switch(input_character)
	{
	
		case 'i':
			 printf("Enter integer value of your choice:\n");
			 scanf("%d",&int_choice);
			 u.i=int_choice;

			 printf("Entered Integer value(u.i):%d\n",u.i);
			 break;
		case 'f':
			printf("Enter float value of your choice:\n");
			scanf("%f",&float_choice);
			u.f=float_choice;
		
			printf("Entered Float value(u.f)=%f\n",u.f);
			break;

		default:
			printf("\nYou have entered some thing else..exiting!!\n");
			break;
	}//switch(input_character)	

	return(0);

}//main()
