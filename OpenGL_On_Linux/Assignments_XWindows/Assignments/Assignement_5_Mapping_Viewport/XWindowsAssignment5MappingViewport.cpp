/************************************************************************

Program: XWindows Single Buffered Mapping Viewports on KeyPress!

*************************************************************************/
#include<iostream> //Namespace [It is always global alike header file, but header file cannnot be extended but namespaces can be!]
#include<stdio.h>  //Console input-output
#include<stdlib.h> //For exit() function.
#include<memory.h> //For memset() function.

#include<X11/Xlib.h>   //Just like "windows.h" in windows.
#include<X11/Xutil.h> //Needed for "XVisualInfo" structure and APIs related to it.
#include<X11/XKBlib.h> //For Keyboard (We started to use this after UNICODE came into picture).
#include<X11/keysym.h> //Equivalent to "Virtual keycodes" in windows.

#include<GL/gl.h>
#include<GL/glx.h> //glx is equivalent to "wgl" in windows (That is nothing but briding API for Xwindows)
#include<GL/glu.h> //for gluPerspective()

//namespaces
//==========
using namespace std; //Will be needed while using functions from the namespaces Ex.If function from this <iostream> namespace used, need it. 


//Global variable declarations:
//==============================

bool gbFullscreen=false;

Display *gpDisplay=NULL;  //"Display" structure has around 77 members.       //DirectX Coding Style.

XVisualInfo *gpXVisualInfo=NULL;

Window gWindow; //Structs

Colormap gColormap;

int giWindowWidth=800;

int giWindowHeight=600;

FILE *gpFile=NULL;

GLXContext gGLXContext;

int iKeyPressed;

/*Entry point function:
========================
Indirectly we are stopping end user from entering any command line arguments by specifying "void" as the function parameter.
Study how to specify the arguments to such program.
*/
int main(void)
{

  //function prototypes
  
  void CreateWindow(void);
  void ToggleFullscreen(void);
  void initialize(void);
  void display(void);
  void resize(int,int);	
  void uninitialize(void);

  //Variable declarations
  
  int winWidth=giWindowWidth;
  int winHeight=giWindowHeight;
 
  bool bDone=false; //c++ boolean variable

  //Code
  gpFile=fopen("XWindowsAssignment5Log.txt","w");

  	if(gpFile==NULL)
   	{
			
		printf("Log file cannot be created!..Exiting!");
		exit(1);
   	}
  	else
  	{	
		fprintf(gpFile,"Log file created...!\n");

	}	
	
  //Create the window
  CreateWindow();


  //Initialize
  initialize();	
		
		
  	
  //GameLoop
  XEvent event;
  KeySym keysym;

	while(bDone==false)
	{
	
		while(XPending(gpDisplay))//Equivalent to PeekMessage()
		{

			XNextEvent(gpDisplay,&event);

			switch(event.type)
			{

				case MapNotify://Equivalent to "WM_CREATE"
					break;
			
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);

					switch(keysym)
					{
						
						case XK_Escape:
								bDone=true;	
								break;
						case XK_F:
						case XK_f:
							  if(gbFullscreen==false)
							  {
								ToggleFullscreen();
 								gbFullscreen=true;					

							  }
							else
							  {
								ToggleFullscreen();	
								gbFullscreen=false;
							  }
							  break;
	
						case 0x61: //Key'1'
						case 0x31:	
							  	iKeyPressed=1;
								resize(giWindowWidth,giWindowHeight);	
							
							  break;

						case 0x62://Key 2
						case 0x32:
							 	 iKeyPressed=2;	  	
							  	 resize(giWindowWidth,giWindowHeight);

							  break;
	
						case 0x63://Key 3
						case 0x33:
							 	iKeyPressed=3;
								resize(giWindowWidth,giWindowHeight);		
							 break;	
	
						case 0x64://Key 4
						case 0x34:

								iKeyPressed=4;
								resize(giWindowWidth,giWindowHeight);
							break;
						
						case 0x65://Key 5
						case 0x35:
								iKeyPressed=5;
								resize(giWindowWidth,giWindowHeight);		
							break;

						//case 0x66://Key 6
						case 0x36:
								iKeyPressed=6;
								resize(giWindowWidth,giWindowHeight);
							break;

						case 0x67://Key 7
						case 0x37:
								iKeyPressed=7;
								resize(giWindowWidth,giWindowHeight);
							break;
	
						case 0x68:
						case 0x38://Key 8
								iKeyPressed=8;
								resize(giWindowWidth,giWindowHeight);
							break;

						case 0x69://Key 9
						case 0x39:
								iKeyPressed=9;
								resize(giWindowWidth,giWindowHeight);
							break;
					
						default:		
							 break;		

					}//KeyPress Swtch						

					break;
				case ButtonPress:
	
					 switch(event.xbutton.button)
					  {
						case 1://Left mouse click
							break;
						case 2://Middle mouse button
							break;
						case 3://Right mouse click
							break;
						default:
							break;
					  } 
					 break;
			case MotionNotify:
					 break;
	
			case ConfigureNotify:
	
					 winWidth=event.xconfigure.width;
	
					 winHeight=event.xconfigure.height;
						
					 resize(winWidth,winHeight);			
			
					 break;
			case Expose:
					break;
	
			case DestroyNotify:
					break;

			case 33:
					bDone=true;
					break;
			default:
					break;		
		
		}//end of switch
	

	   }//Inner-while loop
	   	
	   display();		
	
	}//End of Gameloop

	return(0);

}//main()

void display(void)
{
	//code

	fprintf(gpFile,"Entering Display..\n");

	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glBegin(GL_TRIANGLES);		
		glVertex3f(-1.0f,-1.0f,0.0f);
		glVertex3f(0.0f,1.0f,0.0f);
		glVertex3f(1.0f,-1.0f,0.0f);	

	glEnd();

	glFlush();

	fprintf(gpFile,"Exiting display..\n");

}//display()


void resize(int width,int height)
{

	fprintf(gpFile,"Entering resize..\n");
	
	if(height==0)
		height=1;

	switch(iKeyPressed)
	{		
		case 1:
			fprintf(gpFile,"iKeyPressed=%d\n",iKeyPressed);
			
			glViewport(0,(GLsizei)height/2,(GLsizei)width/2,(GLsizei)height/2);
			
			break;
	
		case 2:
			fprintf(gpFile,"iKeyPressed=%d",iKeyPressed);

			glViewport((GLsizei)width/2,(GLsizei)height/2,(GLsizei)width/2,(GLsizei)height/2);		

			break;

		case 3:
			glViewport(0,0,(GLsizei)width/2,(GLsizei)height/2);
		   	break;


		case 4:
			glViewport((GLsizei)width/2,0,(GLsizei)width/2,(GLsizei)height/2);
			break;

		case 5: //Left half
			glViewport(0,0,(GLsizei)width/2,height);	
			break;
	
		case 6: //Right half

			glViewport((GLsizei)width/2,0,(GLsizei)width/2,height);
			break;

		case 7: //Top Half
			
			glViewport(0,(GLsizei)height/2,width,(GLsizei)height/2);	
			break;
		
		case 8: //Bottom half

			glViewport(0,0,width,(GLsizei)height/2);
			break;
	
		case 9: //center of the screen

			glViewport((GLsizei)width/4,0,(GLsizei)width/2,height);	

			break;	
		default:
			 glViewport(0,0,width,height);
	}//switch

	fprintf(gpFile,"Exitting resize..\n");
	
} //resize()

void CreateWindow(void)
{

  fprintf(gpFile,"Entering function: CreateWindow()\n"); 	

  //function prototypes
   
  void uninitialize(void);

 //variable declaration
 
  XSetWindowAttributes winAttribs;
  int defaultScreen;
  int defaultDepth;
  int styleMask;

  //An array of frame buffer attributes
		
	static int frameBufferAttributes[]=
	{
		GLX_RGBA,	//Rendering type equivalent to "PFD_TYPE_RGBA"
		GLX_RED_SIZE,1, //Equivalent to cRedBits=8.
		GLX_GREEN_SIZE,1, //Equivalent to cGreenBits=8.
		GLX_BLUE_SIZE,1,  //Equivalent to cBlueBits=8.
		GLX_ALPHA_SIZE,1, //Equivalent to cAlphaBits
		None		  //Array terminator!
	};

  //code

  gpDisplay=XOpenDisplay(NULL);
  
  if(gpDisplay==NULL)
   { 
     printf("Error: Unable to open X Display..\n Exiting now..\n");
     uninitialize();	
     exit(1);
   }  
     
  defaultScreen=XDefaultScreen(gpDisplay);

 // defaultDepth=DefaultDepth(gpDisplay,defaultScreen);
 // gpXVisualInfo=(XVisualInfo *)malloc(sizeof(XVisualInfo));

  gpXVisualInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);	 //instead of matching the visual, we will choose the best one

 /*
  if(gpXVisualInfo==NULL)
   {              
	printf("Error:Unable to allocate memory for Visual Info\n Exiting...\n ");	
	uninitialize();
	exit(1);
   }
*/
 //XMatchVisualInfo(gpDisplay,defaultScreen,defaultDepth,TrueColor,gpXVisualInfo);		
	
  winAttribs.border_pixel=0;
  
  winAttribs.background_pixmap=0;
  
  winAttribs.colormap=XCreateColormap(gpDisplay,
                                      RootWindow(gpDisplay,gpXVisualInfo->screen),
				      gpXVisualInfo->visual,
				      AllocNone
			             );

  gColormap=winAttribs.colormap;
 
  winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen); //background color

  winAttribs.event_mask= ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

  styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

  gWindow=XCreateWindow(
			gpDisplay,
			RootWindow(gpDisplay,gpXVisualInfo->screen),
			0,
			0,
			giWindowWidth,
			giWindowHeight,
			0,
			gpXVisualInfo->depth,
			InputOutput,
			gpXVisualInfo->visual,
			styleMask,
			&winAttribs
		       );	  
			
 if(!gWindow)
 {
	printf("ERROR: Failed to create Main Window...\nExiting\n..");	
	uninitialize();
	exit(1);
 }	

 //Caption bar name of the window
 XStoreName(gpDisplay,gWindow,"Assignment 5: Mapping Viewport!!");

 //Specifying the Window Manager Protocols

 Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
 
 XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1); //XSetWMProtocols(which display, which window will have this behavioral change,which rule/Atom, Count of such rules/atoms specified)

 XMapWindow(gpDisplay,gWindow);	
		
 fprintf(gpFile,"Exiting function CreateWindow()\n");	

}//CreateWindow()



void initialize(void)
{

	fprintf(gpFile,"Entering Initialize...\n");

	//function prototype
	void resize(int,int);

	//code
	
	gGLXContext=glXCreateContext(
					gpDisplay,
					gpXVisualInfo,
					NULL,		//You dont want to have "shareable" context
					GL_TRUE		//You want GL_TRUE="HW" rendering or "SW" rendering
				    );
		
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);	//Equivalent to wglMakeCurrent()

	glClearColor(0.0f,0.0f,1.0f,0.0f);		//Blue window

	resize(giWindowWidth,giWindowHeight);		//Warm-up resize call

	fprintf(gpFile,"Exiting Initialize...\n");

} //initialize()



void ToggleFullscreen(void)
{

	//variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	//code

	fprintf(gpFile,"Entering ToggleFullscreen()..\n");

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=gbFullscreen ? 0:1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(
		    gpDisplay,
		    RootWindow(gpDisplay,gpXVisualInfo->screen),
		    False,
		    StructureNotifyMask,
		    &xev
		  );				

	fprintf(gpFile,"Exitting ToggleFullscreen..\n");
		
}//ToggleFullscreen()

void uninitialize(void)
{

	if(gWindow)
	{

		XDestroyWindow(gpDisplay,gWindow);
		
	}	
	if(gColormap)
	{

		XFreeColormap(gpDisplay,gColormap);

	}
	if(gpXVisualInfo)
	{

		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;

	}
	if(gpFile)
	{
		fprintf(gpFile,"Log file closed successfully!!\n");
		fclose(gpFile);
		gpFile=NULL;
	}

}//uninitialize
