/************************************************************************

Program: XWindows Native Window!


*************************************************************************/
#include<iostream> //Namespace [It is always global alike header file, but header file cannnot be extended but namespaces can be!]
#include<stdio.h>  //Console input-output
#include<stdlib.h> //For exit() function.
#include<memory.h> //For memset() function.

#include<X11/Xlib.h>   //Just like "windows.h" in windows.
#include<X11/Xutil.h> //Needed for "XVisualInfo" structure and APIs related to it.
#include<X11/XKBlib.h> //For Keyboard (We started to use this after UNICODE came into picture).
#include<X11/keysym.h> //Equivalent to "Virtual keycodes" in windows.

//namespaces
//==========
using namespace std; //Will be needed while using functions from the namespaces Ex.If function from this <iostream> namespace used, need it. 


//Global variable declarations:
//==============================
bool gbFullscreen=false;
Display *gpDisplay=NULL;  //"Display" structure has around 77 members.       //DirectX Coding Style.
XVisualInfo *gpXVisualInfo=NULL;

Window gWindow; //Structs
Colormap gColormap;

int giWindowWidth=800;
int giWindowHeight=600;
FILE *gpFile=NULL;

/*Entry point function:
========================
Indirectly we are stopping end user from entering any command line arguments by specifying "void" as the function parameter.
Study how to specify the arguments to such program.
*/
int main(void)
{

  //function prototypes
  
  void CreateWindow(void);
  void ToggleFullscreen(void);
  void uninitialize(void);

  //Variable declarations
  
  int winWidth=giWindowWidth;
  int winHeight=giWindowHeight;
 
  //Code
	
  gpFile=fopen("XWindowsFirstProgramLog.txt","w");

  	if(gpFile==NULL)
   	{
			
		printf("Log file cannot be created!..Exiting!");
		exit(1);
   	}
  	else
  	{	
		fprintf(gpFile,"Log file created...!\n");

	}	
	

  CreateWindow();

  //MessageLoop
  XEvent event;
  KeySym keysym;

	while(1)
	{
		XNextEvent(gpDisplay,&event);//Similar to "GetMessage()" in windows!

		switch(event.type)
		{

			case MapNotify://Equivalent to "WM_CREATE"
					break;
			case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);

					switch(keysym)
					{
						
						case XK_Escape:
								uninitialize();
								exit(0);	
								break;
						case XK_F:
						case XK_f:
							  if(gbFullscreen==false)
							  {
								ToggleFullscreen();
 								gbFullscreen=true;					

							  }
							else
							  {
								ToggleFullscreen();	
								gbFullscreen=false;
							  }
							  break;		
						default:
							 break;		

					}//KeyPress Swtch						

					break;
			case ButtonPress:
					 switch(event.xbutton.button)
					  {
						case 1://Left mouse click
							break;
						case 2://Middle mouse button
							break;
						case 3://Right mouse click
							break;
						default:
							break;
					  } 
					 break;
			case MotionNotify:
					 break;
			case ConfigureNotify:
					 winWidth=event.xconfigure.width;
					 winHeight=event.xconfigure.height;
					 break;
			case Expose:
					break;
			case DestroyNotify:
					break;

			case 33:
					uninitialize();
					exit(0);
			default:
					break;		
		
		}//end of switch
	

	}//End of messageloop

	uninitialize();
	return(0);

}//main()

void CreateWindow(void)
{

  fprintf(gpFile,"Entering function: CreateWindow()\n"); 	

  //function prototypes
   
  void uninitialize(void);

 //variable declaration
 
  XSetWindowAttributes winAttribs;
  int defaultScreen;
  int defaultDepth;
  int styleMask;

  //code-1

  gpDisplay=XOpenDisplay(NULL);
  
  if(gpDisplay==NULL)
   { 
     printf("Error: Unable to open X Display..\n Exiting now..\n");
     uninitialize();	
     exit(1);
   }  
     
  defaultScreen=XDefaultScreen(gpDisplay);

  defaultDepth=DefaultDepth(gpDisplay,defaultScreen);
 
  gpXVisualInfo=(XVisualInfo *)malloc(sizeof(XVisualInfo));

  if(gpXVisualInfo==NULL)
   {              
	printf("Error:Unable to allocate memory for Visual Info\n Exiting...\n ");	
	uninitialize();
	exit(1);
   }

  XMatchVisualInfo(gpDisplay,defaultScreen,defaultDepth,TrueColor,gpXVisualInfo);		
	
  winAttribs.border_pixel=0;
  
  winAttribs.background_pixmap=0;
  
  winAttribs.colormap=XCreateColormap(gpDisplay,
                                      RootWindow(gpDisplay,gpXVisualInfo->screen),
				      gpXVisualInfo->visual,
				      AllocNone
			             );

  gColormap=winAttribs.colormap;
 
  winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen); //background color

  winAttribs.event_mask= ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

  styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

  gWindow=XCreateWindow(
			gpDisplay,
			RootWindow(gpDisplay,gpXVisualInfo->screen),
			0,
			0,
			giWindowWidth,
			giWindowHeight,
			0,
			gpXVisualInfo->depth,
			InputOutput,
			gpXVisualInfo->visual,
			styleMask,
			&winAttribs
		       );	  
			
 if(!gWindow)
 {
	printf("ERROR: Failed to create Main Window...\nExiting\n..");	
	uninitialize();
	exit(1);
 }	

 //Caption bar name of the window
  
 XStoreName(gpDisplay,gWindow,"Assignment 1: XWindows Plain Window!");

 //Specifying the Window Manager Protocols

 Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
 
 XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1); //XSetWMProtocols(which display, which window will have this behavioral change,which rule/Atom, Count of such rules/atoms specified)

 XMapWindow(gpDisplay,gWindow);	
		
 fprintf(gpFile,"Exiting function CreateWindow()\n");	

}//CreateWindow()


void ToggleFullscreen(void)
{

	//variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	//code
	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=gbFullscreen ? 0:1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(
		    gpDisplay,
		    RootWindow(gpDisplay,gpXVisualInfo->screen),
		    False,
		    StructureNotifyMask,
		    &xev
		  );				
		
}//ToggleFullscreen()

void uninitialize(void)
{

	if(gWindow)
	{

		XDestroyWindow(gpDisplay,gWindow);
		
	}	
	if(gColormap)
	{

		XFreeColormap(gpDisplay,gColormap);

	}
	if(gpXVisualInfo)
	{

		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;

	}
	if(gpFile)
	{
		fprintf(gpFile,"Log file closed successfully!!\n");
		fclose(gpFile);
		gpFile=NULL;
	}

}//uninitialize

/***************** About XServer*****************************

1. XServer kade janayra sarw requests through API's are blocking.

2. Janayra requests ya direct XServer kade direct kadhich jat nahit, "Xlib" store them into its own buffer called as "request buffer".

3. Mag Xserver kade kadhi jatat requests?

 	situation:1: When your program calls upon "HW" related API.
		     Ex.Xflush();	

	situation 2: XClient calls such an API jyane Xserver la force kele to demand requests from "requests buffer".
		     Ex. XGetWindowAttributes();	
	
	situation 3: If a multi-threaded application is created with "multiple windows".
			

*************************************************************/






