#include<stdio.h> //printf()

#include<stdlib.h> //exit()

#include<dlfcn.h> //dlopen() dlsym() dlclose() dlerror()

//defining the type of our own: A function pointer pointing to a function.
typedef int (*MySquare)(int);
MySquare pfn=NULL; //Creating a variable of our own type.

int main(void)
{

	//variables
	void *lib=NULL;
	int num=5;	
	char *error=NULL;

	//code
	
	lib=dlopen("libMySquareLibrary.so",RTLD_LAZY);

	if(lib==NULL)
	{
		error=dlerror();
		printf("\n Could not open the Source object!\n=%s",error);
		return(EXIT_FAILURE);
	}
	
	pfn=(MySquare)dlsym(lib,"MySquare");
	
	if(pfn==NULL)
	{

		printf("Could not get the address of the function!\n");
		return(EXIT_FAILURE);
	}

	printf("\nSquare of %d=%d\n",num,pfn(num));

	dlclose(lib);

	return(EXIT_SUCCESS);
}//main()
	
