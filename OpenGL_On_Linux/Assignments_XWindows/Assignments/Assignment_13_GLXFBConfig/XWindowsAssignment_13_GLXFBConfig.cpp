/********************************************************************************

Program: GLXFBConfig! 

*********************************************************************************/
///////////////////////// Headers////////////////////////////////////////////////

#include<iostream>
#include<stdio.h>  //For printf()
#include<stdlib.h> //For exit()
#include<memory.h> //For memset()

//Headers for XServer

#include<X11/Xlib.h>   //analogous to "windows.h"
#include<X11/Xutil.h>  //for visuals
#include<X11/XKBlib.h> //XkbKeycodeToKeysym()
#include<X11/keysym.h> //for 'Keysym'

#include<GL/gl.h>
#include<GL/glx.h>     //for 'glx' functions

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

/////////////////////////Global Variables ////////////////////////////////////////
FILE *gpFile=NULL;

Display *gpDisplay=NULL;

XVisualInfo *gpXVisualInfo=NULL;

Colormap gColormap;

Window gWindow;

GLXContext gGLXContext; //Parallel to HGLRC

typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*,GLXFBConfig,GLXContext,Bool,const int*);

glXCreateContextAttribsARBProc glXCreateContextAttribsARB=NULL;

GLXFBConfig gGLXFBConfig;

bool gbFullscreen=false;

//////////////////////// Function ///////////////////////////////////////////////////

int main(int argc, char *argv[])
{

	//function prototypes

	void CreateWindow(void);
	void ToggleFullscreen(void);
	void initialize(void);
	void resize(int,int);
	void display(void);
	void uninitialize(void);	

	//code
	
	//Create a log file
 
	gpFile=fopen("XWindowsGLXFBConfigLog.txt","w");
	
	if(gpFile==NULL)
	{
		printf("\n\t\t Log File Cannot be Created..Exiting now...\n");
		exit(1);			
	}
	else
	{
		fprintf(gpFile,"Log file created and successfully opened!!\n");

	}
	
	//Create the window
	CreateWindow();

	//initialize
	initialize();

	//Message Loop
	
	//Variable declarations
	XEvent event; //Parallel to "MSG" structure in windows
	KeySym keySym;
	int winWidth;
	int winHeight;
	bool bDone=false;

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);//Parallel to "GetMessage()"
			
			switch(event.type) //parallel to 'iMsg'
			{
				case MapNotify: //Parallel to "WM_CREATE"
						
						break;

				 case KeyPress: //Parallel to WM_KEYDOWN
						keySym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
						
						switch(keySym)
						{
							
							case XK_Escape:
									bDone=true;
									break;

							     case XK_F:
							     case XK_f:		
									if(gbFullscreen==false)
									 {
									   ToggleFullscreen();
									   gbFullscreen=true;							
									 }
									else
									 {
									    ToggleFullscreen();									
		                                                	    gbFullscreen=false;	
									 }		
							   default:
								   break;			
						}//keysym switch.		
						break;

				case ButtonPress:
						switch(event.xbutton.button)
						{
							case 1://left button

								break;

							case 2://middle button
	
								break;

							case 3://right button
								
								break;

							default:
								break;	
						}			
						break;

			       case MotionNotify://Parallel to "WM_MOUSEMOVE"
						
						 break;

			       case ConfigureNotify://Parallel to "WM_SIZE"
                                                  
						 winWidth=event.xconfigure.width;
						 winHeight=event.xconfigure.height;
						 resize(winWidth,winHeight);			
					 	break;
			       case Expose: //parallel to "WM_PAINT"
						break;

			       case DestroyNotify://
						break;
	
			       case 33://close button, system menu-->Close
								
						bDone=true;
						break;
				default:  		
						break;
			}//switch

		}//inner while
		
		display(); //Render function

	}//End of Message Loop.
	
	uninitialize();	
	return 0;	
}//main()


void CreateWindow(void)
{
	
	//function prototypes

	void uninitialize(void);
	
	//variable declarations

	XSetWindowAttributes winAttribs;
	GLXFBConfig *pGLXFBConfigs=NULL; //an array which will hold all the FB Configs
	GLXFBConfig bestGLXFBConfig;	
	XVisualInfo *pTempXVisualInfo=NULL; 
	int styleMask;
	int iNumFBConfigs=0;	
	int i;
	//Algorithm Step-1:Define "our own" frame buffer attributes!

	static int frameBufferAttributes[]={
						GLX_X_RENDERABLE, True, 	//Means, we want to have video rendering enabled i.e. "True"
						GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR, //Equivanent to "TrueColor" parameter to XMatchVisualInfo()
						GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
						GLX_RENDER_TYPE, GLX_RGBA_BIT,	   //Single frame is of type "RGBA"
						GLX_DOUBLEBUFFER, True,		   //Application will be "Double buffered".
						GLX_RED_SIZE,8,
						GLX_GREEN_SIZE,8,
						GLX_BLUE_SIZE,8,
						GLX_ALPHA_SIZE,8,
						GLX_DEPTH_SIZE,24,//You have defined depth value, so you won't take it through any function..
						GLX_STENCIL_SIZE,8,
					     // GLX_SAMPLE_BUFFERS,1, //These are the golden lines which will enable you to create the "
					     // GLX_SAMPLES,4,        //"heaven" demo		 
						None		 //Specify end of an array	
			  		   }; //array must be terminated by 0
	//code
	
	gpDisplay=XOpenDisplay(NULL);
	
	if(gpDisplay==NULL)
	{
		printf("ERROR: Unable to obtain X Display..\n");
		uninitialize();
		exit(1);
	}	
					
	//Algorithm:Step-2: Get the respective framebuffer configuration that meets our frame buffer attributes(defined above).

	//glxChooseFBConfig()-->will return more than one FB configurations hence we captured the returned values in pointer.	
	pGLXFBConfigs=glXChooseFBConfig(gpDisplay,DefaultScreen(gpDisplay),frameBufferAttributes,&iNumFBConfigs);			
	
	if(pGLXFBConfigs==NULL)
	{
	
		printf("Failed to get Valid Framebuffer Configurations..Exiting...\n");
		uninitialize();
		exit(1);
	}
		
	printf("%d Matching FB Configs Found. \n",iNumFBConfigs);

	//Algorithm:Step-3&4: Pick that FB Config/Visual which has most samples per pixel.
	
	int bestFrameBufferConfig=-1, worstFrameBufferConfig=-1, bestNumberOfSamples=1, worstNumberOfSamples=999;

	for(i=0;i<iNumFBConfigs;i++)
	{

		pTempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfigs[i]);
		
		//jar visual aale asel tarch pudhe ja, else exit from here itself as there is no point in going ahead
		//if there is no visual, there will be no samples and hence we cannot find the best visual with maximum no.of samples

		if(pTempXVisualInfo)
		{
			int sampleBuffer,samples; //To receive the count of samplebuffers(locations to save samples) and respective count of samples
		
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLE_BUFFERS,&sampleBuffer);
		
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLES,&samples);	
		
			printf("Matching FrameBuffer Config=%d:Visual ID=0x%lu:SAMPLE_BUFFERS=%d:SAMPLES=%d\n",i,pTempXVisualInfo->visualid,sampleBuffer,samples);
		
			if(bestFrameBufferConfig <0 || sampleBuffer && samples > bestNumberOfSamples)
			{
				bestFrameBufferConfig=i;

				bestNumberOfSamples=samples;
			}	
			if(worstFrameBufferConfig < 0 || !sampleBuffer||samples < worstNumberOfSamples);
			{
				worstFrameBufferConfig=i;
				worstNumberOfSamples=samples;
			}
			
			XFree(pTempXVisualInfo); //For each visual
		}
	
	}//end of looping through FBConfigs which will yield "best FB config"=FB config with highest number of samples. 


	//Algorithm Step:5:For this best FB config, obtain the visual matching to it, which is "Best Visual/Visual of highest quality"

	bestGLXFBConfig=pGLXFBConfigs[bestFrameBufferConfig]; //saved into global variable
	
	//be sure to free "pGLXFBConfigs" as it is not needed now, its job is done successfully!
	
	XFree(pGLXFBConfigs);
	
	gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);

	printf("Chosen Visual ID:0x%lu\n",gpXVisualInfo->visualid);
	
	//set window attributes now
		
	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
					    RootWindow(gpDisplay,gpXVisualInfo->screen), //"defaultscreen" can also be specified
					    gpXVisualInfo->visual,
					    AllocNone //for 'movable' memory allocation					
					   );
	winAttribs.event_mask=StructureNotifyMask | KeyPressMask | ButtonPressMask | ExposureMask | VisibilityChangeMask | PointerMotionMask;

	styleMask=CWBorderPixel | CWEventMask | CWColormap;
	
	gColormap=winAttribs.colormap;

	gWindow=XCreateWindow(gpDisplay,
			      RootWindow(gpDisplay,gpXVisualInfo->screen),
			      0,
			      0,
			      WIN_WIDTH,
			      WIN_HEIGHT,
			      0, //border width
			      gpXVisualInfo->depth, //depth of the visual (depth for colormap)
			      InputOutput, //class(type) of your window
			      gpXVisualInfo->visual,
			      styleMask,	
			      &winAttribs
			     );	
	
	if(!gWindow)
	{
	
		printf("Failure in window creation.\n");
		uninitialize();
		exit(1);
	}
	
	XStoreName(gpDisplay,gWindow,"Assignment 13:GLXFBConfig");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_WINDOW_DELETE",True);

	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);

}//CreateWindow()

void initialize(void)
{
	
	//function declarations

	void uninitialize(void);
	void resize(int,int);

	//code
	//Create a new GL Context 4.5 for rendering:
	//Algorithm Step:6 &7: Here we use "glxCreateContextAttribsARB() instead of "glXCreateContext()"
	//glxCreateContextAttribsARB() creates context from: 1]FrameBuffer Config 2]allows us to flexibly specify OpenGL version

	glXCreateContextAttribsARB=(glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glxCreateContextAttribsARB");

	GLint attribs[]={
			  GLX_CONTEXT_MAJOR_VERSION_ARB,4,
			  GLX_CONTEXT_MINOR_VERSION_ARB,5,
			  GLX_CONTEXT_PROFILE_MASK_ARB,   //Mask out "low profiles" and "give me the richest profiles"
			  GLX_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB,
			  0	
			};//array must be terminated by 0	
	
	gGLXContext=glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs); //(Display*,FB Config,"is it a shared context"?,HW rendering?,attribs array)

	if(!gGLXContext) //fallback to safe old style 2.x context
	{
		
	  //When a context version below 3.0 is requested, implementations will return the newest context version compatible with OpenGL versions less than the version 3.0.
	
	  GLint attribs[]={
			   GLX_CONTEXT_MAJOR_VERSION_ARB,1,
			   GLX_CONTEXT_MINOR_VERSION_ARB,0,
			   0
			  }; //array must be terminated by 0
	
	 printf("Failed to create GLX 4.5 context. Hence using old-style GLX Context\n");
	 	
	 gGLXContext=glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);

	}
	else //it means we have obtained OpenGL Context>version 4.1
	{
	  printf("OpenGL Context 4.5 Created!");
	
	}

	//Algorithm-Step:8:Check whether the ontained context is directly processable by GPU (HW rendering) Or we need a SW layer in between
        //(SW Rendering)

	//Verifying the context is a direct context
	
	if(!glXIsDirect(gpDisplay,gGLXContext))
	{
		printf("Indirect GLX Rendering Context Obtained!!!\n");
	}
	else
	{
	
		printf("Direct GLX Rendering Context Obtained!!\n");
	}

	glXMakeCurrent(gpDisplay,gWindow,gGLXContext); //make the obtained context as a current context

	//initialize OpenGL related options
	glShadeModel(GL_SMOOTH);
	
	//set-up depth buffer
	glClearDepth(1.0f);

	//Enable depth testing
	glEnable(GL_DEPTH_TEST);

	//Which test to do?
	glDepthFunc(GL_LEQUAL);
	
	//set really nice perspective calculations?
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	//We will always cull back faces for better performance
	glEnable(GL_CULL_FACE);

	//Set the background color
	glClearColor(0.0f,0.0f,1.0f,0.0f);
				
	//warmup resize call
	resize(WIN_WIDTH,WIN_HEIGHT);
			
}//initialize()


///////// ToggleFullscreen steps:////////////////

// 1. Create an atom which will store non-fullscreen state.
// 2. Since this will be a switch on state of the window, from our side we will fire the event created by us.
//    So create your own event, let the XServer know about it.
// 3. 


void ToggleFullscreen(void)
{
	//function prototypes
	//variables
	Atom wm_state;    //it will store the non-fullscreen state
	Atom fullscreen;  //it will store the fullscreen state
	XEvent eventForToggleFullscreen; //for our own event.

	//code

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	
	eventForToggleFullscreen.type=ClientMessage;     //Event type?
	eventForToggleFullscreen.xclient.window=gWindow;         //Kuthlya window sathi?
	eventForToggleFullscreen.xclient.message_type=wm_state;  //non-fullscreen state
	eventForToggleFullscreen.xclient.format=32;              //message format=32 bit.

	eventForToggleFullscreen.xclient.data.l[0]=gbFullscreen?0:1; //Ternary operator which decides what to store in l[0].

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	
	eventForToggleFullscreen.xclient.data.l[1]=fullscreen; //Storing the "fullscreen" state. 
	
	//Now we will send the event. Parallel to SendMessage() in windows.		
		
	XSendEvent(gpDisplay,
		   RootWindow(gpDisplay,gpXVisualInfo->screen),
		   False, //Do not send this message to sibling windows
		   StructureNotifyMask, //resizing mask(event_mask)
		   &eventForToggleFullscreen
		  );

}//ToggleFullscreen()

void resize(int width,int height)
{

	if(height==0)
		height=1;
	
	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	
}//resize()

void display(void)
{
	
	//code
		
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glXSwapBuffers(gpDisplay,gWindow);	

}//display()


void uninitialize(void)
{
	
	//Code
	//Releasing OpenGL related and XWindow related objects.
		
	GLXContext currentContext=glXGetCurrentContext();

	if(currentContext!=NULL && currentContext==gGLXContext)	
	{
		
		glXMakeCurrent(gpDisplay,0,0);	

	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	
	}
	if(gWindow)
	{
		
		XDestroyWindow(gpDisplay,gWindow);

	}
	if(gColormap)
	{
	
		XFreeColormap(gpDisplay,gColormap);

	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{

		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}	
	if(gpFile)
	{
		fprintf(gpFile,"Log file is successfully closed!");
		fclose(gpFile);
		gpFile=NULL;	
	}
	
}//uninitialize()

