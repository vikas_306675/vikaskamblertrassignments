/**********************************************************************

Assignment Light: Animation of Light!!!

**********************************************************************/

///////////////////// Headers ///////////////////////////////////////

using namespace std;

#include<stdio.h> //printf()
#include<stdlib.h> //malloc() and exit()
#include<memory.h> //memset()

#include<X11/keysym.h> //XkbKeycodeToKeysym()
#include<X11/XKBlib.h> //Keyboard function
#include<X11/Xlib.h> //Xlib APIs
#include<X11/Xutil.h>//Visual

#include<GL/gl.h>//for OpenGL API's
#include<GL/glx.h>//For "briding API's"   
#include<GL/glu.h> //For "glu" API's

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//////////////////// Global Variables////////////////////////////////
bool gbFullscreen=false;
bool gbLighting=false;

FILE *gpFile=NULL;

Display *gpDisplay=NULL;

XVisualInfo *gpXVisualInfo=NULL;

Colormap gColormap;

Window gWindow;

GLXContext gGLXContext; //HGLRC ghrc; (Parallel In Windows)

GLUquadric *quadric=NULL; //For creating "Sphere()"

//////////////////// Angle Variables for Light Sources Rotatio/////////////////////////////
GLfloat angleRedLight=0.0f;
GLfloat angleGreenLight=0.0f;
GLfloat angleBlueLight=0.0f;

////////////////////Define 3 Light Sources:Global Arrays for Lights////////////////////////


//GL_LIGHT0: Red
GLfloat red_light_ambient[]={0.0f,0.0f,0.0f,0.0f};
GLfloat red_light_diffuse[]={1.0f,0.0f,0.0f,0.0f};//Red
GLfloat red_light_specular[]={1.0f,0.0f,0.0f,0.0f};
GLfloat red_light_position[]={0.0f,0.0f,0.0f,0.0f};  //Position of the light source itself will be animated!

//GL_LIGLT1: Green

GLfloat green_light_ambient[]={0.0f,0.0f,0.0f,0.0f};
GLfloat green_light_diffuse[]={0.0f,1.0f,0.0f,0.0f}; //Green
GLfloat green_light_specular[]={0.0f,1.0f,0.0f,0.0f};
GLfloat green_light_position[]={0.0f,0.0f,0.0f,0.0f};

//GL_LIGHT2: Blue
GLfloat blue_light_ambient[]={0.0f,0.0f,0.0f,0.0f};
GLfloat blue_light_diffuse[]={0.0f,0.0f,1.0f,0.0f};//Blue
GLfloat blue_light_specular[]={0.0f,0.0f,1.0f,0.0f};
GLfloat blue_light_position[]={0.0f,0.0f,0.0f,0.0f};

//Material Properties
GLfloat material_ambient[]={0.0f,0.0f,0.0f,0.0f};
GLfloat material_diffuse[]={1.0f,1.0f,1.0f,1.0f};
GLfloat material_specular[]={1.0f,1.0f,1.0f,1.0f};
GLfloat material_shininess=50.0f;


//////////////////// Functions /////////////////////////////////////

int main(void)
{
	//function declarations
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void initialize(void);////////Change
	void resize(int,int);/////////Change
	void update(void);
	void display(void);  /////////Change
	void uninitialize(void);	
	
	//variables
	int winWidth;
	int winHeight;		
	char ascii[26];

	////// c++ boolean variable
	bool bDone=false; 
	

	//code
	gpFile=fopen("XWindowsAssignment_Light_Animation_Log.txt","w");

	if(gpFile==NULL)
	{

		printf("ERROR: Unable to Create Log File...Exiting!!\n");
		exit(1);
	}
	else
	{
		fprintf(gpFile,"Log file created successfully!!\n");
	}
	
        fprintf(gpFile,"/////////////////// Main()/////////////////////////////////////////\n");
		
	//Create "Window" first!
	CreateWindow();	


	////////Initialization for OpenGL changes!
	initialize();

	//Define the GameLoop
	XEvent event;
	KeySym keysym;
	
	while(bDone==false)////////change
	{
		while(XPending(gpDisplay))///////Change
		{

			XNextEvent(gpDisplay,&event);

			switch(event.type)
			{
			
			case MapNotify: //equivalent to WM_CREATE
					break;	

			case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
						
					switch(keysym)
					{

						case XK_Escape:
								bDone=true; ////Change
								break;
						/*		
						case XK_F:	
						case XK_f:
							  if(gbFullscreen==false)
							  {
								ToggleFullscreen();	
								gbFullscreen=true;							
							  }
							 else
							  {
								ToggleFullscreen();
								gbFullscreen=false;

							  }
							  break;				
						*/
						default:
							  break;
					}//switch(keysym)

					XLookupString(&event.xkey,ascii,sizeof(ascii),NULL,NULL);
					
					switch(ascii[0])
					{
						
						case 'F':
						case 'f':
							 if(gbFullscreen==false)
							  {
								ToggleFullscreen();
								gbFullscreen=true;
	
							  }
							else
							  {
								ToggleFullscreen();
								gbFullscreen=false;

							  }	
							  break;

						case 'L'://for lights
						case 'l':
							if(gbLighting==false)
							{
								glEnable(GL_LIGHTING);
								gbLighting=true;
							}
							else
							{
								glDisable(GL_LIGHTING);
								gbLighting=false;
							}			
							break;			
		
					}//switch(ascii[0])		
					break;
			case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:break;
						case 2:break;
						case 3:break;
						default:break; 
					}//event.xbutton.button
					break;
			case MotionNotify:
					break;
			case ConfigureNotify://for resize()

					winWidth=event.xconfigure.width;
					winHeight=event.xconfigure.height;
					resize(winWidth,winHeight);//////Change		
	
					break;
			case Expose:
					break;
			case DestroyNotify:
					break;

			case 33:
				bDone=true;
				break;
			default:
				break;
				
			}//switch(event.type)	

		}//XPending()
		update();
		display();///////change

	}//GameLoop
		
	uninitialize();
	return(0);//successfull exit

}//main()

/*///////////////Gameloop Explanation://///////////////////

	# Almost sarw game engines madhe while() madhe while()-->Mahnje double while loop chi game loop sapdel-->They are UNIX followers..:)

	# while(XPending())
		- PeekMessage() madhe message nasel tar to else la jau shakat hota(Windows) madhe-->Mhnajech ethe aapan block hot navto-->
		  -->Mhanje PeekMessage() is "non-blocking".
		- XPending() madhe joparynt existing event che execution complete hot nahi, toparynt pudhe jauch shakar nahi-->Yala "blocking" 
		  mhantat.-->Mhanun Xpending() is "blocking" aani te ek function aahe mhanun it is a "blocking API".
		- Ya warunch Xlib madhle sarw API he "blocking" aahet.
	# Shabdikritya
		- PeekMessage() and XPending() same aahet pan meaningwise
		- PeekMessage()--> Is "non-blocking" and
		- XPending()   --> Is "blocking".


/////////////// initialize() /////////////////////////////

	#glXCreateContext(): Equivalent to wglCreateContext() in windows.
		- gpDisplay,
		- gpXVisualInfo: OpenGL Context create karnyasathi tumhi tayar kelela visual mala dya.
		- NULL:        : Khup abhyas zala ki mag //:)-->Ex. Fish tayar keli aani 6 monitors through tank aahe aani tyar Fish pohtey..
			       : Jeva tumhala context tayar karun share karaycha asel teva ha wapartat,
			       : NULL specifies whether you want to share the created context having mulptiple displays connected.
			       : Khup abhyas-->Changala Job/Swatachi company-->Khup Paise-->Mag 6 monitors--> Tyawar kara mad rada~!!! :)	
		- GL_TRUE: SW/HW rendering hawey te specify karto.
			 : HW- GPU waparun kele aani graphics acceleration waparle asel
			 : SW- GPU waparle pan kami takdiche aahe.
				Ex. 8 buffers create karayche aahet aani GPU 4 tayar karnyachya takdicha aahe tar uralele 4 XServer tumchyasathi tayar karel-->SW rendering.
			 : In Short, "HW" rendering mhanje GPU chi purn takad waprun je kele jate te. 

	#glXMakeCurrent():Equivalent to wglMakeCurrent()
		- gpDisplay,
		- gWindow,
		- gGLXContext
///////////////////////////////////////////////////////////
*/
void initialize(void)
{
	//Function prototypes
	void resize(int,int);
	void uninitialize(void);

	//variables
	//code

	fprintf(gpFile,"//////////////////////////////////////////Entering initialize()//////////////////////////////:\n");

	gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE); 

	if(gGLXContext==NULL)
	{
		
		printf("initialize()::OpenGL Context could not be created..Exitting!!\n");
		uninitialize();
		exit(1);
	}


	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	//Depth Statements

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);      //Set-up depth buffer
	glEnable(GL_DEPTH_TEST); //Enable the depth test	
	glDepthFunc(GL_LEQUAL);//specify the depth test to do.
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);//Specify the really nice perspective calculations

	glClearColor(0.0f,0.0f,0.0f,0.0f);


	////////////////////////////////Light Initializations///////////////////////////////////////////////////
		
	//GL_LIGHT0::RED

	glLightfv(GL_LIGHT0,GL_AMBIENT,red_light_ambient);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,red_light_diffuse);
	glLightfv(GL_LIGHT0,GL_SPECULAR,red_light_specular);
	//glLightfv(GL_LIGHT0,GL_POSITION,light_position);  //It has been animated in this assignement.

	//GL_LIGHT1: Green
	glLightfv(GL_LIGHT1,GL_AMBIENT,green_light_ambient);
	glLightfv(GL_LIGHT1,GL_DIFFUSE,green_light_diffuse);
	glLightfv(GL_LIGHT1,GL_SPECULAR,green_light_specular);
	
	//GL_LIGHT2: Blue
	glLightfv(GL_LIGHT2,GL_AMBIENT,blue_light_ambient);
	glLightfv(GL_LIGHT2,GL_DIFFUSE,blue_light_diffuse);
	glLightfv(GL_LIGHT2,GL_SPECULAR,blue_light_specular);

	//Material Properties
	glMaterialfv(GL_FRONT,GL_AMBIENT,material_ambient);
	glMaterialfv(GL_FRONT,GL_DIFFUSE,material_diffuse);
	glMaterialfv(GL_FRONT,GL_SPECULAR,material_specular);
	glMaterialf(GL_FRONT,GL_SHININESS,material_shininess);

	//Enable the Light Source defines above
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2); //Note: Until and unless GL_LIGHTING=ON, light will not come or can't be shown! 

	//Create Quadric to Define the Sphere
	
	quadric=gluNewQuadric();
	
	resize(WIN_WIDTH,WIN_HEIGHT);
	
	fprintf(gpFile,"//////////////////////////////////////////Exitting initialize()///////////////////////////////\n");

}//initialize()

void resize(int width,int height)
{
	//function prototypes

	//variables

	//code

	fprintf(gpFile,"///////////////////////////////////////////Entering resize()//////////////////////////////////\n");

	if(height==0)
		height=1;

	glViewport(0,0,(GLsizei)width,(GLsizei)height);//glViewport(x,y,width,height);	

	glMatrixMode(GL_PROJECTION); //select projection matrix
	glLoadIdentity();	     

	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);//gluPerspective(angle,width/height ratio,near,far);

	fprintf(gpFile,"///////////////////////////////////////////Exitting resize()//////////////////////////////////\n");

}//resize()

void CreateWindow(void)
{

	//Function  declarations
	void uninitialize(void);
	

	//variables
	int defaultScreen;
	//int defaultDepth;
	int styleMask;
	XSetWindowAttributes winAttribs;
	

	////////// Your own frame-buffer-attributes
	static int frameBufferAttributes[]= //Equivalent to PIXELFORMATDESCRIPTOR pfd; in Windows.
	{
		GLX_RGBA,	//Rendering type ~~ pfd.PixelType=PFD_TYPE_RGBA;	
		GLX_RED_SIZE,1,  //Equivalent to: pfd.cRedBits, For Single buffer value=1
		GLX_GREEN_SIZE,1,//Green bits 
		GLX_BLUE_SIZE,1, //Blue bits
		GLX_ALPHA_SIZE,1,//Alpha
		GLX_DEPTH_SIZE,24, //Depth  
		GLX_DOUBLEBUFFER,True,//Double buffer change..
		None		 //Array ka thambavinara/array la stop karanara "stopper"
	};
	//code
	
	fprintf(gpFile,"///////////////////////////////////////Entering in CreateWindow()://///////////////////////\n");

	//Step-1) Open Connection to XServer through Display.
	gpDisplay=XOpenDisplay(NULL); //No parameterized string

	if(gpDisplay==NULL)
	{

		printf("CreateWindow()::ERROR: Display connection cannot be extablished..Exiting..!!\n");
		uninitialize();
		exit(1);
	}

	//Step-2) Obtain the Visual for the "Default Display".
		
		//For this, first of all get the "Default Screen": Port to which monitor has been connected!
	
		defaultScreen=XDefaultScreen(gpDisplay);	
		fprintf(gpFile,"CreateWindow()::defaultScreen Number:%d\n",defaultScreen);
		
			
		/*///////////Depth is not taken as we took in first program/////////////////////
		 
		//For this "defaultScreen" get the "depth" supported.
		//defaultDepth=XDefaultDepth(gpDisplay,defaultScreen);
		//fprintf(gpFile,"CreateWindow::For defaultScreen=%d defaultDepth=%d\n",defaultScreen,defaultDepth);
		
		
		//Step) Now obtain the visual for the "defaultScreen".
		//You need to allocate memory for gpXVisualInfo:
	
		gpXVisualInfo=(XVisualInfo *)malloc(sizeof(XVisualInfo));			
		
		*/
	
	////////// Choose the best visual matching to frame buffer attributes defined by you!
	/* 	
	 defaultDepth: Why it is not there? 

	*/
	gpXVisualInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);	
	
	if(gpXVisualInfo==NULL)
	{
		printf("CreateWindow()::ERROR: Cannot allocate memory for XVisualInfo..Exiting..!!\n");
		uninitialize();
		exit(1);
	}
	
	/*/////////////////Now get the best visual into "gpXVisualInfo *" using an API: XMatchVisualInfo():
	if(XMatchVisualInfo(gpDisplay,defaultScreen,defaultDepth,TrueColor,gpXVisualInfo)==0)
	{
		
		printf("CreateWindow::Error: Cannot find the best possible matching visual..Exiting!!\n");
		uninitialize();
		exit(1);
	
	}
	*/

	//Step) Now Create the window of your own by defining the window attributes!

		winAttribs.border_pixel=0; //take default

		winAttribs.border_pixmap=0; //take default

		winAttribs.background_pixmap=0; //default

		winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);//backgound color of the window
			
		winAttribs.colormap=XCreateColormap(
							gpDisplay,
							RootWindow(gpDisplay,gpXVisualInfo->screen),
							gpXVisualInfo->visual,
							AllocNone
						    );	
		
		//copy this colormap to global colormap variable
		gColormap=winAttribs.colormap;

		//define the set of events your XWindow will handle..

		winAttribs.event_mask=ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | 
				      StructureNotifyMask;	
		//define the styles for your window

		styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
		
		//Actual call to XCreateWindow():

		gWindow=XCreateWindow(
					gpDisplay,
					RootWindow(gpDisplay,gpXVisualInfo->screen),
					0, //x
					0, //y
					WIN_WIDTH,
					WIN_HEIGHT,
					0, //border thickness
					gpXVisualInfo->depth,
					InputOutput,// window type is input and output
					gpXVisualInfo->visual,
					styleMask,
					&winAttribs
				     );

		if(!gWindow)
		{

			printf("CreateWindow()::ERROR:Failed to create the Window..Exitting!!\n");
			uninitialize();
			exit(1);

		}	
		//Specify the "Caption bar" for the window
		XStoreName(gpDisplay,gWindow,"Assignment Light Animation!!");
		
		//Define the behavior for the close button
		
		Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);

		XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);
		
		//Show the window
		
		XMapWindow(gpDisplay,gWindow);

		fprintf(gpFile,"////////////////////////////////Exiting CreateWindow()////////////////////////////////\n");

}//CreateWindow()



void ToggleFullscreen(void)
{
	
	//function prototypes


	//variable
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};	

	//code

	fprintf(gpFile,"//////////////////////////////////////////Entering ToggleFullscreen()////////////////////////\n");

	//Create an atom which will store the non-fullscreen state	
	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);//Create an Atom which will store the non-fullscreen state
	memset(&xev,0,sizeof(xev));


	//Create your own event for toggling the "fullscreen" state.
	xev.type=ClientMessage;	//event cha type?
	
	xev.xclient.window=gWindow;//kuthlya window sathi?
	
	xev.xclient.message_type=wm_state; //message type is an atom

	xev.xclient.format=32; //Client message cha format

	xev.xclient.data.l[0]=gbFullscreen ? 0 :1;
	
	//now create an atom representing "fullscreen" state:			

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);

	xev.xclient.data.l[1]=fullscreen;

	//fire your created event based upon the union

	XSendEvent(gpDisplay,
		   RootWindow(gpDisplay,gpXVisualInfo->screen),
		   False, //Should this event be propagted to your window's child windows?
		   StructureNotifyMask,
		   &xev
		  );

	fprintf(gpFile,"////////////////////////////////////////Exiting ToggleFullscreen()/////////////////////////////\n");
	
}//ToggleFullscreen()


void update(void)
{
	
	angleRedLight=angleRedLight+0.1f;
	angleGreenLight=angleGreenLight+0.1f;
	angleBlueLight=angleBlueLight+0.1f;

	if(angleRedLight>=360.0f)
		angleRedLight=0.0f;

	if(angleGreenLight>=360.0f)
		angleGreenLight=0.0f;

	if(angleBlueLight>=360.0f)
		angleBlueLight=0.0f;


}//update()


void display(void)
{	
	//function prototypes
	//variables
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW); 
	glLoadIdentity();

	glPushMatrix(); // It will PUSH the identity matrix.
	
	gluLookAt(0.0f,0.0f,0.1f,
		  0.0f,0.0f,0.0f,
		  0.0f,1.0f,0.0f	
		);//gluLookAt(Where is the viewer/Camera?, Where viewer is looking into?,Up Vector!);
	
	glPushMatrix(); //This will PUSH LookAt Matrix.
	
		glRotatef(angleRedLight,1.0f,0.0f,0.0f);//Red Light Source will rotate alongX-Axis(Y and Z can be used).
		red_light_position[1]=angleRedLight; // We have chosen X-Axis rotation so "(Y,Z) will change.
		glLightfv(GL_LIGHT0,GL_POSITION,red_light_position);		 
	
	glPopMatrix();

	glPushMatrix();
		glRotatef(angleGreenLight,0.0f,1.0f,0.0f);
		green_light_position[0]=angleGreenLight;
		glLightfv(GL_LIGHT1,GL_POSITION,green_light_position);
	glPopMatrix();
	
	glPushMatrix();
		glRotatef(angleBlueLight,0.0f,0.0f,1.0f); //Rotatiom along Z-axis
		blue_light_position[0]=angleBlueLight;
		glLightfv(GL_LIGHT2,GL_POSITION,blue_light_position);
	glPopMatrix();
	glPushMatrix();//For the first Push Matrix!
		
		glTranslatef(0.0f,0.0f,-5.0f);
		
		glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
		
		gluSphere(quadric,0.75f,30,30);
	glPopMatrix();

	glPopMatrix();
	glXSwapBuffers(gpDisplay,gWindow);//Double buffering..
		
}//display()

/*///////////// uninitalize()///////////////////

  #if(currentGLXContext && currentGLXContext==gGLXContext) //imagination madhla code.
	- tya 6 monitor chya code sathi keleli soy,
	- Shared context 0 zaleli nahi chalnar, but jya monitor chi context (6 paiki samaja 3rd monitor display or context off karaychi asel)
	  teva, respective context(3rd display chi) tich 0 zali pahije, baki kuthli nahi.		
	- current context = mala uninitilize karaychi wali context asel tarch ti 0 kar.

/////////////////////////////////////////////////
*/
void uninitialize(void)
{

	//variables
	GLXContext currentGLXContext;

	//code

	fprintf(gpFile,"///////////////////////////////////////// Entering uninitialize()////////////////////// \n");	

	currentGLXContext=glXGetCurrentContext();

	if(quadric)
		gluDeleteQuadric(quadric);
	

	if(currentGLXContext!=NULL && currentGLXContext==gGLXContext)
	{

		glXMakeCurrent(gpDisplay,0,0);
	
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext); //~~wglDeleteContext() in windows.

	}
	if(gWindow)
	{

		XDestroyWindow(gpDisplay,gWindow);

	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;

	}
	if(gpDisplay)
	{

		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
	if(gpFile)
	{

		fprintf(gpFile,"Log file closed successfully!! \n");
		fclose(gpFile);
		gpFile=NULL;
	}
	
}//uninitialize()
