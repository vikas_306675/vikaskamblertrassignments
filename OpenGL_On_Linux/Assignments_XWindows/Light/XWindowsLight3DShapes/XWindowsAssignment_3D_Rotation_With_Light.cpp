/************************************************************************

Program: XWindows Double Buffered 3D Rotating Shapes with Light!

*************************************************************************/
#include<iostream> //Namespace [It is always global alike header file, but header file cannnot be extended but namespaces can be!]
#include<stdio.h>  //Console input-output
#include<stdlib.h> //For exit() function.
#include<memory.h> //For memset() function.

#include<X11/Xlib.h>   //Just like "windows.h" in windows.
#include<X11/Xutil.h> //Needed for "XVisualInfo" structure and APIs related to it.
#include<X11/XKBlib.h> //For Keyboard (We started to use this after UNICODE came into picture).
#include<X11/keysym.h> //Equivalent to "Virtual keycodes" in windows.

#include<GL/gl.h>
#include<GL/glx.h> //glx is equivalent to "wgl" in windows (That is nothing but briding API for Xwindows)
#include<GL/glu.h> //for gluPerspective()

//namespaces
//==========
using namespace std; //Will be needed while using functions from the namespaces Ex.If function from this <iostream> namespace used, need it. 


//Global variable declarations:
//==============================

bool gbFullscreen=false;
bool gbLighting=false;   //To toggle the lighing

Display *gpDisplay=NULL;  //"Display" structure has around 77 members.       //DirectX Coding Style.

XVisualInfo *gpXVisualInfo=NULL;

Window gWindow; //Structs

Colormap gColormap;

int giWindowWidth=800;
int giWindowHeight=600;

FILE *gpFile=NULL;

GLfloat angleTriangle=0.0f;
GLfloat angleSquare=0.0f;

GLXContext gGLXContext;

/////////// Global Arrays for Light///////////////////

GLfloat light_ambient[]={0.5f,0.5f,0.5f,1.0f};
GLfloat light_diffuse[]={1.0f,1.0f,1.0f,1.0f};
GLfloat light_specular[]={1.0f,1.0f,1.0f,1.0f};
GLfloat light_position[]={0.0f,0.0f,1.0f,0.0f}; //Light along the +ve Z-axis


/*Entry point function:
========================
Indirectly we are stopping end user from entering any command line arguments by specifying "void" as the function parameter.
Study how to specify the arguments to such program.
*/
int main(void)
{

  //function prototypes
  
  void CreateWindow(void);
  void ToggleFullscreen(void);
  void initialize(void);
  void display(void);
  void resize(int,int);	
  void update(void);
  void uninitialize(void);

  //Variable declarations
  
  int winWidth=giWindowWidth;
  int winHeight=giWindowHeight;
 
  bool bDone=false; //c++ boolean variable

  //Code
  gpFile=fopen("XWindowsAssignment_3DRotation_Light_Log.txt","w");

  	if(gpFile==NULL)
   	{
			
		printf("Log file cannot be created!..Exiting!");
		exit(1);
   	}
  	else
  	{	
		fprintf(gpFile,"Log file created...!\n");

	}	
	
  //Create the window
  CreateWindow();


  //Initialize
  initialize();	
		
		
  	
  //GameLoop
  XEvent event;
  KeySym keysym;

	while(bDone==false)
	{
	
		while(XPending(gpDisplay))//Equivalent to PeekMessage()
		{

			XNextEvent(gpDisplay,&event);

			switch(event.type)
			{

				case MapNotify://Equivalent to "WM_CREATE"
					break;
			
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);

					switch(keysym)
					{
						
						case XK_Escape:
								bDone=true;	
								break;
						case XK_F:
						case XK_f:
							  if(gbFullscreen==false)
							  {
								ToggleFullscreen();
 								gbFullscreen=true;					

							  }
							else
							  {
								ToggleFullscreen();	
								gbFullscreen=false;
							  }
							  break;
				
						case 'L':
						case 'l':
								if(gbLighting==false)
								{
									glEnable(GL_LIGHTING);
									gbLighting=true;			
								} 
								else
								{

									glDisable(GL_LIGHTING);
									gbLighting=false;
								}
								break;
						default:		
							 break;		

					}//KeyPress Swtch						

					break;
				case ButtonPress:
	
					 switch(event.xbutton.button)
					  {
						case 1://Left mouse click
							break;
						case 2://Middle mouse button
							break;
						case 3://Right mouse click
							break;
						default:
							break;
					  } 
					 break;
			case MotionNotify:
					 break;
	
			case ConfigureNotify:
	
					 winWidth=event.xconfigure.width;
	
					 winHeight=event.xconfigure.height;
						
					 resize(winWidth,winHeight);			
			
					 break;
			case Expose:
					break;
	
			case DestroyNotify:
					break;

			case 33:
					bDone=true;
					break;
			default:
					break;		
		
		}//end of switch
	

	   }//Inner-while loop
	   	
	   update();
	   display();		
	
	}//End of Gameloop

	return(0);

}//main()


void update(void)
{
	
	angleTriangle=angleTriangle + 0.1f;

	angleSquare=angleSquare + 0.1f;

	if(angleTriangle>=360.0f)
		angleTriangle=0.0f;

	if(angleSquare>=360.0f)
		angleSquare=0.0f;

}//update()

void display(void)
{

	
	//code

	fprintf(gpFile,"Entering Display..\n");

	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);//Depth change

	//Draw Triangle
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(-1.5f,0.0f,-5.0f);

	glRotatef(angleTriangle,0.0f,1.0f,0.0f);	

	glBegin(GL_TRIANGLES);		
	
		//Front face of triangle
		glNormal3f(0.0f,0.0f,1.0f); //Front face Normal

		glVertex3f(0.0f,1.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,1.0f);
		glVertex3f(1.0f,-1.0f,1.0f);

		//Left face of triangle
		glNormal3f(-1.0f,0.0f,0.0f);//Left face Normal

		glVertex3f(0.0f,1.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,-1.0f);
		glVertex3f(-1.0f,-1.0f,1.0f);

		//Back face of triangle
		glNormal3f(0.0f,0.0f,-1.0f);//Back face Normal

		glVertex3f(0.0f,1.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,-1.0f);
		glVertex3f(1.0f,-1.0f,-1.0f);

		//Right face of triangle
		glNormal3f(1.0f,0.0f,0.0f);//Right face normal

		glVertex3f(0.0f,1.0f,0.0f);
		glVertex3f(1.0f,-1.0f,-1.0f);
		glVertex3f(1.0f,-1.0f,1.0f);

	glEnd();
	
	
	//Draw 3D CUBE

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();	
	
	glTranslatef(1.5f,0.0f,-5.0f);

	glRotatef(angleSquare,1.0f,0.0f,0.0f);

	glBegin(GL_QUADS);
		
		//Front face of cube
		glNormal3f(0.0f,0.0f,1.0f); //Front face Normal

		glVertex3f(-1.0f,-1.0f,1.0f);
		glVertex3f(-1.0f,1.0f,1.0f);
		glVertex3f(1.0f,1.0f,1.0f);
		glVertex3f(1.0f,-1.0f,1.0f);

		//Left face of Cube
		glNormal3f(-1.0f,0.0f,0.0f);//Left Face Normal

		glVertex3f(-1.0f,1.0f,1.0f);
		glVertex3f(-1.0f,1.0f,-1.0f);
		glVertex3f(-1.0f,-1.0f,-1.0f);
		glVertex3f(-1.0f,-1.0f,1.0f);

		//Back face of Cube
		glNormal3f(0.0f,0.0f,-1.0f);//Back face Normal
		
		glVertex3f(-1.0f,1.0f,-1.0f);
		glVertex3f(1.0f,1.0f,-1.0f);
		glVertex3f(1.0f,-1.0f,-1.0f);
		glVertex3f(-1.0f,-1.0f,-1.0f);

		//Right face of Cube
		glNormal3f(1.0f,0.0f,0.0f);//Right face Normal

		glVertex3f(1.0f,1.0f,-1.0f);
		glVertex3f(1.0f,1.0f,1.0f);
		glVertex3f(1.0f,-1.0f,1.0f);
		glVertex3f(1.0f,-1.0f,-1.0f);

		//Top face of Cube
		glNormal3f(0.0f,1.0f,0.0f);//Top Face Normal

		glVertex3f(-1.0f,1.0f,1.0f);
		glVertex3f(1.0f,1.0f,1.0f);
		glVertex3f(1.0f,1.0f,-1.0f);
		glVertex3f(-1.0f,1.0f,-1.0f);

		//Bottom Face of Cube
		glNormal3f(0.0f,-1.0f,0.0f);//Bottom face Normal			

		glVertex3f(-1.0f,-1.0f,1.0f);
		glVertex3f(-1.0f,-1.0f,-1.0f);
		glVertex3f(1.0f,-1.0f,-1.0f);
		glVertex3f(1.0f,-1.0f,1.0f);


	glEnd();
        
	glXSwapBuffers(gpDisplay,gWindow);

	fprintf(gpFile,"Exiting display..\n");

}//display()


void resize(int width,int height)
{

	fprintf(gpFile,"Entering resize..\n");
	
	if(height==0)
		height=1;

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);

	//glViewport(0,0,(GLsizei)width,(GLsizei)height);

	fprintf(gpFile,"Exitting resize..\n");
	
} //resize()

void CreateWindow(void)
{

  fprintf(gpFile,"Entering function: CreateWindow()\n"); 	

  //function prototypes
   
  void uninitialize(void);

 //variable declaration
 
  XSetWindowAttributes winAttribs;
  int defaultScreen;
  int defaultDepth;
  int styleMask;

  //An array of frame buffer attributes
		
	static int frameBufferAttributes[]=
	{
		GLX_RGBA,	//Rendering type equivalent to "PFD_TYPE_RGBA"
		GLX_RED_SIZE,1, //Equivalent to cRedBits=8.
		GLX_GREEN_SIZE,1, //Equivalent to cGreenBits=8.
		GLX_BLUE_SIZE,1,  //Equivalent to cBlueBits=8.
		GLX_ALPHA_SIZE,1, //Equivalent to cAlphaBits
		GLX_DEPTH_SIZE,24, //For depth enabled shaped	
		GLX_DOUBLEBUFFER,True, //For Double buffering
		None		  //Array terminator!
	};

  //code

  gpDisplay=XOpenDisplay(NULL);
  
  if(gpDisplay==NULL)
   { 
     printf("Error: Unable to open X Display..\n Exiting now..\n");
     uninitialize();	
     exit(1);
   }  
     
  defaultScreen=XDefaultScreen(gpDisplay);

 // defaultDepth=DefaultDepth(gpDisplay,defaultScreen);
 // gpXVisualInfo=(XVisualInfo *)malloc(sizeof(XVisualInfo));

  gpXVisualInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);	 //instead of matching the visual, we will choose the best one

 /*
  if(gpXVisualInfo==NULL)
   {              
	printf("Error:Unable to allocate memory for Visual Info\n Exiting...\n ");	
	uninitialize();
	exit(1);
   }
*/
 //XMatchVisualInfo(gpDisplay,defaultScreen,defaultDepth,TrueColor,gpXVisualInfo);		
	
  winAttribs.border_pixel=0;
  
  winAttribs.background_pixmap=0;
  
  winAttribs.colormap=XCreateColormap(gpDisplay,
                                      RootWindow(gpDisplay,gpXVisualInfo->screen),
				      gpXVisualInfo->visual,
				      AllocNone
			             );

  gColormap=winAttribs.colormap;
 
  winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen); //background color

  winAttribs.event_mask= ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

  styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

  gWindow=XCreateWindow(
			gpDisplay,
			RootWindow(gpDisplay,gpXVisualInfo->screen),
			0,
			0,
			giWindowWidth,
			giWindowHeight,
			0,
			gpXVisualInfo->depth,
			InputOutput,
			gpXVisualInfo->visual,
			styleMask,
			&winAttribs
		       );	  
			
 if(!gWindow)
 {
	printf("ERROR: Failed to create Main Window...\nExiting\n..");	
	uninitialize();
	exit(1);
 }	

 //Caption bar name of the window
 XStoreName(gpDisplay,gWindow,"Assignment Light 2: OpenGL 3D Rotating Shapes with Light!!");

 //Specifying the Window Manager Protocols

 Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
 
 XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1); //XSetWMProtocols(which display, which window will have this behavioral change,which rule/Atom, Count of such rules/atoms specified)

 XMapWindow(gpDisplay,gWindow);	
		
 fprintf(gpFile,"Exiting function CreateWindow()\n");	

}//CreateWindow()



void initialize(void)
{

	fprintf(gpFile,"Entering Initialize...\n");

	//function prototype
	void resize(int,int);

	//code
	
	gGLXContext=glXCreateContext(
					gpDisplay,
					gpXVisualInfo,
					NULL,		//You dont want to have "shareable" context
					GL_TRUE		//You want GL_TRUE="HW" rendering or "SW" rendering
				    );
		
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);	//Equivalent to wglMakeCurrent()

	/////////////// Depth along with beautification//////////////////////////////	

	//initialize opengl related options
	glShadeModel(GL_SMOOTH);

	//Setup depth buffer
	glClearDepth(1.0f);

	//Enable depth test
	glEnable(GL_DEPTH_TEST);	

	//Which depth test to do?
	glDepthFunc(GL_LEQUAL);

	//set really nice perspective calculations
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
	
	//CULL back faces for better performance
	//glEnable(GL_CULL_FACE);

	//Background color
	glClearColor(0.0f,0.0f,0.0f,0.0f);		//Black window

	////////////////// Calls to function w.r.t arrays declared////////////////

	glLightfv(GL_LIGHT0,GL_AMBIENT,light_ambient);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,light_diffuse);
	glLightfv(GL_LIGHT0,GL_SPECULAR,light_specular);
	glLightfv(GL_LIGHT0,GL_POSITION,light_position);

	resize(giWindowWidth,giWindowHeight);		//Warm-up resize call

	fprintf(gpFile,"Exiting Initialize...\n");

} //initialize()



void ToggleFullscreen(void)
{

	//variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	//code

	fprintf(gpFile,"Entering ToggleFullscreen()..\n");

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=gbFullscreen ? 0:1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(
		    gpDisplay,
		    RootWindow(gpDisplay,gpXVisualInfo->screen),
		    False,
		    StructureNotifyMask,
		    &xev
		  );				

	fprintf(gpFile,"Exitting ToggleFullscreen..\n");
		
}//ToggleFullscreen()

void uninitialize(void)
{

	if(gWindow)
	{

		XDestroyWindow(gpDisplay,gWindow);
		
	}	
	if(gColormap)
	{

		XFreeColormap(gpDisplay,gColormap);

	}
	if(gpXVisualInfo)
	{

		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;

	}
	if(gpFile)
	{
		fprintf(gpFile,"Log file closed successfully!!\n");
		fclose(gpFile);
		gpFile=NULL;
	}

}//uninitialize
