/************************************************************************

Program: XWindows Smiley Texture-Tweaked!

*************************************************************************/
#include<iostream> //Namespace [It is always global alike header file, but header file cannnot be extended but namespaces can be!]
#include<stdio.h>  //Console input-output
#include<stdlib.h> //For exit() function.
#include<memory.h> //For memset() function.

#include<X11/Xlib.h>   //Just like "windows.h" in windows.
#include<X11/Xutil.h> //Needed for "XVisualInfo" structure and APIs related to it.
#include<X11/XKBlib.h> //For Keyboard (We started to use this after UNICODE came into picture).
#include<X11/keysym.h> //Equivalent to "Virtual keycodes" in windows.

#include<GL/gl.h>
#include<GL/glx.h> //glx is equivalent to "wgl" in windows (That is nothing but briding API for Xwindows)
#include<GL/glu.h> //for gluPerspective()

#include<SOIL/SOIL.h> //Third party image loading library.	

//namespaces
//==========
using namespace std; //Will be needed while using functions from the namespaces Ex.If function from this <iostream> namespace used, need it. 


//Global variable declarations:
//==============================

bool gbFullscreen=false;

Display *gpDisplay=NULL;  //"Display" structure has around 77 members.       //DirectX Coding Style.

XVisualInfo *gpXVisualInfo=NULL;

Window gWindow; //Structs

Colormap gColormap;

int giWindowWidth=1024;

int giWindowHeight=768;

int gDigitKeyPressed;

FILE *gpFile=NULL;

GLuint Texture_Smiley; //Texture Object for Smiley Texture.

GLXContext gGLXContext;

/*Entry point function:
========================
Indirectly we are stopping end user from entering any command line arguments by specifying "void" as the function parameter.
Study how to specify the arguments to such program.
*/
int main(void)
{

  //function prototypes
  
  void CreateWindow(void);
  void ToggleFullscreen(void);
  void initialize(void);
  void display(void);
  void resize(int,int);	
  void uninitialize(void);

  //Variable declarations
  
  int winWidth=giWindowWidth;
  int winHeight=giWindowHeight;
 
  bool bDone=false; //c++ boolean variable

  //Code
  gpFile=fopen("XWindowsAssignment_Smiley_Texture_TweakedLog.txt","w");

  	if(gpFile==NULL)
   	{
			
		printf("Log file cannot be created!..Exiting!");
		exit(1);
   	}
  	else
  	{	
		fprintf(gpFile,"Log file created...!\n");

	}	
	
  //Create the window
  CreateWindow();


  //Initialize
  initialize();	
				
  	
  //GameLoop
  XEvent event;
  KeySym keysym;

	while(bDone==false)
	{
	
		while(XPending(gpDisplay))//Equivalent to PeekMessage()
		{

			XNextEvent(gpDisplay,&event);

			switch(event.type)
			{

				case MapNotify://Equivalent to "WM_CREATE"
					break;
			
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);

					switch(keysym)
					{
						
						case XK_Escape:
								bDone=true;	
								break;
						case XK_F:
						case XK_f:
							  if(gbFullscreen==false)
							  {
								ToggleFullscreen();
 								gbFullscreen=true;					

							  }
							else
							  {
								ToggleFullscreen();	
								gbFullscreen=false;
							  }
							  break;

						case 0x31: //1
							  gDigitKeyPressed=1;
						          break;
						case 0x32: //2
							  gDigitKeyPressed=2;
							  break;
						case 0x33: //3
							  gDigitKeyPressed=3;
							  break;
						case 0x34: //4
							  gDigitKeyPressed=4;
							  break;	
						default:
							  gDigitKeyPressed=0;		
							 break;		

					}//KeyPress Swtch						

					break;
				case ButtonPress:
	
					 switch(event.xbutton.button)
					  {
						case 1://Left mouse click
							break;
						case 2://Middle mouse button
							break;
						case 3://Right mouse click
							break;
						default:
							break;
					  } 
					 break;
			case MotionNotify:
					 break;
	
			case ConfigureNotify:
	
					 winWidth=event.xconfigure.width;
	
					 winHeight=event.xconfigure.height;
						
					 resize(winWidth,winHeight);			
			
					 break;
			case Expose:
					break;
	
			case DestroyNotify:
					break;

			case 33:
					bDone=true;
					break;
			default:
					break;		
		
		}//end of switch
	

	   }//Inner-while loop
	   	
	   display();		
	
	}//End of Gameloop

	return(0);

}//main()

void display(void)
{
	//code

	//fprintf(gpFile,"Entering Display..\n");

	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);//Depth change

	//Draw Quad
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glBindTexture(GL_TEXTURE_2D,Texture_Smiley);	

	glTranslatef(0.0f,0.0f,-5.0f);	
	
	if(gDigitKeyPressed==0)
	{
		glColor3f(1.0f,1.0f,1.0f);

		glBegin(GL_QUADS);
			
			glVertex3f(1.0f,1.0f,0.0f);
		
			glVertex3f(-1.0f,1.0f,0.0f);

			glVertex3f(-1.0f,-1.0f,0.0f);
		
			glVertex3f(1.0f,-1.0f,0.0f);

		glEnd();

	}
	else if(gDigitKeyPressed==1) 
	{
					
		glBegin(GL_QUADS);
		
			glTexCoord2f(0.5f,0.0f);
			glVertex3f(1.0f,1.0f,0.0f); //right top
		
			glTexCoord2f(0.0f,0.0f);
			glVertex3f(-1.0f,1.0f,0.0f); //left top
		
			glTexCoord2f(0.0f,0.5f);
			glVertex3f(-1.0f,-1.0f,0.0f); //left bottom
		
			glTexCoord2f(0.5f,0.5f);
			glVertex3f(1.0f,-1.0f,0.0f);  //right bottom		

		glEnd();	
	}
	else if(gDigitKeyPressed==2)
	{
		glBegin(GL_QUADS);
			
			glTexCoord2f(1.0f,0.0f); //right top
			glVertex3f(1.0f,1.0f,0.0f);
			
			glTexCoord2f(0.0f,0.0f); //left top
			glVertex3f(-1.0f,1.0f,0.0f);
			
			glTexCoord2f(0.0f,1.0f); //left bottom
			glVertex3f(-1.0f,-1.0f,0.0f);
		
			glTexCoord2f(1.0f,1.0f);//right bottom
			glVertex3f(1.0f,-1.0f,0.0f);
	
		glEnd();
	}
	else if(gDigitKeyPressed==3)
	{
		
		glBegin(GL_QUADS);

			glTexCoord2f(2.0f,0.0f); //right top
			glVertex3f(1.0f,1.0f,0.0f);
			
			glTexCoord2f(0.0f,0.0f);//left top	
			glVertex3f(-1.0f,1.0f,0.0f);

			glTexCoord2f(0.0f,2.0f);//left bottom
			glVertex3f(-1.0f,-1.0f,0.0f);

			glTexCoord2f(2.0f,2.0f);//right bottom
			glVertex3f(1.0f,-1.0f,0.0f);

		glEnd();
	}	
	else if(gDigitKeyPressed==4)
	{
		glBegin(GL_QUADS);

			glTexCoord2f(0.5f,0.5f);//right top
			glVertex3f(1.0f,1.0f,0.0f);

			glTexCoord2f(0.5f,0.5f);//left top
			glVertex3f(-1.0f,1.0f,0.0f);

			glTexCoord2f(0.5f,0.5f);//left bottom
			glVertex3f(-1.0f,-1.0f,0.0f);

			glTexCoord2f(0.5f,0.5f);//right bottom
			glVertex3f(1.0f,-1.0f,0.0f);

		glEnd();
	}
/*
	else
	{
		glColor3f(1.0f,1.0f,1.0f);//White color

		glBegin(GL_QUADS);
			
			glVertex3f(1.0f,1.0f,0.0f);//right top
			glVertex3f(-1.0f,1.0f,0.0f);//left top
			glVertex3f(-1.0f,-1.0f,0.0f);//left bottom
			glVertex3f(1.0f,-1.0f,0.0f);//right bottom

		glEnd();		

	}
*/
	glXSwapBuffers(gpDisplay,gWindow);

	//fprintf(gpFile,"Exiting display..\n");

}//display()


void resize(int width,int height)
{

	fprintf(gpFile,"Entering resize..\n");
	
	if(height==0)
		height=1;

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);

	fprintf(gpFile,"Exitting resize..\n");
	
} //resize()

void CreateWindow(void)
{

  fprintf(gpFile,"Entering function: CreateWindow()\n"); 	

  //function prototypes
   
  void uninitialize(void);

 //variable declaration
 
  XSetWindowAttributes winAttribs;
  int defaultScreen;
  int defaultDepth;
  int styleMask;

  //An array of frame buffer attributes
		
	static int frameBufferAttributes[]=
	{
		GLX_RGBA,	//Rendering type equivalent to "PFD_TYPE_RGBA"
		GLX_RED_SIZE,1, //Equivalent to cRedBits=8.
		GLX_GREEN_SIZE,1, //Equivalent to cGreenBits=8.
		GLX_BLUE_SIZE,1,  //Equivalent to cBlueBits=8.
		GLX_ALPHA_SIZE,1, //Equivalent to cAlphaBits
		GLX_DEPTH_SIZE,24, //For depth enabled shaped	
		GLX_DOUBLEBUFFER,True, //For Double buffering
		None		  //Array terminator!
	};

  //code

  gpDisplay=XOpenDisplay(NULL);
  
  if(gpDisplay==NULL)
   { 
     printf("Error: Unable to open X Display..\n Exiting now..\n");
     uninitialize();	
     exit(1);
   }  
     
  defaultScreen=XDefaultScreen(gpDisplay);

 // defaultDepth=DefaultDepth(gpDisplay,defaultScreen);
 // gpXVisualInfo=(XVisualInfo *)malloc(sizeof(XVisualInfo));

  gpXVisualInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);	 //instead of matching the visual, we will choose the best one

 /*
  if(gpXVisualInfo==NULL)
   {              
	printf("Error:Unable to allocate memory for Visual Info\n Exiting...\n ");	
	uninitialize();
	exit(1);
   }
*/
 //XMatchVisualInfo(gpDisplay,defaultScreen,defaultDepth,TrueColor,gpXVisualInfo);		
	
  winAttribs.border_pixel=0;
  
  winAttribs.background_pixmap=0;
  
  winAttribs.colormap=XCreateColormap(gpDisplay,
                                      RootWindow(gpDisplay,gpXVisualInfo->screen),
				      gpXVisualInfo->visual,
				      AllocNone
			             );

  gColormap=winAttribs.colormap;
 
  winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen); //background color

  winAttribs.event_mask= ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

  styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

  gWindow=XCreateWindow(
			gpDisplay,
			RootWindow(gpDisplay,gpXVisualInfo->screen),
			0,
			0,
			giWindowWidth,
			giWindowHeight,
			0,
			gpXVisualInfo->depth,
			InputOutput,
			gpXVisualInfo->visual,
			styleMask,
			&winAttribs
		       );	  
			
 if(!gWindow)
 {
	printf("ERROR: Failed to create Main Window...\nExiting\n..");	
	uninitialize();
	exit(1);
 }	

 //Caption bar name of the window
 XStoreName(gpDisplay,gWindow,"XWindows Assignment Texture:Tweaked Smiley!!");

 //Specifying the Window Manager Protocols

 Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
 
 XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1); //XSetWMProtocols(which display, which window will have this behavioral change,which rule/Atom, Count of such rules/atoms specified)

 XMapWindow(gpDisplay,gWindow);	
		
 fprintf(gpFile,"Exiting function CreateWindow()\n");	

}//CreateWindow()



void initialize(void)
{

	fprintf(gpFile,"Entering Initialize...\n");

	//function prototype
	void resize(int,int);
	void LoadGLTextures(GLuint*);

	//code
	
	gGLXContext=glXCreateContext(
					gpDisplay,
					gpXVisualInfo,
					NULL,		//You dont want to have "shareable" context
					GL_TRUE		//You want GL_TRUE="HW" rendering or "SW" rendering
				    );
		
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);	//Equivalent to wglMakeCurrent()


	/************** Enable Depth*****************/
	//initialize opengl related options
	glShadeModel(GL_SMOOTH);

	//Setup depth buffer
	glClearDepth(1.0f);

	//Enable depth test
	glEnable(GL_DEPTH_TEST);	

	//Which depth test to do?
	glDepthFunc(GL_LEQUAL);

	//set really nice perspective calculations
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
	
	//CULL back faces for better performance
	//glEnable(GL_CULL_FACE);

	//Background color
	glClearColor(0.0f,0.0f,0.0f,0.0f);		//Black window


	/*********** Enable Texture ***************/
	
	glEnable(GL_TEXTURE_2D);
	LoadGLTextures(&Texture_Smiley);

	resize(giWindowWidth,giWindowHeight);		//Warm-up resize call
	fprintf(gpFile,"Exiting Initialize...\n");

} //initialize()


void LoadGLTextures(GLuint *texture)
{
	int width,height;
	unsigned char *imageData=NULL;
	
	// Get the image data through SOIL (Simple OpenGL Image Library) 
	imageData=SOIL_load_image(
					"/home/viks/RTR/XWindows/FixedFunctionPipeline/Assignments/Assignment_Texture/Smiley.bmp",		// Path of the image
					&width,		// Image Width 
					&height,	// Image height
					0,		// Image madhe kiti channels aahet-R/G/B/A -->0-->Default
					SOIL_LOAD_RGB	
				);
	
	//Texture generation code
	
	glGenTextures(1,texture); //texture is a address jithe rikame bhande bharle jail ashi jaga

	glBindTexture(GL_TEXTURE_2D,*texture);// glBindTexture(Texture type/Target parameter, unique number returned by glGenTextures());

	glPixelStorei(GL_UNPACK_ALIGNMENT,4);

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);

	gluBuild2DMipmaps(GL_TEXTURE_2D,3,width,height,GL_RGB,GL_UNSIGNED_BYTE,imageData);
	
	// Free the object		
	SOIL_free_image_data(imageData);

}//LoadGLTextures()

void ToggleFullscreen(void)
{

	//variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	//code

	fprintf(gpFile,"Entering ToggleFullscreen()..\n");

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=gbFullscreen ? 0:1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(
		    gpDisplay,
		    RootWindow(gpDisplay,gpXVisualInfo->screen),
		    False,
		    StructureNotifyMask,
		    &xev
		  );				

	fprintf(gpFile,"Exitting ToggleFullscreen..\n");
		
}//ToggleFullscreen()

void uninitialize(void)
{

	if(gWindow)
	{

		XDestroyWindow(gpDisplay,gWindow);
		
	}	
	if(gColormap)
	{

		XFreeColormap(gpDisplay,gColormap);

	}
	if(gpXVisualInfo)
	{

		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;

	}
	if(gpFile)
	{
		fprintf(gpFile,"Log file closed successfully!!\n");
		fclose(gpFile);
		gpFile=NULL;
	}

}//uninitialize
